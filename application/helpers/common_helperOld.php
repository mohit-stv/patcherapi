<?php
    include $_SERVER['DOCUMENT_ROOT'] . '/patcherAPI/application/third_party/predis/autoload.php';
    Predis\Autoloader::register();
    use Predis\Command\CommandInterface;
    //use GeospatialGeoRadius\Command\CommandInterface;
     ## Redis connection
    function createConnectionToRedis(){  
        $redis = new Predis\Client([
            //'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);
        
        /*echo "Connection to server sucessfully"; 
        echo "Server is running: ".$redis->ping(); 
        echo '</br>';*/
    // $redis = $this->redis;
        return $redis;
    }


    function stripIncludeFiles(){
        include_once APPPATH . 'third_party/Stripe/lib/Stripe.php';
        // Utilities
        include_once APPPATH . 'third_party/Stripe/lib/Util/AutoPagingIterator.php';
        include_once APPPATH . 'third_party/Stripe/lib/Util/RequestOptions.php';
        include_once APPPATH . 'third_party/Stripe/lib/Util/Set.php';
        include_once APPPATH . 'third_party/Stripe/lib/Util/Util.php';
        // HttpClient
        include_once APPPATH . 'third_party/Stripe/lib/HttpClient/ClientInterface.php';
        include_once APPPATH . 'third_party/Stripe/lib/HttpClient/CurlClient.php';
        // Errors
        include_once APPPATH . 'third_party/Stripe/lib/Error/Base.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/Api.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/ApiConnection.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/Authentication.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/Card.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/InvalidRequest.php';
        include_once APPPATH . 'third_party/Stripe/lib/Error/RateLimit.php';
        // Plumbing
        include_once APPPATH . 'third_party/Stripe/lib/ApiResponse.php';
        include_once APPPATH . 'third_party/Stripe/lib/JsonSerializable.php';
        include_once APPPATH . 'third_party/Stripe/lib/StripeObject.php';
        include_once APPPATH . 'third_party/Stripe/lib/ApiRequestor.php';
        include_once APPPATH . 'third_party/Stripe/lib/ApiResource.php';
        include_once APPPATH . 'third_party/Stripe/lib/SingletonApiResource.php';
        include_once APPPATH . 'third_party/Stripe/lib/AttachedObject.php';
        include_once APPPATH . 'third_party/Stripe/lib/ExternalAccount.php';
        // Stripe API Resources
        include_once APPPATH . 'third_party/Stripe/lib/Account.php';
        include_once APPPATH . 'third_party/Stripe/lib/AlipayAccount.php';
        include_once APPPATH . 'third_party/Stripe/lib/ApplicationFee.php';
        include_once APPPATH . 'third_party/Stripe/lib/ApplicationFeeRefund.php';
        include_once APPPATH . 'third_party/Stripe/lib/Balance.php';
        include_once APPPATH . 'third_party/Stripe/lib/BalanceTransaction.php';
        include_once APPPATH . 'third_party/Stripe/lib/BankAccount.php';
        include_once APPPATH . 'third_party/Stripe/lib/BitcoinReceiver.php';
        include_once APPPATH . 'third_party/Stripe/lib/BitcoinTransaction.php';
        include_once APPPATH . 'third_party/Stripe/lib/Card.php';
        include_once APPPATH . 'third_party/Stripe/lib/Charge.php';
        include_once APPPATH . 'third_party/Stripe/lib/Collection.php';
        include_once APPPATH . 'third_party/Stripe/lib/CountrySpec.php';
        include_once APPPATH . 'third_party/Stripe/lib/Coupon.php';
        include_once APPPATH . 'third_party/Stripe/lib/Customer.php';
        include_once APPPATH . 'third_party/Stripe/lib/Dispute.php';
        include_once APPPATH . 'third_party/Stripe/lib/Event.php';
        include_once APPPATH . 'third_party/Stripe/lib/FileUpload.php';
        include_once APPPATH . 'third_party/Stripe/lib/Invoice.php';
        include_once APPPATH . 'third_party/Stripe/lib/InvoiceItem.php';
        include_once APPPATH . 'third_party/Stripe/lib/Order.php';
        include_once APPPATH . 'third_party/Stripe/lib/OrderReturn.php';
        include_once APPPATH . 'third_party/Stripe/lib/Plan.php';
        include_once APPPATH . 'third_party/Stripe/lib/Product.php';
        include_once APPPATH . 'third_party/Stripe/lib/Recipient.php';
        include_once APPPATH . 'third_party/Stripe/lib/Refund.php';
        include_once APPPATH . 'third_party/Stripe/lib/SKU.php';
        include_once APPPATH . 'third_party/Stripe/lib/Subscription.php';
        include_once APPPATH . 'third_party/Stripe/lib/ThreeDSecure.php';
        include_once APPPATH . 'third_party/Stripe/lib/Token.php';
        include_once APPPATH . 'third_party/Stripe/lib/Transfer.php';
        include_once APPPATH . 'third_party/Stripe/lib/TransferReversal.php';

        \Stripe\Stripe::setApiKey('sk_test_MqJaybZjeKndarB2Biqfjes7'); // Client Secret Key
        
        //\Stripe\Stripe::setApiKey('sk_test_lLtLIr2yE4NIT37o2OS2GFV6'); 
    }


    function payCallOutFee($data){

        $CI = get_instance();
        $CI->load->model(['User_model','Payment_type_model','Transaction_model']);

        $userId = $data['user_id'];
        $lastFourDigit = $data['last_four_digit'];
        $stripInclude = stripIncludeFiles();

        $getTransaction = $CI->Transaction_model
        ->fields('customer_id')
        ->where(['user_id'=>$userId, 'last4'=>$lastFourDigit])
        ->get();

        $getUserDetails = $CI->User_model
        ->fields('email_id')
        ->where(['user_id'=>$userId])->get();

        /*  $getUserDetails = $CI->User_model
        ->with_transaction('fields: customer_id')
        ->fields('email_id')
        ->where(['user_id'=>$userId])->get();*/
        $emailId = $getUserDetails['email_id'];


        if($data['customer_id'] != '') {
            $customerId = $data['customer_id'];
        } else {
                if(!empty($getTransaction)) {
                $customerId = $getTransaction['customer_id'];
            } else {
                $customerArray = ['description'=>'Customer for '.$emailId,
                //'email'=>$emailId,
                'source'=>$data['token']];
                $customer = \Stripe\Customer::create($customerArray);
                $customerId = $customer->id;
            }
        }


        $charge = \Stripe\Charge::create(array(
            'customer'=>$customerId,
            'amount'=>($data['amount']*100),
            'currency'=>'gbp')); 

        if($charge!=null && $charge->status == 'succeeded'){ 
            $data = [
                'request_id'=>$data['request_id'],
                'user_id'=>$userId,
                'customer_id'=>$customerId,
                'transaction_charge_id'=>$charge->id,
                'object_type'=>$charge->object,
                'amount'=>$charge->amount,
                'network_status'=>$charge->outcome->network_status,
                'message'=>$charge->outcome->seller_message,
                'paid_status'=>$charge->paid,
                'source_id'=>$charge->source->id,
                'transaction_type'=>1, // 1 for callout fee
                'last4'=>$lastFourDigit,
            //  'payment_type'=>$charge->source->object, //Which type payment
                'payment_type'=>1, //Which type payment
                'source_brand'=>$charge->source->brand,
                //'source_country'=>$charge->source->country,
                'expire_month'=>$charge->source->exp_month,
                'expire_year'=>$charge->source->exp_year,
                'transaction_status'=>$charge->status
            ];
            $insert = $CI->Transaction_model->insert($data);

            if($insert) {
                $response = ['amount'=>$charge->amount, 'status'=>$charge->status,
                    'request_id'=>$data['request_id']]; 
                    return $response;
            }
        }

    }


    // Refund To User
    function refundCalloutFeeToUser($data){
        
        $CI = get_instance();
        $CI->load->model(['User_model','Transaction_model','Transfer_model'
        ,'Request_model','Mechanic_model']);
        $stripInclude = stripIncludeFiles();

        $accountNumber = $data['account_number'];
        $accountHolderName = $data['account_holder_name'];
        $sortCode = $data['sort_code'];
        $bankName = $data['bank_name'];
        $requestType = $data['request_type'];
        $callOutFee = $data['callout_fee'];
        $mechanicId = $data['mechanic_id'];
        $requestId = $data['request_id'];
        $userId = $data['user_id'];

        $mechanicEmail = $CI->Mechanic_model
        ->fields('email_id')->get(['mechanic_id'=>$mechanicId]);

        $userEmail = $CI->User_model
        ->fields('email_id')->get(['user_id'=>$userId]);

        //Test Account
        /*  $account = \Stripe\Account::create(
            array(
            "email" => $userEmail['email'],
            "type" => "standard",
            'external_account' => array(
                "object" => "bank_account",
                "country" => "US",
                "currency" => "usd",
                "account_holder_name" => 'Jane Austen',
                "account_holder_type" => 'individual',
                "routing_number" => "111000025",
                "account_number" => "000123456789"
            )
        ));
        $accountId = $account->id;*/

        $getAccount = $CI->Transfer_model
        ->fields('destination')
        ->get_All(['user_id'=>$userId]);

        if(!empty($getAccount)) {
            $accountId = $getAccount['destination'];
        } else {
            $account = \Stripe\Account::create(
                array(
                "email" => $userEmail['email_id'],
                "type" => "standard",
                'external_account' => array(
                    "object" => "bank_account",
                //   "country" => "US",
                    "currency" => "gbp",
                    "account_holder_name" => $accountHolderName,
                    "account_holder_type" => 'individual',
                    "routing_number" => $sortCode,
                    "account_number" => $accountNumber
                )
            ));
            $accountId = $account->id;
        }

        $transfer = \Stripe\Transfer::create(
            array( "amount" => ($callOutFee*100),
                "currency" => "gbp",
                "destination" => $accountId,
                )
            );
        if($transfer!=null && $transfer->id != '') {
            $updateTransaction = ['user_id'=>$userId,
                    'request_id'=>$requestId,
                    'transfer_charge_id'=>$transfer->id,
                    'account_id'=>$accountId,
                    'object_type'=>$transfer->object,
                    'amount'=>$transfer->amount,
                    'balance_transaction'=>$transfer->balance_transaction,
                    'currency'=>$transfer->currency,
                    'destination'=>$transfer->destination, // Created Account of AccountId
                    'destination_payment'=>$transfer->destination_payment
                ];

            $insert = $CI->Transfer_model->insert($updateTransaction);
            if($insert) {
                $updateData = ['request_status'=>11]; // Refund To Mechanic
                $update = $CI->Request_model->where('request_id',$requestId)
                ->update($updateData);

                $response = ['amount'=>$transfer->amount, 'request_id'=>$requestId]; 
                return $response;
            }
        } else {
            return false;
        }
    }

    function newPayCallOutFee($data){

        $CI = get_instance();
        $CI->load->model(['User_model','Payment_type_model','Transaction_model']);

        $userId = $data['user_id'];
        $lastFourDigit = $data['last_four_digit'];
        $stripInclude = stripIncludeFiles();

        $getTransaction = $CI->Transaction_model
        ->fields('customer_id')
        ->where(['user_id'=>$userId, 'last4'=>$lastFourDigit])
        ->get();

        $getUserDetails = $CI->User_model
        ->fields('email_id')
        ->where(['user_id'=>$userId])->get();

        /*  $getUserDetails = $CI->User_model
        ->with_transaction('fields: customer_id')
        ->fields('email_id')
        ->where(['user_id'=>$userId])->get();*/
        $emailId = $getUserDetails['email_id'];


        if($data['customer_id'] != '') {
            $customerId = $data['customer_id'];
        } else {
            if(!empty($getTransaction)) {
                $customerId = $getTransaction['customer_id'];
            } else {
                $customerArray = ['description'=>'Customer for '.$emailId,
                //'email'=>$emailId,
                'source'=>$data['token']];
                $customer = \Stripe\Customer::create($customerArray);
                $customerId = $customer->id;
            }
        }


        // $charge = \Stripe\Charge::create(array(
        //     'customer'=>$customerId,
        //     'amount'=>($data['amount']*100),
        //     'currency'=>'gbp'));
        $charge =  authorizePayment(['customerId' =>$customerId,'amount'=> $data['amount']]);

        if($charge!=null && $charge->status == 'succeeded'){ 
            $data = [
                'request_id'=>$data['request_id'],
                'user_id'=>$userId,
                'customer_id'=>$customerId,
                'transaction_charge_id'=>$charge->id,
                'object_type'=>$charge->object,
                'amount'=>$charge->amount,
                'network_status'=>$charge->outcome->network_status,
                'message'=>$charge->outcome->seller_message,
                'paid_status'=>$charge->paid,
                'source_id'=>$charge->source->id,
                'transaction_type'=>1, // 1 for callout fee
                'last4'=>$lastFourDigit,
            //  'payment_type'=>$charge->source->object, //Which type payment
                'payment_type'=>1, //Which type payment
                'source_brand'=>$charge->source->brand,
                //'source_country'=>$charge->source->country,
                'expire_month'=>$charge->source->exp_month,
                'expire_year'=>$charge->source->exp_year,
                'transaction_status'=>$charge->status
            ];
            $insert = $CI->Transaction_model->insert($data);

            if($insert) {
                $response = ['amount'=>$charge->amount, 'status'=>$charge->status,
                    'request_id'=>$data['request_id']]; 
                    return $response;
            }
        } else {
            return $charge;
        }

    }

    function authorizePayment($data){
        
        $stripInclude = stripIncludeFiles();
        $customerId = $data['customerId'];
        $charge = \Stripe\Charge::create(
            array(
            'customer'=>$customerId,
            'amount'=>($data['amount']*100),
            'currency'=>'gbp',
            // "description" => "Example Auth charge",
            "capture" => false,
            // "source" => $token,
            )
        ); 

        return $charge;
    }

    function captureAmount($transID){
        
        $stripInclude = stripIncludeFiles();

        $charge = \Stripe\Charge::retrieve($transID);
        return $charge->capture();
    }

    function refundAuthAmount($transID){

        $stripInclude = stripIncludeFiles();
        $re = \Stripe\Refund::create(array(
            "charge" => $transID
          ));

        return $re;
    }

    function processPayment($data){

        $stripInclude = stripIncludeFiles();
        $charge = \Stripe\Charge::create(array(
            'customer'=>$data['customerId'],
            'amount'=>($data['amount']*100),
            'currency'=>'gbp'));
        return $charge;
    }

    function transferToAccount($data){
        
        $CI = get_instance();
        stripIncludeFiles();

        $userId = $data['user_id'];
        $amount = $data['amount'];
        $requestId = $data['request_id'];

        $userDetail = $CI->User_model
        ->with_bankDetails(['account_number','account_holder_name','sort_code'])
        ->get(['user_id'=>$userId]);

        $getAccount = $CI->Transfer_model
        ->fields('destination')
        ->get(['user_id'=>$userId]);

        if(!empty($getAccount)) {
            $accountId = $getAccount['destination'];
        } else {
            $dataAccount = array(
                "email" => $userDetail['email_id'],
                "type" => "standard",
                'external_account' => array(
                    "object" => "bank_account",
                    "currency" => "gbp",
                    "country" => "GB",
                    "account_holder_name" => $userDetail['bankDetails']['account_holder_name'],
                    "account_holder_type" => 'individual',
                    "routing_number" => $userDetail['bankDetails']['sort_code'],
                    "account_number" => $userDetail['bankDetails']['account_number']
                )
            );

            // print_r($dataAccount);die;
            $account = \Stripe\Account::create($dataAccount);
            $accountId = $account->id;
        }

        $transfer = \Stripe\Transfer::create(
            array( "amount" => ($amount*100),
                "currency" => "gbp",
                "destination" => $accountId,
                )
            );
        if($transfer!=null && $transfer->id != '') {
            $updateTransaction = ['user_id'=>$userId,
                    'request_id'=>$requestId,
                    'transfer_charge_id'=>$transfer->id,
                    'account_id'=>$accountId,
                    'object_type'=>$transfer->object,
                    'amount'=>$transfer->amount,
                    'balance_transaction'=>$transfer->balance_transaction,
                    'currency'=>$transfer->currency,
                    'destination'=>$transfer->destination, // Created Account of AccountId
                    'destination_payment'=>$transfer->destination_payment
                ];

            $insert = $CI->Transfer_model->insert($updateTransaction);
            if($insert) {
                $updateData = ['request_status'=>11]; // Refund To User
                $update = $CI->Request_model->where('request_id',$requestId)
                ->update($updateData);

                $response = ['amount'=>$transfer->amount, 'request_id'=>$requestId,'transactionDetails' => $updateTransaction]; 
                return $response;
            }else {
                return false;
            }
        } else {
            return false;
        }
    }

    function transferToMechanicAccount($data){
        
        $CI = get_instance();
        stripIncludeFiles();

        $mechanicId = $data['mechanic_id'];
        $amount = $data['amount'];

        $mechanicDetail = $CI->Mechanic_model
        ->with_bankDetails(['account_number','account_holder_name','sort_code'])
        ->get(['mechanic_id'=>$mechanicId]);

        $getAccount = $CI->Transfer_model
        ->fields('destination')
        ->get(['mechanic_id'=>$mechanicId]);

        if(!empty($getAccount)) {
            $accountId = $getAccount['destination'];
        } else {
            $account = \Stripe\Account::create(
                array(
                "email" => $mechanicDetail['email_id'],
                "type" => "standard",
                'external_account' => array(
                    "object" => "bank_account",
                    "currency" => "gbp",
                    "account_holder_name" => $mechanicDetail['bankDetails']['account_holder_name'],
                    "account_holder_type" => 'individual',
                    "routing_number" => $mechanicDetail['bankDetails']['sort_code'],
                    "account_number" => $mechanicDetail['bankDetails']['account_number']
                )
            ));
            $accountId = $account->id;
        }

        $transfer = \Stripe\Transfer::create(
            array( "amount" => ($amount*100),
                "currency" => "gbp",
                "destination" => $accountId,
                )
            );
        if($transfer!=null && $transfer->id != '') {
            $updateTransaction = ['user_id'=>$mechanicId,
                    'request_id'=>$requestId,
                    'transfer_charge_id'=>$transfer->id,
                    'account_id'=>$accountId,
                    'object_type'=>$transfer->object,
                    'amount'=>$transfer->amount,
                    'balance_transaction'=>$transfer->balance_transaction,
                    'currency'=>$transfer->currency,
                    'destination'=>$transfer->destination, // Created Account of AccountId
                    'destination_payment'=>$transfer->destination_payment
                ];

            $insert = $CI->Transfer_model->insert($updateTransaction);
            if($insert) {
                
                $response = ['amount'=>$transfer->amount,'status' => true]; 
                return $response;
            }
        } else {
            return ['status' => false,'response' => $transfer];
        }
    }

?>
