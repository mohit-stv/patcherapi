<?php

# Send Email By MailChimp or Mandrill
    function sendMandrillEmail($args){ 
        $CI = get_instance();
        $CI->load->library('mandrill');
        $mandrill = new Mandrill();

        $content = array(
            'html' => $args['message'],
            'text' => 'Patcher',
            'subject' => $args['subject'],
            'from_email' => 'info@patcher.co.uk',
            'from_name' => 'Patcher',
            'to' => array(
                array(
                    'email' => $args['email_id'],
                    'name' => 'Patcher',
                    'type' => 'to'
                )
            ),
            'track_opens'=>true
        );
        $async = true;
        $ip_pool = 'Main Pool';
        $send_at = 'example send_at';
        $result = $mandrill->messages->send($content, $async);
        return $result;
    }

    # Send mail with template
    function sendMandrillEmailTemlate($args){ 
        $CI = get_instance();
        // $CI->load->library('mandrill',['K5wfquKWdM1Khi7b4Wyiqw']);
        // $mandrill = new Mandrill('K5wfquKWdM1Khi7b4Wyiqw');
        $CI->load->library('mandrill');
        $mandrill = new Mandrill();

        $message = array(
                    'subject' => $args['subject'],
                    'from_email' => 'info@patcher.co.uk',
                    'from_name' => 'Patcher',
                    'to' => array( array('email' => $args['email_id'], 'name' => 'Patcher') ),
                    'merge_vars' => array(array(
                        'rcpt' => $args['email_id'],
                        'vars' => (isset($args['vars'])) ? $args['vars'] : []
                        // 'vars' => array(
                        //             array('name' => 'TOTAL',
                        //                   'content' => '2018'
                        //                 ),
                        //         )
                        )
                    )
                );

        $template_name = $args['template_name'];

        $template_content = array(
            array('name' => 'main',
                'content' => 'Hi *|FIRSTNAME|* *|LASTNAME|*, thanks for signing up.'
            ),

            array('name' => 'footer',
                'content' => 'Copyright 2017.'
                ),
            array('name' => 'total',
                'content' => '2017'
                )
        );

        $response = $mandrill->messages->sendTemplate($template_name, $template_content, $message);
        return $response;
    }
?>