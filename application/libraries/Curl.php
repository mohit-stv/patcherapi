<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Curl {
    private $CI;
    private $settings = array ();

    public function __construct() {
        //$this->CI = get_instance();
    }


    // 'Content-Type: application/vnd.api+json'
	function sendPostData($url, $reqTimeout){
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-API-KEY:1234'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$reqTimeout); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $reqTimeout); //timeout in seconds
        $curlData  = curl_exec($ch);
        curl_close($ch);

        if($curlData){
            return array("curlStatus" => true, "curlData"=> json_decode($curlData));
        }
        else{
            return array("curlStatus" => false, "curlData"=> $curlData);
        }
    }

    function _pubsubscribe($url){
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','X-API-KEY:1234'));
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $status  = curl_exec($ch);
        curl_close($ch);
        if($status == TRUE){
            return $status;
        }
        else{
           return $status;
        }
    }


}