<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Googleapi {
    private $CI;
    private $settings = array ();

    public function __construct() {
        //$this->CI = get_instance();
    }

	function getAddress($lat,$long){
        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=AIzaSyAy8t4Z1JBJMAKoHk-bXg2mTebThcER5i4";

        $detailAddress = (array)json_decode(file_get_contents($url));
        if(!$detailAddress){
            return false;
        } else {
            if(isset($detailAddress['results'][0]->formatted_address) && !empty($detailAddress['results'][0]->formatted_address)){
                return $detailAddress['results'][0]->formatted_address;
            } else{
                return false;
            }
        }
    }
}