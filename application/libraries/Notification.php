<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Notification
{
    private $CI;
    private $settings = array();

    public function __construct()
    {
        $this->CI = get_instance();
    }

    # FCM For Android Customer
    public function fcmUser($device_id, $message)
    {

        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        if ($device_id == "" || $device_id == null) {
            return false;
        }

        /*api_key available in:*/
        // $api_key = 'AAAAKhAlYWw:APA91bGiPvfW7wyXCfRvaOWmVKnqW2kOEw1fRrtMAxYuHWaUqMtUzfJ-XA1NREHBz3NXuWCDFdKeAMjnYbJB-KIDqU45f3r5_5zlSg8xvYsuEs8XDbyWz1VaXED-Q7O6kom4Cgr8CZOv';
        $api_key = 'AAAAahzUlFo:APA91bFlrKZIJYVv1CcSLsgvjieImyaclvKFztgaqsOxWY0Y_-4zZwSpcYcA8zjyT1XLs715rMA3mVwU16dyukwKxp9mBKY5U6QN-hK7a7ptfDFMc4_NX0eoCzQAe8UGtRIhKdiNdvQT';
        $fields = array
            (
            'to' => $device_id,
            'data' => $message,
        );

        //header includes Content type and api key
        $headers = array
            (
            'Authorization: key=' . $api_key,
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    # FCM For Android Customer
    public function fcmMechanic($device_id, $message)
    {

        if ($device_id == "" || $device_id == null) {
            return false;
        }
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        // $api_key = 'AAAAhqCwloE:APA91bG6vmS66UyN3dIETnxV521KHuYScxTvYnMIpkbdZvl3KTAGF0VIPfrNuKUJYh8k4AA2sKVYXqAXcYQ7c8Kk5HKOvVCQLPoHLtgiqhesQg5X5VeNEwmGRzB8QJ6HbaTrGQdNw4tg';
        $api_key = 'AAAAONsTgGo:APA91bGdu0U4LQZHel6sRJE6Q8L51Vk7g8Ph3Y13GSM1x2c3LDT7Lhrb-eWQVVpBWMoE0hhN6H6xkCuQANsymHCQSGmZtWpOda40lC4eCJ84KA247yB6bZgkGKa6Ksqh3dN0cCmWZ84B';
        $fields = array
            (
            'to' => $device_id,
            'data' => $message,
        );

        //header includes Content type and api key
        $headers = array
            (
            'Authorization: key=' . $api_key,
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    # For Iphone Customer App To User
    public function apnsUser($device_id, $message, $isProduction = 100, $platform_type, $jsonData)
    {

        if ($device_id == "" || $device_id == null) {
            return false;
        }

        if ($isProduction == 101) {
            $apnsCert = APPPATH . 'third_party/apns/customer_prod.pem';
            $apnsHost = 'gateway.push.apple.com';
        } else {
            $apnsCert = APPPATH . 'third_party/apns/customer_dev.pem';
            $apnsHost = 'gateway.sandbox.push.apple.com';
        }

        $apnsPort = 2195;
        $apnsPass = '';
        $token = $device_id;

        /*$apnsHost = 'gateway.sandbox.push.apple.com';
        $apnsCert = APPPATH.'third_party/apns/ck_user.pem';
        $apnsPort = 2195;
        $apnsPass = '123456';
        $token = 'A731FBC7A2041DD89D9F47CFB0111A46806539F96CC97026E52D45FF7A40E683';*/

        $payload['aps'] = array('alert' => $message, 'badge' => 0, 'sound' => 'default', 'content-available' => 1, 'body' => $jsonData);
        //  $payload['person'] = array('name' => 'new');
        $output = json_encode($payload);

        $token = pack('H*', str_replace(' ', '', $token));
        $apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
        stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

        $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
        fwrite($apns, $apnsMessage);
        fclose($apns);
        return $apns;
    }

    # For Iphone Customer App To Mechanic
    public function apnsMechanic($device_id, $message, $isProduction = 100, $platform_type, $jsonData)
    {

        if ($device_id == "" || $device_id == null) {
            return false;
        }

        if ($isProduction == 101) {
            $apnsCert = APPPATH . 'third_party/apns/mechanic_prod.pem';
            $apnsHost = 'gateway.push.apple.com';
        } else {
            $apnsCert = APPPATH . 'third_party/apns/mechanic_dev.pem';
            $apnsHost = 'gateway.sandbox.push.apple.com';
        }

        $apnsPort = 2195;
        $apnsPass = '';
        $token = $device_id;

        $payload['aps'] = array('alert' => $message, 'badge' => 0, 'sound' => 'default', 'content-available' => 1, 'body' => $jsonData);
        $output = json_encode($payload);
        $token = pack('H*', str_replace(' ', '', $token));
        $apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
        stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

        $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
        fwrite($apns, $apnsMessage);
        fclose($apns);
        return $apns;
    }

}
