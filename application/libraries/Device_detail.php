<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Device_detail
{
    private $CI;
    private $settings = array();
    public function __construct()
    {
        $CI = &get_instance();

    }

    /*
     * TABLE SCHEMA

    CREATE TABLE `user_device_details`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `user_type` VARCHAR(255) NOT NULL COMMENT '1->user, 2->Mechanic',
    `app_version` VARCHAR(255)  NOT NULL DEFAULT '1.0.0',
    `brand` VARCHAR(255) NOT NULL,
    `device_name` VARCHAR(255) NOT NULL,
    `model_name` VARCHAR(255) NOT NULL,
    `os_version` VARCHAR(255) NOT NULL,
    `os_type` TINYINT NOT NULL COMMENT '1->android, 2->ios',
    `unique_id` VARCHAR(255) NOT NULL COMMENT 'android->IMEI, ios->UDID',
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL,
    `deleted_at` DATETIME NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
    ) ENGINE = InnoDB;

     */

    public function registerDetail($args)
    {
        $CI = &get_instance();
        $CI->load->model('User_device_detail_model');

        if (!empty($args) || $args != null) {

            $detail = $CI->User_device_detail_model->fields('*')->get(['user_id' => $args['user_id'], 'user_type' => $args['user_type']]);

            if (isset($detail) && !empty($detail)) {
                $id = $CI->User_device_detail_model->where(['user_id' => $args['user_id'], 'user_type' => $args['user_type']])->update($args);
            } else {
                $id = $CI->User_device_detail_model->insert($args);
            }

            if ($id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function deviceList()
    {
        $CI = &get_instance();
        $CI->load->model('User_device_detail_model');

        $a = $CI->User_device_detail_model->fields('*')->get();
        return $a;
    }

    public function deviceDetail($id)
    {
        $CI = &get_instance();
        $CI->load->model('User_device_detail_model');

        $a = $CI->User_device_detail_model->fields('*')->get(['id' => $id]);
        return $a;
    }
}
