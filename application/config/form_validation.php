<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config = array(

   
    /*
    |------------------------------------------------------------------------------------------
    | CUSTOMER
    |------------------------------------------------------------------------------------------
    */
    'Registration'=>array(
        array('field'=>'name','Label'=>'name','rules'=>'required'),
        // arrsay('field'=>'mechanic_id_id','Label'=>'email id','rules'=>'required|valid_email'),
        //array('field'=>'password','Label'=>'password','rules'=>'required'),
        // array('field'=>'contact_number','Label'=>'contact','rules'=>'required'),
        array('field'=>'country_code','Label'=>'countryCode ','rules'=>'required|numeric'),
        array('field'=>'platform_type','Label'=>'platformType','rules'=>'required|numeric'),
        array('field'=>'device_key','Label'=>'deviceKey','rules'=>'required'),
    ),

    'updateUser'=>array(
        array('field'=>'name','Label'=>'name','rules'=>'required'),
        array('field'=>'email_id','Label'=>'email id','rules'=>'required'),
        array('field'=>'password','Label'=>'password','rules'=>'required'),
       
        array('field'=>'country_id','Label'=>'country Id ','rules'=>'required'),
    ),

    'ForgotPassword'=> array(
        array('field'=>'contact_number','Label'=>'contactNumber','rules'=>'required')
    ),

    'ResetPassword'=>array(
        array('field'=>'otp','Label'=>'otp','rules'=>'required'),
        array('field'=>'new_password','Label'=>'password','rules'=>'required'),
    ),

    'ResetPasswordUser'=>array(
        array('field'=>'user_id','Label'=>'User Id','rules'=>'required'),
        array('field'=>'new_password','Label'=>'password','rules'=>'required'),
    ),

    'ResetPasswordMech'=>array(
        array('field'=>'mechanic_id','Label'=>'Mechanic Id','rules'=>'required'),
        array('field'=>'new_password','Label'=>'password','rules'=>'required'),
    ),

    'ChangePasswordUser'=>array(
        array('field'=>'user_id','Label'=>'userId','rules'=>'required'),
        array('field'=>'new_password','Label'=>'newPassword','rules'=>'required'),
        array('field'=>'old_password','Label'=>'oldPassword','rules'=>'required'),
     ),

    'ChangePasswordMechanic'=>array(
        array('field'=>'mechanic_id','Label'=>'mechanic_id','rules'=>'required'),
        array('field'=>'new_password','Label'=>'new_password','rules'=>'required'),
        array('field'=>'old_password','Label'=>'old_password','rules'=>'required'),
     ),

     'VerifyNumber' => array(
        array('field' => 'user_id', 'label' => 'User Id','rules' => 'required'),
        array('field' => 'contact_number', 'label' => 'Contact Number','rules' => 'required')
    ),

     'MechanicVerifyNumber' => array(
        array('field' => 'mechanic_id', 'label' => 'Mechanic Id','rules' => 'required'),
        array('field' => 'contact_number', 'label' => 'Contact Number','rules' => 'required')
    ),

    'verifyOtp'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'required'),
        array('field'=>'otp','Label'=>'otp','rules'=>'required'),
     ),

    'resendOtp'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'required'),
     ),

    'numberChange'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'required'),
        array('field'=>'contact','Label'=>'contact','rules'=>'required'),
     ),

    'Logout'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'required'),
     ),

     'mechaniclogout'=>array(
        array('field'=>'mechanic_id','Label'=>'mechanic_id','rules'=>'required'),
     ), 

     'vehicleRegistration'=>array(
        array('field'=>'vehicle_name','Label'=>'vehicle_name','rules'=>'required'),
        array('field'=>'color','Label'=>'color','rules'=>'required'),
        array('field'=>'manufacturer','Label'=>'manufacturer','rules'=>'required'),
        array('field'=>'chassis_number','Label'=>'chassis_number','rules'=>'required'),
        array('field'=>'vehicle_type_id','Label'=>'vehicle_type_id','rules'=>'required'),
        array('field'=>'vehicle_number','Label'=>'vehicle_number','rules'=>'required'),
        array('field'=>'registration_number','Label'=>'registration_number','rules'=>'required'),
        array('field'=>'vehicle_model_no','Label'=>'vehicle_model_no','rules'=>'required'),
        array('field'=>'regcard_image_url','Label'=>'regcard_image_url','rules'=>'required'),
        array('field'=>'regcard_image_publicid','Label'=>'regcard_image_publicid','rules'=>'required'),
     ),

    'smsToReceiver'=>array(
        array('field'=>'receiver_contact','Label'=>'receiver contact','rules'=>'required'),
    ),
    
    'receiverReplySms'=>array(
        array('field'=>'receiver_contact','Label'=>'receiver contact','rules'=>'required'),
    ), 
    // ---------------------------------------
     
    #Vehicles

    'addVehicles'=>array(
        array('field'=>'userId','Label'=>'User Id','rules'=>'required'),
    ),

    'editVehicles'=>array(
        array('field'=>'userId','Label'=>'User Id','rules'=>'required'),
    ),

    'vehicleDetails'=>array(
        array('field'=>'userId','Label'=>'User Id','rules'=>'required'),
    ),

    'deleteVehicles'=>array(
        array('field'=>'userId','Label'=>'User Id','rules'=>'required'),
        array('field'=>'vehicleId','Label'=>'Vehicle Id','rules'=>'required'),
    ),

    #Services
    
    'addService'=>array(
        array('field'=>'service_name','Label'=>'Service','rules'=>'required'),
    ),

    'editService'=>array(
        array('field'=>'service_id','Label'=>'Service ID','rules'=>'required'),
    ),

    'deleteSercives'=>array(
        array('field'=>'serviceId','Label'=>'Service Id','rules'=>'required'),
    ),

    #Charges
    
    'addCharges'=>array(
        array('field'=>'charges[]','Label'=>'Charges','rules'=>'required'),
    ),

    'editCharges'=>array(
        array('field'=>'charges[]','Label'=>'Charges','rules'=>'required'),
    ),

    'deleteCharges'=>array(
        array('field'=>'chargeId','Label'=>'Charge Id','rules'=>'required'),
    ),

    #Documents
    
    'addDocuments'=>array(
        array('field'=>'documents[]','Label'=>'Charges','rules'=>'required'),
    ),

    'editDocuments'=>array(
        array('field'=>'documents[]','Label'=>'Charges','rules'=>'required'),
    ),

    'deleteDocuments'=>array(
        array('field'=>'documentId','Label'=>'Document Id','rules'=>'required'),
    ),

    #Experiences
    
    'addExperiences'=>array(
        array('field'=>'experiences[]','Label'=>'Experiences','rules'=>'required'),
    ),

    'editExperiences'=>array(
        array('field'=>'experiences[]','Label'=>'Experiences','rules'=>'required'),
    ),

    'deleteExperiences'=>array(
        array('field'=>'experienceId','Label'=>'Experience Id','rules'=>'required'),
    ),

    #Issues
    
    'addIssues'=>array(
        array('field'=>'issues[]','Label'=>'Issues','rules'=>'required'),
    ),

    'editIssues'=>array(
        array('field'=>'issues[]','Label'=>'Issues','rules'=>'required'),
    ),

    'deleteIssues'=>array(
        array('field'=>'issueId','Label'=>'Issue Id','rules'=>'required'),
    ),

    #Qualifications
    
    'addQualifications'=>array(
        array('field'=>'qualifications[]','Label'=>'Qualifications','rules'=>'required'),
    ),

    'editQualifications'=>array(
        array('field'=>'qualifications[]','Label'=>'Qualifications','rules'=>'required'),
    ),

    'deleteQualifications'=>array(
        array('field'=>'qualificationId','Label'=>'Qualification Id','rules'=>'required'),
    ),

    #Questions
    
    'addQuestion'=>array(
        array('field'=>'question','Label'=>'question','rules'=>'required'),
       // array('field'=>'answerIds[]','Label'=>'answerIds','rules'=>'required'),
    ),

    'editQuestion'=>array(
        array('field'=>'question','Label'=>'Question','rules'=>'required'),
        array('field'=>'answerIds[]','Label'=>'answerIds','rules'=>'required'),
    ),

    'deleteQuestion'=>array(
        array('field'=>'questionId','Label'=>'Question Id','rules'=>'required'),
    ),

    #Answers
    
    'addAnswer'=>array(
        array('field'=>'answer','Label'=>'answer','rules'=>'required'),
    ),

    'editAnswer'=>array(
        array('field'=>'answerId','Label'=>'Answer Id','rules'=>'required'),
        array('field'=>'answer','Label'=>'Answer','rules'=>'required'),
    ),

    'deleteAnswer'=>array(
        array('field'=>'answerId','Label'=>'Answer Id','rules'=>'required'),
    ),

    #Requests

    'addRequests'=>array(
        array('field'=>'requestType','Label'=>'requestType','rules'=>'required'),
        array('field'=>'userId','Label'=>'userId','rules'=>'required'),
        array('field'=>'requestDate','Label'=>'requestDate','rules'=>'required'),
        array('field'=>'requestTime','Label'=>'requestTime','rules'=>'required'),
    ),

    #Tool Itineraries
    
    'addToolItineraries'=>array(
        array('field'=>'toolItineraries[]','Label'=>'toolItineraries','rules'=>'required'),
    ),

    'editToolItineraries'=>array(
        array('field'=>'toolItineraries[]','Label'=>'toolItineraries','rules'=>'required'),
    ),

    'deleteToolItineraries'=>array(
        array('field'=>'toolItinerarieId','Label'=>'Tool Itineraries Id','rules'=>'required'),
    ),

    #Inspections
    
    'addInspections'=>array(
        array('field'=>'inspections[]','Label'=>'inspections','rules'=>'required'),
    ),

    #Profile

    'mechanicProfiles'=>array(
        array('field'=>'mechanicId','Label'=>'mechanicId','rules'=>'required'),
    ),

    'mechanicDetails'=>array(
        array('field'=>'mechanicId','Label'=>'mechanicId','rules'=>'required'),
    ),

    'websiteContact'=>array(
        array('field'=>'name','Label'=>'Name','rules'=>'required'),
        array('field'=>'email','Label'=>'Name','rules'=>'required'),
    ),


    #Machanics

    'mechanicsRegistration'=>array(
        array('field'=>'name','Label'=>'name','rules'=>'trim|required'),
        array('field'=>'email_id','Label'=>'email_id','rules'=>'required|trim|valid_email'),
        array('field'=>'password','Label'=>'password','rules'=>'required|trim'),
        array('field'=>'contact_number','Label'=>'contact_number','rules'=>'required|trim|numeric|min_length[10]'),
        array('field'=>'mechanic_dob','Label'=>'mechanic_dob','rules'=>'trim|required'),
        array('field'=>'mechanic_address','Label'=>'mechanic_address','rules'=>'trim|required')
    ),

    'updateMechanicsInformation'=>array(
        array('field'=>'mechanic_itinerary','Label'=>'mechanic_itinerary','rules'=>'trim|required'),
    ),

    'vehicleMake'=>array(
        array('field'=>'vehicle_make_name','Label'=>'vehicle_make_name','rules'=>'trim|required'),
        array('field'=>'vehicle_modelname','Label'=>'vehicle_modelname','rules'=>'trim|required'),
    ),

    'vehicleBodyType'=>array(
        array('field'=>'vehicle_bodytype_name','Label'=>'vehicle_bodytype_name','rules'=>'trim|required')
    ),

    'vehicleYearType'=>array(
        array('field'=>'vehicle_years','Label'=>'vehicle_years','rules'=>'trim|required')
    ),

    'vehicleWeightType'=>array(
        array('field'=>'vehicle_weight','Label'=>'vehicle_weight','rules'=>'trim|required')
    ),
    'vehicleEngineType'=>array(
        array('field'=>'vehicle_engine','Label'=>'vehicle_engine','rules'=>'trim|required')
    ),

    'vehicleDriveType'=>array(
        array('field'=>'vehicle_drivetype_name','Label'=>'vehicle_drivetype_name','rules'=>'trim|required')
    ),

    'userVehicles'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'trim|required'),
        array('field'=>'vehicle_make_name','Label'=>'vehicle_make_name','rules'=>'trim|required'),
        array('field'=>'vehicle_modelname','Label'=>'vehicle_modelname','rules'=>'trim|required'),
        array('field'=>'vehicle_bodytype_name','Label'=>'vehicle_bodytype_name','rules'=>'trim|required'),
        array('field'=>'vehicle_years','Label'=>'vehicle_years','rules'=>'trim|required'),
        // array('field'=>'vehicle_weight','Label'=>'vehicle_weight','rules'=>'trim|required'),
        array('field'=>'vehicle_engine','Label'=>'vehicle_engine','rules'=>'trim|required'),
        array('field'=>'vehicle_drivetype_name','Label'=>'vehicle_drivetype_name','rules'=>'trim|required')
    ),

    'requestDetails'=>array(
        array('field'=>'request_id','Label'=>'Request Id','rules'=>'trim|required')
    ),

    'payToMechanic'=>array(
        array('field'=>'request_ids[]','Label'=>'Request Ids','rules'=>'trim|required'),
        array('field'=>'mechanic_id','Label'=>'Mechanic Id','rules'=>'trim|required'),
    ),

    'addToolItinerary'=>array(
        array('field'=>'service_id','Label'=>'service_id','rules'=>'trim|required'),
        array('field'=>'tool_itinerary_name','Label'=>'tool_itinerary_name','rules'=>'trim|required'),
        array('field'=>'tool_description','Label'=>'tool_description','rules'=>'trim')
    ),

    'updateToolItinerary'=>array(
        array('field'=>'service_id','Label'=>'service_id','rules'=>'trim|required'),
        array('field'=>'tool_itinerary_name','Label'=>'tool_itinerary_name','rules'=>'trim|required'),
        array('field'=>'tool_description','Label'=>'tool_description','rules'=>'trim'),
        array('field'=>'tool_itinerary_id','Label'=>'tool_itinerary_id','rules'=>'trim|required'),
    ),

    'ratings'=>array(
        array('field'=>'user_id','Label'=>'user_id','rules'=>'required'),
        array('field'=>'mechanic_id','Label'=>'mechanic_id','rules'=>'required'),
        array('field'=>'rating','Label'=>'rating','rules'=>'required'),
        array('field'=>'request_id','Label'=>'request_id','rules'=>'required'),
        array('field'=>'user_type','Label'=>'user_type','rules'=>'required'),
    ),

    'adminLogin' => array(
        array('field'=>'email_id','Label'=>'email_id','rules'=>'trim|required|valid_email'),
        array('field'=>'password','Label'=>'password','rules'=>'trim|required'),
    ),

    'signupOTP' => array(
        array('field'=>'email_id','Label'=>'Email','rules'=>'trim|required|valid_email|is_unique[mechanics.email_id]'),
        array('field'=>'contact_number','Label'=>'Contact Number','rules'=>'trim|required|numeric|is_unique[mechanics.contact_number]'),
    ),
);