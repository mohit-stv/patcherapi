<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Repair extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
        $this->load->helper('common');
        $this->load->library('Googleapi');
    }

    ## Get the all Repair Work
    public function getRepairWork_post()
    {
        $this->load->model(['Request_model', 'Request_issues_model']);

        $request_id = $this->param['request_id'];
        //$user_id = $this->param['user_id'];
        $repairdata = [];

        $issue = $this->Request_issues_model->fields(['timepicker', 'location'])->where(['request_id' => $request_id, 'request_issue_status' => 101, ' 	isDeleted' => 101])->with_issue('fields:issue_name,issue_status,issuse_price')->get_all();

        foreach ($issue as $issues) {
            $result['repairdata'][] = [
                'timepicker' => $issues['timepicker'],
                'location' => $issues['location'],
                'issue_name' => $issues['issue']['issue_name'],
                'issue_status' => $issues['issue']['issue_status'],
                'issue_price' => $issues['issue']['issuse_price'],
            ];
        }

        $mechanic = $this->Request_model->fields('mechanic_id')->where(['request_id' => $request_id])->with_mechanic('fields:name,email_id,mechanic_address,mechanic_picture')->get();
        $result['mechanic'][] = $mechanic['mechanic'];

        if ($result) {
            $this->response(['status' => true, 'message' => 'Repair issuses with price ', 'response' => $result]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## For User Get the Repair Work with Total Payment of Issuse
    public function getRepairWorkWithPayment_post()
    {
        $this->form_validation->set_data($this->param);
        if (!$this->form_validation->run('requestDetails')) {
            #requestDetails uses only request_id
            $this->response(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);
        }

        $this->load->model(['Mechanic_rating_model', 'Request_issues_model', 'Mechanic_add_service_model', 'User_vehicles_model', 'Mechanic_issuses_model', 'Issue_model', 'Request_model']);
        $requestId = $this->param['request_id'];

        $getAllRequestIssuse = $this->Request_issues_model->fields('issue_id,request_issue_status,location,isCompleted,date,timepicker')->with_issue('fields:issue_name,issuse_price')->where(['request_id' => $requestId, 'request_issue_status' => 101, 'isDeleted' => 101])->get_all();

        $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id', 'part_name', 'price', 'isDeleted', 'isCompleted', 'description'])->where(['request_id' => $requestId, 'isDeleted' => 101])->get_all();

        $getRatingVal = $this->Mechanic_rating_model->fields('rating,comment')
            ->where(['request_id' => $requestId, 'from_at' => 1])
            ->get();
        if (!empty($getRatingVal)) {
            $isRating = true;
            $rating = $getRatingVal['rating'];
            $ratingComment = $getRatingVal['comment'];
        } else {
            $isRating = false;
            $rating = 0;
            $ratingComment = "";
        }

        $ratingDetails = array('isRating' => $isRating, 'rating' => $rating, 'ratingComment' => $ratingComment);

        $isCompleted = 100;

        $getResult = array();
        $ratingSum = '';
        $getMechanic = $this->Request_model->fields('request_id,user_id,request_type,mechanic_id,request_status,request_date,user_vehicles_id,comment,request_lat,request_lng')
            ->with_transaction('fields: transaction_charge_id,transaction_reference')
            ->with_transactionAll()
            ->with_mechanic('fields: name, email_id, contact_number,mechanic_uuid, mechanic_address, mechanic_picture')
            ->with_newService('fields:part_name, price, description,isCompleted')
            ->get(['request_id' => $requestId]);

        if (empty($getMechanic)) {
            $this->response(['status' => false, 'message' => 'Request Deatails not available.']);die;
        }

        $address = "";

        $user_vehicles_id = $getMechanic['user_vehicles_id'];
        if ($user_vehicles_id == "" || $user_vehicles_id == 0) {
            $where = ['user_id' => $getMechanic['user_id'], 'isDefault' => 101];
        } else {
            $where = ['user_vehicles_id' => $user_vehicles_id];
        }

        $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault', 'vehicle_number', 'vehicle_picture'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where($where)->get();

        if ($userVehicle) {
            $vehicleDetails = [
                'user_vehicles_id' => $userVehicle['user_vehicles_id'],
                'vehicle_number' => $userVehicle['vehicle_number'],
                'vehicle_picture' => $userVehicle['vehicle_picture'],
                'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
            ];
        } else {
            $vehicleDetails = array();
        }

        if ($getMechanic['request_status'] == 2 || $getMechanic['request_status'] == 9 || $getMechanic['request_status'] == 10) {
            $isJourney = true;
        } else {
            $isJourney = false;
        }

        if ($getMechanic['request_type'] == 2) {
            $address = $this->googleapi->getAddress($getMechanic['request_lat'], $getMechanic['request_lng']);
        }

        $requestDate = $getMechanic['request_date'];
        $explodeDate = explode(" ", $requestDate);
        $date = $explodeDate[0];
        $time = $explodeDate[1];

        $transactions = [];
        if (isset($getMechanic['transactionAll']) & !empty($getMechanic['transactionAll'])) {
            foreach ($getMechanic['transactionAll'] as $trans) {

                $transactionDateTime = $trans['created_at'];
                $explodeTransactionDateTime = explode(' ', $transactionDateTime);
                $transactionDate = $explodeTransactionDateTime[0];
                $transactionTime = $explodeTransactionDateTime[1];
                $transaction_type = $trans['transaction_type'];
                if ($transaction_type == 1) {
                    $transaction_type = "Callout Fee";
                } else {
                    $transaction_type = "Service Fee";
                }

                $object_type = $trans['object_type'];

                $transactions[] = [
                    'transaction_id' => !empty($trans['transaction_reference']) ? $trans['transaction_reference'] : $trans['transaction_charge_id'],
                    'amount' => $trans['amount'],
                    'payment_type' => $trans['payment_type'],
                    'transaction_status' => $trans['transaction_status'],
                    'transaction_date' => $transactionDate,
                    'transaction_time' => $transactionTime,
                    'transaction_type' => $transaction_type,
                    'remark' => ($object_type == "charge") ? "Paid" : ucfirst($object_type) ,
                    'remark_status' => ($object_type == "charge") ? 1 : 2, //Charge = 1 || refund = 2
                    'isJourney' => $isJourney,
                ];
            }
        }

        $getResult['request'] = [
            'request_id' => $getMechanic['request_id'],
            'request_type' => $getMechanic['request_type'],
            'request_status' => $getMechanic['request_status'],
            'transaction_id' => !empty($getMechanic['transaction']['transaction_reference']) ? $getMechanic['transaction']['transaction_reference'] : $getMechanic['transaction']['transaction_charge_id'],
            'isJourney' => $isJourney,
            'transactions' => $transactions
        ];

        $getResult['mechanics'] = $getMechanic['mechanic'];
        $mechanicsID = $getMechanic['mechanic_id'];

        if ($mechanicsID != '') {
            $getRating = $this->Mechanic_rating_model
                ->where(['mechanic_id' => $mechanicsID, 'from_at' => 1])->get_all();
            if (!empty($getRating)) {
                foreach ($getRating as $getRatings) {
                    $ratingSum += $getRatings['rating'];
                }
            }
            $avgRating = $ratingSum / count($getRating);
            if (isset($avgRating) && $avgRating != '') {
                $getResult['mechanics']['avgRating'] = round($avgRating);
            } else {
                $getResult['mechanics']['avgRating'] = 0;
            }
        } else {
            $getResult['mechanics']['avgRating'] = 0;
        }

        $comment = $getMechanic['comment'];
        if (empty($comment) || $comment == null) {
            $comment = "";
        }

        if (!empty($getAllRequestIssuse)) {

            $total = '';
            $newServicePrice = '';
            $result = [];
            foreach ($getAllRequestIssuse as $key => $getAllRequestIssuses) {
                $date = $getAllRequestIssuses['date'];
                $address = $getAllRequestIssuses['location'];
                $time = $getAllRequestIssuses['timepicker'];
                $issuseID = $getAllRequestIssuses['issue_id'];
                $getIssuePrice = $this->Issue_model->fields(['issue_id', 'issue_name', 'issuse_price'])->where('issue_id', $issuseID)->get();

                $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price', 'issuse_id', 'isCompleted'])->get(['request_id' => $requestId, 'issuse_id' => $issuseID]);

                if (!empty($getNewPriceOfIssuse['issuse_price'])) {
                    $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                    $isCompleted = $getNewPriceOfIssuse['isCompleted'];
                } else {
                    $issusePrice = $getIssuePrice['issuse_price'];
                    $isCompleted = $getAllRequestIssuses['isCompleted'];
                }

                $service = [];
                if (!empty($getNewAddService)) {
                    foreach ($getNewAddService as $getNewAddServices) {
                        $service[] = [
                            'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                            'issue_name' => $getNewAddServices['part_name'],
                            'issue_price' => $getNewAddServices['price'],
                            'status' => 101, //101 for edit service price
                            'request_issue_status' => $getNewAddServices['isDeleted'],
                            'isCompleted' => $getNewAddServices['isCompleted'],
                            'description' => $getNewAddServices['description'],
                            // 'status' => '101' //Services added by mechanincs
                        ];
                    }
                }

                $result[] = ['issue_id' => $getAllRequestIssuses['issue']['issue_id'],
                    'issue_name' => $getAllRequestIssuses['issue']['issue_name'],
                    'issue_price' => $issusePrice,
                    'status' => 100, //100 for edit issue price
                    'request_issue_status' => $getAllRequestIssuses['request_issue_status'],
                    'isCompleted' => $isCompleted,
                    'description' => '',
                    // 'status' => '100' //Issue added by User
                ];
            }

            $getResult['request']['request_date'] = $date;
            $getResult['request']['request_time'] = $time;
            $getResult['request']['location'] = $address;

            $response = array_merge($result, $service);
            if (!empty($response)) {
                $this->response(['status' => true, 'message' => 'All Completed Issse With Price', 'response' => $response, 'comment' => $comment, 'vehicleDetails' => $vehicleDetails, 'ratingDetails' => $ratingDetails, 'requestDetails' => $getResult]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }
        } elseif (!empty($getNewAddService)) {
            $service = [];

            $getResult['request']['request_date'] = $date;
            $getResult['request']['request_time'] = $time;
            $getResult['request']['location'] = $address;

            if (!empty($getNewAddService)) {
                foreach ($getNewAddService as $getNewAddServices) {
                    $service[] = [
                        'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                        'issue_name' => $getNewAddServices['part_name'],
                        'issue_price' => $getNewAddServices['price'],
                        'request_issue_status' => $getNewAddServices['isDeleted'],
                        'isCompleted' => $getNewAddServices['isCompleted'],
                        'status' => '101', //Services added by mechanincs
                        'description' => $getNewAddServices['description'],
                    ];
                }

            }
            if (!empty($service)) {
                $this->response(['status' => true, 'message' => 'All Completed Issue With Price', 'response' => $service, 'vehicleDetails' => $vehicleDetails, 'ratingDetails' => $ratingDetails, 'comment' => $comment, 'requestDetails' => $getResult]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }

        } else {
            $getResult['request']['request_date'] = $date;
            $getResult['request']['request_time'] = $time;
            $getResult['request']['location'] = $address;
            $this->response(['status' => true, 'message' => 'Issue Details not available', 'response' => [], 'vehicleDetails' => $vehicleDetails, 'ratingDetails' => $ratingDetails, 'comment' => $comment, 'requestDetails' => $getResult]);die;
        }
    }

    ## Submit Repair Issue Based on RequestID
    public function submitRepairIssuse_post()
    {
        $this->load->library(['Curl', 'Mypubnub', 'Notification']);
        $this->load->model(['Request_issues_model', 'User_model', 'Request_model', 'Mechanic_model', 'Vehicle_make_model', 'User_vehicles_model']);
        $this->load->helper('common');

        $pubnubResponse = [];

        $issueResponse = $this->param['request_response'];
        if (!empty($issueResponse)) {
            $userId = $this->param['user_id'];
            $mechanicId = $this->param['mechanic_id'];
            $latitude = $this->param['latitude'];
            $longitude = $this->param['longitude'];
            $comment = "";
            if (isset($this->param['comment']) && !empty($this->param['comment'])) {
                $comment = $this->param['comment'];
            }

            $user_vehicles_id = 0; //maintain for previous app version
            if (isset($this->param['user_vehicles_id']) && $this->param['user_vehicles_id'] != "") {
                $user_vehicles_id = $this->param['user_vehicles_id'];
            }

            #Process Starts here....
            $this->db->trans_start();

            foreach ($issueResponse as $issueResponses) {
                $data[] = ['issue_id' => $issueResponses['id'],
                    'request_issue_status' => $issueResponses['response'],
                    'date' => $this->param['date'],
                    'timepicker' => $this->param['time'],
                    'location' => $this->param['location']];
            }
            $request_issuse_id = $this->Request_issues_model->insert($data);
            $insert['request_issue'] = $request_issuse_id;

            $requestrray = [
                'mechanic_id' => $mechanicId,
                'request_type' => $this->param['request_type'],
                'user_id' => $userId,
                'request_date' => date('Y:m:d H:i:s'),
                'request_lat' => $latitude,
                'request_lng' => $longitude,
                'comment' => $comment,
                'user_vehicles_id' => $user_vehicles_id,
                //'request_status'=> 1
            ];
            $request = $this->Request_model->insert($requestrray);

            // Pay Callout Fee
            if (isset($this->param['callout_fee']) && $this->param['callout_fee'] != '') {
                $callOutFee = $this->param['callout_fee'];
                $lastfour_digit = $this->param['lastfour_digit'];
                if (isset($this->param['customer_id']) && $this->param['customer_id'] != '') {
                    $customerId = $this->param['customer_id'];
                } else {
                    $customerId = '';
                }

                if (isset($this->param['token']) && $this->param['token'] != '') {
                    $token = $this->param['token'];
                } else {
                    $token = '';
                }

                $updateCallOutFee = $this->Request_model->where('request_id', $request)->update(['callout_fee' => $callOutFee]);

                $paymentData = [
                    'user_id' => $userId,
                    'request_id' => $request,
                    'amount' => $callOutFee,
                    'token' => $token,
                    'last_four_digit' => $lastfour_digit,
                    'customer_id' => $customerId,
                ];
                $payCallout = newPayCallOutFee($paymentData);
                if (!isset($payCallout['status']) || $payCallout['status'] != 'succeeded') {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $this->response(['status' => false, 'message' => 'Payment failed. Please try again after sometime.', 'response' => $payCallout]);die;
                }
            }
            // EDO Of Callout Fee

            //$insert['request_id'] = $request;
            $update_data = ['request_id' => $request];
            $insert['update'] = $this->Request_issues_model->where('request_issue_id', $request_issuse_id)->update($update_data);

            $getAllRequestIssuse = $this->Request_issues_model->fields('issue_id')->with_issue('fields:issue_name,issuse_price')->where(['request_id' => $request, 'request_issue_status' => 101, 'isDeleted' => 101])->get_all();
            $issueDetails = [];
            if (!empty($getAllRequestIssuse)) {
                //    $issueDetails=[];
                foreach ($getAllRequestIssuse as $getAllRequestIssuses) {
                    $issueDetails[] = ['issue_id' => $getAllRequestIssuses['issue_id'],
                        'issue_name' => $getAllRequestIssuses['issue']['issue_name'],
                        'issue_price' => $getAllRequestIssuses['issue']['issuse_price'],
                    ];
                }
            }

            $redis = createConnectionToRedis();
            $specificMechanicLatLong = $redis->geopos('mechanics', $mechanicId);
            $mechanicLong = $specificMechanicLatLong[0][0];
            $mechanicLat = $specificMechanicLatLong[0][1];

            $addDist1 = $redis->geoadd('repairnotification', $mechanicLong, $mechanicLat, $mechanicId);
            $addDist2 = $redis->geoadd('repairnotification', $longitude, $latitude, 'user');

            $getDistance = $redis->geodist('repairnotification', $mechanicId, 'user', 'mi');
            $mechanicName = $this->Mechanic_model->where(['mechanic_id' => $mechanicId, 'isOnline' => 101])->fields(['name', 'device_key', 'isProduction', 'platform_type', 'mechanic_uuid'])->get();

            $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'vehicle_number', 'isDefault'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_vehicles_id' => $user_vehicles_id])->get();

            $userDetail = $this->User_model->fields(['name'])->where(['user_id' => $userId])->get();

            ## Send Notification For Android To Mechanic
            $device_id = $mechanicName['device_key'];
            $arrayvalMsg = ['user_id' => $userId,
                'user_name' => $userDetail['name'],
                'request_id' => $request,
                'issues' => $issueDetails,
                'vehicle_number' => $userVehicle['vehicle_number'],
                'year' => (isset($userVehicle['year']['vehicle_years']) ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => (isset($userVehicle['make']['vehicle_make_name']) ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => (isset($userVehicle['model']['vehicle_modelname']) ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => (isset($userVehicle['body']['vehicle_bodytypes_name']) ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => (isset($userVehicle['engine']['vehicle_engine']) ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => (isset($userVehicle['drivetype']['vehicle_drivetype_name']) ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
                'address' => $this->param['location'],
                'date' => $this->param['date'],
                'time' => $this->param['time'],
                'distance' => round($getDistance, 2),
                'request_type' => 1, // For repair
                'location' => $this->param['location'],
            ];

            if ($mechanicName['platform_type'] == 1) {
                $message = [
                    'title' => 'Patcher Request For Repair',
                    'body' => $arrayvalMsg,
                    'tag' => ['notification_type' => 1],
                ];
                $notify = $this->notification->fcmMechanic($device_id, $message);
            } else if ($mechanicName['platform_type'] == 2) {

                $mechanicUuid = $mechanicName['mechanic_uuid'];
                $pubnub_args = ['channel' => $mechanicName['mechanic_uuid'],
                    'message' => ['type' => 'R2', 'userDetailsForRepair' => $arrayvalMsg]];

                $publish_status = $this->mypubnub->publish($pubnub_args);

                $pubnubResponse = [
                    'requestData' => $pubnub_args,
                    'responseData' => $publish_status,
                ];

                $jsonData = ['mechanic_uuid' => $mechanicUuid, 'request_id' => $request];

                $message = [
                    'title' => 'Patcher Request For Repair',
                    'category' => ['notification_type' => 1,'request_id' => $request],
                    // 'notification_type' => 1,
                ];
                //$device_id = 'A731FBC7A2041DD89D9F47CFB0111A46806539F96CC97026E52D45FF7A40E683';
                $platform_type = $mechanicName['platform_type'];
                $isProduction = $mechanicName['isProduction'];
                $notify = $this->notification->apnsMechanic($device_id, $message, $isProduction, $platform_type, $jsonData);
            }

            //$aa = json_encode(['value','key','test']);
            //$msg = json_encode($aa);
            $response = array_merge($insert, $arrayvalMsg);

            $this->db->trans_complete();
            if (!empty($insert)) {
                $this->db->trans_commit();
                $this->response(['status' => true, 'message' => 'Your request sent to ' . $mechanicName['name'] . '.', 'response' => $response, 'pubnubResponse' => $pubnubResponse]);die;
            } else {

                $this->db->trans_rollback();
                $this->response(['status' => false, 'message' => 'Sorry You Not Added Schedule.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    public function updateRepairIssuse_post()
    {
        $this->load->model(['Request_issues_model', 'Request_model', 'Mechanic_issuses_model']);
        $this->load->helper('common');

        $request_id = $this->param['request_id'];
        $user_id = $this->param['user_id'];
        $issueList = $this->param['request_response'];
        $date = $this->param['date'];
        $timepicker = $this->param['timepicker'];
        $location = $this->param['location'];
        $comment = "";
        if (isset($this->param['comment']) && !empty($this->param['comment'])) {
            $comment = $this->param['comment'];
            $requestrray = [
                'comment' => $comment,
            ];

            $this->Request_model->where(['request_id' => $request_id])->update($requestrray);
        }

        $insertNew = array();
        $updateOld = "";

        foreach ($issueList as $issue) {
            $issue_id = $issue['issue_id'];
            $issueFlag = $issue['flag']; // 100 -> Disable and 101 ->  Enable

            $isIssueExist = $this->Request_issues_model->fields('request_issue_id')->get(['request_id' => $request_id, 'issue_id' => $issue_id]);

            $isMechanicIssueExist = $this->Request_issues_model->fields('request_issue_id')->get(['request_id' => $request_id, 'issue_id' => $issue_id]);

            if ($isIssueExist == true && $issueFlag == 100) {
                $updateOld .= $isIssueExist['request_issue_id'] . ",";
            } else if ($isIssueExist == false && $issueFlag == 101) {
                $insertNew[] = array(
                    'request_id' => $request_id,
                    'issue_id' => $issue_id,
                    'date' => $date,
                    'timepicker' => $timepicker,
                    'location' => $location,
                );
            }
        }

        $this->db->trans_start();

        if (!empty($updateOld)) {
            $updateOld = explode(",", $updateOld);
            $this->Request_issues_model->where('request_issue_id', $updateOld)->force_delete();
        }

        if (!empty($insertNew)) {
            $this->Request_issues_model->insert($insertNew);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $this->db->trans_commit();

            //Send Notification to Mechanic
            $this->load->library(['Notification', 'Mypubnub']);
            $requestMechanicDetails = $this->Request_model->fields('request_id,request_type')->with_mechanic('fields: device_key,isProduction,platform_type,mechanic_uuid')->get(['request_id' => $request_id]);

            $device_id = $requestMechanicDetails['mechanic']['device_key'];
            $platFormType = $requestMechanicDetails['mechanic']['platform_type'];
            $isProduction = $requestMechanicDetails['mechanic']['isProduction'];
            $uuid = $requestMechanicDetails['mechanic']['mechanic_uuid'];

            $message = ['notification_type' => 10, 'request_id' => $request_id];
            if ($platFormType == 1) {

                $notificationMessage = [
                    'title' => 'Issues have been updated by User.',
                    'body' => $message,
                    'tag' => ['notification_type' => 10],
                    'request_type' => $requestMechanicDetails['request_type'],
                ];

                $notify = $this->notification->fcmMechanic($device_id, $notificationMessage);
                // print_r($notify);
            } elseif ($platFormType == 2) {
                $pubnub_args = ['channel' => $uuid,
                    'message' => ['type' => 10, 'meesage' => $message]];

                $publish_status = $this->mypubnub->publish($pubnub_args);

                $bodyData = [
                    'message' => 'Issues have been updated by User.',
                    'data' => $message,
                ];

                $messageTitle = [
                    'title' => 'Issues have been updated by User.',
                    'category' => ['notification_type' => 10],
                ];

                $notify = $this->notification->apnsMechanic($device_id, $messageTitle, $isProduction, $platFormType, $bodyData);
            }

            $this->response(['status' => true, 'message' => 'Issues list have been updated.']);
        } else {
            $this->db->roll_back();
            $this->response(['status' => false, 'message' => 'Error in processing. Please try again later.']);
        }

    }

    ## Pay Call Out Fee
    public function payCallOutFee($data)
    {
        $this->load->model(['common_model', 'User_model', 'Payment_type_model', 'Transaction_model', 'Request_model']);
        $this->load->library('Notification');
        $this->load->helper('common');

        $userId = $data['user_id'];
        $stripInclude = stripIncludeFiles();

        $getUserDetails = $this->User_model
            ->with_transaction('fields: customer_id')
            ->fields('email_id')
            ->where('user_id', $userId)->get();

        $emailId = $getUserDetails['email_id'];

        if (!empty($getUserDetails['transaction'])) {
            $customerId = $getUserDetails['transaction']['customer_id'];
        } else {
            $customerArray = ['description' => 'Customer for ' . $emailId,
                'email' => $emailId,
                'source' => $data['token']];
            $customer = \Stripe\Customer::create($customerArray);
            $customerId = $customer->id;
        }

        $charge = \Stripe\Charge::create(array(
            'customer' => $customerId,
            'amount' => $data['amount'],
            'currency' => 'gbp'));

        if ($charge != null && $charge->status == 'succeeded') {

        } else {

        }
    }

    ## Get Mechanic Details For Check Avalibility with Time Slot
    public function getMechanicDetails_post()
    {
        $this->load->model(['Mechanic_rating_model', 'User_review_model', 'Mechanic_model', 'Request_issues_model', 'User_model', 'Busy_schedule_model', 'Mechanic_service_model']);
        $mechanicId = $this->param['mechanic_id'];
        $latitude = $this->param['latitude'];
        $longitude = $this->param['longitude'];

        $redis = createConnectionToRedis();
        $getRating = $this->Mechanic_rating_model
            ->with_userDetail()
            ->where(['mechanic_id' => $mechanicId, 'from_at' => 1])->get_all();

        $allReview = [];

        $ratingSum = '';
        if (!empty($getRating)) {
            foreach ($getRating as $getRatings) {
                $ratingSum += $getRatings['rating'];

                if (isset($getRatings['userDetail']) && !empty($getRatings['userDetail']) && !empty($getRatings['comment'])) {
                    $allReview[] = [
                        'review_message' => $getRatings['comment'],
                        'review_given_by' => $getRatings['userDetail']['name'],
                        'profile_picture' => $getRatings['userDetail']['profile_picture'],
                        'rating' => $getRatings['rating'],
                        'rating_date' => $getRatings['created_at'],
                    ];
                }
            }
            $average_rating = round($ratingSum / count($getRating));
        }

        if (isset($mechanicId) && $mechanicId != '') {
            // $specificMechanicLatLong = $redis->geopos('mechanics', $mechanicId);
            $specificMechanicLatLong = $redis->geopos('mechanicSpotLatLng', $mechanicId);

            $mechanicLong = $specificMechanicLatLong[0][0];
            $mechanicLat = $specificMechanicLatLong[0][1];

            $addDist1 = $redis->geoadd('dist', $mechanicLong, $mechanicLat, $mechanicId);
            $addDist2 = $redis->geoadd('dist', $longitude, $latitude, 'user');

            if (isset($mechanicLong) && isset($mechanicLat)) {
                $getDistance = $redis->geodist('dist', $mechanicId, 'user', 'mi');
            }

            $mechanic = $this->Mechanic_model
                ->fields('mechanic_id,name,contact_number,mechanic_address,mechanic_picture,hourly_rate')
                ->where('mechanic_id', $mechanicId)
                ->get();

            $checkReq = $this->Request_model
                ->fields('request_id,request_status')
                ->where('request_status', [1, 2])
            // ->where('request_id', $requestIds)
                ->where('mechanic_id', $mechanicId)
                ->get_all();

            if (!empty($checkReq)) {
                $allRequestIds = [];
                foreach ($checkReq as $checkReqs) {
                    $allRequestIds[] = $checkReqs['request_id'];
                }
                $requestTimeSlot = $this->Request_issues_model->fields(['date', 'timepicker', 'request_id'])->where('request_id', $allRequestIds)->group_by('request_id')->get_all(['date >=' => date('Y-m-d')]);
                if (!empty($requestTimeSlot)) {
                    foreach ($requestTimeSlot as $requestTimeSlots) {
                        $timeSlots[] = ['date' => $requestTimeSlots['date'],
                            'timepicker' => $requestTimeSlots['timepicker'],
                            'request_id' => $requestTimeSlots['request_id'],
                            'isRequest' => 101,
                        ];
                    }
                }
            }

            // Manage The Busy Schedule
            $getBusySchedule = $this->Busy_schedule_model
                ->fields('start_date,end_date,start_time,end_time')
                ->where(['mechanic_id' => $mechanicId, 'isDeleted' => 101, 'end_Date >=' => date('Y-m-d')])
                ->get_all();
            if (!empty($getBusySchedule)) {
                foreach ($getBusySchedule as $getBusySchedules) {

                    $timeSlots[] = ['startDate' => $getBusySchedules['start_date'],
                        'startTime' => $getBusySchedules['start_time'],
                        'endDate' => $getBusySchedules['end_date'],
                        'endTime' => $getBusySchedules['end_time'],
                        'isRequest' => 100];
                }
            }

            $getService = $this->Mechanic_service_model->where(['mechanic_id' => $mechanicId, 'mechanic_services_status' => 101])->with_service('fields:service_name')->get_all();
            if (!empty($getService)) {
                $serviceName = [];
                foreach ($getService as $getServices) {
                    $serviceName[] = $getServices['service']['service_name'];
                }
            }

            $response = [
                'mechanic_id' => $mechanic['mechanic_id'],
                'name' => $mechanic['name'],
                'contact_number' => $mechanic['contact_number'],
                'mechanic_address' => (isset($mechanic['mechanic_address']) ? $mechanic['mechanic_address'] : ''),
                'mechanic_picture' => (isset($mechanic['mechanic_picture']) ? $mechanic['mechanic_picture'] : ''),
                'latitude' => (isset($mechanicLat) ? $mechanicLat : ''),
                'longitude' => (isset($mechanicLong) ? $mechanicLong : ''),
                //'requests'=>$request_details,
                'distance' => (isset($getDistance) ? $getDistance : ''),
                'rating' => (isset($average_rating) ? $average_rating : 0),
                'hourly_rate' => (isset($mechanic['hourly_rate']) ? $mechanic['hourly_rate'] : '0'),
            ];

            if (isset($allReview) && !empty($allReview)) {
                $response['reviews'] = $allReview;
            }

            if (!empty($timeSlots)) {
                $response['time_slot'] = $timeSlots;
            }

            if (!empty($serviceName)) {
                $response['service_types'] = $serviceName;
            }

            unset($average_rating, $requestIds);

            if (!empty($response)) {
                $this->response(['status' => true, 'message' => 'Please select time.', 'response' => $response]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Request Here']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Inavlid request for mechanic details.']);die;
        }
    }

    ## Cancel Request By User (Repair Request)
    public function cancelRepairRequest_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model', 'Mechanic_model', 'Bank_detail_model'
            , 'Request_issues_model', 'Transaction_model']);

        $this->load->helper('common');
        $stripInclude = stripIncludeFiles();

        $requestId = $this->param['request_id'];
        $userId = $this->param['user_id'];
        $status = $this->param['status']; // 4 for cancel

        $userData = $this->User_model->fields('email_id')->where('user_id', $userId)->get();

        $checkStatusByUser = $this->Request_model
            ->with_request_issues(['date', 'timepicker'])
            ->with_transaction(['fields' => 'transaction_id,amount,transaction_charge_id', 'where' => ['transaction_type' => 1]])
            ->fields(['request_status', 'mechanic_id', 'created_at'])
            ->get(['request_id' => $requestId]);

        if (!empty($checkStatusByUser)) {
            if ($status == 4) {
                $selectDate = $checkStatusByUser['request_issues']['date'];
                $selectTimepicker = $checkStatusByUser['request_issues']['timepicker'];
                $requestDate = $selectDate . ' ' . $selectTimepicker;

                $remainHours = (time() - strtotime($requestDate)) / 3600;

                if ($checkStatusByUser['request_status'] == 1) {

                    if (empty($checkStatusByUser['transaction']) || $checkStatusByUser['transaction']['transaction_charge_id'] == "") {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User Transaction Details not available.']);die;
                    }
                    $requestTransactionId = $checkStatusByUser['transaction']['transaction_id'];
                    $requestTransactionRef = $checkStatusByUser['transaction']['transaction_charge_id'];

                    $refundPayment = refundAuthAmount($requestTransactionRef);
                    if ($refundPayment != null && $refundPayment->status == 'succeeded') {
                        $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                        $refundPaymentData = [
                            'transaction_charge_id' => $refundPayment->id,
                            'object_type' => $refundPayment->object,
                            'amount' => ($refundPayment->amount / 100),
                            'transaction_reference' => $transaction_reference,
                            'paid_status' => 3, //Refund Amount
                            'transaction_type' => 1, // 1 for callout fee
                            'payment_type' => 1, //Which type payment
                            'transaction_status' => $refundPayment->status,
                        ];
                        $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($refundPaymentData);

                        if (!$updateTransaction) {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                        }
                    } else {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                    }

                    $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => $status]);
                    $this->response(['status' => true, 'message' => 'Request cancelled.']);

                } elseif ($checkStatusByUser['request_status'] == 2) {

                    $requestDate = $this->Request_model
                        ->fields('mechanic_id,request_status,callout_fee,created_at,updated_at')
                        ->with_mechanic('fields: name,email_id,platform_type,isProduction,device_key')
                        ->where('request_id', $requestId)
                        ->get();

                    $deviceId = $requestDate['mechanic']['device_key'];
                    $plateformType = $requestDate['mechanic']['platform_type'];
                    $isProduction = $requestDate['mechanic']['isProduction'];

                    if ($remainHours > 48) {
                        if (empty($checkStatusByUser['transaction']) || $checkStatusByUser['transaction']['transaction_charge_id'] == "") {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User Transaction Details not available.']);die;
                        }
                        $requestTransactionId = $checkStatusByUser['transaction']['transaction_id'];
                        $requestTransactionRef = $checkStatusByUser['transaction']['transaction_charge_id'];

                        $refundPayment = refundAuthAmount($requestTransactionRef);
                        if ($refundPayment != null && $refundPayment->status == 'succeeded') {

                            $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                            $refundPaymentData = [
                                'transaction_charge_id' => $refundPayment->id,
                                'object_type' => $refundPayment->object,
                                'transaction_reference' => $transaction_reference,
                                'amount' => ($refundPayment->amount / 100),
                                'paid_status' => 3, //Refund Amount
                                'transaction_type' => 1, // 1 for callout fee
                                'payment_type' => 1, //Which type payment
                                'transaction_status' => $refundPayment->status,
                            ];
                            $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($refundPaymentData);

                            if (!$updateTransaction) {
                                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                            }
                        } else {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                        }
                    } else {

                        //First Capture amount then refund 50 %

                        #Now need account details thus temporary added to full refund
                        $requestTransactionId = $checkStatusByUser['transaction']['transaction_id'];
                        $requestTransactionRef = $checkStatusByUser['transaction']['transaction_charge_id'];
                        $requestAmount = $amountToTransfer['transaction']['amount'];

                        $captureAmount = $requestAmount; //Amount Captured
                        $confirmPayment = captureAmount($requestTransactionRef);
                        if ($confirmPayment != null && $confirmPayment->status == 'succeeded') {
                            $confirmPaymentData = [
                                'transaction_charge_id' => $confirmPayment->id,
                                'object_type' => $confirmPayment->object,
                                'amount' => ($confirmPayment->amount / 100),
                                'network_status' => $confirmPayment->outcome->network_status,
                                'message' => $confirmPayment->outcome->seller_message,
                                'paid_status' => 2,
                                'source_id' => $confirmPayment->source->id,
                                'transaction_type' => 1, // 1 for callout fee
                                'last4' => $lastFourDigit,
                                'payment_type' => 1, //Which type payment
                                'source_brand' => $confirmPayment->source->brand,
                                'transaction_status' => $confirmPayment->status,
                            ];
                            $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($confirmPaymentData);

                            if (!$updateTransaction) {
                                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                            }
                        } else {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                        }

                        if ($remainHours >= 24 && $remainHours <= 48) {
                            $captureAmount = round($amountToTransfer / 2);
                            $tarnsferData = [
                                'user_id' => $userId,
                                'amount' => $captureAmount,
                                'request_id' => $requestId,
                            ];

                            $transferToAccount = transferToAccount($tarnsferData);
                            if (!$transferToAccount) {
                                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to refund user callout fee.']);
                            }

                            $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                            $refundPaymentData = [
                                'transaction_charge_id' => $refundPayment->id,
                                'transaction_reference' => $transaction_reference,
                                'object_type' => $refundPayment->object,
                                'amount' => ($refundPayment->amount / 100),
                                'paid_status' => 3, //Refund Amount
                                'transaction_type' => 1, // 1 for callout fee
                                'payment_type' => 1, //Which type payment
                                'transaction_status' => $refundPayment->status,
                            ];

                            $updateTransaction['request_id'] = $requestId;
                            $updateTransaction['user_id'] = $userId;
                            $updateTransaction['mechanic_id'] = $requestDate['mechanic_id'];

                            $updateTransaction = $this->Transaction_model->insert($refundPaymentData);
                        }

                    }

                    $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => $status]);

                    // Send Notification To Mechanic
                    if ($plateformType == 1) {
                        $notificationMessage = [
                            'title' => 'Request Cancel By User',
                            'body' => ['callout_fee' => $callOutFee],
                            'icon' => 'myicon',
                            'sound' => 'mySound',
                            'tag' => ['notification_type' => 7], //Cancel By User
                        ];
                        $notify = $this->notification->fcmMechanic($deviceId, $notificationMessage);
                    } elseif ($plateformType == 2) {
                        $bodyData = ['request_id' => $requestId, 'callout_fee' => $callOutFee];
                        $messageTitle = [
                            'title' => 'Request Cancel By User',
                            'category' => ['notification_type' => 7], //Cancel By User
                        ];
                        $notify = $this->notification->apnsMechanic($deviceId, $messageTitle, $isProduction, $plateformType, $bodyData);
                    }

                    $this->response(['status' => true, 'message' => 'Request cancelled.']);

                }
            } else {
                $this->response(['status' => false, 'message' => 'Invalid Status.']);
            }
        }
    }

    ## For FCM Testing
    public function testfcmwithrequest_post()
    {
        $this->load->library('Notification');
        $device_id = 'djnzrKOPPW4:APA91bGnLxVxavVG7tX7IPzZBlU2dhajOwpjTqkreWISX_cMPebCsOsQFr2BactOFa51IeP4GuFmTKmdl8FZMISGzn_xNFYhE9QGqvkY5396lMoNBjGYiSX-AUzV2jRK5aKXFdkKXbYv';
        $message = [
            'title' => 'Patcher Request For Test',
            'body' => ['User_id' => 10,
                'user_name' => 'tes',
                'year' => '200',
                'make' => 'abc',
                'model' => '10',
                'body' => 'bodyname',
                'engine_type' => 'engina',
                'drive_type' => 'drive',
                'latitude' => '70',
                'logitude' => '80',
                'distance' => ''],
            'tag' => ['notification_type' => $this->param['request_type']],
            'icon' => 'myicon',
        ];

        $notify = $this->notification->fcmMechanic($device_id, $message);
        echo '<pre>';
        print_r($notify);
        die;
    }

    ## For FCM Testing
    public function testfcm_post()
    {
        $this->load->library('Notification');
        $device_id = 'djnzrKOPPW4:APA91bGnLxVxavVG7tX7IPzZBlU2dhajOwpjTqkreWISX_cMPebCsOsQFr2BactOFa51IeP4GuFmTKmdl8FZMISGzn_xNFYhE9QGqvkY5396lMoNBjGYiSX-AUzV2jRK5aKXFdkKXbYv';
        $message = [
            'title' => 'Title Of Notification',
            'body' => 'Body  Of Notification',

            'icon' => 'myicon', /*Default Icon*/
            'sound' => 'mySound', /*Default sound*/
        ];
        $notify = $this->notification->fcmMechanic($device_id, $message);
        echo '<pre>';
        print_r($notify);
        die;
    }

    ## For APNS Testing
    public function testapns_post()
    {
        $this->load->library('Notification');
        $device_id = 'A731FBC7A2041DD89D9F47CFB0111A46806539F96CC97026E52D45FF7A40E683';
        $message = [
            'body' => 'Body  Of Notification',
            'title' => ['Year' => 2000, 'model' => 'test'],
            'icon' => 'myicon', /*Default Icon*/
            'sound' => 'mySound', /*Default sound*/
            'category' => ['notification_type' => 6],
        ];
        $isProduction = 100;
        $platform_type = 2;
        $notify = $this->notification->apnsUser($device_id, $message, $isProduction, $platform_type);
        echo 'dsdsdsdsd' . '<pre>';
        print_r($notify);
        die;
    }
}
