<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charges extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addCharges_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addCharges') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Charges_model']);             
            if(isset($this->param['charges']) && $this->param['charges'] != NUll){
                for($i=0;$i<count($this->post('charges'));$i++){               
                    $charges_array=[
                        'chargeName' => $this->param['charges'][$i]['chargeName'],
                        'description' => $this->param['charges'][$i]['description'],
                        'chargeStatus' => $this->param['charges'][$i]['chargeStatus'],
                    ];
                    $insert = $this->Charges_model->insert($charges_array);
                }
            }
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editCharges_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editCharges') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Charges_model']);             
            if(isset($this->param['charges']) && $this->param['charges'] != NUll){
                for($i=0;$i<count($this->post('charges'));$i++){               
                    $services_array=[
                        'chargeName' => $this->param['charges'][$i]['chargeName'],
                        'description' => $this->param['charges'][$i]['description'],
                        'chargeStatus' => $this->param['charges'][$i]['chargeStatus'],
                    ];
                    $update = $this->Charges_model->where('chargeId',$this->param['charges'][$i]['chargeId'])->update($services_array);
                }
            }
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    public function allCharges_post() {
    	
    	$this->load->model(['Charges_model']);              	
		if(isset($this->param['chargeId']) && $this->param['chargeId'] != NULL){
            $where = array("chargeId"=>$this->param['chargeId']);
        }else{
            $where = array();
        }
		$charges   =  $this->Charges_model->where($where)->fields('chargeId,chargeName')->get_all();

        if($charges){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => $charges]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteCharges_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteCharges') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 
        	$this->load->model(['Charges_model']);         
        	$chargeId = $this->param['chargeId'];

			$updateOptions = array(
                'where' => array('chargeId' => $chargeId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'charges'
            );
            $deleteCharge = $this->common_model->customUpdate($updateOptions);

	        if($deleteCharge){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteCharge]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}