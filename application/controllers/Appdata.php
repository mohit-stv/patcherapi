<?php defined("BASEPATH") OR exit("No direct script access allowed");
header("Content-Type:application/json");
class Appdata extends CI_Controller {
    private $param =[];
    // private $param2 =[];
    public function __construct()
    { 
        parent::__construct();
    	$this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->helper("file"); 
    }  

    public function appVersion(){
        $this->load->model(['App_version_model']);

        $appVersion = $this->param['app_version'];
        $platform_type = $this->param['platform_type'];
        $user_type = $this->param['user_type'];

        if(isset($this->param['deviceDetails']) && !empty($this->param['deviceDetails'])){
            $this->load->library('Device_detail');
            $deviceDetails = $this->param['deviceDetails'];
            $deviceDetails['user_type'] = $user_type;
            $deviceDetails['os_type'] = $platform_type;
            $deviceDetails['app_version'] = $appVersion;
            $this->device_detail->registerDetail($deviceDetails);
        }
    
        $currentAppDetails = $this->App_version_model->get(['platform_type' => $platform_type, 'app_type' => $user_type]);
        if(!$currentAppDetails){
            echo json_encode(['status' => false,'message'=> 'No Details available']);
            die;
        }

        $currenapp = $currentAppDetails['version'];
        if($appVersion != $currenapp){
            $appUpdate = [
                'version' => $currenapp,
                'description' => $currentAppDetails['description'],
                'show_dailog' => $currentAppDetails['show_dailog'],
                'app_code' => $currentAppDetails['note']
            ];

            echo json_encode(['status' => true,'message'=>'Update Available' ,'reponse' => $appUpdate]);
            die;
        }else{
            echo json_encode(['status' => false,'message'=> 'No New Details available']);
            die;
        }
        
    }
}