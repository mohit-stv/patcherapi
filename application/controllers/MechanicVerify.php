<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MechanicVerify extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
    }
   
     /*----------------------------------------------------------------------------------------
    |  VERIFY NUMBER 
    |----------------------------------------------------------------------------------------- 
    |  It is used for verify mechanic mobile number.
    |-----------------------------------------------------------------------------------------
    */

    public function mechanicVerifyNumber_post()
    {
        $this->load->model('Mechanic_model');
        $this->form_validation->set_data($this->param);
        
        if($this->form_validation->run('MechanicVerifyNumber')){
            $mechanicId = $this->param['mechanic_id'];

            $contact_number = $this->param['contact_number'];

            if(isset($this->param['otp']) && $this->param['otp'] != NULL){
                $otp = $this->param['otp'];
                $mechanic = $this->Mechanic_model->fields(['name','email_id','contact_number','otp','otp_created','mobVerified'])->get($mechanicId);
               
                if($mechanic){ 
                    if($mechanic['contact_number'] == $contact_number){
                        if($mechanic['otp'] == $otp){
                            $this->db->trans_start();
                            $update = $this->Mechanic_model->where('mechanic_id',$mechanicId)->update(['mobVerified' => 101]);
                            $this->db->trans_complete();
                            if($this->db->trans_status()){

                                $response = $this->MechanicResponse($mechanicId);
                                echo json_encode(['status' => true,'message' => 'Successfully Verified','response' => $response]);die;
                            }else{
                                $this->db->trans_rollback();
                                echo json_encode(['status' => false,'message' => 'Failed In Process']);die;
                            }
                        }else{
                            echo json_encode(['status' => false,'message' => 'OTP not match']);die;
                        }
                    }else{
                        $this->response(['status' => false,'message' => 'Please enter your registered Contact number' ]);
                    }
                } else {
                    $this->response(['status'=> false,'message' => 'Something went wrong.']);
                }
            }
            else{

                # for facebook & google user
                # Genrate OTP and send to mobile
                $this->load->library('nexmo');
                $otp =  mt_rand(1001,9999);
                // $otp = 2222;
                
                # Check number exists
                $mechanic = $this->Mechanic_model->get(['contact_number' => $contact_number]);
                if($mechanic){
                    if($mechanic['mechanic_id'] != $mechanicId){
                        $this->response(['status' => false,'message' => 'contact_number already exist']);
                    }
                }
                $update_data = [
                    'otp' => $otp,'otp_created' => date('Y-m-d H:i:s'),
                    'contact_number' => $contact_number,
                    'country_code' => $country_code
                ];

                // $otpSend = $this->nexmo->send_message("Patcher", '44'.$contact_number,["text" => "Patcher OTP: $otp"]);

                $update = $this->Mechanic_model->where('mechanic_id',$mechanicId)->update($update_data);
                if($update){
                    
                $otpMesssage = $otp.' is your one-time verification code, which is valid for the next 10 minutes. Please do not share it with anyone.';

                $this->load->helper('api');
                //$send_otp = SendOtp($to,$message); //print_r($send_otp);die;
                $otpSend = $this->nexmo->send_message("Patcher", '44'.$contact_number,["text" => $otpMesssage]);
                
                $args = [
                    'message' => $otpMesssage,
                    'subject' => 'Patcher: OTP Verifiction',
                    'email_id' => $mechanic['email_id'],
                    'template_name' => 'Sign Up',
                    'vars' => [
                        [
                            'name' => 'message',
                            'content' => $otpMesssage
                        ]
                    ],
                ];
                
                $mailResponse = sendMandrillEmailTemlate($args);

                    $response = $this->MechanicResponse($mechanicId);
                    $this->response(['status' => true,'message' => 'OTP sent to your mobile number','response' => $response]);
                }else{
                    $this->response(['status' => false,'message' => 'Failed In Procss']);
                }
            }
        }else{
            echo json_encode(['status' => false,'message' => $this->form_validation->get_errors_as_array()],200);die;
        }
    }



    private function MechanicResponse($id)
    { 
        $this->load->model('Mechanic_model');
         $some = ['mechanic_id','name','country_code','contact_number','mechanic_address','mechanic_dob','email_id','mechanic_uuid','mobVerified','otp']; 
         $mechanic = $this->Mechanic_model->fields($some)->get($id);
         if($mechanic){
            $this->load->model('Key_model');
            $keys = $this->Key_model->fields('key')->get(['mechanic_id' => $mechanic['mechanic_id']]);// it will be userwise
            $mechanic['X-API-KEY'] = isset($keys['key']) ? $keys['key'] : '';
            $mechanic['api_key'] = "PATCHERAUTHKEY";
            $mechanic['api_value'] = isset($keys['key']) ? $keys['key'] : '';

            // $user['publish_key'] = PUBNUB_PUB_KEY;
            // $user['subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;

            return $mechanic;
         }else{
            return false;
         }
    }

}