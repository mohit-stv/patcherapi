<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issues extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addIssues_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addIssues') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Issues_model']);             
            if(isset($this->param['issues']) && $this->param['issues'] != NUll){
                for($i=0;$i<count($this->post('issues'));$i++){             
                    $issues_array=[
                        'issueName' => $this->param['issues'][$i]['issueName'],
                        'issueStatus' => $this->param['issues'][$i]['issueStatus'] 
                    ];
                    $insert = $this->Issues_model->insert($issues_array);
                }
            }
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editIssues_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editIssues') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Issues_model']);             
            if(isset($this->param['issues']) && $this->param['issues'] != NUll){
                for($i=0;$i<count($this->post('issues'));$i++){               
                    $issues_array=[
                        'issueName' => $this->param['issues'][$i]['issueName'],
                        'issueStatus' => $this->param['issues'][$i]['issueStatus']
                    ];
                    $update = $this->Issues_model->where('issueId',$this->param['issues'][$i]['issueId'])->update($issues_array);
                }
            }
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }   

    public function allIssues_post() {
    	
        $this->load->model(['Issue_model','Constant_calloutfee_model','User_vehicles_model']);
        $where = ['issue_status' => 101];
		if(isset($this->param['issueId']) && $this->param['issueId'] != NULL){
            $where['issue_id'] = $this->param['issueId'];
        }
        
		$issues = $this->Issue_model->where($where)->fields(['issue_id','issue_name'])->get_all();

         // Request_type 1 for Repair
        $callout_fee = $this->Constant_calloutfee_model
        ->fields('value')
        ->where('request_type',1)->get();

        if(isset($this->param['user_id']) && $this->param['user_id'] != ""){
            $user_id = $this->param['user_id'];
        } else {
            $user_id = 0;
        }

        $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id','isDefault'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_id' => $user_id,'isDefault' => 101])->get();

        if($userVehicle){
            $isDefault = 101;
            $vehicleDetails = [
                'user_vehicles_id' => $userVehicle['user_vehicles_id'],
                'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
            ];
        } else {
            $isDefault = 100;
            $vehicleDetails = array();
        }

        if($issues){
          	 $this->response(['status' => true , 'message' => 'Successfully',
               'response' => ['issue'=>$issues,'calloutfee'=>(isset($callout_fee['value']) ? $callout_fee['value'] : '')],'vehicles' => ['isDefault' => $isDefault, 'vehicleDetails' => $vehicleDetails]]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteIssues_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteIssues') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
        	$issueId = $this->param['issueId'];

			$updateOptions = array(
                'where' => array('issueId' => $issueId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'issues'
            );
            $deleteissue = $this->common_model->customUpdate($updateOptions);

	        if($deleteissue){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteissue]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}