<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guidedanswer extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }


    ## Get All Question Answers Based on Request Id
    public function getGuidedAnswer_post() {
    	$this->load->model(['user_question_answers_model','question_model','answer_model','request_image_model']);
    	$request_id=$this->param['request_id'];

    	$getQuestionAnswer = $this->user_question_answers_model->fields(['user_id','question_id','answer_id','questionIsMultiple','comment'])->get_all(['request_id'=>$request_id]);
		
		if(!empty($getQuestionAnswer)) {
			$result = array();
			foreach($getQuestionAnswer as $key=>$getQuestionAnswers) {
				$questionID = $getQuestionAnswers['question_id'];
				$answerID = $getQuestionAnswers['answer_id'];
	
				$question = $this->question_model->fields(['question','question_description'])->where(['question_id'=>$questionID])->get();
				$result[$key]['question'] = $question['question'];
				$result[$key]['answer'] = $answerID;
				$result[$key]['question_description'] = $question['question_description'];
				$result[$key]['questionIsMultiple'] = $getQuestionAnswers['questionIsMultiple'];
				$result[$key]['mcqAnswer'] = $answerID;

	
				//$result[$key]['answer'] = $this->answer_model->fields(['answer_id','answer'])->where(['answer_id'=>$answerID])->get();
			}
			$comment = $getQuestionAnswer[0]['comment'];
			$images = $this->request_image_model->fields('request_image_id,image_url')->where(['request_id'=>$request_id,'status'=>101])
					->get_all();

			if($result) {
				$this->response(['status' => true, 'message'=> 'Get All Question Answer','response' => ['result'=>$result, 'comment'=>(isset($comment) ? $comment : ''), 'images'=>(!empty($images) ? $images : [])]]);die;
			} 
		} else {
			$this->response(['status' => false, 'message' => 'Customer doesn\'t filled any question' ]);die;
		}	
    }
}