<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
	private $param = array();
	public function __construct(){
		parent::__construct();
		$this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['User_model']);

	}

    /*----------------------------------------------------------------------------------------
    |  user RESPONSE 
    |----------------------------------------------------------------------------------------- 
    |  It is used to fetch user details.
    |-----------------------------------------------------------------------------------------
    */
    private function UserResponse($id)
    { 
         $some = ['user_id','name','country_code','contact_number','email_id','signup_type','user_channel','uuid','mobVerified']; 
         $user = $this->User_model->fields($some)->get($id);
         if($user){
            $this->load->model(['Key_model']);
            $keys = $this->Key_model->fields('key')->get(['user_id' => $user['user_id']]);// it will be userwise
            $user['X-API-KEY'] = $keys['key'];
            $user['api_key'] = "PATCHERAUTHKEY";
            $user['api_value'] = $keys['key'];
            
            // $user['publish_key'] = PUBNUB_PUB_KEY;
            // $user['subscribe_key'] = PUBNUB_SUB_KEY;
            if($user['contact_number'] != NULL || !empty($user['contact_number'])){
                $user['contact_number'] = $user['country_code'].'-'.$user['contact_number'];
            }else{
                $user['contact_number'] = $user['contact_number'];
            }
            unset($user['country_code']);
            return $user;
         }else{
            return false;
         }
    }

    # Genrate Random String
    function GenrateRandomString($length=8){
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";//length:36
        $random_string="";
        for($i=0;$i<$length; $i++)
        {
            $random_string .= $chars[ rand(0,strlen($chars)-1)];// dont remove . before =
        }
        return $random_string;
    }

    # Send SMS
    // private function SendOtp($to,$message)
    // {
    //     $this->load->library('nexmo');
    //     //$this->load->model('Restaurant/Country_model');
    //     //$country = $ci->Country_model->fields('country_code')->get(['id' => $country_id]);
    //     // $to = $country['country_code'].$to;
    //     $from = '918871305041';
    //     $response = $this->nexmo->send_message($from, $to, $message);
    //    // $this->nexmo->d_print($response);
    //     return $this->nexmo->get_http_status();
    // }

	/*----------------------------------------------------------------------------------------
    | GET user 
    |----------------------------------------------------------------------------------------- 
    | It is used to fetch user details.
    |-----------------------------------------------------------------------------------------
    */
    public function Getuser_post()
    { 
        $this->form_validation->set_data($this->post());
        if($this->form_validation->run('userId')){
            $user_id = $this->post('user_id');
            $user = $this->Cs_user_model->fields('user_id,user_name,email_id,contact_number,user_image')->get($user_id);
            if($user){
                $this->response(['status'=> true ,'message'=> 'user List','response'=> $user]);  
            }else{
                $this->response(['status'=> false  ,'message'=> 'user Not Found']); 
            }
        }else{
            $this->response(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()],200);
        }
    }

	/*----------------------------------------------------------------------------------------
    |  UPDATE user 
    |----------------------------------------------------------------------------------------- 
    |  It is used for update user details.
    |-----------------------------------------------------------------------------------------
    */
    public function Updateuser_post()
    { 
        $this->form_validation->set_data($this->post());
        if($this->form_validation->run('Updateuser')){
            $data = $this->post();
            $user = $this->Cs_user_model->fields('user_id,email_id,signup_type')->get(['email_id' => $data['email_id']]);
            if($user){
                if($data['user_id'] == $user['user_id']){
                    unset($data['email_id']);
                }else{
                    $this->response(['status' => false,'message' => ' This email_id already used by other user']);
                }
            }else{
                $email = $this->Cs_user_model->fields('email_id')->get($data['user_id']);
                if($email){
                    if(!empty($email['email_id'])){
                        unset($data['email_id']);
                    }
                }
            }
           
            $update = $this->Cs_user_model->update($data,'user_id');
            if($update){
                $this->response(['status'=> true ,'message'=> 'Successfully Updated']);  
            }else{
                $this->response(['status'=> false  ,'message'=> 'Failedd In Process']); 
            }
        }else{
            $this->response(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()],200);
        }
    }

    /*----------------------------------------------------------------------------------------
    |  VERIFY NUMBER 
    |----------------------------------------------------------------------------------------- 
    |  It is used for verify user mobile number.
    |-----------------------------------------------------------------------------------------
    */
    public function VerifyNumber_post()
    {
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('VerifyNumber')){
            $user_id = $this->param['user_id'];
            
            // $signin_type = $this->param['signin_type'];
            $contact_number = $this->param['contact_number'];
            
            if(isset($this->param['otp']) && $this->param['otp'] != ''){
                $otp = $this->param['otp'];
                $user = $this->User_model->fields(['name','email_id','country_code','contact_number','otp','otp_created','mobVerified'])->get($user_id);
                
                if($user){ 
                    if($user['contact_number'] == $contact_number){
                        if($user['otp'] == $otp ){
                            // $exp_time = 600;
                            // $time = strtotime($user['otp_created']);
                            // $curtime = time();
                            // if(($curtime - $time) > $exp_time) {     
                            //    echo json_encode(["status" => false, "message" => "OTP has expired"]);die;
                            // }
                            $this->db->trans_start();
                            // if($user['contact_number'] != $contact_number){
                            //     $this->Cs_user_model->where('user_id',$user_id)->update(['contact_number' => $contact_number]);
                            // }
                            $update = $this->User_model->where('user_id',$user_id)->update(['mobVerified' => 101]);
                            $this->db->trans_complete();
                            if($this->db->trans_status()){
                                $response = $this->userResponse($user_id);
                                echo json_encode(['status' => true,'message' => 'Successfully Verified','response' => $response]);die;
                            }else{
                                $this->db->trans_rollback();
                                echo json_encode(['status' => false,'message' => 'Failed In Process']);die;
                            }
                        }else{
                            echo json_encode(['status' => false,'message' => 'OTP not match']);die;
                        }
                    }else{
                        $this->response(['status' => false,'message' => 'You have entered another number verify this number' ]);
                    }
                }
            }
            else{
                # for facebook & google user
                # Genrate OTP and send to mobile
                $otp =  mt_rand(1001,9999);
                
                // $otp = 2222;
                $this->load->library('nexmo');
                # Check number exists
                $user = $this->User_model->get(['user_id' => $user_id]);

                if($user['contact_number'] == '' || $user['contact_number'] != $contact_number) {
                    
                    $checkContactNumber = $this->User_model->get(['contact_number' => $contact_number]);
                    if(!empty($checkContactNumber)){
                        if($checkContactNumber['user_id'] != $user_id){
                            $this->response(['status' => false,'message' => 'Contact Number already exist']);
                        }
                    } else {
                        
                        $country_code = $user['country_code'];
                        $update_data = [
                            'otp' => $otp,'otp_created' => date('Y-m-d H:i:s'),
                            'contact_number' => $contact_number,
                            'mobVerified' => 100
                        ];

                        $otpSend = $this->nexmo->send_message("Patcher", '44'.$contact_number,["text" => "Patcher OTP: $otp"]);


                        $update = $this->User_model->where('user_id',$user_id)->update($update_data);
                    }
                } else {
                    $country_code = $user['country_code'];
                    $update_data = [
                        'otp' => $otp,'otp_created' => date('Y-m-d H:i:s'),
                        // 'contact_number' => $contact_number,
                        'mobVerified' => 100
                    ];
                    $otpSend = $this->nexmo->send_message("Patcher", '44'.$contact_number,["text" => "Patcher OTP: $otp"]);

                    $update = $this->User_model->where('user_id',$user_id)->update($update_data);
                }
                
                if($update){
                    //if($signin_type == 1){
                        $to = $country_code.$contact_number; 
                        $message = array('text' => 'OTP: '.$otp.'. Valid for 30 minutes');
                        $this->load->helper('api');
                        // $send_otp = SendOtp($to,$message); 
                    //}

                    $response = $this->UserResponse($user_id);
                    $this->response(['status' => true,'message' => 'OTP sent to your mobile number','response' => $response]);
                }else{
                    $this->response(['status' => false,'message' => 'Failed In Procss']);
                }
           // } 
            } 
        }else{
            echo json_encode(['status' => false,'message' => $this->form_validation->get_errors_as_array()],200);die;
        }
    }
    

    private function decodeImage($base64Image){
        $img = $base64Image; 
        $img = explode(',',$img);
        $mime = explode('/',$img[0]);
        $mime = explode(';',$mime[1]);
        $img = str_replace(' ', '+', $img[1]);
        $data = base64_decode($img);
        $fileName = uniqid() . '.'.$mime[0];
        $filePath = 'images/'.uniqid() . '.'.$mime[0];
        $fileUpload = PATH.$filePath;
       // $imageUrl = IMAGE_URL.$fileName;
        $success = file_put_contents($fileUpload, $data);
        if($success){
            //return $imageUrl;
            $data = ['fileName' => $fileName,'fileUpload' => $fileUpload];
            return $data;
        }else{
            return false;
        }
    }

    # Insert Image
    public function InsertImage_post()
    {
        if(isset($this->param['image_url']) && !empty($this->param['image_url'])){ 
            $image_url = $this->param['image_url'];
        }
        elseif(isset($this->param['base64_image']) && !empty($this->param['base64_image'])){
            $image = $this->param['base64_image'];
            $data = $this->decodeImage($image);
            if($data){   
                $cloud_image_info = \Cloudinary\Uploader::upload($data['fileUpload'], ['public_id' => 'restaurant_services/menu/products/'.$data['fileName']]);
                $image_url = $cloud_image_info['secure_url'];
            }
            else{
                $this->response(['status' => false,'message' => 'Failed In Process']);
            }
        }
        else{
            $this->response(['status' => false,'message' => 'image_url or base64_image field is required']);
        }

        if(isset($image_url)){
            $insert_data = [
                'image' => $image_url,
            ];
            $this->load->model('Dummy_image_model');
            $image_id = $this->Dummy_image_model->insert($insert_data); 
            if($image_id){
                $this->response(['status' => true,'message' => 'Successfully Uploaded' ,'response'=> ['image_id' => $image_id]]);
            }
            else{
                $this->response(['status' => false,'message' => 'Failed In Process']);

            }
        }

    }

    /*----------------------------------------------------------------------------------------
    |  UPDATE USER  
    |----------------------------------------------------------------------------------------- 
    |  It is responsable for update user.
    |-----------------------------------------------------------------------------------------
    */

    public function updateUserInformation_post()
    {
        $this->load->model(['User_model','common_model']);
        
        if(isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $userId = $this->param['user_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'UserId is required.']);
            die;
        }

        if(isset($this->param['profile_picture']) && !empty($this->param['profile_picture'])) {

            $img = $this->param['profile_picture']; 
            $strposition = strpos($img,'http://');
           
            if($strposition === false) {
                $imageData = $this->common_model->convertBase64ToImage($img);

                $data = $imageData['data'];
                $mime = $imageData['mime'];

                $image_name ='uploads/user/'.rand(1000,5000) . '.'.$mime;
                file_put_contents($image_name, $data);
                chmod($image_name, 0777);
                $user_array['profile_picture'] = ROOTPATH.$image_name;
            }   
            
        }

        if(isset($this->param['name']) && !empty($this->param['name'])) {
            $user_array['name'] = $this->param['name'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Name is required.']);
            die;
        }

       /* if(isset($this->param['email_id']) && !empty($this->param['email_id'])) {
            $alreadyEmail_id = $this->User_model->fields('email_id')->get(['email_id' => $this->param['email_id'], 'user_id'=>$userId]);

            $email_id = $this->User_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);

            if(!empty($alreadyEmail_id)) {
                 $user_array['email_id'] = $this->param['email_id'];
            }
            elseif($email_id) {
                echo json_encode(['status' => false,'message' => 'Email Id already exits.']);
                die;
            } else {
                $user_array['email_id'] = $this->param['email_id'];
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'EmailID is Required.']);
            die;
        }

        if(isset($this->param['contact_number']) && !empty($this->param['contact_number'])) {
            $number_exist = $this->User_model->fields('contact_number')->get(['contact_number' =>$this->param['contact_number']]);

            $alreadyContact_number = $this->User_model->fields('contact_number')->get(['contact_number' => $this->param['contact_number'], 'user_id'=>$userId]);

            if(!empty($alreadyContact_number)) {
                $user_array['contact_number'] = $this->param['contact_number'];

                if(isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                    $user_array['country_code'] = $this->param['country_code'];
                }
            }elseif($number_exist) {
                echo json_encode(['status' => false,'message' => 'Contact number already exist.']);
                die;
            }else {
                $user_array['contact_number'] = $this->param['contact_number'];

                if(isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                    $user_array['country_code'] = $this->param['country_code'];
                }
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'Contact Number Required.']);
            die;
        }*/


        if(isset($this->param['address']) && !empty($this->param['address'])) {
            $user_array['address'] = $this->param['address'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Address is required.']);
            die;
        }

        $update = $this->User_model->where(['user_id'=>$userId])->update($user_array);
        if($update) {
              echo json_encode(['status' => true, 'message'=> 'Profile updated successfully.','response' => $user_array]);die;
        } else {
              echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    public function getUserData_post() {
        $this->load->model(['User_model','common_model']);

        if(isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $userId = $this->param['user_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'userId is required.']);
            die;
        }

        $userData = $this->User_model
        ->fields('name,email_id,country_code,contact_number,profile_picture,address')
        ->get(['user_id'=>$userId]);

        if($userData) {
            echo json_encode(['status' => true, 'message'=> 'User Profile Here.','response' => $userData]);die;
        } else {
                echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    #Change Contact Numbber
    public function changeContactNumber_post(){
        $this->load->model('Otp_contact_number_model');
        $userId = $this->param['user_id'];
        $contactNumber = $this->param['contact_number'];

        #check existence of contact number
        $isContactExists = $this->User_model->fields('user_id')->get(['contact_number' => $contactNumber,'user_id !=' => $userId]);

        if($isContactExists){
            $this->response(['status' => false,'message' => 'Contact Number already exists.']);
        }

        $userDetails = $this->User_model->fields('user_id,email_id')->get(['user_id' => $userId]);

        // $OTP = 2222;
        $OTP = mt_rand(1000,9999);
        
        $insertData = [
            'user_id' => $userId,
            'contact_number' => $contactNumber,
            'user_type' => 1
        ];

        $sendOTP = $this->Otp_contact_number_model->fields('id')->get($insertData);
        if($sendOTP){
            $updateOTP = $this->Otp_contact_number_model->where(['id'=>$sendOTP['id']])->update(['OTP' => $OTP]);
            if($updateOTP){
                $sendOTP = $sendOTP['id'];
            } else {
                $sendOTP = false;
            }
        } else {
            $insertData['otp'] = $OTP;
            $sendOTP = $this->Otp_contact_number_model->insert($insertData);
        }

        if($sendOTP){
            $this->load->library('Nexmo');
            $otpMesssage = $OTP.' is your one-time verification code, which is valid for the next 10 minutes. Please do not share it with anyone.';
            $this->load->helper('api');

            $otpSend = $this->nexmo->send_message("Patcher", '44'.$contactNumber,["text" => $otpMesssage]);
            
            $args = [
                'message' => $otpMesssage,
                'subject' => 'Patcher: OTP Verifiction',
                'email_id' => $userDetails['email_id'],
                'template_name' => 'Sign Up',
                'vars' => [
                    [
                        'name' => 'message',
                        'content' => $otpMesssage
                    ]
                ],
            ];
            
            $mailResponse = sendMandrillEmailTemlate($args);
            
            $this->response(['status' => true,'message' => 'An OTP send to your number/Email. Please verify it.','response' => ['change_id' => $sendOTP]]);
        } else {
            $this->response(['status' => false,'message' => 'Error in process. Please try again after some time.']);
        }
    }

    #Change Contact Numbber
    public function verifyContactNumber_post(){
        $this->load->model('Otp_contact_number_model');
        $changeId = $this->param['change_id'];
        $OTP = $this->param['otp'];

        $this->db->trans_start();
        $detailOTP = $this->Otp_contact_number_model->get(['id' => $changeId]);

        if($detailOTP){
            if($OTP == $detailOTP['otp']){

                #Update Mechanic Contact
                $updateContact = $this->User_model->where(['user_id' => $detailOTP['user_id']])->update(['contact_number' => $detailOTP['contact_number']]);

                #Delete OTP entry from table
                $this->Otp_contact_number_model->where(['id' => $changeId])->force_delete();

                $this->db->trans_complete();
                if($this->db->trans_status()){
                    $this->response(['status' => true,'message' => 'OTP verified.']);
                } else {
                    $this->response(['status' => false,'message' => 'Error in process. Please try again after some time.']);
                }
            } else {
                $this->response(['status' => false,'message' => 'OTP does not match.']);
            }
        } else {
            $this->response(['status' => false,'message' => 'Invalid OTP details.']);
        }
    }
}