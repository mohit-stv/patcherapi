<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mechanic extends MY_Controller
{

    public $redis;
    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model', 'Mechanic_model', 'Mechanic_registrations_model']);
        $this->load->helper('common_helper');
        ini_set('upload_max_filesize', '2MB');
        ini_set('max_execution_time', 300);
        $this->redis = createConnectionToRedis();
    }

    public function addMechanics_post()
    {
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run('mechanicsRegistration') == false) {
            $message = $this->form_validation->error_array();
            $response = array('status' => false, 'message' => $message);
            $this->response($response, 400);
        } else {
            $mechanics_array = [
                'name' => $this->param['name'],
                'email_id' => $this->param['emailId'],
                'password' => $this->param['password'],
                'country_code' => $this->param['countryCode'],
                'contact_number' => $this->param['contactNumber'],
                'mechanic_dob' => $this->param['mechanicDob'],
                'mechanic_address' => $this->param['mechanicAddress'],
                'mechanic_services' => $this->param['mechanicServices'],
                'mechanic_experience' => $this->param['mechanicExperience'],
                'mechanic_itinerary' => $this->param['mechanicItinerary'],
            ];

            if(isset($this->param['request_commission']) &&  !empty($this->param['request_commission'])){
                $mechanics_array['request_commission'] = $this->param['request_commission'];
            }

            $insert = $this->Mechanics_model->insert($mechanics_array);
            if ($insert) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $insert]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    ## For Add And Update Mechanics Health
    public function addUpdateMechanicHealth_post()
    {
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_health']) && !empty($this->param['mechanic_health'])) {
            $this->load->model('Mechanic_health_model');
            $mechanicsHealthDocuments_array = [
                'health_id' => trim($this->param['mechanic_health']),
                'mechanic_id' => $id,
            ];

            $getVal = $this->Mechanic_health_model->get(['mechanic_id' => $id]);
            if (!empty($getVal)) {
                $updateData = ['health_id' => trim($this->param['mechanic_health'])];
                $update = $this->Mechanic_health_model->where('mechanic_id', $id)->update($updateData);
                if ($update) {

                    $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['health_status' => 101, 'comment' => 'Health Updated by Mechanic']);

                    //$this->redis->zrem('mechanics',$id);
                    $this->redis->set($id, 'PROFILE_PENDING');

                    $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateData]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
                }
            } else {
                $insert = $this->Mechanic_health_model->insert($mechanicsHealthDocuments_array);

                if ($insert) {

                    $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['health_status' => 101, 'comment' => 'Health Updated by Mechanic']);

                    //$this->redis->zrem('mechanics',$id);
                    $this->redis->set($id, 'PROFILE_PENDING');

                    $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
                }
            }
        }
    }

    ## For Add And Update Mechanics Itinerary
    public function addUpdateMechanicItineraryOld_post()
    {
        $this->load->model(['Mechanic_itineraries_model', 'Mechanic_service_model', 'Service_model', 'Tool_itinerary_model']);

        $id = $this->param['mechanic_id'];
        $toolId = $this->param['tool_itinerary_id'];

        if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId = $this->param['service_id'];

            $updatedata = ['tool_itinerary_id' => trim($toolId)];

            $update = $this->Mechanic_service_model->where(['mechanic_id' => $id, 'services_id' => $serviceId])->update($updatedata);
        } else {
            if (isset($this->param['tool_itinerary_id'])) {
                $updatedata = ['tool_itinerary_id' => trim($toolId)];

                $explodeToolId = explode(",", $toolId);

                $getMechanicService = $this->Mechanic_service_model->get(['mechanic_id' => $id]);

                $getService = $this->Tool_itinerary_model->fields(['tool_itinerary_id', 'service_id'])->where('tool_itinerary_id', $explodeToolId)->get_all();

                if (!empty($getMechanicService)) {
                    $mechanicServiceId = $getMechanicService['mechanic_services_id'];
                    foreach ($getService as $getServices) {
                        $serviceId = $getServices['service_id'];
                        $updateData[$serviceId] = [
                            'tool_itinerary_id' => $toolId,
                            'services_id' => $getServices['service_id']];
                    }
                    $updateVal = array_shift($updateData);

                    $update = $this->Mechanic_service_model->where(['mechanic_id' => $id])->update($updateVal);
                } else {
                    foreach ($getService as $getServices) {
                        $serviceId = $getServices['service_id'];
                        $serviceToolId = $getServices['tool_itinerary_id'];
                        $insertData[$serviceId] = [
                            'services_id' => $serviceToolId,
                            'mechanic_id' => $id,
                            'tool_itinerary_id' => $toolId,
                        ];
                    }
                    $insert = $this->Mechanic_service_model->insert($insertData);
                }
            }
        }

        if (!empty($insert)) {
            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
            //$this->redis->zrem('mechanics',$id);
            $this->redis->set($id, 'PROFILE_PENDING');
            $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
        } elseif ($update) {
            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
            //$this->redis->zrem('mechanics',$id);
            $this->redis->set($id, 'PROFILE_PENDING');
            $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateVal]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    ## For Add And Update Mechanics Itinerary BY MOHIT 09-01-2018 14:55:00
    public function addUpdateMechanicItinerary_post()
    {
        $this->load->model(['Mechanic_itineraries_model', 'Mechanic_service_model', 'Service_model', 'Tool_itinerary_model']);

        $id = $this->param['mechanic_id'];
        $toolId = $this->param['tool_itinerary_id'];
        if (empty($toolId)) {
            $this->response(['status' => false, 'message' => 'Tool Itinerary missing.']);
        }

        if (is_array($toolId)) {
            $toolItineraries = $toolId;
            $tools = [];
            foreach ($toolItineraries as $toolItinerary) {
                if ($toolItinerary['available']) {
                    $serviceId = $toolItinerary['service_id'];
                    $tools[$serviceId]['tool_itinerary_id'][] = $toolItinerary['tool_itinerary_id'];
                    $tools[$serviceId]['mechanic_services_id'] = $toolItinerary['mechanic_services_id'];
                }
            }

            $updateTools = [];
            if (count($tools) > 0) {
                foreach ($tools as $key => $tool) {
                    if (count($tool) > 0) {
                        $updateTools[] = [
                            // 'service_id' => $key,
                            'mechanic_services_id' => $tool['mechanic_services_id'],
                            'tool_itinerary_id' => implode(",", $tool['tool_itinerary_id']),
                        ];
                    }
                }
            }

            $update = $this->Mechanic_service_model->update($updateTools, 'mechanic_services_id');

            if ($update) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');

                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateTools]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong. Please try again later']);die;
            }

        } else {
            ##Used for old application version till above date##
            #Remove else section once App update in App Stores and shared with client
            if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
                $serviceId = $this->param['service_id'];

                $updatedata = ['tool_itinerary_id' => trim($toolId)];

                $update = $this->Mechanic_service_model->where(['mechanic_id' => $id, 'services_id' => $serviceId])->update($updatedata);
            } else {
                if (isset($this->param['tool_itinerary_id'])) {
                    $updatedata = ['tool_itinerary_id' => trim($toolId)];

                    $explodeToolId = explode(",", $toolId);

                    $getMechanicService = $this->Mechanic_service_model->get(['mechanic_id' => $id]);

                    $getService = $this->Tool_itinerary_model->fields(['tool_itinerary_id', 'service_id'])->where('tool_itinerary_id', $explodeToolId)->get_all();

                    if (!empty($getMechanicService)) {
                        $mechanicServiceId = $getMechanicService['mechanic_services_id'];
                        foreach ($getService as $getServices) {
                            $serviceId = $getServices['service_id'];
                            $updateData[$serviceId] = [
                                'tool_itinerary_id' => $toolId,
                                'services_id' => $getServices['service_id']];
                        }
                        $updateVal = array_shift($updateData);

                        $update = $this->Mechanic_service_model->where(['mechanic_id' => $id])->update($updateVal);
                    } else {
                        foreach ($getService as $getServices) {
                            $serviceId = $getServices['service_id'];
                            $serviceToolId = $getServices['tool_itinerary_id'];
                            $insertData[$serviceId] = [
                                'services_id' => $serviceToolId,
                                'mechanic_id' => $id,
                                'tool_itinerary_id' => $toolId,
                            ];
                        }
                        $insert = $this->Mechanic_service_model->insert($insertData);
                    }
                }
            }

            if (!empty($insert)) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
            } elseif ($update) {

                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateVal]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
            }
        }
    }

    ## For Add And Update Mechanics Service
    public function addUpdateMechanicsService_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model('Mechanic_service_model');
        if (isset($this->param['mechanic_services']) && !empty($this->param['mechanic_services'])) {
            $services = $this->param['mechanic_services'];
            $mechanicsInsertData = array();
            $update = array();
            $updateData = array();
            $insert = array();

            foreach ($services as $key => $service) {
                $serviceId = $service['service_id'];

                $mechanicsService_array = [
                    'mechanic_id' => $id,
                    'services_id' => $serviceId,
                    'mechanic_services_status' => $service['mechanic_services_status'],
                ];

                $getVal = $this->Mechanic_service_model->get(['mechanic_id' => $id, 'services_id' => $serviceId]);
                if (!empty($getVal)) {

                    $updateWhere = array(
                        'services_id' => $serviceId,
                        'mechanic_id' => $id,
                    );

                    $singleList = [];

                    $updateData[] = $singleList = [
                        'services_id' => $serviceId,
                        'mechanic_services_status' => $service['mechanic_services_status'],
                    ];

                    if ($service['mechanic_services_status'] == 100) {
                        $singleList['tool_itinerary_id'] = '';
                    }

                    $update[] = $this->Mechanic_service_model->where($updateWhere)->update($singleList);
                } else {
                    $insert[] = $this->Mechanic_service_model->insert($mechanicsService_array);

                    $mechanicsInsertData[] = ['mechanic_id' => $id,
                        'services_id' => $serviceId,
                        'mechanic_services_status' => $service['mechanic_services_status']];
                }
            }
            if ($insert) {

                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['service_status' => 101, 'comment' => 'Services Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $mechanicsInsertData]);
            } elseif ($update) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['service_status' => 101, 'comment' => 'Services Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateData]);
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong.']);
            }
        }
    }

    ## For Add And Update Mechanics Documentation
    public function addUpdateMechanicsDocumentation_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_documentation']) && !empty($this->param['mechanic_documentation'])) {

            $this->load->model('Mechanic_document_model');
            $documentation = $this->param['mechanic_documentation'];

            $update = array();
            $insert = array();
            foreach ($documentation as $key => $documentations) {

                $img = $documentations['mechanicDocumentationFile'];

                if (!filter_var($img, FILTER_VALIDATE_URL)) {
                    $img = explode(',', $img);
                    $mime = explode('/', $img[0]);
                    $mime = explode(';', $mime[1]);
                    $img = str_replace(' ', '+', $img[1]);
                    $data = base64_decode($img);
                    $fileName = 'uploads/documentation/' . rand(1000, 5000) . '.' . $mime[0];

                    $fileUpload = $fileName;
                    file_put_contents($fileUpload, $data);
                    chmod($fileUpload, 0777);

                    $imgUp = ROOTPATH . $fileName;
                } else {
                    $imgUp = $img;
                }

                $mechanicsDocumentDocuments_array = [
                    'document_id' => $documentations['document_id'],
                    'mechanic_id' => $id,
                    'document_url' => $imgUp,
                    'mechanic_document_status' => 101,
                ];

                $getVal = $this->Mechanic_document_model->get(['mechanic_id' => $id, 'document_id' => $documentations['document_id']]);

                if (!empty($getVal)) {
                    $updateWhere = array('document_id' => $documentations['document_id'],
                        'mechanic_id' => $id,
                    );
                    $update[] = $this->Mechanic_document_model->where($updateWhere)->update(
                        ['document_url' => $imgUp, 'mechanic_document_status' => 101]);

                } else {
                    $insert[] = $this->Mechanic_document_model->insert($mechanicsDocumentDocuments_array);
                }
            }

            if (!empty($insert)) {

                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['documentation_status' => 101, 'comment' => 'Document Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
            } elseif (!empty($update)) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['documentation_status' => 101, 'comment' => 'Document Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $update]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
            }
        }
    }

    ## For Add ANd Update Mechanics Experience
    public function addUpdateMechanicsExperience_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model', 'common_model']);
        $experience = $this->param['experience_id'];

        $updateData = array();
        $insert = array();
        $update = array();

        $experienceData = [
            'experience_id' => $experience,
            'mechanic_id' => $id,
            'mechanic_experience_status' => 101,
        ];

        $getExperience = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_url' => '', 'isPass' => '0']);
        if (!empty($getExperience)) {
            $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_url' => '', 'isPass' => '0'])->update(['experience_id' => $experience]);
            $updateData['range'] = ['experience_id' => $experience];
        } else {
            $insert[] = $this->Mechanic_experience_model->insert($experienceData);
        }

        if (isset($this->param['isPass']) && !empty($this->param['isPass'])) {

            $isPass = $this->param['isPass'];
            foreach ($isPass as $isPasses) {
                if (isset($isPasses['mechanicExperienceFile']) && $isPasses['mechanicExperienceFile'] != '') {

                    $img = $isPasses['mechanicExperienceFile'];
                    $imageData = $this->common_model->convertBase64ToImage($img);

                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $image_name = 'uploads/experience/' . rand(1000, 5000) . '.' . $mime;
                    file_put_contents($image_name, $data);
                    chmod($image_name, 0777);
                    $imagePath = ROOTPATH . $image_name;

                    $isPassData = ['experience_id' => $isPasses['experience_id'],
                        'mechanic_id' => $id,
                        'isPass' => $isPasses['mechanic_experience_status'],
                        'experience_url' => $imagePath,
                        'mechanic_experience_status' => 101];

                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']]);

                    if (!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']])->update(['experience_url' => ROOTPATH . $image_name, 'isPass' => $isPasses['mechanic_experience_status']]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id'], 'experience_url' => ROOTPATH . $image_name, 'isPass' => $isPasses['mechanic_experience_status']];

                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                } else {
                    $isPassData = ['experience_id' => $isPasses['experience_id'],
                        'mechanic_id' => $id,
                        'isPass' => $isPasses['mechanic_experience_status'],
                        'mechanic_experience_status' => 101];

                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']]);

                    if (!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']])->update(['isPass' => $isPasses['mechanic_experience_status'], 'experience_url' => '']);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id'], 'experience_url' => '', 'mechanic_experience_status' => $getIsPass['isPass']];
                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                }
            }
        }

        if (isset($this->param['refrences']) && !empty($this->param['refrences'])) {
            $refrence = $this->param['refrences'];
            foreach ($refrence as $refrences) {

                if (isset($refrences['mechanicExperienceFile']) && $refrences['mechanicExperienceFile'] != '') {

                    $img = $refrences['mechanicExperienceFile'];
                    $imageData = $this->common_model->convertBase64ToImage($img);

                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $refrenceimage_name = 'uploads/experience/' . rand(1000, 5000) . '.' . $mime;
                    file_put_contents($refrenceimage_name, $data);
                    chmod($refrenceimage_name, 0777);
                    $root_Refrenceimage_name = ROOTPATH . $refrenceimage_name;
                } else {
                    $root_Refrenceimage_name = '';
                }

                $refrenceData = ['experience_id' => $refrences['experience_id'],
                    'mechanic_id' => $id,
                    'experience_url' => $root_Refrenceimage_name,
                    'mechanic_experience_status' => 101];

                $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'isPass' => 0, 'experience_id' => $refrences['experience_id']]);

                if (!empty($getIsPass)) {
                    if ($root_Refrenceimage_name != '') {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'isPass' => '0'])->update(['experience_url' => $root_Refrenceimage_name, 'mechanic_experience_status' => 101]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'experience_url' => $root_Refrenceimage_name];
                    } else {
                        $update = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'isPass' => '0'])->update(['experience_url' => $root_Refrenceimage_name]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], ['experience_url' => $root_Refrenceimage_name]];
                    }

                } else {
                    $insert[] = $this->Mechanic_experience_model->insert($refrenceData);
                }
            }
        }

        if (!empty($insert)) {
            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['experience_status' => 101, 'comment' => 'Experience Updated by Mechanic']);
            //$this->redis->zrem('mechanics',$id);
            $this->redis->set($id, 'PROFILE_PENDING');
            $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
        } elseif (!empty($update)) {
            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['experience_status' => 101, 'comment' => 'Experience Updated by Mechanic']);
            //$this->redis->zrem('mechanics',$id);
            $this->redis->set($id, 'PROFILE_PENDING');
            $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $updateData]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    ## For Add and Update Mechanics Qualification
    public function addUpdateMechanicsQualification_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_qualification']) && !empty($this->param['mechanic_qualification'])) {
            $this->load->model(['Mechanic_qualification_model', 'common_model']);
            $qualification = $this->param['mechanic_qualification'];

            $update = array();
            $insert = array();
            $updateResponse = [];

            foreach ($qualification as $key => $qualifications) {

                /* $image_name = "uploads/experience/".rand(1000,5000).time().".png";
                $image = $this->common_model->getImageBase64Code($qualifications['mechanicQualificationFile']);
                file_put_contents($image_name,$image);
                chmod($image_name, 0777);*/

                if (isset($qualifications['mechanicQualificationFile']) && $qualifications['mechanicQualificationFile'] != '') {
                    $img = $qualifications['mechanicQualificationFile'];
                    $imageData = $this->common_model->convertBase64ToImage($img);

                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $image_name = 'uploads/qualification/' . rand(1000, 5000) . '.' . $mime;
                    file_put_contents($image_name, $data);
                    chmod($image_name, 0777);
                    $qualificationStatus = 101;
                } else {
                    $qualificationStatus = 100;
                }

                $mechanicsQualification = [
                    'qualification_id' => $qualifications['qualification_id'],
                    'mechanic_id' => $id,
                    'qualification_url' => (isset($image_name) ? ROOTPATH . $image_name : ""),
                    'mechanic_qualification_status' => $qualificationStatus,
                ];

                $getVal = $this->Mechanic_qualification_model->get(['mechanic_id' => $id, 'qualification_id' => $qualifications['qualification_id']]);

                if (!empty($getVal)) {
                    $updateWhere = array('qualification_id' => $qualifications['qualification_id'], 'mechanic_id' => $id);
                    $updateResponse[] = ['qualification_url' => (isset($image_name) ? ROOTPATH . $image_name : ""), 'mechanic_qualification_status' => $qualificationStatus];

                    $updateData = ['qualification_url' => (isset($image_name) ? ROOTPATH . $image_name : ""), 'mechanic_qualification_status' => $qualificationStatus];

                    $update[] = $this->Mechanic_qualification_model->where($updateWhere)->update($updateData);

                } else {
                    $insert[] = $this->Mechanic_qualification_model->insert($mechanicsQualification);
                }
            }

            if (!empty($insert)) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['qualification_status' => 101, 'comment' => 'Qualification Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
            } elseif (!empty($update)) {
                $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['qualification_status' => 101, 'comment' => 'Qualification Updated by Mechanic']);
                //$this->redis->zrem('mechanics',$id);
                $this->redis->set($id, 'PROFILE_PENDING');
                $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $update]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
            }
        }
    }

    ## Get Mechanics Services Based On Selection
    public function getMechanicServices_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_service_model', 'Service_model']);

        $servicesList = $this->Service_model->where('service_status', 101)->fields(['service_id', 'parent_id', 'service_name', 'service_status'])->get_all();

        $allServices = array();
        foreach ($servicesList as $key => $servicesLists) {
            $serviceId = $servicesLists['service_id'];
            $allServices[$key]['service_id'] = $servicesLists['service_id'];
            $allServices[$key]['service_name'] = $servicesLists['service_name'];
            $allServices[$key]['parent_id'] = $servicesLists['parent_id'];
            $allServices[$key]['service_status'] = $servicesLists['service_status'];

            $serviceData = $this->Mechanic_service_model->fields(['mechanic_services_status'])->get(['mechanic_id' => $id, 'services_id' => $serviceId]);

            if (!empty($serviceData)) {
                $serviceVal = $serviceData;
                $allServices[$key]['mechanic_services_status'] = $serviceVal['mechanic_services_status'];
            } else {
                $allServices[$key]['mechanic_services_status'] = '100';
            }
        }

        // $AllServices = $this->Mechanic_service_model->fields(['services_id','mechanic_services_status'])->with_service('fields:service_name')->get_all(['mechanic_id' => $id]);
        if (!empty($allServices)) {
            $this->response(['status' => true, 'message' => 'Mechanics Services Details Here', 'response' => $allServices]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    ## Get The Mechancis Documents
    public function getMechanicsDocuments_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_document_model', 'Document_model']);

        $documentList = $this->Document_model->where('document_status', 101)->fields(['document_id', 'document_name', 'document_description', 'isOptional', 'document_status'])->get_all();

        $AllDocuments = array();
        foreach ($documentList as $key => $documentLists) {
            $document_id = $documentLists['document_id'];
            $allDocuments[$key]['document_id'] = $documentLists['document_id'];
            $allDocuments[$key]['document_name'] = $documentLists['document_name'];
            $allDocuments[$key]['document_description'] = $documentLists['document_description'];
            $allDocuments[$key]['document_status'] = $documentLists['document_status'];

            $documentData = $this->Mechanic_document_model->fields(['document_id', 'document_url', 'mechanic_document_status'])->get(['mechanic_id' => $id, 'document_id' => $document_id]);

            if (!empty($documentData)) {
                $documentVal = $documentData;

                $allDocuments[$key]['document_url'] = ($documentVal['document_url'] != '') ? $documentVal['document_url'] : '';

                $allDocuments[$key]['document_verified'] = ($documentVal['mechanic_document_status'] != '') ? $documentVal['mechanic_document_status'] : '100';
            } else {
                $allDocuments[$key]['document_verified'] = '100'; //Document status incomplete
            }
        }
        if (!empty($allDocuments)) {
            $this->response(['status' => true, 'message' => 'Mechanics Documents Details Here', 'response' => $allDocuments]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    ## Get The Mechancis Experience
    public function getMechanicsExperience_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model', 'Experience_model']);

        $experienceList = $this->Experience_model->where('experience_status', 101)->fields(['experience_id', 'experience_name', 'isOptional', 'isUpload', 'experience_status'])->get_all();

        $allExperience = array();
        $allExperience1 = array();
        $allExperience['isPass'] = [];

        foreach ($experienceList as $key => $experienceLists) {
            $experience_id = $experienceLists['experience_id'];
            $experience_name = $experienceLists['experience_name'];

            $experienceData = $this->Mechanic_experience_model->fields(['experience_url', 'experience_id', 'mechanic_experience_status', 'isPass'])->get(['mechanic_id' => $id, 'experience_id' => $experience_id]);

            if ($experienceData['mechanic_experience_status'] == null) {
                $mechanicExperienceStatus = '100';
            } else {
                $mechanicExperienceStatus = $experienceData['mechanic_experience_status'];
            }

            if ($experienceData['experience_url'] == null) {
                $experienceUrl = '';
            } else {
                $experienceUrl = $experienceData['experience_url'];
            }

            if ($experienceData['isPass'] == null) {
                $experienceIsPass = '100';
            } else {
                $experienceIsPass = $experienceData['isPass'];
            }

            if ($experienceLists['isOptional'] == '101') {
                $allExperience['isPass'][] = [
                    'experience_id' => $experienceLists['experience_id'],
                    'experience_name' => $experienceLists['experience_name'],
                    'experience_status' => $experienceLists['experience_status'],
                    'experience_url' => $experienceUrl,
                    'mechanic_experience_status' => $mechanicExperienceStatus,
                    'isRefrence' => "101",
                ];
            } elseif ($experienceLists['isUpload'] == '100') {
                $allExperience['isPass'][] = [
                    'experience_id' => $experienceLists['experience_id'],
                    'experience_name' => $experienceLists['experience_name'],
                    'experience_status' => $experienceLists['experience_status'],
                    'isRefrence' => "100", //Relace isPass To isRefrence
                    'experience_url' => $experienceUrl,
                    'mechanic_experience_status' => $experienceIsPass,
                ];
            } else {
                $allExperience['range'][] = [
                    'experience_id' => $experienceLists['experience_id'],
                    'experience_name' => $experienceLists['experience_name'],
                    'experience_status' => $experienceLists['experience_status'],
                    'mechanic_experience_status' => $mechanicExperienceStatus,
                ];
            }
        }

        if (!empty($allExperience)) {
            $this->response(['status' => true, 'message' => 'Mechanics Experience Details Here', 'response' => $allExperience]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    ## Get The Mechancis Qualification
    public function getMechanicsQualification_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_qualification_model', 'Qualification_model']);
        $qualificationList = $this->Qualification_model->where('qualification_status', 101)->fields(['qualification_id', 'qualification_name', 'qualification_status', 'isRefrence'])->get_all();

        $allQualification = array();
        foreach ($qualificationList as $key => $qualificationLists) {
            $qualification_id = $qualificationLists['qualification_id'];
            $qualification_name = $qualificationLists['qualification_name'];

            $qualificationData = $this->Mechanic_qualification_model->fields(['qualification_url', 'mechanic_qualification_status'])->get(['mechanic_id' => $id, 'qualification_id' => $qualification_id]);

            if ($qualificationData['mechanic_qualification_status'] == null) {
                $qualificationVerified = "100";
            } else {
                $qualificationVerified = $qualificationData['mechanic_qualification_status'];
            }

            if ($qualificationData['qualification_url'] == null) {
                $qualificationUrl = "";
            } else {
                $qualificationUrl = $qualificationData['qualification_url'];
            }

            if ($qualificationLists['isRefrence'] == '101') {
                $allQualification['refrence'][] = [
                    'qualification_id' => $qualificationLists['qualification_id'],
                    'qualification_name' => $qualificationLists['qualification_name'],
                    'qualification_status' => $qualificationLists['qualification_status'],
                    'qualification_url' => $qualificationUrl,
                    'mechanic_qualification_status' => $qualificationVerified,
                ];
            } else {
                $allQualification['qualification'][$key] = [
                    'qualification_id' => $qualificationLists['qualification_id'],
                    'qualification_name' => $qualificationLists['qualification_name'],
                    'qualification_status' => $qualificationLists['qualification_status'],
                    'qualification_url' => $qualificationUrl,
                    'mechanic_qualification_status' => $qualificationVerified,
                ];
            }
        }
        if (!empty($allQualification)) {
            $this->response(['status' => true, 'message' => 'Mechanics Qualification Details Here', 'response' => $allQualification]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    ## Get The Mechanics Itinerary
    public function getMechanicsItineraryOld_post()
    {
        $id = $this->param['mechanic_id'];

        $this->load->model(['Mechanic_itineraries_model', 'Tool_itinerary_model', 'Mechanic_service_model']);

        if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId = $this->param['service_id'];
            $getTool = $this->Tool_itinerary_model->where(['tool_itinerary_status' => 101, 'service_id' => $serviceId])->fields(['tool_itinerary_id', 'tool_itinerary_name'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id, 'services_id' => $serviceId]);

            $arrayval = explode(",", $getMechanicItineraries[0]['tool_itinerary_id']);

            $toolDetails = array();
            foreach ($getTool as $key => $getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $toolDetails[$key]['tool_itinerary_id'] = $getTools['tool_itinerary_id'];

                $toolDetails[$key]['tool_itinerary_name'] = $getTools['tool_itinerary_name'];
                if (in_array($toolId, $arrayval)) {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '101';
                } else {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '100';
                }
            }
        } else {

            $getTool = $this->Tool_itinerary_model->with_service('fields:service_name')->fields(['tool_itinerary_id', 'tool_itinerary_name', 'tool_itinerary_status', 'service_id'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id]);

            if (!empty($getMechanicItineraries)) {
                $arrayval = explode(",", $getMechanicItineraries[0]['tool_itinerary_id']);
            }

            foreach ($getTool as $key => $getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $serviceName = $getTools['service']['service_name'];

                if (!empty($arrayval)) {
                    if (in_array($toolId, $arrayval)) {
                        $toolStatus = '101';
                    } else {
                        $toolStatus = '100';
                    }
                } else {
                    $toolStatus = '100';
                }

                $toolDetails[] = [
                    'tool_itinerary_id' => $getTools['tool_itinerary_id'],
                    'tool_itinerary_name' => $getTools['tool_itinerary_name'],
                    'tool_itinerary_status' => $getTools['tool_itinerary_status'],
                    'mechanic_itinerary_status' => $toolStatus,
                ];
            }
        }
        if (!empty($toolDetails)) {
            $this->response(['status' => true, 'message' => 'Mechanics Itinerary Details Here', 'response' => $toolDetails]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function getMechanicsItinerary_post()
    {

        $this->load->model(['Mechanic_itineraries_model', 'Tool_itinerary_model', 'Mechanic_service_model']);

        if (!isset($this->param['mechanic_id']) || empty($this->param['mechanic_id'])) {
            $this->response(['status' => false, 'message' => 'Mechanic Id is mandatory field.']);
        }

        $mechanic_id = $this->param['mechanic_id'];
        $serviceWhere = "";

        if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceWhere = ['service_id' => $this->param['service_id']];
        }

        $mechSerivces = $this->Mechanic_service_model->fields('services_id,tool_itinerary_id,mechanic_services_id')->get_all(['mechanic_id' => $mechanic_id, 'mechanic_services_status' => 101, $serviceWhere]);
        if (!$mechSerivces) {
            $this->response(['status' => false, 'message' => 'Mechanic Services not found. Please select atleast 1 service.', 'response' => []]);
        }

        $responseItnineray = [];
        foreach ($mechSerivces as $mechSerivce) {
            $toolIds = $mechSerivce['tool_itinerary_id'];
            if (!empty($toolIds)) {
                $toolId = explode(',', $toolIds);
            } else {
                $toolId = [];
            }

            $getTools = $this->Tool_itinerary_model->where(['tool_itinerary_status' => 101, 'service_id' => $mechSerivce['services_id']])->fields(['tool_itinerary_id', 'tool_itinerary_status', 'tool_itinerary_name'])->get_all();

            if ($getTools) {
                foreach ($getTools as $key => $getTool) {
                    if (in_array($getTool['tool_itinerary_id'], $toolId)) {
                        $getTool['mechanic_itinerary_status'] = 101;
                    } else {
                        $getTool['mechanic_itinerary_status'] = 100;
                    }
                    $getTool['services_id'] = $mechSerivce['services_id'];
                    $getTool['service_id'] = $mechSerivce['services_id']; //Will be used in update tool api
                    $getTool['mechanic_services_id'] = $mechSerivce['mechanic_services_id'];
                    $responseItnineray[] = $getTool;
                }
            }
        }

        $this->response(['status' => true, 'message' => 'Mechanic Itineray Details.', 'response' => $responseItnineray]);
    }

    ## Get The Mechanics Health
    public function getMechanicsHealth_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_health_model', 'Health_safety_check_model']);
        $getHealth = $this->Health_safety_check_model->fields(['health_safety_check_id', 'health_safety_name', 'health_safety_description', 'health_safety_check_status'])->get_All();

        $getMechanichealth = $this->Mechanic_health_model->fields(['health_id', 'mechanic_health_status'])->get_all(['mechanic_id' => $id]);
        $arrayval = explode(",", $getMechanichealth[0]['health_id']);
        $healthDetails = array();
        foreach ($getHealth as $key => $getHealths) {
            $healthId = $getHealths['health_safety_check_id'];
            $healthDetails[$key] = [
                'health_safety_check_id' => $getHealths['health_safety_check_id'],
                'health_safety_name' => $getHealths['health_safety_name'],
                'health_safety_check_status' => $getHealths['health_safety_check_status'],
            ];

            if (in_array($healthId, $arrayval)) {
                $healthDetails[$key]['mechanic_health_status'] = '101';
            } else {
                $healthDetails[$key]['mechanic_health_status'] = '100';
            }
        }
        if (!empty($healthDetails)) {
            $this->response(['status' => true, 'message' => 'Mechanics Health Details Here', 'response' => $healthDetails]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    // Manage The Mechanic Registartion Status
    public function getmechanicRegistrationStatus_post()
    {
        $this->load->model('Mechanic_registrations_model');
        $id = $this->param['mechanic_id'];
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        if (!empty($getStatus)) {
            $response = [
                'mechanic_id' => $id,
                'application_status' => $applicationStatus,
                'isAppVerified' => $isAppVerified,
            ];
            $this->response(['status' => true, 'message' => 'Get Mechanic Status', 'response' => $response]);
        } else {
            $defaultStatus = [
                'mechanic_id' => $id,
                'application_status' => $applicationStatus,
                'isAppVerified' => false,
            ];
            $this->response(['status' => true, 'message' => 'Get Mechanic Status', 'response' => $defaultStatus]);
        }
    }

    ## Get All Request of mechanics
    public function getAllRequestOfMechanic_post()
    {
        $this->load->model(['Request_model', 'Request_issues_model']);
        $mechanicId = $this->param['mechanic_id'];

        if (isset($this->param['mechanic_id'])) {
            $getAllRequest = $this->Request_model->get_all(['mechanic_id' => $this->param['mechanic_id']]);

            if (!empty($getAllRequest)) {
                foreach ($getAllRequest as $getAllRequests) {
                    $requestId = $getAllRequests['request_id'];
                    $getAllRepairRequest = $this->Request_issues_model->fields(['date', 'timepicker'])->where('request_id', $requestId)->get();

                    if (!empty($getAllRepairRequest)) {
                        $date = $getAllRepairRequest['date'];
                        $time = $getAllRepairRequest['timepicker'];
                    } else {
                        $getDateTime = explode(" ", $getAllRequests['request_date']);
                        $date = $getDateTime[0];
                        $time = $getDateTime[1];
                    }

                    $requestDetails[] = [
                        'request_id' => $requestId,
                        'request_status' => $getAllRequests['request_status'],
                        'request_type' => $getAllRequests['request_type'],
                        'request_date' => $date,
                        'request_time' => $time,
                        'latitude' => $getAllRequests['request_lat'],
                        'longitute' => $getAllRequests['request_lng'],
                    ];
                }
                if (!empty($requestDetails)) {
                    $this->response(['status' => true, 'message' => 'All Request Here!!', 'response' => $requestDetails]);die;
                }
            } else {
                $this->response(['status' => true, 'message' => 'You have no request at the moment. .']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Get the All Verification Status
    public function getMechanicStatus_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(array('Mechanic_itineraries_model', 'Mechanic_service_model', 'Mechanic_document_model', 'Mechanic_experience_model', 'Mechanic_qualification_model', 'Mechanic_health_model'));

        $Itineraries = $this->Mechanic_itineraries_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['itinerary'] = !empty($Itineraries) ? '1' : '0';

        $health = $this->Mechanic_health_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['health'] = !empty($health) ? '1' : '0';

        $service = $this->Mechanic_service_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['service'] = !empty($service) ? '1' : '0';

        $document = $this->Mechanic_document_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['document'] = !empty($document) ? '1' : '0';

        $experience = $this->Mechanic_experience_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['experience'] = !empty($experience) ? '1' : '0';

        $qualification = $this->Mechanic_qualification_model->get_all(['mechanic_id' => $id]);
        $mechanicStatus['qualification'] = !empty($qualification) ? '1' : '0';

        if (!empty($mechanicStatus)) {
            $this->response(['status' => true, 'message' => 'All Verification Status Here', 'response' => $mechanicStatus]);
        } else {
            $this->response(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    #Change Contact Numbber
    public function changeContactNumber_post()
    {
        $this->load->model('Otp_contact_number_model');
        $mechanicId = $this->param['mechanic_id'];
        $contactNumber = $this->param['contact_number'];

        #check existence of contact number
        $isContactExists = $this->Mechanic_model->fields('mechanic_id')->get(['contact_number' => $contactNumber, 'mechanic_id !=' => $mechanicId]);

        if ($isContactExists) {
            $this->response(['status' => false, 'message' => 'Contact Number already exists.']);
        }

        #check existence of contact number
        $mechanicDetails = $this->Mechanic_model->fields('mechanic_id,email_id')->get(['mechanic_id' => $mechanicId]);
        // $OTP = 2222;
        $OTP = mt_rand(1000, 9999);
        $insertData = [
            'user_id' => $mechanicId,
            'contact_number' => $contactNumber,
            'user_type' => 2,
        ];

        $sendOTP = $this->Otp_contact_number_model->fields('id')->get($insertData);
        if ($sendOTP) {
            $updateOTP = $this->Otp_contact_number_model->where(['id' => $sendOTP['id']])->update(['OTP' => $OTP]);
            if ($updateOTP) {
                $sendOTP = $sendOTP['id'];
            } else {
                $sendOTP = false;
            }
        } else {
            $insertData['otp'] = $OTP;
            $sendOTP = $this->Otp_contact_number_model->insert($insertData);
        }

        if ($sendOTP) {
            $this->load->library('nexmo');
            $otpMesssage = $OTP . ' is your one-time verification code, which is valid for the next 10 minutes. Please do not share it with anyone.';
            $this->nexmo->send_message("Patcher", '44' . $contactNumber, ["text" => $otpMesssage]);

            $args = [
                'message' => $otpMesssage,
                'subject' => 'Patcher: OTP Verifiction',
                'email_id' => $mechanicDetails['email_id'],
                'template_name' => 'Sign Up',
                'vars' => [
                    [
                        'name' => 'message',
                        'content' => $otpMesssage,
                    ],
                ],
            ];
            $mailResponse = sendMandrillEmailTemlate($args);

            $this->response(['status' => true, 'message' => 'An OTP send to your number. Please verify it.', 'response' => ['change_id' => $sendOTP]]);
        } else {
            $this->response(['status' => false, 'message' => 'Error in process. Please try again after some time.']);
        }
    }

    #Change Contact Numbber
    public function verifyContactNumber_post()
    {
        $this->load->model('Otp_contact_number_model');
        $changeId = $this->param['change_id'];
        $OTP = $this->param['otp'];

        $this->db->trans_start();
        $detailOTP = $this->Otp_contact_number_model->get(['id' => $changeId]);

        if ($detailOTP) {
            if ($OTP == $detailOTP['otp']) {

                #Update Mechanic Contact
                $updateContact = $this->Mechanic_model->where(['mechanic_id' => $detailOTP['user_id']])->update(['contact_number' => $detailOTP['contact_number']]);

                #Delete OTP entry from table
                $this->Otp_contact_number_model->where(['id' => $changeId])->force_delete();

                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $this->response(['status' => true, 'message' => 'OTP verified.']);
                } else {
                    $this->response(['status' => false, 'message' => 'Error in process. Please try again after some time.']);
                }
            } else {
                $this->response(['status' => false, 'message' => 'OTP does not match.']);
            }
        } else {
            $this->response(['status' => false, 'message' => 'Invalid OTP details.']);
        }
    }
}
