<?php defined('BASEPATH') or exit('No direct script access allowed');
header("Content-Type:application/json");
class WebsiteContact extends CI_Controller
{
    private $param = array();
    public function __construct()
    {
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['Website_contact_model']);
    }

    public function addWebsiteContact()
    {
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('websiteContact') == false) {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 400);die;
        } else {
            $data['name'] = $this->param['name'];
            $data['email'] = $this->param['email'];
            if (isset($this->param['message']) && $this->param['message'] != null) {
                $data['message'] = $this->param['message'];
            }
            if (isset($this->param['user_type']) && $this->param['user_type'] != null) {
                $data['user_type'] = $this->param['user_type'];
            }
            if (isset($this->param['contact']) && $this->param['contact'] != null) {
                $data['contact'] = $this->param['contact'];
            }
            $add = $this->Website_contact_model->insert($data);
            if ($add) {

                $args = [
                    'subject' => 'Thank You For Pre Registration',
                    'email_id' => $data['email'],
                    'template_name' => 'Pre Reg Email Text',
                ];

                $mail = sendMandrillEmailTemlate($args);
                echo json_encode(['status' => true, 'message' => 'Sucessfully Registered', 'response' => ['contact_id' => $add]]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }
    public function contactList()
    {

        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('website_contacts')->count_all_results();
        $contacts = $this->Website_contact_model->fields('name,contact,email,message,user_type')->limit($limit, $offset)->get_all();
        if ($contacts) {
            echo json_encode(['status' => true, 'message' => 'Sucessfully Registered', 'response' => ['count' => $count, 'contacts' => $contacts]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }

    }

}
