<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Machanics extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model','Mechanic_model']);
    }

    public function addMechanics_post() {
    	$this->form_validation->set_data($this->post());  
        if ($this->form_validation->run('mechanicsRegistration') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status'=> FALSE, 'message'=> $message);
            $this->response($response, 400);
        } else {
            $mechanics_array = [
                    'name'=>$this->param['name'],
                    'email_id'=>$this->param['emailId'],
                    'password'=>$this->param['password'],
                    'country_code'=>$this->param['countryCode'],
                    'contact_number'=>$this->param['contactNumber'],
                    'mechanic_dob'=>$this->param['mechanicDob'],
                    'mechanic_address'=>$this->param['mechanicAddress'],
                    'mechanic_services'=>$this->param['mechanicServices'],
                    'mechanic_experience'=>$this->param['mechanicExperience'],
                    'mechanic_itinerary'=>$this->param['mechanicItinerary'],
                ];
            $insert = $this->Mechanics_model->insert($mechanics_array);
             if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }



    ## For Add And Update Mechanics Health
    public function forMechanicHealth_post()
    {
        $id=$this->param['mechanic_id'];
        if(isset($this->param['mechanic_health']) && !empty($this->param['mechanic_health'])) {
            $this->load->model('Mechanic_health_model');
            $mechanicsHealthDocuments_array = [
                    'health_id' => trim($this->param['mechanic_health']),
                    'mechanic_id' => $id,
                    ];

            $getVal = $this->Mechanic_health_model->get(['mechanic_id' => $id]);
            if(!empty($getVal)) {
                $updateData = ['health_id' => trim($this->param['mechanic_health'])];
                $update = $this->Mechanic_health_model->where('mechanic_id', $id)->update($updateData);
            } else {
                $insert = $this->Mechanic_health_model->insert($mechanicsHealthDocuments_array);
            } 

            if($insert) {
                 echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $insert]);die;
             }elseif($update) {
                echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $updateData]);die;
             } else {
                 echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
             }
        }
    }




    ## For Add And Update Mechanics Itinerary
    public function forMechanicItinerary_post()
    {
        $this->load->model(['Mechanic_itineraries_model','Mechanic_service_model','Service_model','Tool_itinerary_model']);

        $id=$this->param['mechanic_id'];
        $toolId=$this->param['tool_itinerary_id'];

        if(isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId=$this->param['service_id'];

            $updatedata = ['tool_itinerary_id' => trim($toolId)];

            $update = $this->Mechanic_service_model->where(['mechanic_id'=>$id, 'services_id'=>$serviceId])->update($updatedata);
        } else {
            $updatedata = ['tool_itinerary_id' => trim($toolId)];

            $explodeToolId = explode(",",$toolId);

            $getMechanicService = $this->Mechanic_service_model->get(['mechanic_id'=>$id]);

            $getService = $this->Tool_itinerary_model->fields(['tool_itinerary_id','service_id'])->where('tool_itinerary_id', $explodeToolId)->get_all();

            if(!empty($getMechanicService)) {
                $mechanicServiceId = $getMechanicService['mechanic_services_id'];
                foreach($getService as $getServices) {
                    $serviceId = $getServices['service_id'];
                    $updateData[$serviceId] = [
                            'tool_itinerary_id'=>$toolId,
                            'services_id'=>$getServices['service_id']];
                }
                $updateVal = array_shift($updateData);

                $update = $this->Mechanic_service_model->where(['mechanic_id'=>$id])->update($updateVal);
            } else {
                foreach($getService as $getServices) {
                    $serviceId = $getServices['service_id'];
                    $serviceToolId = $getServices['tool_itinerary_id'];
                    $insertData[$serviceId] = [
                            'services_id' => $serviceToolId,
                            'mechanic_id' => $id,
                            'tool_itinerary_id' => $toolId
                        ];
                }
                $insert = $this->Mechanic_service_model->insert($insertData);
            }
        }
       
        if(!empty($insert)) {
             echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $insert]);die;
        } elseif($update) {
            echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $updateVal]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }




    ## For Add And Update Mechanics Service
    public function forMechanicsService_post() {
        $id=$this->param['mechanic_id'];
        $this->load->model('Mechanic_service_model');
        if(isset($this->param['mechanic_services']) && !empty($this->param['mechanic_services'])) {
            $services = $this->param['mechanic_services'];
            $mechanicsInsertData = array();
            $update = array();
            $updateData = array();
            $insert = array();

            foreach($services as $key => $service) {
                $serviceId = $service['service_id'];
                

                $mechanicsService_array = ['mechanic_id' => $id,
                        'services_id' => $serviceId,
                        'mechanic_services_status'=>$service['mechanic_services_status']];


                $getVal = $this->Mechanic_service_model->get(['mechanic_id' => $id, 'services_id' => $serviceId]);
                if(!empty($getVal)) {
                    $updateWhere = array('services_id' => $serviceId,
                                        'mechanic_id' => $id
                                        );
                    $updateData[] = ['services_id' => $serviceId,'mechanic_services_status' => $service['mechanic_services_status']];

                    $update[] = $this->Mechanic_service_model->where($updateWhere)->update(['mechanic_services_status' => $service['mechanic_services_status']]);
                } else {
                     $insert[] = $this->Mechanic_service_model->insert($mechanicsService_array);

                     $mechanicsInsertData[] = ['mechanic_id' => $id,
                        'services_id' => $serviceId,
                        'mechanic_services_status'=>$service['mechanic_services_status']];
                }
            }  
            if($insert) {
                echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $mechanicsInsertData]);
            } elseif($update) {
                echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $updateData]);
            }else {
                echo json_encode(['status' => false, 'message' => 'Something went wrong.']);
            }
        }
    }


    ## For Add And Update Mechanics Documentation
    public function forMechanicsDocumentation_post() {
        $id=$this->param['mechanic_id'];
        if(isset($this->param['mechanic_documentation']) && !empty($this->param['mechanic_documentation'])) {
            
            $this->load->model('Mechanic_document_model');
            $documentation = $this->param['mechanic_documentation'];

            $update = array();
            $insert = array();
            foreach($documentation as $key => $documentations) {
               /* $image_name = "uploads/documentation/".rand(1000,5000).time().".png";
                $image = $this->common_model->getImageBase64Code($documentations['mechanicDocumentationFile']);
                file_put_contents($image_name,$image);
                chmod($image_name, 0777);*/

                $img = $documentations['mechanicDocumentationFile']; 
                
                $img = explode(',',$img);
                $mime = explode('/',$img[0]);
                $mime = explode(';',$mime[1]);
                $img = str_replace(' ', '+', $img[1]);
                $data = base64_decode($img);
                $fileName ='uploads/documentation/'.rand(1000,5000) . '.'.$mime[0];

                $fileUpload = $fileName;
                file_put_contents($fileUpload, $data);
                chmod($fileUpload, 0777);


                $mechanicsDocumentDocuments_array = [
                        'document_id' => $documentations['document_id'],
                        'mechanic_id' => $id,
                        'document_url' => ROOTPATH.$fileName,
                        'mechanic_document_status' => 101
                        ];

                    $getVal = $this->Mechanic_document_model->get(['mechanic_id' => $id, 'document_id' => $documentations['document_id']]);

                    if(!empty($getVal)) {
                    $updateWhere = array('document_id' => $documentations['document_id'],
                                        'mechanic_id' => $id
                                        );
                    $update[] = $this->Mechanic_document_model->where($updateWhere)->update(
                        ['document_url' => ROOTPATH.$fileName, 'mechanic_document_status' => 101]);
                    
                    } else {
                        $insert[] = $this->Mechanic_document_model->insert($mechanicsDocumentDocuments_array);
                }
            }

            if(!empty($insert)) {
                echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $insert]);die;
            } elseif(!empty($update)) {
                echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $update]);die;
            }
            else {
                echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
            }


        }
    }


    ## For Add ANd Update Mechanics Experience
    public function forMechanicsExperience_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model','common_model']);
        $experience = $this->param['experience_id'];
        
        $updateData = array();
        $insert = array();
        $update = array();

        $experienceData = ['experience_id'=>$experience, 
                            'mechanic_id' => $id,
                            'mechanic_experience_status'=>101];

        $getExperience = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_url' => '', 'isPass' => '0']);
        if(!empty($getExperience)) {
            $update[] = $this->Mechanic_experience_model->where(['mechanic_id'=>$id, 'experience_url' => '', 'isPass' => '0'])->update(['experience_id'=>$experience]);
            $updateData['range'] = ['experience_id'=>$experience];
        } else {
            $insert[] = $this->Mechanic_experience_model->insert($experienceData);
        }
        
      
        if(isset($this->param['isPass']) && !empty($this->param['isPass'])) {

            $isPass = $this->param['isPass'];
            foreach($isPass as $isPasses) {
                if(!empty($isPasses['mechanicExperienceFile'])) {

                    $img = $isPasses['mechanicExperienceFile']; 
                    $imageData = $this->common_model->convertBase64ToImage($img);

                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $image_name ='uploads/experience/'.rand(1000,5000) . '.'.$mime;
                    file_put_contents($image_name, $data);
                    chmod($image_name, 0777);
                    $imagePath = ROOTPATH.$image_name;

                    $isPassData = ['experience_id'=> $isPasses['experience_id'],
                            'mechanic_id' => $id,
                            'isPass' => $isPasses['mechanic_experience_status'],
                            'experience_url' => $imagePath,
                            'mechanic_experience_status'=>101];

                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id'=>$id,'experience_id'=>$isPasses['experience_id']]);


                    if(!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id'=>$id, 'experience_id'=>$isPasses['experience_id']])->update(['experience_url'=>ROOTPATH.$image_name, 'isPass'=>$isPasses['mechanic_experience_status']]);

                        $updateData['isPass'][] = ['mechanic_id'=>$id,'experience_id'=>$isPasses['experience_id'], 'experience_url'=>ROOTPATH.$image_name, 'isPass'=>$isPasses['mechanic_experience_status']];

                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                } else {
                    $isPassData = ['experience_id'=> $isPasses['experience_id'],
                            'mechanic_id' => $id,
                            'isPass' => $isPasses['mechanic_experience_status'],
                            'mechanic_experience_status'=>101];


                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id'=>$id,'experience_id'=>$isPasses['experience_id']]);

                    if(!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id'=>$id, 'experience_id'=>$isPasses['experience_id']])->update(['isPass'=>$isPasses['mechanic_experience_status'], 'experience_url'=>'']);

                        $updateData['isPass'][] = ['mechanic_id'=>$id,'experience_id'=>$isPasses['experience_id'], 'experience_url'=>'', 'mechanic_experience_status'=>$getIsPass['isPass']];
                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                }
            }
        }



        if(isset($this->param['refrences']) && !empty($this->param['refrences'])) {
            $refrence = $this->param['refrences'];
            foreach($refrence as $refrences) {
               

                if(!empty($refrences['mechanicExperienceFile'])) {

                    $img = $refrences['mechanicExperienceFile']; 
                    $imageData = $this->common_model->convertBase64ToImage($img);

                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $refrenceimage_name ='uploads/experience/'.rand(1000,5000) . '.'.$mime;
                    file_put_contents($refrenceimage_name, $data);
                    chmod($refrenceimage_name, 0777);
                    $root_Refrenceimage_name = ROOTPATH.$refrenceimage_name;
                } else {
                    $root_Refrenceimage_name = '';
                }


                $refrenceData = ['experience_id'=> $refrences['experience_id'],
                            'mechanic_id' => $id,
                            'experience_url' => $root_Refrenceimage_name,
                            'mechanic_experience_status'=>101];

                $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id'=>$id,'isPass'=>0,'experience_id'=>$refrences['experience_id']]);

                if(!empty($getIsPass)){
                    if($root_Refrenceimage_name != '') {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id'=>$id, 'experience_id'=>$refrences['experience_id'], 'isPass' => '0'])->update(['experience_url'=>$root_Refrenceimage_name, 'mechanic_experience_status'=>101]);

                        $updateData['isPass'][] = ['mechanic_id'=>$id, 'experience_id'=>$refrences['experience_id'],'experience_url'=>$root_Refrenceimage_name];
                    } else {
                        $update = $this->Mechanic_experience_model->where(['mechanic_id'=>$id, 'experience_id'=>$refrences['experience_id'], 'isPass' => '0'])->update(['experience_url'=>$root_Refrenceimage_name]);


                        $updateData['isPass'][] = ['mechanic_id'=>$id, 'experience_id'=>$refrences['experience_id'],['experience_url'=>$root_Refrenceimage_name]];
                    }
                    
                } else {
                    $insert[] = $this->Mechanic_experience_model->insert($refrenceData);
                }
            }
        }


        if(!empty($insert)) {
            echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $insert]);die;
        }
        if(!empty($update)) {
            echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $updateData]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }


     ## For Add and Update Mechanics Qualification
    public function forMechanicsQualification_post()
    {
        $id=$this->param['mechanic_id'];
        if(isset($this->param['mechanic_qualification']) && !empty($this->param['mechanic_qualification'])) {
            $this->load->model(['Mechanic_qualification_model','common_model']);
            $qualification = $this->param['mechanic_qualification'];

            $update=array();
            $insert=array();

            foreach($qualification as $key => $qualifications) {
                   /* $image_name = "uploads/experience/".rand(1000,5000).time().".png";
                    $image = $this->common_model->getImageBase64Code($qualifications['mechanicQualificationFile']);
                    file_put_contents($image_name,$image);
                    chmod($image_name, 0777);*/
                   
                    if(!empty($qualifications['mechanicQualificationFile'])) {
                         $img = $qualifications['mechanicQualificationFile'];
                         $imageData = $this->common_model->convertBase64ToImage($img);

                        $data = $imageData['data'];
                        $mime = $imageData['mime'];

                        $image_name ='uploads/qualification/'.rand(1000,5000) . '.'.$mime;
                        file_put_contents($image_name, $data);
                        chmod($image_name, 0777);
                        $qualificationStatus = 101;
                    } else {
                        $qualificationStatus = 100;
                    }


                    $mechanicsQualification = [
                            'qualification_id' => $qualifications['qualification_id'],
                            'mechanic_id' => $id,
                            'qualification_url' => ROOTPATH.$image_name,
                            'mechanic_qualification_status'=>$qualificationStatus
                            ];
                            
                    $getVal = $this->Mechanic_qualification_model->get(['mechanic_id' => $id, 'qualification_id' => $qualifications['qualification_id']]);

                    if(!empty($getVal)) {
                        $updateWhere = array('qualification_id' => $qualifications['qualification_id'], 'mechanic_id' => $id );

                        $updateData = ['qualification_url' => ROOTPATH.$image_name, 'mechanic_qualification_status'=>$qualificationStatus];

                        $update[] = $this->Mechanic_qualification_model->where($updateWhere)->update($updateData);

                    } else {
                         $insert[] = $this->Mechanic_qualification_model->insert($mechanicsQualification);
                     }
                }

            if(!empty($insert)) {
                 echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $insert]);die;
            }elseif(!empty($update)){
                 echo json_encode(['status' => true, 'message'=> 'Update Successful.','response' => $update]);die;
            }else {
                 echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
            }
        } 
    }


    ## Get Mechanics Services Based On Selection
    public function getMechanicServices_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_service_model','Service_model']);

        $servicesList = $this->Service_model->where('service_status', 101)->fields(['service_id','parent_id','service_name','service_status'])->get_all(); 

        $allServices = array();
        foreach($servicesList as $key=>$servicesLists){
            $serviceId = $servicesLists['service_id'];
            $allServices[$key]['service_id'] = $servicesLists['service_id'];
            $allServices[$key]['service_name'] = $servicesLists['service_name'];
            $allServices[$key]['parent_id'] = $servicesLists['parent_id'];
            $allServices[$key]['service_status'] = $servicesLists['service_status'];

           $serviceData = $this->Mechanic_service_model->fields(['mechanic_services_status'])->get(['mechanic_id' => $id, 'services_id'=>$serviceId]);

            if(!empty($serviceData)) {
                $serviceVal = $serviceData;
                $allServices[$key]['mechanic_services_status'] = $serviceVal['mechanic_services_status'];
            } else {
                $allServices[$key]['mechanic_services_status'] = '100';
            }
        }

       // $AllServices = $this->Mechanic_service_model->fields(['services_id','mechanic_services_status'])->with_service('fields:service_name')->get_all(['mechanic_id' => $id]);
        if(!empty($allServices)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Services Details Here', 'response'=>$allServices]);
        }else{
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }


     ## Get The Mechancis Documents
    public function getMechanicsDocuments_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_document_model','Document_model']);

        $documentList = $this->Document_model->where('document_status', 101)->fields(['document_id','document_name','document_description','isOptional','document_status'])->get_all(); 

        $AllDocuments = array();
        foreach($documentList as $key=>$documentLists) {
            $document_id = $documentLists['document_id'];
            $allDocuments[$key]['document_id'] = $documentLists['document_id'];
            $allDocuments[$key]['document_name'] = $documentLists['document_name'];
            $allDocuments[$key]['document_description'] = $documentLists['document_description'];
            $allDocuments[$key]['document_status'] = $documentLists['document_status'];

            $documentData = $this->Mechanic_document_model->fields(['document_id','document_url','mechanic_document_status'])->get(['mechanic_id' => $id, 'document_id'=>$document_id]);

            if(!empty($documentData)) {
                $documentVal = $documentData;

                $allDocuments[$key]['document_url'] =  ($documentVal['document_url'] != '') ? $documentVal['document_url'] : '';

                $allDocuments[$key]['document_verified'] =  ($documentVal['mechanic_document_status'] != '') ? $documentVal['mechanic_document_status'] : '';
            }
        }
        if(!empty($allDocuments)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Documents Details Here', 'response'=>$allDocuments]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }


     ## Get The Mechancis Experience
    public function getMechanicsExperience_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model','Experience_model']);

        $experienceList = $this->Experience_model->where('experience_status', 101)->fields(['experience_id','experience_name','isOptional','isUpload','experience_status'])->get_all();


        $allExperience = array();
        $allExperience1 = array();
        foreach($experienceList as $key=>$experienceLists) {
            $experience_id = $experienceLists['experience_id'];
            $experience_name = $experienceLists['experience_name'];

            $experienceData = $this->Mechanic_experience_model->fields(['experience_url','experience_id','mechanic_experience_status','isPass'])->get(['mechanic_id' => $id, 'experience_id'=>$experience_id]);  

            if($experienceData['mechanic_experience_status'] == null) {
                $mechanicExperienceStatus = '100';
            } else {
                $mechanicExperienceStatus = $experienceData['mechanic_experience_status'];
            }

            if($experienceData['experience_url'] == null) {
                $experienceUrl = '';
            } else {
                $experienceUrl = $experienceData['experience_url'];
            }
            

            if($experienceData['isPass'] == null) {
                $experienceIsPass = '100';
            } else {
                $experienceIsPass = $experienceData['isPass'];
            }



            if($experienceLists['isOptional'] == '101') {
                $allExperience['isPass'][] = [
                        'experience_id'=>$experienceLists['experience_id'],
                        'experience_name'=>$experienceLists['experience_name'],
                        'experience_status'=>$experienceLists['experience_status'],
                        'experience_url'=>$experienceUrl,
                        'mechanic_experience_status'=>$mechanicExperienceStatus,
                        'isRefrence'=>"101"
                    ];
            } 
            elseif($experienceLists['isUpload'] == '100') {
                $allExperience['isPass'][] = [
                        'experience_id'=>$experienceLists['experience_id'],
                        'experience_name'=>$experienceLists['experience_name'],
                        'experience_status'=>$experienceLists['experience_status'],
                        'isPass'=>"100",
                        'experience_url'=>$experienceUrl,
                        'mechanic_experience_status'=>$experienceIsPass
                    ];
            }
            else {
                $allExperience['range'][] = [
                        'experience_id'=>$experienceLists['experience_id'],
                        'experience_name'=>$experienceLists['experience_name'],
                        'experience_status'=>$experienceLists['experience_status'],
                        'mechanic_experience_status'=>$mechanicExperienceStatus
                ];
            }
        }

        if(!empty($allExperience)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Experience Details Here', 'response'=>$allExperience]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }


    ## Get The Mechancis Qualification
    public function getMechanicsQualification_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_qualification_model','Qualification_model']);
        $qualificationList = $this->Qualification_model->where('qualification_status',101)->fields(['qualification_id','qualification_name','qualification_status','isRefrence'])->get_all();

        $allQualification = array();
        foreach($qualificationList as $key => $qualificationLists) {
            $qualification_id = $qualificationLists['qualification_id'];
            $qualification_name = $qualificationLists['qualification_name'];

            $qualificationData = $this->Mechanic_qualification_model->fields(['qualification_url','mechanic_qualification_status'])->get(['mechanic_id' => $id, 'qualification_id'=>$qualification_id]);

            if($qualificationData['mechanic_qualification_status'] == null) {
                $qualificationVerified = "100";
            } else {
                $qualificationVerified = $qualificationData['mechanic_qualification_status'];
            }


            if($qualificationData['qualification_url'] == null) {
                $qualificationUrl = "";
            } else {
                $qualificationUrl = $qualificationData['qualification_url'];
            }


            if($qualificationLists['isRefrence'] == '101') {
                $allQualification['refrence'][] = [
                    'qualification_id'=>$qualificationLists['qualification_id'],
                    'qualification_name'=>$qualificationLists['qualification_name'],
                    'qualification_status'=>$qualificationLists['qualification_status'],
                    'qualification_url'=>$qualificationUrl,
                    'mechanic_qualification_status'=>$qualificationVerified
                ];
            } else {
                $allQualification['qualification'][$key] = [
                    'qualification_id'=>$qualificationLists['qualification_id'],
                    'qualification_name'=>$qualificationLists['qualification_name'],
                    'qualification_status'=>$qualificationLists['qualification_status'],
                    'qualification_url'=>$qualificationUrl,
                    'mechanic_qualification_status'=>$qualificationVerified
                ];
            }
        }
        if(!empty($allQualification)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Qualification Details Here', 'response'=>$allQualification]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }


    ## Get The Mechanics Itinerary
    public function getMechanicsItinerary_post()
    {
        $id=$this->param['mechanic_id'];

        $this->load->model(['Mechanic_itineraries_model','Tool_itinerary_model','Mechanic_service_model']);

        if(isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId=$this->param['service_id'];
            $getTool = $this->Tool_itinerary_model->where(['tool_itinerary_status'=>101, 'service_id'=>$serviceId])->fields(['tool_itinerary_id','tool_itinerary_name'])->get_all();


            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id,'services_id'=>$serviceId]);
          
            $arrayval = explode(",",$getMechanicItineraries[0]['tool_itinerary_id']);

            $toolDetails = array();
            foreach($getTool as $key=>$getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $toolDetails[$key]['tool_itinerary_id'] = $getTools['tool_itinerary_id'];

                $toolDetails[$key]['tool_itinerary_name'] = $getTools['tool_itinerary_name'];
                if(in_array($toolId, $arrayval)) {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '101';
                } else {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '100';
                }
            }

        } else {

            $getTool = $this->Tool_itinerary_model->with_service('fields:service_name')->fields(['tool_itinerary_id','tool_itinerary_name','tool_itinerary_status','service_id'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id]);

            if(!empty($getMechanicItineraries)) {
                $arrayval = explode(",",$getMechanicItineraries[0]['tool_itinerary_id']);
            }

            foreach($getTool as $key=>$getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $serviceName = $getTools['service']['service_name'];

                if(in_array($toolId, $arrayval)) {
                    $toolStatus = '101';
                } else {
                    $toolStatus = '100';
                }

                $toolDetails[] = [
                    'tool_itinerary_id'=>$getTools['tool_itinerary_id'],
                    'tool_itinerary_name'=>$getTools['tool_itinerary_name'],
                    'tool_itinerary_status'=>$getTools['tool_itinerary_status'],
                    'mechanic_itinerary_status'=>$toolStatus
                ];
            }
        }
        if(!empty($toolDetails)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Itinerary Details Here', 'response'=>$toolDetails]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }



     ## Get The Mechanics Health
    public function getMechanicsHealth_post()
    {
        $id=$this->param['mechanic_id'];
        $this->load->model(['Mechanic_health_model','Health_safety_check_model']);
        $getHealth = $this->Health_safety_check_model->fields(['health_safety_check_id','health_safety_name','health_safety_description','health_safety_check_status'])->get_All();

        $getMechanichealth = $this->Mechanic_health_model->fields(['health_id','mechanic_health_status'])->get_all(['mechanic_id' => $id]);
        $arrayval = explode(",",$getMechanichealth[0]['health_id']);
        $healthDetails = array();
        foreach($getHealth as $key=>$getHealths) {
            $healthId = $getHealths['health_safety_check_id'];
            $healthDetails[$key] = [
                    'health_safety_check_id' => $getHealths['health_safety_check_id'],
                    'health_safety_name' => $getHealths['health_safety_name'],
                    'health_safety_check_status' => $getHealths['health_safety_check_status']
                            ];

            if(in_array($healthId, $arrayval)) {
                $healthDetails[$key]['mechanic_health_status'] = '101';
            } else {
                $healthDetails[$key]['mechanic_health_status'] = '100';
            }
        }
        if(!empty($healthDetails)) {
            echo json_encode(['status'=>true, 'message'=>'Mechanics Health Details Here', 'response'=>$healthDetails]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }

     public function getmechanicRegistrationStatus_post() {
        $this->load->model('Mechanic_registrations_model');
        $id=$this->param['mechanic_id'];
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id'=>$id]);
        if(!empty($getStatus)) {
            echo json_encode(['status'=>true, 'message'=>'Get Mechanic Status', 'response'=>$getStatus]);
        }else {
            $defaultStatus = [
                        'mechanic_id'=>$id,
                        'basic_info'=>'100',
                        'service_status'=>'100',
                        'documentation_status'=>'100',
                        'experience_status'=>'100',
                        'qualification_status'=>'100',
                        'tools_status'=>'100',
                        'health_status'=>'100',
                        'comment'=>''
                    ];
             echo json_encode(['status'=>true, 'message'=>'Get Mechanic Status', 'response'=>$defaultStatus]);
        }
     }



    ## Get All Request of mechanics
    public function getAllRequestOfMechanic_post()
    {
        $this->load->model('Request_model'); 
        $mechanicId = $this->param['mechanic_id'];
           
        if(isset($this->param['mechanic_id'])) {
            $getAllRequest = $this->Request_model->get_all(['mechanic_id'=>$this->param['mechanic_id']]);

            foreach($getAllRequest as $getAllRequests) {
                $getDateTime = explode(" ", $getAllRequests['request_date']);
                $requestDetails[] = [
                    'request_id'=>$getAllRequests['request_id'],
                    'request_type'=>$getAllRequests['request_type'],
                    'request_date'=>$getDateTime[0],
                    'request_time'=>$getDateTime[1],
                    'latitude'=>$getAllRequests['request_lat'],
                    'longitute'=>$getAllRequests['request_lng'],
                ];
            }

            if(!empty($getAllRequest)) {
                 $this->response(['status' => true, 'message'=> 'Get All Request of Specific Mechanic!!','response' => $requestDetails]);die;
            }else {
             $this->response(['status' => true, 'message' => 'You have yet to make any requests.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    
     ## Get the All Verification Status
     public function getMechanicStatus_post()
     {
         $id=$this->param['mechanic_id'];
         $this->load->model(array('Mechanic_itineraries_model', 'Mechanic_service_model', 'Mechanic_document_model', 'Mechanic_experience_model', 'Mechanic_qualification_model', 'Mechanic_health_model'));
 
         $Itineraries = $this->Mechanic_itineraries_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['itinerary'] = !empty($Itineraries) ? '1' : '0';
 
         $health = $this->Mechanic_health_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['health'] = !empty($health) ? '1' : '0';
 
         $service = $this->Mechanic_service_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['service'] = !empty($service) ? '1' : '0';
 
         $document = $this->Mechanic_document_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['document'] = !empty($document) ? '1' : '0';
 
         $experience = $this->Mechanic_experience_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['experience'] = !empty($experience) ? '1' : '0';
 
         $qualification = $this->Mechanic_qualification_model->get_all(['mechanic_id' => $id]);
         $mechanicStatus['qualification'] = !empty($qualification) ? '1' : '0';
 
         if(!empty($mechanicStatus)) {
             echo json_encode(['status'=>true, 'message'=>'All Verification Status Here', 'response'=>$mechanicStatus]);
         } else {
             echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
         }
     }

}