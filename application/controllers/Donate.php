<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donate extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model','Transaction_donate_model']);
        $this->load->helper('common');
    }


    public function payForDonate_post()
    {
        $stripInclude = stripIncludeFiles();
        if(isset($this->param['name']) && !empty($this->param['name'])) {
            $name = $this->param['name'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Name is Required.']);
            die;
        }

        if(isset($this->param['cardnumber']) && !empty($this->param['cardnumber'])) {
            $carNumber = $this->param['cardnumber'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Card Number is Required.']);
            die;
        }

        if(isset($this->param['exp_month']) && !empty($this->param['exp_month'])) {
            $expireMonth = $this->param['exp_month'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Expire Month is Required.']);
            die;
        }

        if(isset($this->param['exp_year']) && !empty($this->param['exp_year'])) {
            $expireYear = $this->param['exp_year'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Expire Year is Required.']);
            die;
        }

        if(isset($this->param['cvv']) && !empty($this->param['cvv'])) {
            if (strlen($this->param['cvv']) != 3) {
                echo json_encode(['status' => false, 'message' => 'Pleae enter correct cvv number.']);
                die;
             } else {
                $cvv = $this->param['cvv'];
             }
        } else {
            echo json_encode(['status' => false, 'message' => 'cvv is Required.']);
            die;
        }

        if(isset($this->param['amount']) && !empty($this->param['amount'])) {
            $amount = $this->param['amount'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Amount is Required.']);
            die;
        }
        if(isset($this->param['currency']) && !empty($this->param['currency'])) {
            $currency = $this->param['currency'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Amount is Required.']);
            die;
        }


        $myCard = [
            'number' => $carNumber,
            'exp_month' => $expireMonth, 
            'exp_year' => $expireYear,
            'cvv' => $cvv
          ];
        
         /* $myCard = [
            'number' => '4242424242424242',
           'exp_month' => 8, 
           'exp_year' => 2018
          ];*/

        $charge = \Stripe\Charge::create(array('card' => $myCard, 'amount' => $amount, 'currency' => 'gbp')); 
        if($charge!=null && $charge->status == 'succeeded'){ 
            $data = [
                'name'=>$this->param['name'],
                'transaction_charge_id'=>$charge->id,
                'object_type'=>$charge->object,
                'amount'=>$charge->amount,
                'message'=>$charge->outcome->seller_message,
                'paid_status'=>$charge->paid,
                'source_id'=>$charge->source->id,
                'payment_type'=>1, //Payment type is donate
                'brand'=>$charge->source->brand,
                'country'=>$charge->source->country,
                'status'=>$charge->status
            ];
        $insert = $this->Transaction_donate_model->insert($data);
            if($insert) {
                $response = ['amount'=>$charge->object, 'transaction_status'=>$charge->status, 'message'=>$charge->outcome->seller_message];
                $this->response(['status' => true, 'message'=> 'Donated Successfully.','response' => $response]);
            } else {
                $this->response(['status' => true, 'message'=> 'Some Error In Transaction.']);
            }
        }
    }


    public function allDonation_post()
    {
        $limit = $this->param['limit'];
        $offset = $this->param['offset'];

        $total_rows = $this->Transaction_donate_model->count_rows();
        $donation = $this->Transaction_donate_model->fields(['transaction_donate_id','name','amount','message','country','status','created_at'])->where(['status'=>'succeeded', 'paid_status'=>1])->paginate($limit,$total_rows,$offset);
       
        $allDonation = $this->Transaction_donate_model->fields('amount')->where(['status'=>'succeeded', 'paid_status'=>1])->get_all();
        $total='';
        foreach($allDonation as $allDonations) {
            $total += $allDonations['amount'];
        }

       /* echo $this->Transaction_donate_model->all_pages; 
        echo $this->Transaction_donate_model->previous_page; 
        echo $this->Transaction_donate_model->next_page; */

        $response = [
                        'donation_details'=>$donation,
                        'total_rows'=>$total_rows,
                        'total_donation'=>$total
                    ];
        if(!empty($donation)) {
            $this->response(['status' => true, 'message'=> 'All Donation List Here. ','response' => $response]);
        } else {
            $this->response(['status' => true, 'message'=> 'Not Found']);
        }
    }
}