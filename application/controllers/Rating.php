<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model']);
        $this->load->library('Notification');
    }

 // User to mechanic
    public function ratingByUser_post()
    {
        $this->form_validation->set_data($this->param);  
        if ($this->form_validation->run('ratings') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status'=> FALSE, 'message'=> $message);
            $this->response($response, 400);
        }

        $this->load->model('Mechanic_rating_model');

        $comment = "";
        if(isset($this->param['comment']) && $this->param['comment'] != ""){
            $comment = $this->param['comment'];
        }

        $data = ['user_id'=>$this->param['user_id'],
                 'mechanic_id'=>$this->param['mechanic_id'],
                 'rating'=>$this->param['rating'],
                 'request_id'=>$this->param['request_id'],
                 'from_at'=>$this->param['user_type'], // 1 for user, 2 for mechanic
                 'comment' => $comment
                 ];
                 
        $where = ['user_id'=>$this->param['user_id'],
                'mechanic_id'=>$this->param['mechanic_id'],
                'request_id'=>$this->param['request_id'],
                'from_at'=>$this->param['user_type']];
        
        $getRating = $this->Mechanic_rating_model->where($where)->get();
       
        if(!empty($getRating)) {
            $update = $this->Mechanic_rating_model->where($where)->update(['rating'=>$this->param['rating']]);
        } else {
            $update = $this->Mechanic_rating_model->insert($data);
        }

        if($update) {
            $this->response(['status' => true, 'message' => 'Rating Updated Successful.']);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something Went Wrong.']);die;
        }
    }

}