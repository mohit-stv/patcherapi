<?php
defined('BASEPATH') or exit('No direct script access allowed');
header("Content-Type:application/json");
class MechanicLogin extends CI_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(array('Mechanic_model', 'Mechanic_registrations_model', 'common_model', 'User_model'));
        $this->load->library('encrypt');
        $this->load->helper('common');
    }

    # Genrate Key Using Email And Password.
    public function GenrateKey($value)
    {
        $key = $this->encrypt->encode($value);
        return $key;
    }

    # Function is used to insert genrated key in key table.
    public function InsertKey($key_parameters)
    {
        $this->load->model('Key_model');
        $key_id = $this->Key_model->insert($key_parameters);
        return $key_id;
    }

    public function mechanicsRegistration()
    {
        $this->form_validation->set_data($this->param);

        if (isset($this->param['isProduction']) && isset($this->param['device_key'])) {
            $mechanics_array = [
                'name' => $this->param['name'],
                'platform_type' => (isset($this->param['platform_type'])) ? $this->param['platform_type'] : 1,
                'isProduction' => (isset($this->param['isProduction']) ? $this->param['isProduction'] : 101),
                'device_key' => $this->param['device_key'],
            ];
        } else {
            $mechanics_array = [
                'name' => $this->param['name'],
                'platform_type' => (isset($this->param['platform_type'])) ? $this->param['platform_type'] : 3,
                'isProduction' => (isset($this->param['isProduction']) ? $this->param['isProduction'] : 101),
                'device_key' => (isset($this->param['device_key'])) ? $this->param['device_key'] : 0000,
            ];
        }

        if (isset($this->param['email_id']) && !empty($this->param['email_id'])) {
            $email_id = $this->Mechanic_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);
            if ($email_id) {
                echo json_encode(['status' => false, 'message' => 'Email Id already exits.']);
                die;
            }
            $mechanics_array['email_id'] = $this->param['email_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'EmailID is Required.']);
            die;
        }

        if (isset($this->param['contact_number']) && !empty($this->param['contact_number'])) {
            $number_exist = $this->Mechanic_model->fields('contact_number')->get(['contact_number' => $this->param['contact_number']]);
            if ($number_exist) {
                echo json_encode(['status' => false, 'message' => 'Contact number already exist.']);
                die;
            }
            $mechanics_array['contact_number'] = $this->param['contact_number'];
            if (isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                $mechanics_array['country_code'] = $this->param['country_code'];
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'Contact Number Required.']);
            die;
        }

        if (isset($this->param['password']) && !empty($this->param['password'])) {
            $mechanics_array['password'] = $this->encrypt->encode($this->param['password']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Password is required.']);
            die;
        }

        if (isset($this->param['hourly_rate']) && !empty($this->param['hourly_rate'])) {
            $mechanics_array['hourly_rate'] = $this->param['hourly_rate'];
        }

        if (isset($this->param['platform_type']) && $this->param['platform_type'] == '3') {
            if (isset($this->param['mechanic_dob']) && !empty($this->param['mechanic_dob'])) {
                $mechanics_array['mechanic_dob'] = $this->param['mechanic_dob'];
            }

            if (isset($this->param['mechanic_address']) && !empty($this->param['mechanic_address'])) {
                $mechanics_array['mechanic_address'] = $this->param['mechanic_address'];
            }
        }

        if(isset($this->param['request_commission']) &&  !empty($this->param['request_commission'])){
            $mechanics_array['request_commission'] = $this->param['request_commission'];
        }

        $result = $this->doRegister($mechanics_array);
        if ($result) {

            if(isset($this->param['deviceDetails']) && !empty($this->param['deviceDetails'])){
                $this->load->library('Device_detail');
                $deviceDetails = $this->param['deviceDetails'];
                $deviceDetails['user_type'] = 2;
                $deviceDetails['user_id'] = $result['mechanic_id'];
                $deviceDetails['os_type'] = (isset($this->param['platform_type']) && !empty($this->param['platform_type'])) ? $this->param['platform_type'] : 3;
                $this->device_detail->registerDetail($deviceDetails);
            }

            $this->Mechanic_registrations_model->insert(['mechanic_id' => $result['mechanic_id']]);
            echo json_encode(['status' => true, 'message' => 'Update Successful.', 'response' => $result], JSON_UNESCAPED_SLASHES);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);
            die;
        }
        //}
    }

    /*----------------------------------------------------------------------------------------
    |  DO REGISTER
    |-----------------------------------------------------------------------------------------
    |  It is used for registration service.
    |-----------------------------------------------------------------------------------------
     */
    private function doRegister($mechanics_array)
    {
        $this->load->model('Mechanic_model');
        $this->load->library('nexmo');
        # Call Genratekey function to genrate key using user email combination.
        $session_key = $this->GenrateKey($mechanics_array['name']);
        $this->db->trans_start();
        # Create user channel

        $this->load->library('mypubnub');
        $uuid = $this->mypubnub->uuid();

        $mechanics_array['mechanic_uuid'] = $uuid;

        $otp = mt_rand(1000, 9999);
        // $otp = '2222';
        $mechanics_array['otp'] = $otp;
        $mechanics_array['otp_created'] = date('Y:m:d H:i:s');
        $otpMesssage = $otp . ' is your one-time verification code. Please do not share it with anyone.';

        $mechanic_id = $this->Mechanic_model->insert($mechanics_array);
        $otpSend = $this->nexmo->send_message("Patcher", '44' . $mechanics_array['contact_number'], ["text" => $otpMesssage]);
        $args = [
            'message' => $otpMesssage,
            'subject' => 'Patcher: OTP Verifiction',
            'email_id' => $mechanics_array['email_id'],
            'template_name' => 'Sign Up',
            'vars' => [
                [
                    'name' => 'OTP',
                    'content' => $otp,
                ],
            ],
        ];
        $mailResponse = sendMandrillEmailTemlate($args);

        #Send Welcome Mail
        $welcomeArgs = [
            'subject' => 'Welcome to Patcher',
            'email_id' => $mechanics_array['email_id'],
            'template_name' => 'Welcome Mechanic',
        ];
        $welcomeMailResponse = sendMandrillEmailTemlate($welcomeArgs);

        # Insert Session Key In key Table.
        $key_parameters = [
            'key' => $session_key,
            'mechanic_id' => $mechanic_id,
            'user_type' => 2,
        ];
        $key_id = $this->InsertKey($key_parameters);

        /*--End Of Signupcode code*/

        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            /*--Send SMS--*/
            // if($user_data['signup_type'] == 1 ||(isset($user_data['signin_type'])) && $user_data['signin_type'] == 1){

            $to = '44' . $mechanics_array['contact_number']; //with country code
            $message = array('text' => $otp . '. is your verification code');
            $this->load->helper('api');
            // $send_otp = SendOtp($to,$message); //print_r($send_otp);

            /*---send mail to user--*/
            if (isset($mechanics_array['email_id'])) {
                # Send Email by Mailchimp
                $msg = 'Welcome to Patcher you are successfully registered.';
                $to = $mechanics_array['email_id'];
                // $this->sendEmail( $msg, $to );
            }

            $response = $this->UserResponse($mechanic_id);
            return $response;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    private function UserResponse($id)
    {
        $this->load->model('Mechanic_model');
        $some = ['mechanic_id', 'name', 'email_id', 'country_code', 'contact_number', 'mechanic_uuid', 'mobVerified', 'otp', 'mechanic_dob', 'mechanic_address'];
        $mechanic = $this->Mechanic_model->fields($some)->get($id);
        if ($mechanic) {
            $this->load->model('Key_model');
            $keys = $this->Key_model->fields('key')->get(['mechanic_id' => $mechanic['mechanic_id']]); // it will be userwise
            $mechanic['X-API-KEY'] = isset($keys['key']) ? $keys['key'] : '';
            $mechanic['api_key'] = "PATCHERAUTHKEY";
            $mechanic['api_value'] = isset($keys['key']) ? $keys['key'] : '';

            $mechanic['patcher_channel'] = 'User';
            $mechanic['pubnub_publish_key'] = PUBNUB_PUB_KEY;
            $mechanic['pubnub_subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;
            $mechanic['pubnub_secret_key'] = PUBNUB_SECRET_KEY;
            $mechanic['stripe_key'] = $this->_isproduction();


            // $user['publish_key'] = PUBNUB_PUB_KEY;
            // $user['subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;

            return $mechanic;
        } else {
            return false;
        }
    }

    ## Get the Mechanic Information
    public function getMechanicInformation()
    {
        $mechanicID = $this->param['mechanic_id'];
        // $result = $this->Mechanic_model->fields(['mechanic_id', 'name', 'email_id', 'country_code', 'contact_number', 'mechanic_dob', 'mechanic_address', 'mechanic_picture','vehicle_registration_number','latitude','longitude','national_insurance', 'about_me'])->where(['mechanic_id'=>$mechanicID])->get();

        $result = $this->Mechanic_model->fields(['mechanic_id', 'name', 'email_id', 'country_code', 'contact_number', 'mechanic_dob', 'mechanic_address', 'mechanic_picture', 'vehicle_registration_number', 'hourly_rate', 'national_insurance', 'about_me'])->where(['mechanic_id' => $mechanicID])->get();

        if ($result) {
            echo json_encode(['status' => true, 'message' => 'Get Mechanic Information.', 'response' => $result], JSON_UNESCAPED_SLASHES);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);
            die;
        }
    }

    ## Update General Information Of Mechanic
    public function updateMechanicsInformation()
    {
        $mechanicID = $this->param['mechanic_id'];

        if (isset($this->param['mechanic_picture']) && !empty($this->param['mechanic_picture'])) {
            $img = $this->param['mechanic_picture'];
            $imageData = $this->common_model->convertBase64ToImage($img);

            $data = $imageData['data'];
            $mime = $imageData['mime'];

            $image_name = 'uploads/mechanics/' . rand(1000, 5000) . '.' . $mime;
            file_put_contents($image_name, $data);
            chmod($image_name, 0777);
            $mechanics_array['mechanic_picture'] = ROOTPATH . $image_name;
        }

        if (isset($this->param['name']) && !empty($this->param['name'])) {
            $mechanics_array['name'] = $this->param['name'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Name is required.']);
            die;
        }

        if (isset($this->param['vehicle_registration_number']) && !empty($this->param['vehicle_registration_number'])) {
            $mechanics_array['vehicle_registration_number'] = $this->param['vehicle_registration_number'];
        } /*else {
        echo json_encode(['status' => false, 'message' => 'Vehicle registration is required.']);
        die;
        }*/

        if (isset($this->param['national_insurance']) && !empty($this->param['national_insurance'])) {
            $mechanics_array['national_insurance'] = $this->param['national_insurance'];
        } /*else {
        echo json_encode(['status' => false, 'message' => 'National Insurance is required.']);
        die;
        }*/

        if (isset($this->param['about_me']) && !empty($this->param['about_me'])) {
            $mechanics_array['about_me'] = $this->param['about_me'];
        }

        if (isset($this->param['hourly_rate']) && !empty($this->param['hourly_rate'])) {
            $mechanics_array['hourly_rate'] = $this->param['hourly_rate'];
        }

        if (isset($this->param['mechanic_dob']) && !empty($this->param['mechanic_dob'])) {
            $mechanics_array['mechanic_dob'] = $this->param['mechanic_dob'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Mechanic DOB is required.']);
            die;
        }

        if (isset($this->param['mechanic_address']) && !empty($this->param['mechanic_address'])) {
            $mechanics_array['mechanic_address'] = $this->param['mechanic_address'];

        } else {
            echo json_encode(['status' => false, 'message' => 'Mechanic Address is required.']);
            die;
        }

        if (isset($this->param['contact_number']) && !empty($this->param['contact_number'])) {

            $number_exist = $this->Mechanic_model->fields('contact_number')->get(['contact_number' => $this->param['contact_number']]);

            $alreadyContact_number = $this->Mechanic_model->fields('contact_number')->get(['contact_number' => $this->param['contact_number'], 'mechanic_id' => $mechanicID]);

            if (!empty($alreadyContact_number)) {
                $mechanics_array['contact_number'] = $this->param['contact_number'];

                if (isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                    $mechanics_array['country_code'] = $this->param['country_code'];
                }
            } elseif ($number_exist) {
                echo json_encode(['status' => false, 'message' => 'Contact number already exist.']);
                die;
            } else {
                $mechanics_array['contact_number'] = $this->param['contact_number'];

                if (isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                    $mechanics_array['country_code'] = $this->param['country_code'];
                }
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'Contact Number Required.']);
            die;
        }

        if (isset($this->param['email_id']) && !empty($this->param['email_id'])) {
            $alreadyEmail_id = $this->Mechanic_model->fields('email_id')->get(['email_id' => $this->param['email_id'], 'mechanic_id' => $mechanicID]);

            $email_id = $this->Mechanic_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);

            if (!empty($alreadyEmail_id)) {
                $mechanics_array['email_id'] = $this->param['email_id'];
            } else if ($email_id) {
                echo json_encode(['status' => false, 'message' => 'Email Id already exits.']);
                die;
            } else {
                $mechanics_array['email_id'] = $this->param['email_id'];
            }

        } else {
            echo json_encode(['status' => false, 'message' => 'EmailID is Required.']);
            die;
        }

        if (isset($this->param['longitude']) && isset($this->param['latitude'])) {
            $longitude = $this->param['longitude'];
            $latitude = $this->param['latitude'];

            if ($longitude != "" && $latitude != "") {
                $redis = createConnectionToRedis(); //For Redis Connection
                $redis->geoadd('mechanicSpotLatLng', $longitude, $latitude, $mechanicID);
                $mechanics_array['latitude'] = $this->param['latitude'];
                $mechanics_array['longitude'] = $this->param['longitude'];
            }
        }

        $update = $this->Mechanic_model->where(['mechanic_id' => $mechanicID])->update($mechanics_array);
        if ($update) {
            echo json_encode(['status' => true, 'message' => 'Update Successful.', 'response' => $mechanics_array], JSON_UNESCAPED_SLASHES);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    public function showAllDocumentOfMechanic()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(array('Mechanic_itineraries_model', 'Mechanic_service_model', 'Mechanic_document_model', 'Mechanic_experience_model', 'Mechanic_qualification_model', 'Mechanic_health_model'));
        $Itineraries = $this->Mechanic_itineraries_model->fields('itinerary_id')->get_all(['mechanic_id' => $id]);

        $tools = explode(',', $Itineraries[0]['itinerary_id']);

        $AllDocuments['Itineraries'] = $this->Mechanic_itineraries_model->getToolItinerary($tools);

        $health = $this->Mechanic_health_model->fields('health_id')->get_all(['mechanic_id' => $id]);
        $healthID = explode(',', $health[0]['health_id']);
        $AllDocuments['Health'] = $this->Mechanic_health_model->getToolHealth($healthID);

        $AllDocuments['Service'] = $this->Mechanic_service_model->fields('document_url')->get_all(['mechanic_id' => $id]);
        $AllDocuments['Documents'] = $this->Mechanic_document_model->fields('document_url')->get_all(['mechanic_id' => $id]);
        $AllDocuments['Experience'] = $this->Mechanic_experience_model->fields('experience_url')->get_all(['mechanic_id' => $id]);
        $AllDocuments['Qualification'] = $this->Mechanic_qualification_model->fields('qualification_url')->get_all(['mechanic_id' => $id]);

        if (!empty($AllDocuments)) {
            echo json_encode(['status' => true, 'message' => 'All Documents Here', 'response' => $AllDocuments]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicRegistrationInformation()
    {
        $id = $this->param['mechanic_id'];

        $basicinfo = ($this->param['basic_info'] == 'verify' ? 1 : ($this->param['basic_info'] == 'pending' ? 2 : 3));

        $service_status = ($this->param['service_status'] == 'verify' ? 1 : ($this->param['service_status'] == 'pending' ? 2 : 3));

        $documentation_status = ($this->param['documentation_status'] == 'verify' ? 1 : ($this->param['documentation_status'] == 'pending' ? 2 : 3));

        $experience_status = ($this->param['experience_status'] == 'verify' ? 1 : ($this->param['experience_status'] == 'pending' ? 2 : 3));

        $qualification_status = ($this->param['qualification_status'] == 'verify' ? 1 : ($this->param['qualification_status'] == 'pending' ? 2 : 3));

        $tools_status = ($this->param['tools_status'] == 'verify' ? 1 : ($this->param['tools_status'] == 'pending' ? 2 : 3));

        $health_safety = ($this->param['health_safety'] == 'verify' ? 1 : ($this->param['health_safety'] == 'pending' ? 2 : 3));
        $data = ['mechanic_id' => $id,
            'profile' => $basicinfo,
            'services' => $service_status,
            'documentations' => $documentation_status,
            'experience' => $experience_status,
            'qualification' => $qualification_status,
            'tool_itinerary' => $tools_status,
            'health_safety' => $health_safety,
        ];

        $this->load->model('Mechanic_registrations_model');
        $result = $this->Mechanic_registrations_model->insert($data);
        if ($result) {
            echo json_encode(['status' => true, 'message' => 'Document Verified Successfully!!']);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Document Not Verified!!']);die;
        }
    }

    # IsEmail check that user name is email_id or contact_number.
    private function IsEmail($user_name)
    {
        # If the username input string is an e-mail, return true
        if (filter_var($user_name, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    public function mechanicsLogin()
    {
        $this->load->library('encrypt');
        if (isset($this->param['device_key'])) {
            $data['device_key'] = $this->param['device_key'];
        } else {
            $data['device_key'] = '';
        }

        if (isset($this->param['isProduction'])) {
            $data['isProduction'] = $this->param['isProduction'];
        } else {
            $data['isProduction'] = '';
        }

        if (isset($this->param['platform_type'])) {
            $data['platform_type'] = $this->param['platform_type'];
        } else {
            $data['platform_type'] = '';
        }

        if (isset($this->param['email_id']) && !empty($this->param['email_id'])) {
            $user_name = $this->param['email_id'];
        }

        $check_email = $this->IsEmail($user_name);
        if ($check_email) {
            # email & password combination
            $where = ["email_id" => $user_name];
        } else {
            # phone_no & password combination
            $where = ["contact_number" => $user_name];
        }

        $data['where'] = $where;
        if (isset($this->param['name'])) {
            $data['name'] = $this->param['name'];
        }

        if (isset($this->param['password']) && !empty($this->param['password'])) {
            $data['password'] = $this->param['password'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Password field is required']);die;
        }

        # Call function to login
        $response = $this->doLogin($data);
        if ($response) {
            echo json_encode(['status' => true, 'message' => 'Successfully Login', 'response' => $response]);
        } else {
            echo json_encode(['status' => false, 'message' => 'Failed in process ']);
        }

    }

    private function doLogin($data)
    {
        $this->load->model(['Mechanic_model']);
        $mechanic = $this->Mechanic_model->fields(['mechanic_id', 'password', 'name', 'email_id', 'contact_number', 'mobVerified', 'isBlocked', 'isOnline', 'platform_type', 'isProduction'])
            ->where($data['where'])->get();

        if (!empty($mechanic)) {
            if ($mechanic['isBlocked'] == 101) {
                echo json_encode(['status' => false, "error" => ['message' => 'Mechanic is Blocked']]);die;
            }
            if ($mechanic['isOnline'] == 101) {
                $this->Mechanic_model->where('mechanic_id', $mechanic['mechanic_id'])->update(['isOnline' => 100]);
            }

            if (isset($data['platform_type'])) {
                $update_user_data['platform_type'] = $this->param['platform_type'];
            }

            if (isset($data['isProduction'])) {
                $update_user_data['isProduction'] = $this->param['isProduction'];
            }

            if (isset($data['device_key'])) {
                $update_user_data['device_key'] = $this->param['device_key'];
            }

            if (!empty($update_user_data)) {
                $this->Mechanic_model->where('mechanic_id', $mechanic['mechanic_id'])->update($update_user_data);
            }

            if (!isset($this->param['password']) || $this->param['password'] == null) {
                echo json_encode(['status' => false, 'message' => 'Password field is required']);die;
            }
            $password = $this->param['password'];
            $decrypted_password = $this->encrypt->decode($mechanic['password']);
            $login_status = ($decrypted_password == $password) ? true : false;
            if ($login_status === true) {
                # Mechanics response
                $response = $this->loginResponse($mechanic['mechanic_id']);
                $isAppVerified = $this->isAppVerified($mechanic['mechanic_id']);
                $response['isAppVerified'] = $isAppVerified;

                # Update Status
                $this->checkDeviceToken($data['device_key']);
                $if_update = $this->Mechanic_model->where('mechanic_id', $mechanic['mechanic_id'])->update(['isOnline' => 101, 'device_key' => $data['device_key']]);
                if ($if_update) {
                    // $this->session->set_userdata('is_loggedIn', $response);
                    echo json_encode(['status' => true, 'message' => 'Successfully Login', 'response' => $response]);die;
                } else {
                    echo json_encode(['status' => false, 'message' => 'Failed In Process']);die;
                }
            } else {
                echo json_encode(['status' => false, 'message' => 'User name or password is incorrect']);die;
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'User Not Found']);die;
        }
    }

    private function loginResponse($id)
    {
        $this->load->model('Mechanic_model');
        $some = ['mechanic_id', 'name', 'email_id', 'country_code', 'contact_number', 'mechanic_uuid', 'mobVerified', 'otp', 'mechanic_dob', 'mechanic_address'];
        $mechanic = $this->Mechanic_model->fields($some)->get($id);
        if ($mechanic) {
            $this->load->model('Key_model');
            $keys = $this->Key_model->fields('key')->get(['mechanic_id' => $mechanic['mechanic_id']]); // it will be userwise

            $session_key = $this->GenrateKey($mechanic['name']);
            if(empty($keys)) {
                echo json_encode(['status' => false, 'message' => 'Failed In Process']);die;
            }

            $mechanicId = $mechanic['mechanic_id'];

            //Update Auth Key
            $updateKey = $this->Key_model->where('mechanic_id', $mechanicId)->update(['key' => $session_key]);

            if ($updateKey) {
                
                $getKeys = $this->Key_model->fields('key')->get(['mechanic_id' => $mechanicId]); // it will be userwise
                $mechanic['X-API-KEY'] = isset($getKeys['key']) ? $getKeys['key'] : '';
                $mechanic['api_key'] = "PATCHERAUTHKEY";
                $mechanic['api_value'] = isset($getKeys['key']) ? $getKeys['key'] : '';
            }

            $mechanic['patcher_channel'] = 'User';
            $mechanic['pubnub_publish_key'] = PUBNUB_PUB_KEY;
            $mechanic['pubnub_subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;
            $mechanic['pubnub_secret_key'] = PUBNUB_SECRET_KEY;
            $mechanic['stripe_key'] = $this->_isproduction();


            // $user['publish_key'] = PUBNUB_PUB_KEY;
            // $user['subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;

            return $mechanic;
        } else {
            return false;
        }
    }

    /*----------------------------------------------------------------------------------------
    |  CHANGE PASSWORD
    |-----------------------------------------------------------------------------------------
    |  It is responsable for change user password.
    |-----------------------------------------------------------------------------------------
     */
    public function ChangePassword()
    {
        $this->load->model(['Mechanic_model']);
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('ChangePasswordMechanic')) {
            $mechanic_id = $this->param['mechanic_id'];
            $previous_password = $this->param['old_password'];
            $new_password = $this->param['new_password'];

            $mechanic = $this->Mechanic_model->fields('password')->get($mechanic_id);
            $decrypted_password = $this->encrypt->decode($mechanic['password']);
            if ($decrypted_password == $previous_password) {
                $encrypt_newpwd = $this->encrypt->encode($new_password);
                $data = ['password' => $encrypt_newpwd];
                $update = $this->Mechanic_model->where('mechanic_id', $mechanic_id)->update($data);
                if ($update) {
                    echo json_encode(['status' => true, 'message' => 'Successfully Changed Password']);die;
                } else {
                    echo json_encode(['status' => false, 'message' => 'Failed In Process']);die;
                }
            } else {
                echo json_encode(['status' => false, 'message' => 'Previous password in incorrect']);die;
            }
        } else {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }
    }

    /*----------------------------------------------------------------------------------------
    |  FORGOT PASSWORD
    |-----------------------------------------------------------------------------------------
    |  It is responsible for send otp to Mechanic mob.no. to reset password.
    |-----------------------------------------------------------------------------------------
     */
    public function ForgotPassword()
    {
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('ForgotPassword')) {

            $this->load->library('nexmo');
            $contact_number = $this->param['contact_number'];

            $mechanic = $this->Mechanic_model->fields('mechanic_id,country_code,email_id')->get(['contact_number' => $contact_number]);

            if ($mechanic) {
                # Generate OTP
                //$otp =  mt_rand(1001,9999);
                $otp = mt_rand(1001, 9999);
                // $otp = '2222';
                $where = ['mechanic_id' => $mechanic['mechanic_id']];
                $date = date('Y-m-d H:i:s');
                $update = $this->Mechanic_model->where($where)->update(['otp' => $otp, 'otp_created' => $date]);
                if ($update) {

                    $otpMesssage = '<b>'.$otp.'</b>' . ' is your one-time verification code, which is valid for the next 10 minutes. Please do not share it with anyone.';
                    // $to = $mechanic['country_code'].$contact_number;
                    // $message = array('text' => $msg);
                    $this->load->helper('api');
                    //$send_otp = SendOtp($to,$message); //print_r($send_otp);die;
                    $otpSend = $this->nexmo->send_message("Patcher", '44' . $contact_number, ["text" => $otpMesssage]);

                    $args = [
                        'message' => $otpMesssage,
                        'subject' => 'Patcher: Forgot Password OTP',
                        'email_id' => $mechanic['email_id'],
                        'template_name' => 'Sign Up',
                        'vars' => [
                            [
                                'name' => 'message',
                                'content' => $otpMesssage,
                            ],
                        ],
                    ];

                    $mailResponse = sendMandrillEmailTemlate($args);

                    $send_otp = true;
                    if ($send_otp) {
                        echo json_encode(["status" => true, "message" => "OTP sent to your mobile number", "response" => ["otp" => "$otp", 'mechanic_id' => $mechanic['mechanic_id']]]);die;
                    } else {
                        echo json_encode(["status" => true, "message" => "Failed To Send OTP"]);die;
                    }
                } else {
                    echo json_encode(["status" => true, "message" => "Failed in process"]);die;
                }
            } else {
                echo json_encode(["status" => false, "message" => "User Not found "]);die;
            }
        } else {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }
    }

    /*----------------------------------------------------------------------------------------
    |  RESET PASSWORD
    |-----------------------------------------------------------------------------------------
    |  It is responsable for reset passwprd.
    |-----------------------------------------------------------------------------------------
     */
    public function ResetPassword()
    {
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('ResetPasswordMech')) {
            $new_password = $this->param['new_password'];
            $mechanic_id = $this->param['mechanic_id'];
            # Fetch OTP
            $mechanic = $this->Mechanic_model->fields('mechanic_id,otp,otp_created')->get(['mechanic_id' => $mechanic_id]);
            if ($mechanic) {
                $encrypt_pwd = $this->encrypt->encode($new_password);
                $update = $this->Mechanic_model->where('mechanic_id', $mechanic_id)->update(['password' => $encrypt_pwd]);
                if ($update) {
                    echo json_encode(['status' => true, 'message' => 'Successfully Reset Password']);die;
                } else {
                    echo json_encode(['status' => false, 'message' => 'Failed In Process']);die;
                }
            } else {
                echo json_encode(['status' => false, 'message' => 'Otp is incorrect']);die;
            }
        } else {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }
    }

    /*----------------------------------------------------------------------------------------
    |  LOGOUT
    |-----------------------------------------------------------------------------------------
    |  It is used for logout mechanic.
    |-----------------------------------------------------------------------------------------
     */
    public function mechaniclogout()
    {
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('mechaniclogout')) {
            $this->load->model('Mechanic_model');
            $mechanic_id = $this->param['mechanic_id'];
            // $this->session->unset_userdata('is_loggedIn');
            $logout = $this->Mechanic_model->where('mechanic_id', $mechanic_id)->update(['isOnline' => 100]);
            if ($logout) {
                $redis = createConnectionToRedis();
                $redis->set($mechanic_id, 'OFFLINE');
                echo json_encode(['status' => true, 'message' => 'Successfully Logout']);die;
            } else {
                echo json_encode(['status' => false, 'message' => 'Failed In Logout Process']);die;
            }
        } else {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }
    }

    private function checkDeviceToken($deviceId)
    {
        $this->load->model('Mechanic_model');
        $redis = createConnectionToRedis();
        $isExists = $this->Mechanic_model->fields('mechanic_id')->get_all(['device_key' => $deviceId]);
        if ($isExists) {
            foreach ($isExists as $mechanic) {
                $redis->set($mechanic['mechanic_id'], 'OFFLINE');
                // $redis->zrem('mechanics', $mechanic['mechanic_id']);
            }
            $this->Mechanic_model->where(['device_key' => $deviceId])->update(['device_key' => "", "isOnline" => 100]);
        } else {
            return true;
        }
    }

    private function isAppVerified($id)
    {
        $this->load->model('Mechanic_registrations_model');
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        return $isAppVerified;
    }

    public function signupOTP()
    {
        $this->form_validation->set_data($this->param);
        if (!$this->form_validation->run('signupOTP')) {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }
        $this->load->library('Nexmo');

        $otp = mt_rand(1000, 9999);
        $otpMesssage = $otp . ' is your one time SignUp verification code, Valid for only 10 minutes. Please do not share it with any one.';

        $otpSend = $this->nexmo->send_message("Patcher", '91' . $this->param['contact_number'], ["text" => $otpMesssage]);

        $args = [
            'message' => $otpMesssage,
            'subject' => 'Patcher: OTP Verifiction',
            'email_id' => $this->param['email_id'],
            'template_name' => 'Sign Up',
            'vars' => [
                [
                    'name' => 'message',
                    'content' => $otpMesssage,
                ],
            ],
        ];
        $mailResponse = sendMandrillEmailTemlate($args);

        echo json_encode(["status" => true, "message" => "OTP Send to your cotact details.", "otp" => $otp]);die;
    }

    public function signup()
    {
        $this->form_validation->set_data($this->param);
        if (!$this->form_validation->run('signupOTP')) {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }

        if (isset($this->param['isProduction']) && isset($this->param['device_key'])) {
            $mechanics_array = [
                'name' => $this->param['name'],
                'platform_type' => (isset($this->param['platform_type'])) ? $this->param['platform_type'] : 1,
                'isProduction' => (isset($this->param['isProduction']) ? $this->param['isProduction'] : 101),
                'device_key' => $this->param['device_key'],
            ];
        } else {
            $mechanics_array = [
                'name' => $this->param['name'],
                'platform_type' => (isset($this->param['platform_type'])) ? $this->param['platform_type'] : 3,
                'isProduction' => (isset($this->param['isProduction']) ? $this->param['isProduction'] : 101),
                'device_key' => (isset($this->param['device_key'])) ? $this->param['device_key'] : 0000,
            ];
        }

        if (isset($this->param['email_id']) && !empty($this->param['email_id'])) {
            $email_id = $this->Mechanic_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);
            if ($email_id) {
                echo json_encode(['status' => false, 'message' => 'Email Id already exits.']);
                die;
            }
            $mechanics_array['email_id'] = $this->param['email_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'EmailID is Required.']);
            die;
        }

        if (isset($this->param['contact_number']) && !empty($this->param['contact_number'])) {
            $number_exist = $this->Mechanic_model->fields('contact_number')->get(['contact_number' => $this->param['contact_number']]);
            if ($number_exist) {
                echo json_encode(['status' => false, 'message' => 'Contact number already exist.']);
                die;
            }
            $mechanics_array['contact_number'] = $this->param['contact_number'];
            if (isset($this->param['country_code']) && !empty($this->param['country_code'])) {
                $mechanics_array['country_code'] = $this->param['country_code'];
            }
        } else {
            echo json_encode(['status' => false, 'message' => 'Contact Number Required.']);
            die;
        }

        if (isset($this->param['password']) && !empty($this->param['password'])) {
            $mechanics_array['password'] = $this->encrypt->encode($this->param['password']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Password is required.']);
            die;
        }

        if (isset($this->param['platform_type']) && $this->param['platform_type'] == '3') {
            if (isset($this->param['mechanic_dob']) && !empty($this->param['mechanic_dob'])) {
                $mechanics_array['mechanic_dob'] = $this->param['mechanic_dob'];
            }

            if (isset($this->param['mechanic_address']) && !empty($this->param['mechanic_address'])) {
                $mechanics_array['mechanic_address'] = $this->param['mechanic_address'];
            }
        }

        $session_key = $this->GenrateKey($mechanics_array['name']);
        $this->db->trans_start();
        # Create user channel

        $this->load->library('mypubnub');
        $uuid = $this->mypubnub->uuid();

        if (isset($this->param['mobVerified']) && !empty($this->param['mobVerified'])) {
            $mechanics_array['mobVerified'] = $this->param['mobVerified'];
        }

        $mechanics_array['mechanic_uuid'] = $uuid;

        $mechanic_id = $this->Mechanic_model->insert($mechanics_array);
        if ($mechanic_id) {
            $this->Mechanic_registrations_model->insert(['mechanic_id' => $mechanic_id]);
        }
        # Insert Session Key In key Table.
        $key_parameters = [
            'key' => $session_key,
            'mechanic_id' => $mechanic_id,
            'user_type' => 2,
        ];
        $key_id = $this->InsertKey($key_parameters);

        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $result = $this->UserResponse($mechanic_id);

            #Send Welcome Mail
            $welcomeArgs = [
                'subject' => 'Welcome to Patcher',
                'email_id' => $mechanics_array['email_id'],
                'template_name' => 'Welcome Mechanic',
            ];
            $welcomeMailResponse = sendMandrillEmailTemlate($welcomeArgs);

            $this->db->trans_commit();

            echo json_encode(['status' => true, 'message' => 'Signup Successful.', 'response' => $result], JSON_UNESCAPED_SLASHES);die;
        } else {
            $this->db->trans_rollback();
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }

    }

    private function _isproduction(){
        $data = $this->Constant_model->get(['keyname' => 'isProduction']);

        if($data){
            if($data['value'] == "true"){
                return PROD_STRIPE_API_KEY;
            } else {
                return TEST_STRIPE_API_KEY;
            }
        } else {
            return TEST_STRIPE_API_KEY;
        }
    }

}
