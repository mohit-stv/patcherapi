<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experience extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addExperiences_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addExperiences') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Experiences_model']);             
            if(isset($this->param['experiences']) && $this->param['experiences'] != NUll){
                for($i=0;$i<count($this->post('experiences'));$i++){             
                    $experiences_array=[
                        'experienceName' => $this->param['experiences'][$i]['experienceName'],
                        'experienceStatus' => $this->param['experiences'][$i]['experienceStatus']       
                    ];
                    $insert = $this->Experiences_model->insert($experiences_array);
                }
            }
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editExperiences_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editExperiences') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Experiences_model']);             
            if(isset($this->param['experiences']) && $this->param['experiences'] != NUll){
                for($i=0;$i<count($this->post('experiences'));$i++){               
                    $experiences_array=[
                        'experienceName' => $this->param['experiences'][$i]['experienceName']       
                    ];
                    $update = $this->Experiences_model->where('experienceId',$this->param['experiences'][$i]['experienceId'])->update($experiences_array);
                }
            }
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function allExperience_post() {
    	
    	$this->load->model(['Experience_model']);              	
		
        $experiences   =  $this->Experience_model->fields('experience_id,experience_name')
                                                         ->where('experience_status','101')->get_all();

        if($experiences){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => $experiences]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteExperiences_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteExperiences') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
        	$experienceId = $this->param['experienceId'];

			$updateOptions = array(
                'where' => array('experienceId' => $experienceId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'experiences'
            );
            $deleteExperience = $this->common_model->customUpdate($updateOptions);

	        if($deleteExperience){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteExperience]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}