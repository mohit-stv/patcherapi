<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterdata extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['Service_model','Common_model','Document_model','Experience_model','Health_safety_check_model',
                    'Tool_itinerary_model','Issue_model','Question_model',
                    'Emergency_tip_model']);
    }

    public function service_post() {
     //    $status = $this->param['status']; // For delete the service
        if(isset($this->param['service_name']) && $this->param['service_name'] != '') {
            $data = ['service_name'=>$this->param['service_name'],
            'service_description'=>$this->param['service_description'],
            'parent_id'=>(isset($this->param['parent_id']) ? $this->param['parent_id'] : 0)];
         
            if(isset($this->param['service_id']) && $this->param['service_id'] != '') {
                $update = $this->Service_model->update($data, $this->param['service_id']);
            } else {
                $update = $this->Service_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['service_id'])) {
            $data = ['service_status'=>$this->param['status']];
            $update = $this->Service_model->update($data, $this->param['service_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter Status Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }

    public function getService_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('services')->count_all_results();

        $service = $this->Service_model->fields('service_id,service_name,parent_id,service_description,service_status')->limit($limit,$offset)->get_all();
        if($service) {
            $this->response(['status' => true, 'message'=> 'All Services Here.','response' => ['count'=>$count, 'result'=>$service]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function document_post() {
        if(isset($this->param['document_name']) && $this->param['document_name'] != '') {
            $data = ['document_name'=>$this->param['document_name'],
                    'document_description'=>$this->param['document_description'],
                    'isOptional'=>101];

            if(isset($this->param['document_id']) && $this->param['document_id'] != '') {
                $update = $this->Document_model->update($data, $this->param['document_id']);
            } else {
                $update = $this->Document_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['document_id'])) {
            $data = ['document_status'=>$this->param['status']];
            $update = $this->Document_model->update($data, $this->param['document_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }

    public function getDocument_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('documents')->count_all_results();

        $service = $this->Document_model->fields('document_id,document_name,document_description,document_status')->limit($limit,$offset)->get_all();
        if($service) {
            $this->response(['status' => true, 'message'=> 'All Documents Here.','response' => ['count'=>$count, 'result'=>$service]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    
    public function experience_post() {
        if(isset($this->param['experience_name']) && $this->param['experience_name'] != '') {
            $data = ['experience_name'=>$this->param['experience_name'],
                    'experience_description'=>$this->param['experience_description'],
                    'isUpload'=>$this->param['isUpload'], 
                    'isOptional'=>$this->param['isOptional']];

            if(isset($this->param['experience_id']) && $this->param['experience_id'] != '') {
                $update = $this->Experience_model->update($data, $this->param['experience_id']);
            } else {
                $update = $this->Experience_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['experience_id'])) {
            $data = ['experience_status'=>$this->param['status']];
            $update = $this->Experience_model->update($data, $this->param['experience_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getExperience_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('experiences')->count_all_results();

        $experience = $this->Experience_model->fields('experience_id,experience_name,experience_description,experience_status')->limit($limit,$offset)->get_all();
        if($experience) {
            $this->response(['status' => true, 'message'=> 'All Experience Here.','response' => ['count'=>$count, 'result'=>$experience]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function healthSafety_post() {
        if(isset($this->param['health_safety_name']) && $this->param['health_safety_name'] != '') {
            $data = ['health_safety_name'=>$this->param['health_safety_name'],
                    'health_safety_description'=>$this->param['health_safety_description']];

            if(isset($this->param['health_safety_check_id']) && $this->param['health_safety_check_id'] != '') {
                $update = $this->Health_safety_check_model->update($data, $this->param['health_safety_check_id']);
            } else {
                $update = $this->Health_safety_check_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['health_safety_check_id'])) {
            $data = ['health_safety_check_status'=>$this->param['status']];
            $update = $this->Health_safety_check_model->update($data, $this->param['health_safety_check_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getHealthSafety_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('health_safety_checks')->count_all_results();

        $healthSafety = $this->Health_safety_check_model->fields('health_safety_check_id,health_safety_name,health_safety_description,health_safety_check_status')->limit($limit,$offset)->get_all();
        if($healthSafety) {
            $this->response(['status' => true, 'message'=> 'All Health Safety Here.','response' => ['count'=>$count, 'result'=>$healthSafety]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

  
    public function toolItinerary_post() {
        if(isset($this->param['service_id']) && isset($this->param['tool_itinerary_name']) && $this->param['tool_itinerary_name'] != '') {
            $id = $this->param['service_id'];
            $data = ['service_id'=>$id,
                    'tool_itinerary_name'=>$this->param['tool_itinerary_name'],
                    'tool_description'=>$this->param['tool_description']];

            if(isset($this->param['tool_itinerary_id']) && $this->param['tool_itinerary_id'] != '') {
                $update = $this->Tool_itinerary_model->update($data, $this->param['tool_itinerary_id']);
            } else {
                $update = $this->Tool_itinerary_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['issue_id'])) {
            $data = ['tool_itinerary_status'=>$this->param['status']];
            $update = $this->Tool_itinerary_model->update($data, $this->param['issue_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }

    public function getToolItinerary_post() {
        $id = $this->param['service_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('tool_itineraries')->where('service_id', $id)->count_all_results();
        
        $toolItinerary = $this->Tool_itinerary_model->fields('tool_itinerary_name,tool_description,tool_itinerary_status')
        ->with_service('fields:service_name,parent_id,service_status')
        ->limit($limit,$offset)->where('service_id',$id)->get_all();
    
        if($toolItinerary) {
            $this->response(['status' => true, 'message'=> 'All Tool Itinerary Here.','response' => ['count'=>$count, 'result'=>$toolItinerary]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function issue_post() {
        if(isset($this->param['issue_name']) && $this->param['issue_name'] != '') {
            $data = ['issue_name'=>$this->param['issue_name'],
                    'issuse_price'=>$this->param['issuse_price']];

            if(isset($this->param['issue_id']) && $this->param['issue_id'] != '') {
                $update = $this->Issue_model->update($data, $this->param['issue_id']);
            } else {
                $update = $this->Issue_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['issue_id'])) {
            $data = ['issue_status'=>$this->param['status']];
            $update = $this->Issue_model->update($data, $this->param['issue_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getissue_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('issues')->count_all_results();

        $issue = $this->Issue_model->fields('issue_id,issue_name,issuse_price,issue_status')->limit($limit,$offset)->get_all();
        if($issue) {
            $this->response(['status' => true, 'message'=> 'All Repair issues Here.','response' => ['count'=>$count, 'result'=>$issue]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }



    public function question_post() {
        if(isset($this->param['question']) && $this->param['question'] != '') {
            $data = ['question'=>$this->param['question'],
                    'question_description'=>$this->param['question_description'],
                    'question_type'=>$this->param['question_type']];

            if(isset($this->param['question_id']) && $this->param['question_id'] != '') {
                $update = $this->Question_model->update($data, $this->param['question_id']);
            } else {
                $update = $this->Question_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['question_id'])) {
            $data = ['question_status'=>$this->param['status']];
            $update = $this->Question_model->update($data, $this->param['question_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getQuestion_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('questions')->count_all_results();

        $question = $this->Question_model->fields('question_id,question,question_description,question_type,question_status')->limit($limit,$offset)->get_all();
        if($question) {
            $this->response(['status' => true, 'message'=> 'All Question Here.','response' => ['count'=>$count, 'result'=>$question]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function emergencyTip_post() {
        if(isset($this->param['tip']) && $this->param['tip'] != '') {
            $data = ['tips'=>$this->param['tip']];

            if(isset($this->param['emergency_tips_id']) && $this->param['emergency_tips_id'] != '') {
                $update = $this->Emergency_tip_model->update($data, $this->param['emergency_tips_id']);
            } else {
                $update = $this->Emergency_tip_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['emergency_tips_id'])) {
            $data = ['status'=>$this->param['status']];
            $update = $this->Emergency_tip_model->update($data, $this->param['emergency_tips_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getEmergencyTip_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('emergency_tips')->count_all_results();

        $tip = $this->Emergency_tip_model
        ->fields('emergency_tips_id,tips,status')
        ->limit($limit,$offset)->get_all();

        foreach($tip as $tips) {
            $response[] = [
                            'emergency_tips_id' => $tips['emergency_tips_id'],
                            'tips' => $tips['tips'],
                            'emergency_status' => $tips['status']
                          ];
        }

        if(!empty($response)) {
            $this->response(['status' => true, 'message'=> 'All Emergency Tips Here.','response' => ['count'=>$count, 'result'=>$response]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function cancelReason_post() {
        $this->load->model('Cancel_reason_model');
        if(isset($this->param['reason']) && $this->param['reason'] != '') {
            $data = ['user_type'=>$this->param['user_type'],
                    'reason'=>$this->param['reason'],
                    'request_type'=>$this->param['request_type']];

            if(isset($this->param['cancel_reason_id']) && $this->param['cancel_reason_id'] != '') {
                $update = $this->Cancel_reason_model->update($data, $this->param['cancel_reason_id']);
            } else {
                $update = $this->Cancel_reason_model->insert($data);
            }
        } elseif(isset($this->param['status']) == 100 && isset($this->param['cancel_reason_id'])) {
            $data = ['status'=>$this->param['status']];
            $update = $this->Cancel_reason_model->update($data, $this->param['cancel_reason_id']);
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }


    public function getCancelReason_post() {
        $this->load->model('Cancel_reason_model');
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('cancel_reasons')->count_all_results();

        $cancelReason = $this->Cancel_reason_model->fields('cancel_reason_id,user_type,reason,request_type,status')->limit($limit,$offset)->get_all();
        if($cancelReason) {
            $this->response(['status' => true, 'message'=> 'All Cancel reason Here.','response' => ['count'=>$count, 'result'=>$cancelReason]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    public function callOutFee_post() {
        $this->load->model('Constant_calloutfee_model');

        if(isset($this->param['value']) && $this->param['value'] != '') {
            $data = ['value'=>$this->param['value'],
                    'request_type'=>$this->param['request_type']];

            if(isset($this->param['constant_calloutfee_id']) && $this->param['constant_calloutfee_id'] != '') {
                $update = $this->Constant_calloutfee_model->update($data, $this->param['constant_calloutfee_id']);
            } else {
                $update = $this->Constant_calloutfee_model->insert($data);
            }
        } else {
            $this->response(['status' => false, 'message' => 'Please Enter The Field..']);die;
        }

        if($update) {
            $this->response(['status' => true, 'message'=> 'Updated Successfully.','response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Updated']);die;
        }
    }

    public function dashboard_post(){

        $today = date('Y-m-d');
        $oneMonth = date('Y-m-d',(strtotime($today) - 30*24*3600));
        $weekDate = date('Y-m-d',(strtotime($today) - 7*24*3600));

        $this->db->select_sum('amount');
        $this->db->from('transactions');
        $this->db->where('transaction_type = 2 AND paid_status = 1');
        $totalPayment = $this->db->get()->row_array();

        $this->db->select_sum('amount');
        $this->db->from('transactions');
        $this->db->where("transaction_type = 2 AND paid_status = 1 AND created_at BETWEEN '$oneMonth' AND '$today' ");
        $totalOneMonth = $this->db->get()->row_array();

        $this->db->select_sum('amount');
        $this->db->from('transactions');
        $this->db->where("transaction_type = 2 AND paid_status = 1 AND created_at BETWEEN '$weekDate' AND '$today'");
        $totalOneWeek = $this->db->get()->row_array();

        $this->db->select('COUNT(mechanic_id)  as total');
        $this->db->from('mechanics');
        $totalMechanics = $this->db->get()->row_array();

        $this->db->select('COUNT(request_id) as total');
        $this->db->from('requests');
        $totalRequests = $this->db->get()->row_array();

        $this->db->select('COUNT(user_id)  as total');
        $this->db->from('users');
        $totalUsers = $this->db->get()->row_array();

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("isChargable = 100 AND paid_status = 100");
        $totalPendingPayments = $this->db->get()->row_array();

        $response = [
            'totalPayment' => (!empty($totalPayment['amount'])) ? $totalPayment['amount'] : 0,
            'totalOneMonth' => (!empty($totalOneMonth['amount'])) ? $totalOneMonth['amount'] : 0,
            'totalOneWeek' => (!empty($totalOneWeek['amount'])) ? $totalOneWeek['amount'] : 0,
            'totalRequests' => (!empty($totalRequests['total'])) ? $totalRequests['total'] : 0,
            'totalMechanics' => (!empty($totalMechanics['total'])) ? $totalMechanics['total'] : 0,
            'totalUsers' => (!empty($totalUsers['total'])) ? $totalUsers['total'] : 0,
            'totalPendingPayments' => (!empty($totalPendingPayments['comission'])) ? round($totalPendingPayments['comission'],2) : 0,
        ];

        $this->response(['status' => true,'details'=> $response]);
    }

    public function getToolItineraryList_post() {
        
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];

        $count = $this->db->from('tool_itineraries')->count_all_results();
        $toolItinerary = $this->Tool_itinerary_model->fields('tool_itinerary_id,tool_itinerary_name,tool_description,tool_itinerary_status')
        ->with_service('fields: service_id,service_name,parent_id,service_status')
        ->limit($limit,$offset)
        ->get_all();
    
        if($toolItinerary) {
            $toolList = [];
            $serviceList = [];
            $serviceIds = [];
            foreach($toolItinerary as $tools){
                $serviceId = $tools['service']['service_id'];
                if(!in_array($serviceId,$serviceIds)){
                    $serviceList[] = $tools['service'];
                    $serviceIds[] = $serviceId;
                }
                
                $toolList[] = [
                    'tool_itinerary_id' => $tools['tool_itinerary_id'],
                    'tool_itinerary_name' => $tools['tool_itinerary_name'],
                    'tool_description' => $tools['tool_description'],
                    'tool_itinerary_status' => $tools['tool_itinerary_status'],
                    'service_id' => $serviceId,
                    'service_name' => $tools['service']['service_name']
                ];
            }

            $toolItineraries = [
                'services' => $serviceList,
                'tool_itineraries' => $toolList
            ];

            $this->response(['status' => true, 'message'=> 'All Tool Itinerary Here.','response' => ['count'=>$count, 'result'=> $toolItineraries]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    public function addUpdateToolItinerary_post(){

        $this->db->trans_start();
        if(isset($this->param['tool_itinerary_id']) && !empty($this->param['tool_itinerary_id'])){
            $toolData = $this->param;
            $this->form_validation->set_data($this->param);   
            if (!$this->form_validation->run('updateToolItinerary')) {
                $message = $this->form_validation->error_array();
                $response = array('status' => FALSE, 'message' => $message);
                $this->response($response, 400);
            }

            $toolItineraryUpdate = $this->Tool_itinerary_model->update($toolData,'tool_itinerary_id');
            // $toolItineraryId = $this->param['tool_itinerary_id'];
        } else {
            $this->form_validation->set_data($this->param);   
            if (!$this->form_validation->run('addToolItinerary')) {
                $message = $this->form_validation->error_array();
                $response = array('status' => FALSE, 'message' => $message);
                $this->response($response, 400);
            }
            $toolData = $this->param;
            $toolItineraryId = $this->Tool_itinerary_model->insert($toolData);
            $toolData['tool_itinerary_id'] = $toolItineraryId;
        }

        $this->db->trans_complete();
        if($this->db->trans_status()){
            $this->db->trans_commit();
            $this->response(['status' => true,'message' => 'Details Update.','data' => $toolData]);
        }else{
            $this->db->trans_rollback();
            $this->response(['status' => false,'message' => 'Something went wrong. Please try again later.']);
        }
    }

    public function updateToolItineraryStatus_post(){

        if(!isset($this->param['tool_itinerary_id']) || empty($this->param['tool_itinerary_id'])){
            $this->response(['status' => false, 'message' => 'Tool Itinerary Id is mandatory']);die;
        } 
        
        if(!isset($this->param['tool_itinerary_status']) || empty($this->param['tool_itinerary_status'])){
            $this->response(['status' => false, 'message' => 'Tool Itinerary Status']);die;
        } 

        $toolData = $this->param;

        $this->db->trans_start();
        $toolItineraryUpdate = $this->Tool_itinerary_model->update($toolData,'tool_itinerary_id');

        $this->db->trans_complete();
        if($this->db->trans_status()){
            $this->db->trans_commit();
            $this->response(['status' => true,'message' => 'Details Update.','data' => $toolData]);
        }else{
            $this->db->trans_rollback();
            $this->response(['status' => false,'message' => 'Something went wrong. Please try again later.']);
        }
    }

}