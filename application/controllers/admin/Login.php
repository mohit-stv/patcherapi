<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Content-Type:application/json");

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['Admin_model']);
    }

    public function index(){
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('adminLogin')){
            $this->load->model('Key_model');
            $email_id = $this->param['email_id'];
            $password = $this->param['password'];
            $admin = $this->Admin_model->fields('admin_id,name,email_id,password')->get(['email_id' => $email_id]);

            if($admin){
            
                $this->load->library('encrypt');
                $decrypt_password = $this->encrypt->decode($admin['password']); 
                if($decrypt_password == $password){
                    /*key update*/
                    $this->load->config('rest');
                    $keyAPI =   $this->config->item('rest_key_name');
                    $keyValue  = md5(microtime().rand());

                    $key_data = ['key' => $keyValue]; 
                    $key_where = ['user_id' => $admin['admin_id'], 'user_type' => 3];
                    $this->Key_model->where($key_where)->update($key_data); 
                     
                    $admin['authentication']= ['key' => $keyAPI,'value' => $keyValue];

                    unset($admin['password']);
                    echo json_encode(['status' => true, 'message' => 'Successfully login','response' => $admin]);die;
                }else{
                    echo json_encode(['status' => false, 'message' => 'Password is incorrect']);die;
                }  
            }else{
                echo json_encode(['status' => false, 'message' => 'User name is incorrect']);die;
            }
        }else{
            echo json_encode(["status" => false, "message" => $this->form_validation->get_errors_as_array()]);die;
        }
    }

}