<?php defined('BASEPATH') or exit('No direct script access allowed');
class Cronjob extends CI_Controller
{
    private $param = [];
    public function __construct()
    {
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(array('common_model', 'Question_model', 'Answer_model'));
        $this->load->helper('common');
        $this->load->library(['Mypubnub','Notification']);
    }

    public function requestStatus(){

        $this->load->library('Notification');
        $this->load->model(['Request_model','User_model','Mechanic_model','Transaction_model','Mechanic_rating_model']);

        $currnet_DT = date("Y-m-d H:i:s", strtotime("-2 minutes"));
        
        $requests = $this->Request_model
        ->with_user('fields: device_key,uuid,platform_type,isProduction')
        ->with_mechanic('fields:mechanic_id,name,mechanic_address,contact_number,mechanic_uuid,country_code,mechanic_picture,platform_type,device_key,isProduction', 'where:isOnline=101')
        ->with_transaction(['fields'=>'transaction_id,source_id,transaction_charge_id,customer_id,amount','where' =>['transaction_type' => 1]])
        ->fields(['request_type','request_status','req_uuid','mechanic_id','request_id'])
        ->where(['request_type' => 2,'request_status' => 1, 'created_at <' => $currnet_DT])
        ->get_all();
        
        if(!$requests){
            die;
        }
        foreach($requests as $request){

            $userDetail = $request['user'];
            $requestId = $request['request_id'];

            if($request['request_status'] == 1) {

                $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => 7]);

                if(empty($request['transaction']) || $request['transaction']['transaction_charge_id'] == "" ){
                    continue;
                }

                $requestTransactionId = $request['transaction']['transaction_id'];
                $requestTransactionRef = $request['transaction']['transaction_charge_id'];
    
                $refundPayment = refundAuthAmount($requestTransactionRef);
                if($refundPayment!=null && $refundPayment->status == 'succeeded'){ 
                    $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                    $refundPaymentData = [
                        'message' => "Payment Refunded.",
                        'transaction_reference' => $transaction_reference,
                        'request_id' => $requestId,
                        'user_id' => $userDetail['user_id'],
                        'transaction_charge_id'=>$refundPayment->id,
                        'object_type'=>$refundPayment->object,
                        'amount'=>($refundPayment->amount / 100),
                        'paid_status'=>3, //Refund Amount
                        'transaction_type'=>1, // 1 for callout fee
                        'payment_type'=>1, //Which type payment
                        'transaction_status'=>$refundPayment->status,
                        'transaction_type' => 1,
                        'payment_type' => 1,
                        'amount' => $request['transaction']['amount'],
                        'customer_id' => $request['transaction']['customer_id'],
                        'source_id' => $request['transaction']['source_id'],
                    ];
                    $updateTransaction = $this->Transaction_model->insert($refundPaymentData);
                } 

                $response = [
                    'request_status'=> 7,
                    'req_uuid'=> $request['req_uuid'],
                    'amount'=> $request['transaction']['amount'],
                    'notification_type' => 7,
                    'request_id' => $requestId,
                ];

                $notificationData = [
                        'message'	=> 'Mechanic are not available. '.$request['transaction']['amount'].'Amount has been refunded.',
                        'data' => $response
                ];

                $pubnub_args = [
                    'channel' => $userDetail['uuid'],
                    'message'=> [
                        'type'=>'No mechanic available.',
                        'mechanicDetails'=>$response,
                    ]
                ]; 

                $bodyData = [
                    'request_id'=>$request['request_id'],
                    'message'	=> 'Mechanic are not available. '.$request['transaction']['amount'].'Amount has been refunded.',
                ];
                    
                $messageTitle =[
                    'message'	=> 'Mechanic are not available. '.$request['transaction']['amount'].'Amount has been refunded.',
                    'category'=>['notification_type' => 7]
                ];
                
            } elseif($request['request_status'] == 2) {

                $mechanicId = $request['mechanic']['mechanic_id'];
                $redis=createConnectionToRedis();
                $specificMechanicLatLong=$redis->geopos('mechanics',$mechanicId);
                $mechanicLong = $specificMechanicLatLong[0][0];
                $mechanicLat = $specificMechanicLatLong[0][1];

                $ratingSum = 0;
                $getRating = $this->Mechanic_rating_model
                ->where(['mechanic_id'=>$mechanicId, 'from_at'=>1])->get_all();
                if(!empty($getRating)) {
                    foreach($getRating as $getRatings) {
                        $ratingSum += $getRatings['rating'];
                    }
                    $mechanicAvgRating = $ratingSum/count($getRating);
                }

                $response = [
                    'mechanic_name'=>$request['mechanic']['name'],
                    'mechanic_address'=>$request['mechanic']['mechanic_address'],
                    'contact_number'=>$request['mechanic']['contact_number'],
                    'mechanic_uuid'=>$request['mechanic']['mechanic_uuid'],
                    'latitude'=>$mechanicLat,
                    'longitude'=>$mechanicLong,
                    'request_status'=>2,
                    'mechanic_id' => $request['mechanic_id'],
                    'req_uuid'=> $request['req_uuid'],
                    'country_code' => $request['mechanic']['country_code'],
                    'contact_number' => $request['mechanic']['contact_number'],
                    'mechanic_image'=>(isset($request['mechanic']['mechanic_picture']) ? $request['mechanic']['mechanic_picture'] : ''),
                    'notification_type' => 6,
                    'rating'=>(isset($mechanicAvgRating) ? $mechanicAvgRating : 0),
                    'request_id' => $requestId,
                    'request_type' => $request['request_type'],
                ];

                $notificationData = [
                        'message'	=> 'Accepted Your Emergency Request',
                        'data' => $response
                ];

                $pubnub_args = [
                    'channel' => $userDetail['uuid'],
                    'message'=> [
                        'type'=>'EmergencyNotificationToUser',
                        'mechanicDetails'=>$response,
                        'longitude'=>(string)$mechanicLong,
                        'latitude'=>(string)$mechanicLat
                    ]
                ]; 

                $bodyData = [
                    'request_id'=>$request['request_id'],
                    'acceptMechanic'=>$request['mechanic']['mechanic_uuid']
                ];
                    
                $messageTitle =[
                    'title'	=> 'Accepted Your Emergency Request',
                    'category'=>['notification_type' => 6,'request_id' => $requestId,'request_type' => $request['request_type'],]
                ];
            }

            if(isset($notificationData) && !empty($notificationData)){
                $device_id = $userDetail['device_key'];

                $publish_status = $this->mypubnub->publish($pubnub_args);
        
                if($userDetail['platform_type'] == 1) {
                    
                
                    $notify = $this->notification->fcmUser($device_id, $notificationData);
                    // print_r($notify);
                } elseif($userDetail['platform_type'] == 2) {
                    $platform_type = $userDetail['platform_type'];
                    $isProduction = $userDetail['isProduction'];

                    $notify = $this->notification->apnsUser($device_id, $messageTitle, $isProduction, $platform_type, $bodyData);

                }
            }
        }

        die;
    }
}
