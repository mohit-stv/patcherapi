<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mechanic extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model', 'Mechanic_model', 'Mechanic_service_model', 'Mechanic_document_model', 'Mechanic_experience_model', 'Experience_model', 'Mechanic_qualification_model', 'Qualification_model', 'Tool_itinerary_model', 'Mechanic_itineraries_model', 'Mechanic_health_model', 'Health_safety_check_model', 'Admin_transaction_model', 'Mechanic_issuses_model', 'Mechanic_registrations_model', 'Mechanic_request_detail_model', 'Bank_detail_model', 'Transaction_model']);
        $this->load->library(['Curl', 'Mypubnub', 'Notification']);
        $this->load->helper('common');
    }

    public function mechanicList_post()
    {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('mechanics')->count_all_results();
        $mechanicData = $this->Mechanic_model->fields('mechanic_id,name,email_id,country_code,contact_number,mechanic_picture,request_commission,created_at,isBlocked')->limit($limit, $offset)->order_by('mechanic_id', 'desc')->get_all();
        if (!empty($mechanicData)) {
            echo json_encode(['status' => true, 'message' => 'Mechanic List.', 'response' => ['count' => $count, 'contacts' => $mechanicData]]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'No Mechanics here']);die;
        }
    }

    public function mechanicDetails_post()
    {
        $id = $this->param['mechanic_id'];
        $mechanicData = $this->Mechanic_model->fields('mechanic_id,name,email_id,country_code,contact_number,national_insurance,vehicle_registration_number,request_commission,mechanic_address,mechanic_picture,mechanic_dob,hourly_rate,created_at,isBlocked')->where('mechanic_id', $id)->get();
        if (!empty($mechanicData)) {
            echo json_encode(['status' => true, 'message' => 'Mechanic Details.', 'response' => $mechanicData]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'No Mechanics Details']);die;
        }
    }

    public function mechanicService_post()
    {
        $id = $this->param['mechanic_id'];

        $servicesList = $this->Service_model->where('service_status', 101)->fields(['service_id', 'parent_id', 'service_name', 'service_status'])->get_all();

        $allServices = array();
        foreach ($servicesList as $key => $servicesLists) {
            $serviceId = $servicesLists['service_id'];
            $allServices[$key]['service_id'] = $servicesLists['service_id'];
            $allServices[$key]['service_name'] = $servicesLists['service_name'];
            $allServices[$key]['parent_id'] = $servicesLists['parent_id'];

            $serviceData = $this->Mechanic_service_model->fields(['mechanic_services_status'])->get(['mechanic_id' => $id, 'services_id' => $serviceId]);

            if (!empty($serviceData)) {
                $serviceVal = $serviceData;
                $allServices[$key]['mechanic_status'] = $serviceVal['mechanic_services_status'];
            } else {
                $allServices[$key]['mechanic_status'] = '100';
            }
        }
        if (!empty($allServices)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Services Details Here', 'response' => $allServices]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicDocument_post()
    {
        $id = $this->param['mechanic_id'];

        $documentList = $this->Document_model->where('document_status', 101)->fields(['document_id', 'document_name', 'document_description', 'isOptional', 'document_status'])->get_all();

        $AllDocuments = array();
        foreach ($documentList as $key => $documentLists) {
            $document_id = $documentLists['document_id'];
            $allDocuments[$key]['document_id'] = $documentLists['document_id'];
            $allDocuments[$key]['document_name'] = $documentLists['document_name'];
            $allDocuments[$key]['document_description'] = $documentLists['document_description'];

            $documentData = $this->Mechanic_document_model->fields(['document_id', 'document_url', 'public_id', 'mechanic_document_status', 'mechanic_document_id'])->get(['mechanic_id' => $id, 'document_id' => $document_id]);

            $documentVal = $documentData;

            $allDocuments[$key]['document_url'] = ($documentVal['document_url'] != '') ? $documentVal['document_url'] : '';

            $allDocuments[$key]['mechanic_document_id'] = ($documentVal['mechanic_document_id'] != '') ? $documentVal['mechanic_document_id'] : '';

            $allDocuments[$key]['document_status'] = ($documentVal['mechanic_document_status'] != '') ? $documentVal['mechanic_document_status'] : 100;

            $allDocuments[$key]['public_id'] = ($documentVal['public_id'] != '') ? $documentVal['public_id'] : 00;

        }
        if (!empty($allDocuments)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Documents Details Here', 'response' => $allDocuments]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicExperience_post()
    {
        $id = $this->param['mechanic_id'];

        $experienceList = $this->Experience_model->where('experience_status', 101)->fields(['experience_id', 'experience_name', 'isOptional', 'isUpload', 'experience_status'])->get_all();

        if (!empty($experienceList)) {
            $allExperience = array();
            $allExperience1 = array();
            foreach ($experienceList as $key => $experienceLists) {
                $experience_id = $experienceLists['experience_id'];
                $experience_name = $experienceLists['experience_name'];

                $experienceData = $this->Mechanic_experience_model->fields(['experience_url', 'public_id', 'experience_id', 'mechanic_experience_status', 'mechanic_experience_id', 'isPass'])->get(['mechanic_id' => $id, 'experience_id' => $experience_id]);

                if ($experienceData['mechanic_experience_status'] == null) {
                    $mechanicExperienceStatus = '100';
                } else {
                    $mechanicExperienceStatus = $experienceData['mechanic_experience_status'];
                }

                if ($experienceData['experience_url'] == null) {
                    $experienceUrl = '';
                } else {
                    $experienceUrl = $experienceData['experience_url'];
                }

                if ($experienceData['isPass'] == null) {
                    $experienceIsPass = '100';
                } else {
                    $experienceIsPass = $experienceData['isPass'];
                }

                $mechanic_experience_id = ($experienceData['mechanic_experience_id'] != '') ? $experienceData['mechanic_experience_id'] : '';

                $public_id = ($experienceData['public_id'] != '') ? $experienceData['public_id'] : '';

                if ($experienceLists['isOptional'] == '101') {
                    $allExperience['isPass'][] = [
                        'experience_id' => $experienceLists['experience_id'],
                        'experience_name' => $experienceLists['experience_name'],
                        'mechanic_experience_id' => $mechanic_experience_id,
                        'public_id' => $public_id,
                        'experience_url' => $experienceUrl,
                        'experience_status' => $mechanicExperienceStatus,
                        'isRefrence' => "101",
                    ];
                } elseif ($experienceLists['isUpload'] == '100') {
                    $allExperience['isPass'][] = [
                        'experience_id' => $experienceLists['experience_id'],
                        'experience_name' => $experienceLists['experience_name'],
                        'mechanic_experience_id' => $mechanic_experience_id,
                        'public_id' => $public_id,
                        'isRefrence' => "100", //Relace isPass To isRefrence
                        'experience_url' => $experienceUrl,
                        'experience_status' => $experienceIsPass,
                    ];
                } else {
                    $allExperience['range'][] = [
                        'experience_id' => $experienceLists['experience_id'],
                        'experience_name' => $experienceLists['experience_name'],
                        'mechanic_experience_id' => $mechanic_experience_id,
                        'public_id' => $public_id,
                        'experience_status' => $mechanicExperienceStatus,
                    ];
                }
            }
        }

        if (!empty($allExperience)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Experience Details Here', 'response' => $allExperience]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicQualification_post()
    {
        $id = $this->param['mechanic_id'];
        $qualificationList = $this->Qualification_model->where('qualification_status', 101)->fields(['qualification_id', 'qualification_name', 'qualification_status', 'isRefrence'])->get_all();

        $allQualification = array();
        foreach ($qualificationList as $key => $qualificationLists) {
            $qualification_id = $qualificationLists['qualification_id'];
            $qualification_name = $qualificationLists['qualification_name'];

            $qualificationData = $this->Mechanic_qualification_model->fields(['qualification_url', 'mechanic_qualification_id', 'mechanic_qualification_status', 'public_id'])->get(['mechanic_id' => $id, 'qualification_id' => $qualification_id]);

            if ($qualificationData['mechanic_qualification_status'] == null) {
                $qualificationVerified = "100";
            } else {
                $qualificationVerified = $qualificationData['mechanic_qualification_status'];
            }

            if ($qualificationData['qualification_url'] == null) {
                $qualificationUrl = "";
            } else {
                $qualificationUrl = $qualificationData['qualification_url'];
            }

            if (isset($qualificationData['public_id'])) {
                $public_id = $qualificationData['public_id'];
            } else {
                $public_id = 0;
            }

            $mechanic_qualification_id = ($qualificationData['mechanic_qualification_id'] != '') ? $qualificationData['mechanic_qualification_id'] : '';

            if ($qualificationLists['isRefrence'] == '101') {
                $allQualification['refrence'][] = [
                    'qualification_id' => $qualificationLists['qualification_id'],
                    'qualification_name' => $qualificationLists['qualification_name'],
                    'mechanic_qualification_id' => $mechanic_qualification_id,
                    'qualification_url' => $qualificationUrl,
                    'public_id' => $public_id,
                    'qualification_status' => $qualificationVerified,
                ];
            } else {
                $allQualification['qualification'][$key] = [
                    'qualification_id' => $qualificationLists['qualification_id'],
                    'qualification_name' => $qualificationLists['qualification_name'],
                    'mechanic_qualification_id' => $mechanic_qualification_id,
                    'public_id' => $public_id,
                    'qualification_url' => $qualificationUrl,
                    'qualification_status' => $qualificationVerified,
                ];
            }
        }
        if (!empty($allQualification)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Qualification Details Here', 'response' => $allQualification]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicItineraryOld_post()
    {
        $id = $this->param['mechanic_id'];
        if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId = $this->param['service_id'];
            $getTool = $this->Tool_itinerary_model->where(['tool_itinerary_status' => 101, 'service_id' => $serviceId])->fields(['tool_itinerary_id', 'tool_itinerary_name'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id, 'services_id' => $serviceId]);

            $arrayval = explode(",", $getMechanicItineraries[0]['tool_itinerary_id']);

            $toolDetails = array();
            foreach ($getTool as $key => $getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $toolDetails[$key]['tool_itinerary_id'] = $getTools['tool_itinerary_id'];

                $toolDetails[$key]['tool_itinerary_name'] = $getTools['tool_itinerary_name'];
                if (in_array($toolId, $arrayval)) {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '101';
                } else {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '100';
                }
            }
        } else {

            $getTool = $this->Tool_itinerary_model->with_service('fields:service_name')->fields(['tool_itinerary_id', 'tool_itinerary_name', 'tool_itinerary_status', 'service_id'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id]);

            if (!empty($getMechanicItineraries)) {
                $arrayval = explode(",", $getMechanicItineraries[0]['tool_itinerary_id']);
            }

            foreach ($getTool as $key => $getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $serviceName = $getTools['service']['service_name'];
                $serviceId = $getTools['service']['service_id'];

                if (!empty($arrayval)) {
                    if (in_array($toolId, $arrayval)) {
                        $toolStatus = '101';
                    } else {
                        $toolStatus = '100';
                    }
                } else {
                    $toolStatus = '100';
                }

                $toolDetails[] = [
                    'tool_itinerary_id' => $getTools['tool_itinerary_id'],
                    'tool_itinerary_name' => $getTools['tool_itinerary_name'],
                    'tool_itinerary_status' => $getTools['tool_itinerary_status'],
                    'mechanic_itinerary_status' => $toolStatus,
                ];
            }
        }
        if (!empty($toolDetails)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Itinerary Details Here', 'response' => $toolDetails]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicHealth_post()
    {
        $id = $this->param['mechanic_id'];
        $getHealth = $this->Health_safety_check_model->fields(['health_safety_check_id', 'health_safety_name', 'health_safety_description', 'health_safety_check_status'])->get_All();

        $getMechanichealth = $this->Mechanic_health_model->fields(['health_id', 'mechanic_health_status'])->get_all(['mechanic_id' => $id]);
        $arrayval = explode(",", $getMechanichealth[0]['health_id']);
        $healthDetails = array();
        foreach ($getHealth as $key => $getHealths) {
            $healthId = $getHealths['health_safety_check_id'];
            $healthDetails[$key] = [
                'health_safety_check_id' => $getHealths['health_safety_check_id'],
                'health_safety_name' => $getHealths['health_safety_name'],
                'health_safety_check_status' => $getHealths['health_safety_check_status'],
            ];

            if (in_array($healthId, $arrayval)) {
                $healthDetails[$key]['mechanic_health_status'] = '101';
            } else {
                $healthDetails[$key]['mechanic_health_status'] = '100';
            }
        }
        if (!empty($healthDetails)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Health Details Here', 'response' => $healthDetails]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicRequest_post()
    {
        $id = $this->param['mechanic_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('requests')->where('mechanic_id', $id)->count_all_results();
        // $requestList = $this->Request_model->fields('request_id,request_type,request_date,request_status')->with_user('fields: name,contact_number')->with_request_issues('fields: location')->with_transaction('fields: amount,paid_status')->limit($limit,$offset)->get_all(['mechanic_id'=>$id]);
        $requestList = $this->Request_model->fields('request_id,request_type,request_date,request_status')->with_user('fields: name,contact_number')->limit($limit, $offset)->get_all(['mechanic_id' => $id]);

        if (!empty($requestList)) {
            foreach ($requestList as $key => $requestLists) {
                $requestId = $requestLists['request_id'];
                $requestLocation = $this->Request_issues_model->fields('location')->where('request_id', $requestId)->get();
                $requestList[$key]['location'] = (isset($requestLocation['location']) ? $requestLocation['location'] : '');

                $requestTransaction = $this->Transaction_model->fields('amount,paid_status,transaction_status,created_at')->where(['user_id' => $id, 'request_id' => $requestId])->get();
                $requestList[$key]['transaction'] = (!empty($requestTransaction) ? $requestTransaction : []);
            }
        }

        /* foreach($requestList as $requestLists) {
        $requestList[] = ['request_id'=>$requestLists['request_id'],
        'request_type'=>$requestLists['request_type'],
        'request_date'=>$requestLists['request_date'],
        'request_status'=>$requestLists['request_status'],
        'user_name'=>$requestLists['user']['name'],
        'user_contactnumber'=>$requestLists['user']['contact_number'],
        'location'=>(!empty($requestLists['request_issues']['location']) ? $requestLists['request_issues']['location'] : ''),
        'transaction'=>(!empty($requestLists['transaction']['amount']) ? $requestLists['transaction']['amount'] : ''),
        'paid_status'=>(!empty($requestLists['transaction']['paid_status']) ? $requestLists['transaction']['paid_status'] : '')
        ];
        }*/
        if (!empty($requestList)) {
            echo json_encode(['status' => true, 'message' => 'All Request Of Mechanic Here', 'response' => ['count' => $count, 'request' => $requestList]]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicRequestDetail_post()
    {

        $id = $this->param['request_id'];
        $requestDetail = $this->Request_model->fields('request_status')->where('request_id', $id)->get();
        $response['request_detail'] = $requestDetail;

        $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id', 'part_name', 'price'])->where(['request_id' => $id])->get_all();

        $requestIssue = $this->Request_issues_model->fields(['issue_id', 'date', 'timepicker', 'location'])->where(['request_id' => $id, 'request_issue_status' => 101])->get_all();

        if (!empty($requestIssue)) {
            foreach ($requestIssue as $key => $requestIssues) {
                $issueId = $requestIssues['issue_id'];
                $issueDetail = $this->Issue_model->fields(['issue_id', 'issue_name', 'issuse_price'])->get(['issue_id' => $issueId]);

                $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price', 'issuse_id'])->get(['request_id' => $id, 'issuse_id' => $issueId]);

                if (!empty($getNewPriceOfIssuse['issuse_price'])) {
                    $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                } else {
                    $issusePrice = $issueDetail['issuse_price'];
                }

                $service = [];
                if (!empty($getNewAddService)) {
                    foreach ($getNewAddService as $getNewAddServices) {
                        $service[] = [
                            'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                            'issue_name' => $getNewAddServices['part_name'],
                            'issue_price' => $getNewAddServices['price'],
                            'status' => 101, //101 for service price
                        ];
                    }
                }

                $result[] = ['issue_id' => $issueDetail['issue_id'],
                    'issue_name' => $issueDetail['issue_name'],
                    'issue_price' => $issusePrice,
                    'status' => 100, //100 for issue price
                ];
            }
            $response['issue'] = array_merge($result, $service);
            if (!empty($response)) {
                $this->response(['status' => true, 'message' => 'All Completed Issse With Price', 'response' => $response]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }
        } elseif (!empty($getNewAddService)) {
            $service = [];
            if (!empty($getNewAddService)) {
                foreach ($getNewAddService as $getNewAddServices) {
                    $service[] = [
                        'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                        'issue_name' => $getNewAddServices['part_name'],
                        'issue_price' => $getNewAddServices['price'],
                    ];
                }
            }
            if (!empty($service)) {
                $this->response(['status' => true, 'message' => 'All Completed Issue With Price', 'response' => $service]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    public function mechanicVerifyStatus_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model('Mechanic_registrations_model');

        $mechanicData = $this->Mechanic_model->fields(['name', 'device_key', 'isProduction', 'platform_type', 'email_id', 'mechanic_uuid'])
            ->get(['mechanic_id' => $id]);

        if (!$mechanicData) {
            $this->response(['status' => false, 'message' => 'Mechanic not found.']);
        }

        $data = $updateData = [];
        $data['mechanic_id'] = $id;

        #For Specific Notification of Verify/Decline
        $verification = $decline = [];

        if (isset($this->param['basic_info'])) {
            $data['basic_info'] = $this->param['basic_info'];
            $updateData['basic_info'] = $this->param['basic_info'];

            if ($data['basic_info'] == 102) {
                $verification[] = "Basic Profile";
            } elseif ($data['basic_info'] == 103) {
                $decline[] = "Basic Profile";
            }
        }

        if (isset($this->param['service_status'])) {
            $data['service_status'] = $this->param['service_status'];
            $updateData['service_status'] = $this->param['service_status'];
            if ($data['service_status'] == 102) {
                $verification[] = "Services";
            } elseif ($data['service_status'] == 103) {
                $decline[] = "Services";
            }
        }

        if (isset($this->param['documentation_status'])) {
            $data['documentation_status'] = $this->param['documentation_status'];
            $updateData['documentation_status'] = $this->param['documentation_status'];
            if ($data['documentation_status'] == 102) {
                $verification[] = "Document";
            } elseif ($data['documentation_status'] == 103) {
                $decline[] = "Document";
            }
        }

        if (isset($this->param['experience_status'])) {
            $data['experience_status'] = $this->param['experience_status'];
            $updateData['experience_status'] = $this->param['experience_status'];
            if ($data['experience_status'] == 102) {
                $verification[] = "Experience";
            } elseif ($data['experience_status'] == 103) {
                $decline[] = "Experience";
            }
        }

        if (isset($this->param['qualification_status'])) {
            $data['qualification_status'] = $this->param['qualification_status'];
            $updateData['qualification_status'] = $this->param['qualification_status'];
            if ($data['qualification_status'] == 102) {
                $verification[] = "Qualification";
            } elseif ($data['qualification_status'] == 103) {
                $decline[] = "Qualification";
            }
        }

        if (isset($this->param['tools_status'])) {
            $data['tools_status'] = $this->param['tools_status'];
            $updateData['tools_status'] = $this->param['tools_status'];
            if ($data['tools_status'] == 102) {
                $verification[] = "Itinerary";
            } elseif ($data['tools_status'] == 103) {
                $decline[] = "Itinerary";
            }
        }

        if (isset($this->param['health_status'])) {
            $data['health_status'] = $this->param['health_status'];
            $updateData['health_status'] = $this->param['health_status'];
            if ($data['health_status'] == 102) {
                $verification[] = "Health and Safety";
            } elseif ($data['health_status'] == 103) {
                $decline[] = "Health and Safety";
            }
        }

        $data['comment'] = '';
        if (isset($this->param['comment']) && $this->param['comment'] != '') {
            $data['comment'] = $this->param['comment'];
            $updateData['comment'] = $this->param['comment'];
        }

        $getMechanic = $this->Mechanic_registrations_model->get_all(['mechanic_id' => $id]);

        if (!empty($getMechanic)) {
            $result = $this->Mechanic_registrations_model->where('mechanic_id', $id)->update($updateData);
        } else {
            $result = $this->Mechanic_registrations_model->insert($data);
        }

        if ($result) {

            if ($this->isAppVerified($id)) {
                $redis = createConnectionToRedis();
                $redis->set($id, 'ONLINE');
            }

            $device_id = $mechanicData['device_key'];

            $notificationMsg = $emailMsg = $verificationMsg = $declineMsg = "";

            if (count($verification) > 1) {
                $notificationMsg = $verificationMsg = implode(',', $verification) . " sections have been verified";
                $emailMsg = "We are pleased to inform you that your " . implode(',', $verification) . " sections have been verified.";

            } elseif (count($verification) == 1) {
                $notificationMsg = $verificationMsg = implode(',', $verification) . " section has been verified";
                $emailMsg = "We are pleased to inform you that your " . implode(',', $verification) . " section has been verified.";
            }

            if (count($decline) > 1) {
                $notificationMsg = $declineMsg = implode(',', $decline) . " sections of your application have been declined";
                $emailMsg .= "<br>The " . implode(',', $decline) . " sections have been declined. Please verify your details with admin to continue our services.";

            } elseif (count($decline) == 1) {
                $notificationMsg = $declineMsg = implode(',', $decline) . " section of your application has been declined";

                $emailMsg .= "<br>The " . implode(',', $decline) . " section has been declined. Please verify your details with admin to continue our services.";

            }

            $args = [
                'message' => $notificationMsg,
                'subject' => 'Patcher: Profile Status Updates',
                'email_id' => $mechanicData['email_id'],
                'template_name' => 'Profile Status Updates',
                'vars' => [
                    [
                        'name' => 'message',
                        'content' => $emailMsg,
                    ],
                ],
            ];

            $notificationMsg = "The $notificationMsg By Admin.";

            $mailResponse = sendMandrillEmailTemlate($args);

            if ($mechanicData['platform_type'] == 1) {
                $message = [
                    'title' => $notificationMsg,
                    'body' => $notificationMsg,
                    'tag' => ['notification_type' => 15],
                    'icon' => 'myicon',
                ];
                $notify = $this->notification->fcmMechanic($device_id, $message);
            } else if ($mechanicData['platform_type'] == 2) {
                $mechanicUuid = $mechanicData['mechanic_uuid'];
                $pubnub_args = ['channel' => $mechanicData['mechanic_uuid'],
                    'message' => ['message' => $notificationMsg, 'type' => 'R15']];

                $publish_status = $this->mypubnub->publish($pubnub_args);

                $jsonData = ['mechanic_uuid' => $mechanicUuid];

                $message = [
                    'title' => 'Profile Status Updates',
                    'notification_type' => '15',
                ];
                //$device_id = 'A731FBC7A2041DD89D9F47CFB0111A46806539F96CC97026E52D45FF7A40E683';
                $platform_type = $mechanicData['platform_type'];
                $isProduction = $mechanicData['isProduction'];
                $notify = $this->notification->apnsMechanic($device_id, $message, $isProduction, $platform_type, $jsonData);
            }

            echo json_encode(['status' => true, 'message' => 'Verified Successfully!!', 'resonse' => $data]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Not Verified!!']);die;
        }
    }

    public function getMechanicStatus_post()
    {
        if (isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
            $id = $this->param['mechanic_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'Mechanic Id is required.']);
            die;
        }

        $this->load->model('Mechanic_registrations_model');
        $getMechanicStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $response = ['basic_info' => (isset($getMechanicStatus['basic_info']) ? $getMechanicStatus['basic_info'] : 100),
            'service_status' => (isset($getMechanicStatus['service_status']) ? $getMechanicStatus['service_status'] : 100),
            'documentation_status' => (isset($getMechanicStatus['documentation_status']) ? $getMechanicStatus['documentation_status'] : 100),
            'experience_status' => (isset($getMechanicStatus['experience_status']) ? $getMechanicStatus['experience_status'] : 100),
            'qualification_status' => (isset($getMechanicStatus['qualification_status']) ? $getMechanicStatus['qualification_status'] : 100),
            'tools_status' => (isset($getMechanicStatus['tools_status']) ? $getMechanicStatus['tools_status'] : 100),
            'health_status' => (isset($getMechanicStatus['health_status']) ? $getMechanicStatus['health_status'] : 100),
            'comment' => (isset($getMechanicStatus['comment']) ? $getMechanicStatus['comment'] : ''),
        ];

        if (!empty($response)) {
            echo json_encode(['status' => true, 'message' => 'Mechanic Verify Status Here.', 'response' => $response]);
        } else {
            echo json_encode(['status' => false, 'message' => 'Not Verified!!']);
        }
    }

    public function payInList_post()
    {
        $id = $this->param['mechanic_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];

        $payInList = $this->Request_model->fields('request_type,request_status')->with_transaction('fields: amount,message,paid_status,transaction_status,created_at,updated_at')->where('mechanic_id', $id)->get_all();

        if (!empty($payInList)) {
            echo json_encode(['status' => true, 'message' => 'Transaction Details Of User.', 'response' => $payInList]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function payOutList_post()
    {
        $payOutList = $this->Admin_transaction_model->with_mechanic('fields: name,contact_number')->with_request('fields: request_id,request_type,request_status,created_by,updated_by')->get_all();
        if (!empty($payOutList)) {
            echo json_encode(['status' => true, 'message' => 'Transaction Details Admin To Mechanic.', 'response' => $payOutList]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Details not available.']);
        }
    }

    public function blockUnblockMechanic_post()
    {
        $status = $this->param['status'];
        $mechanicId = $this->param['mechanic_id'];

        $updateStatus = $this->Mechanic_model
            ->where('mechanic_id', $mechanicId)
            ->update(['isBlocked' => $status]);

        if ($updateStatus) {
            echo json_encode(['status' => true, 'message' => 'Mechaincs status updated.']);
        } else {
            echo json_encode(['status' => true, 'message' => 'Error in process.']);
        }
    }

    public function mechanicItinerary_post()
    {
        $id = $this->param['mechanic_id'];
        if (isset($this->param['service_id']) && !empty($this->param['service_id'])) {
            $serviceId = $this->param['service_id'];
            $getTool = $this->Tool_itinerary_model->where(['tool_itinerary_status' => 101, 'service_id' => $serviceId])->fields(['tool_itinerary_id', 'tool_itinerary_name'])->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id, 'services_id' => $serviceId]);

            $arrayval = explode(",", $getMechanicItineraries[0]['tool_itinerary_id']);

            $toolDetails = array();
            foreach ($getTool as $key => $getTools) {
                $toolId = $getTools['tool_itinerary_id'];
                $toolDetails[$key]['tool_itinerary_id'] = $getTools['tool_itinerary_id'];

                $toolDetails[$key]['tool_itinerary_name'] = $getTools['tool_itinerary_name'];
                if (in_array($toolId, $arrayval)) {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '101';
                } else {
                    $toolDetails[$key]['mechanic_itinerary_status'] = '100';
                }
            }
        } else {

            $arrayval = [];
            $services = $this->Service_model->fields('service_name')->with_tools('fields: tool_itinerary_id,tool_itinerary_name,tool_itinerary_status,service_id')->get_all();

            $getMechanicItineraries = $this->Mechanic_service_model->fields(['tool_itinerary_id'])->get_all(['mechanic_id' => $id]);

            if (!empty($getMechanicItineraries)) {
                $toolsList = "";
                foreach ($getMechanicItineraries as $Itineraries) {
                    $toolsList .= $Itineraries['tool_itinerary_id'] . ",";
                }
                $toolsList = substr($toolsList, 0, -1);
                // echo $toolsList;die;
                $arrayval = explode(",", $toolsList);
            }

            foreach ($services as $key => $service) {
                if (isset($service['tools'])) {
                    $getTools = $service['tools'];
                    $toolDetails = array();
                    foreach ($getTools as $getTool) {
                        if (!empty($arrayval)) {
                            if (in_array($getTool['tool_itinerary_id'], $arrayval)) {
                                $getTool['mechanic_itinerary_status'] = 101;
                            } else {
                                $getTool['mechanic_itinerary_status'] = '100';
                            }
                        } else {
                            $getTool['mechanic_itinerary_status'] = '100';
                        }
                        $toolDetails[] = $getTool;
                    }

                    $service['toolDetails'] = $toolDetails;
                    unset($service['tools']);
                    $details[] = $service;
                }
            }
        }
        if (!empty($details)) {
            echo json_encode(['status' => true, 'message' => 'Mechanics Itinerary Details Here', 'response' => $details]);
        } else {
            echo json_encode(['status' => false, 'messgae' => 'Not Found']);
        }
    }

    public function mechanicPayments_post()
    {
        $mechanicId = $this->param['mechanic_id'];
        $status = 100; //100 for pending || 101 for Paid || 102 for Both

        if (isset($this->param['flag']) && !empty($this->param['flag'])) {
            $status = $this->param['flag'];
        }

        $where = ['mechanic_id' => $mechanicId, 'isChargable' => 100];
        if ($status != 102) {
            $where['paid_status'] = $status;
        }

        $getPayments = $this->Mechanic_request_detail_model->fields('id,request_id,request_amount as trip_amount,comission,remarks')->with_request('fields: request_id,user_id')->get_all($where);

        if (!$getPayments) {
            $this->response(['status' => false, 'message' => 'No payment details available.']);
        } else {
            $this->response(['status' => true, 'message' => 'Payment details available.', 'response' => $getPayments]);
        }

    }

    public function paymentDashboard_post()
    {
        $this->form_validation->set_data($this->param);
        if (!$this->form_validation->run('mechaniclogout')) {
            //mechaniclogout uses only mechanic_id
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);die;
        }

        $mechanicId = $this->param['mechanic_id'];

        $this->db->select_sum('request_amount');
        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 100");
        $total = $this->db->get()->row_array();

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 100 AND paid_status = 100");
        $totalPending = $this->db->get()->row_array();

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 100 AND paid_status = 101");
        $totalPaid = $this->db->get()->row_array();

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 101");
        $totalCharge = $this->db->get()->row_array();

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 101 AND paid_status = 100");
        $totalPendingCharge = $this->db->get()->row_array();

        $responses = array(
            'total_request_amount' => $total['request_amount'],
            'total_comission' => $total['comission'],
            'total_pending_comission' => $totalPending['comission'],
            'total_paid_comission' => $totalPaid['comission'],
            'total_charge' => $totalCharge['comission'],
            'total_pending_charge' => $totalPendingCharge['comission'],
        );

        $response = [];
        foreach ($responses as $key => $value) {
            if ($value == null || $value == "") {
                $value = 0;
            }
            $response[$key] = round($value, 2);
        }

        $this->response(['status' => true, 'message' => 'Details Available', 'response' => $response]);
    }

    public function payToMechanic_post()
    {
        $this->form_validation->set_data($this->param);
        if (!$this->form_validation->run('payToMechanic')) {
            #mechaniclogout uses only mechanic_id
            $this->response(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()], 200);
        }

        if (empty($this->param['request_ids'])) {
            $this->response(["status" => false, "error_status" => 2, "message" => 'Invalid payment request details']);
        }

        $mechanicId = $this->param['mechanic_id'];
        $requestIds = $this->param['request_ids'];
        $requestIds = implode(',', $requestIds);

        $mechanicAccountDetails = $this->Bank_detail_model->fields(['bank_detail_id', 'account_number', 'bank_id', 'account_holder_name', 'sort_code', 'status', 'created_at'])->with_bank('fields:bank_name')
            ->where(['mechanic_id' => $mechanicId])
            ->get();

        if (!$mechanicAccountDetails) {
            $this->response(["status" => false, "error_status" => 2, "message" => "Unable to find any account details. Please update first"]);
        }

        $this->db->select_sum('comission');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND request_id IN ($requestIds) AND isChargable = 100 AND paid_status = 100");
        $pendingCommission = $this->db->get()->row_array();

        // echo $this->db->last_query().'<br>';

        if (!$pendingCommission || $pendingCommission['comission'] == null || $pendingCommission['comission'] == 0) {
            $this->response(["status" => false, "error_status" => 2, "message" => "Unable to find any details for $requestIds"]);
        }

        $this->db->select_sum('comission');
        $this->db->select('max(request_id) as max_request_id');
        $this->db->from('mechanic_request_details');
        $this->db->where("mechanic_id = $mechanicId AND isChargable = 101 AND paid_status = 100");
        $totalPendingCharge = $this->db->get()->row_array();

        // echo $this->db->last_query();die;

        $pendingAmount = $pendingCommission['comission'];
        $chargeAmount = $totalPendingCharge['comission'];

        if ($chargeAmount >= $pendingAmount) {
            $this->response(["status" => false, "error_status" => 3, "message" => "Pending Charges are greater than amount to be transfer."]);
        }

        $transferAmount = round($pendingAmount - $chargeAmount, 2);
        $transferToMechanicAccount = transferToMechanicAccount(['mechanic_id' => $mechanicId, 'amount' => $transferAmount]);

        if (!$transferToMechanicAccount['status']) {
            $this->response(["status" => false, "error_status" => 2, "message" => "Error in process. Please try again after sometime.", "details" => $transferToMechanicAccount['response']]);
        }

        $this->Mechanic_request_detail_model->where(['mechanic_id' => $mechanicId, 'isChargable' => 101, 'paid_status' => 100, 'request_id' => $totalPendingCharge['max_request_id']])->update(['paid_status' => 101]);

        $updateDetails = $this->Mechanic_request_detail_model->where(['mechanic_id' => $mechanicId, 'request_id' => $requestIds])->update(['paid_status' => 101]);

        $this->response(["status" => true, "message" => "Successfully Paid"]);
    }

    private function isAppVerified($id)
    {
        $this->load->model('Mechanic_registrations_model');
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        return $isAppVerified;
    }

}
