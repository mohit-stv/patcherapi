<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model','Payment_type_model']);
    }

    public function allpayIn_post() {
        $this->load->model(['Transaction_model','User_model','Request_model','Mechanic_model']);
        $limit=$this->param['limit'];
        $offset=$this->param['offset'];
        
        $count = $this->db->from('transactions')->count_all_results();
        
        $response = false;
        
        $allTransaction = $this->Transaction_model
        ->fields('transaction_id,amount')
        ->with_request('fields:mechanic_id')
        ->with_user('fields: name,email_id')
        ->limit($limit,$offset)
        ->get_all();

        if($allTransaction){
            foreach($allTransaction as $allTransactions) {
                if(!isset($allTransactions['request']) || empty($allTransactions['request'])){
                    continue;
                }
                $mechanicId = $allTransactions['request']['mechanic_id'];
                $mechanicDetail = $this->Mechanic_model->fields('name,email_id,contact_number')->where('mechanic_id',$mechanicId)->get();
    
                $response[] = ['transaction_id'=>$allTransactions['transaction_id'],
                            'amount'=>$allTransactions['amount'],
                            'mechanic_name'=>$mechanicDetail['name'],
                            'mechanic_email_id'=>$mechanicDetail['email_id'],
                            'mechanic_contact_number'=>$mechanicDetail['contact_number'],
                            'user_name'=>$allTransactions['user']['name'],
                            'user_email_id'=>$allTransactions['user']['email_id']
                        ];
            }
        }

        if($response) {
            $this->response(['status' => true, 'message'=> 'All User Transaction Here.','response' => ['count'=>$count, 'result'=>$response]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }


    public function detailsOfpayIn_post() {
        $id = $this->param['transaction_id'];
        $this->load->model(['Transaction_model','Request_model']);
        $transactionDetail = $this->Transaction_model
        ->fields('transaction_charge_id,request_id,user_id,
        customer_id,amount,network_status,message,paid_status,transaction_status')
        ->with_user('fields:name')
        ->with_request('fields:request_type,created_at')
        ->where('transaction_id',$id)
        ->get();

        $requestDetail = $this->Request_model
        ->fields('request_status')
        ->with_mechanic('fields:mechanic_id, name')
        ->where('request_id',$transactionDetail['request_id'])
        ->get();

        if($transactionDetail) {
            $this->response(['status' => true, 'message'=> 'Transaction Details Here.','response' => ['transactionDetail'=>$transactionDetail, 'requestDetail'=>$requestDetail]]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }
 
    public function transactionToMechanic_post() {
        $this->load->helper('common');
        $this->load->model(['Bank_detail_model','Transaction_model','Admin_transaction_model']);
        $stripInclude = stripIncludeFiles();

        $requestId = $this->param['request_id'];
        $userId = $this->param['user_id'];
        $mechanicId = $this->param['mechanic_id'];

        $requestDetails = $this->Transaction_model->fields('amount')->get(['request_id'=>$requestId,'user_id'=>$userId,'paid_status'=>1]);

        if(!empty($requestDetails)) {
            $amount = $requestDetails['amount'];
            $bankDetails = $this->Bank_detail_model->fields('account_number,account_holder_name,status')->with_bank('fields:bank_name','where: status=101')->get(['mechanic_id'=>$mechanicId]);
           
            // $token = \Stripe\Token::create(array(
            //     "bank_account" => array(
            //     "country" => 'US',
            //     "currency" => 'usd',
            //     "account_holder_name" => "Alexander Wilson",
            //     "account_holder_type" => 'individual',
            //     "routing_number"=> 110000000, // Its is fixed for US
            //     "account_number"=> '000123456789'
            //     )
            // ));

            // $bankTokenId = $token->id;
            // $bankAccountId = $token->bank_account->id;
            	
            $account = \Stripe\Account::create(
                array("country" => "US",
                 "email" => 'mohit@patcher.com',
                 "type" => "custom",
                 'external_account' => array(
                    "object" => "bank_account",
                    "country" => "US",
                    "currency" => "usd",
                    "account_holder_name" => 'Jane Austen',
                    "account_holder_type" => 'individual',
                    "routing_number" => "111000025",
                    "account_number" => "000123456789"
                )
            ));

            

             /*   $account = \Stripe\Account::create(
                    array( "country" => "US",
                    "email" => 'new12345@patcher.com',
                  //  "managed" => true,
                    "type" => "standard",
                    "legal_entity" => array(
                        'address' => array(
                            'city' => 'Maxico',
                            'country' => 'US',
                            "line1" => 'H65',
                            "line2" => 'standfort street',
                            "postal_code" => '90046',
                            "state" => 'CA'
                        ),
                        'business_name' => 'test business name',
                        'business_tax_id' => '000000000',
                        'dob' => array(
                            'day' => '10',
                            'month' => '01',
                            'year' => '1988'
                        ),
                        'first_name' => 'Test',
                        'last_name' => 'Tester',
                        'personal_id_number' => '000000000',
                        'ssn_last_4' => '0000',
                        'type' => 'sole_prop'
                    ),
                    'tos_acceptance' => array(
                        'date' => time(),
                        'ip' => $_SERVER['REMOTE_ADDR']
                    ),
                    'external_account' => array(
                        "object" => "bank_account",
                        "country" => "US",
                        "currency" => "usd",
                        "account_holder_name" => 'Jane Austen',
                        "account_holder_type" => 'individual',
                        "routing_number" => "111000025",
                        "account_number" => "000123456789"
                    )
                    ));*/

            /*$retriveAccount = \Stripe\Account::retrieve('acct_1BBfHQLuBg6oTVcF');
            
                $val = $retriveAccount->external_account->create(array(
                    "object" => "bank_account",
                    "country" => "US",
                    "currency" => "usd",
                    "account_holder_name" => 'Jane Austen',
                    "account_holder_type" => 'individual',
                    "routing_number" => "110000000",
                    "account_number" => "000123456789"
                ));

               // $retriveAccount->default_source = $val->id;
                $retriveAccount->save();*/


          /*  $recipient = \Stripe\Recipient::create(
                array( "name" => 'Patcher',
                       "type" => 'individual',
                       "email"=>"patcher@example.com",
                       "tax_id"=>000000000 )
                );
            echo '<pre>';
            print_r($recipient);
            die;*/
             
            $payment = \Stripe\Transfer::create(
                array( "amount" => '0.5',
                       "currency" => "GBP",
                      // "source" => $bankTokenId,
                      "destination" => $account->id,
                      // "destination" => 'acct_1BBiu6KKhYfO4VwA',
                      /// "transfer_group" => "ORDER_95"
                    )
                );
                echo '<pre>';
                print_r($payment);
                die;

            $insertData = ['request_id'=>$requestId,
                           'user_id'=>$userId,
                           'mechanic_id'=>$mechanicid,
                           'amount'=>$amount,
                           'paid_status'=>$paidStatus,
                           'transaction_status'=>$transactionStatus];
            $insert = $this->Admin_transaction_model->insert($insertData);
            if($insert) {
                $this->response(['status' => true, 'message'=> 'Transaction Successfully.','response' => $insert]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Updated']);die;
            }
        }
    }

     // All Transaction History (User and Mechanic Both)
    public function transactionHistory_post() {
        $this->load->model(['Request_model','Transaction_model','Request_issues_model','Mechanic_issuses_model','Mechanic_add_service_model']);
        
        
        if(isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
            $mechanicId = $this->param['mechanic_id'];
            $getMechanicRequest = $this->Request_model->fields(['request_id','user_id','request_type','request_date'])->where(['request_status'=>6, 'mechanic_id'=>$mechanicId])->get_all();
            $requestId = [];
            if($getMechanicRequest){
                foreach($getMechanicRequest as $getRequests) {
                    $requestId[] = $getRequests['request_id'];
                }
            }
        } else if(isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $userId = $this->param['user_id'];
            $getRequest = $this->Request_model->fields(['request_id','mechanic_id','request_type','request_date'])->where(['request_status'=>6, 'user_id'=>$userId])->get_all();
            $requestId = [];
            if($getRequest){
                foreach($getRequest as $getRequests) {
                    $requestId[] = $getRequests['request_id'];
                }
            }
        }

        if(!empty($requestId)) {
            $getTransaction = $this->Transaction_model->where('request_id', $requestId)->order_by('created_at','desc')->get_all();
            if(!empty($getTransaction)) {
                foreach($getTransaction as $getTransactions) {
                    $transactionDateTime = $getTransactions['created_at'];
                    $explodeTransactionDateTime = explode(' ',$transactionDateTime);
                    $transactionDate = $explodeTransactionDateTime[0];
                    $transactionTime = $explodeTransactionDateTime[1];
        
                    $response[] = ['request_id'=>$getTransactions['request_id'],
                                'user_id'=>$getTransactions['user_id'],
                                'transaction_id'=>$getTransactions['transaction_charge_id'],
                                'amount'=>$getTransactions['amount'],
                                'payment_type'=>$getTransactions['payment_type'],
                                'transaction_status'=>$getTransactions['transaction_status'],
                                'transaction_date'=>$transactionDate,
                                'transaction_time'=>$transactionTime
                                ];
                }
            }
              
        }

        if(!empty($response)) {
            $transactionCount = count($response);
            $this->response(['status' => true, 'message'=> "Your have $transactionCount transaction history.",'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'You have no transaction history.']);die;
        }
    }   

    public function getTransactionDetails_post() {
        $this->load->model(['Transaction_model','Request_issues_model','Request_model','Mechanic_issuses_model','Issue_model','Mechanic_add_service_model']);
        $requestId=$this->param['request_id'];
        $userId=$this->param['user_id'];

        $getStatus = $this->Request_model->fields(['request_id','request_type','request_date'])->get(['request_id'=>$requestId, 'user_id'=>$userId]);
       
        $requestDate = $getStatus['request_date'];
        $explodeDate = explode(" ", $requestDate);
        if(!empty($getStatus)) {
            $getTransaction = $this->Transaction_model->get(['request_id'=>$requestId, 'user_id'=>$userId]);
            $getIssue = $this->Request_issues_model->fields('issue_id')->with_issue('fields:issue_name,issuse_price')->get_all(['request_id'=>$requestId, 'request_issue_status'=>101]);
            $getNewService = $this->Mechanic_add_service_model->get_all(['request_id'=>$requestId]);
 
            if(!empty($getIssue)) {
                $issueDetails = [];
                $issuePrice = '';
                foreach($getIssue as $key=>$getIssues) {
                    $issuseID = $getIssues['issue_id'];
                    $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price','issuse_id'])->get(['request_id'=>$requestId, 'issuse_id'=>$issuseID]);
                    
                    if(!empty($getNewPriceOfIssuse['issuse_price'])) {
                        $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                    } else {
                        $issusePrice = $getIssues['issue']['issuse_price'];
                    }

                    $issueDetails[] = [
                                        'service_id'=>$getIssues['issue_id'],
                                        'service_name'=>$getIssues['issue']['issue_name'],
                                        'service_price'=>$issusePrice
                                    ];
                    $issuePrice += $issusePrice;
                }
            }

            if(!empty($getNewService)) {
                $newService = [];
                $newServicePrice = '';
                foreach($getNewService as $key=>$getNewServices) {
                    $newService[] = [
                                        'service_id'=>$getNewServices['mechanic_add_service_id'],
                                        'service_name'=>$getNewServices['part_name'],
                                        'service_price'=>$getNewServices['price']
                                    ];
                    $newServicePrice += $getNewServices['price'];
                }
            }

            if($issuePrice != '' && $newServicePrice != '') {
                $total_amount = $issuePrice+$newServicePrice;
            } elseif($issuePrice == '' && $newServicePrice != '') {
                $total_amount = $newServicePrice;
            } elseif($issuePrice != '' && $newServicePrice == '') {
                $total_amount = $issuePrice;
            } else {
                $total_amount = '';
            }
           
           
            $result = [
                    'transaction_id'=>$getTransaction['transaction_id'],
                    'request_id' => $requestId,
                    'user_id' => $userId,
                    'total' => $getTransaction['amount'],
                    'country' => $getTransaction['source_country'],
                    'transaction_datetime' => $getTransaction['created_at'],
                    'issue_detail'=>(isset($issueDetails) ? $issueDetails : ''),
                    'new_service'=>(isset($newService) ? $newService : ''),
                    'request_type'=>$getStatus['request_type'],
                    'total_amount'=>$total_amount,
                    'request_date'=>$explodeDate[0],
                    'request_time'=>$explodeDate[1]
                ];
            if(!empty($getTransaction))
            {
               $this->response(['status' => true, 'message'=> 'Transaction Details.','response' => $result]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Not Any Details For This Request.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Not Any Details For This Request.']);die;
        }
    }
}