<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
		$this->load->model(['Common_model','Request_model','Mechanic_model','Mechanic_issuses_model','Request_issues_model']);
		$this->load->library('Googleapi');
    }

    public function allRequest_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('requests')->count_all_results();
		$requestList = $this->Request_model->fields('request_id,user_id,mechanic_id,request_type,request_status,request_date')
		->with_user('fields:name')
		->limit($limit,$offset)
		->order_by(['updated_at','DESC'])
		->get_all();
       
            foreach($requestList as $key=>$requestLists) {
                $mechanicId = $requestLists['mechanic_id'];
                $getMechanic = $this->Mechanic_model->fields('name')->where('mechanic_id',$mechanicId)->get();
                $requestList[$key]['mechanic_name'] = $getMechanic['name'];
            }

        if(!empty($requestList)) {
            echo json_encode(['status'=>true, 'message'=>'All Request Of Mechanic Here', 'response'=>['count'=>$count, 'request'=>$requestList]]);
        } else {
            echo json_encode(['status'=>false, 'messgae'=>'Not Found']);
        }
    }


    public function requestDetail_post() {
         $id = $this->param['request_id'];
         $requestDetail = $this->Request_model->fields('request_status')->where('request_id', $id)->get();
         $response['request_detail'] = $requestDetail;
         
         $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id','part_name','price'])->where(['request_id'=>$id])->get_all();
 
         $requestIssue = $this->Request_issues_model->fields(['issue_id','date','timepicker','location'])->where(['request_id'=>$id,'request_issue_status'=>101])->get_all();
         
         if(!empty($requestIssue)) {
             foreach($requestIssue as $key=>$requestIssues) {
                $issueId = $requestIssues['issue_id'];
                $issueDetail = $this->Issue_model->fields(['issue_id','issue_name','issuse_price'])->get(['issue_id'=>$issueId]);
            
                $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price','issuse_id'])->get(['request_id'=>$id, 'issuse_id'=>$issueId]);
                
                if(!empty($getNewPriceOfIssuse['issuse_price'])) {
                    $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                } else {
                    $issusePrice = $issueDetail['issuse_price'];
                }
    
                $service = [];
                if(!empty($getNewAddService)) {
                    foreach($getNewAddService as $getNewAddServices) {
                        $service[] = [
                            'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
                            'issue_name'=>$getNewAddServices['part_name'],
                            'issue_price'=>$getNewAddServices['price'],
                            'status'=>101  //101 for service price
                        ];
                    }
                }
    
                $result[] = ['issue_id'=>$issueDetail['issue_id'],
                'issue_name'=>$issueDetail['issue_name'],
                'issue_price'=>$issusePrice,
                'status'=>100 //100 for issue price
                ];
            }
            $response['issue'] = array_merge($result, $service);
            if(!empty($response)) {
                $this->response(['status' => true, 'message'=> 'All Completed Issse With Price','response' => $response]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            } 
        } elseif(!empty($getNewAddService)) {
            $service = [];
            if(!empty($getNewAddService)) {
                foreach($getNewAddService as $getNewAddServices) {
                    $service[] = [
                        'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
                        'issue_name'=>$getNewAddServices['part_name'],
                        'issue_price'=>$getNewAddServices['price']
                    ];
                }
            }
            if(!empty($service)) {
                $this->response(['status' => true, 'message'=> 'All Completed Issue With Price','response' => $service]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            } 
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    public function requestCompleteDetails_post()
	{
		$this->load->model(['Mechanic_rating_model','Request_issues_model','Mechanic_add_service_model','User_vehicles_model','Mechanic_issuses_model','Issue_model','Request_model']);
		$requestId = $this->param['request_id'];

		$getAllRequestIssuse = $this->Request_issues_model->fields('issue_id,request_issue_status,location,isCompleted,date,timepicker')->with_issue('fields:issue_name,issuse_price')->where(['request_id'=>$requestId, 'request_issue_status'=>101,'isDeleted'=>101])->get_all();
		
		$getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id','part_name','price','isDeleted','isCompleted'])->where(['request_id'=>$requestId, 'isDeleted'=>101])->get_all();

		$getRatingVal = $this->Mechanic_rating_model->fields('rating,comment')
		->where(['request_id' => $requestId, 'from_at'=>1])
		->get();
		if(!empty($getRatingVal)) {
			$isRating = true;
			$rating = $getRatingVal['rating'];
			$ratingComment = $getRatingVal['comment'];
		} else {
			$isRating = false;
			$rating = 0;
			$ratingComment = "";
		}

		$ratingDetails = array('isRating' => $isRating,'rating' => $rating,'ratingComment' => $ratingComment);

		$isCompleted = 100;

		$getResult=array();
		$ratingSum = '';
		$getMechanic = $this->Request_model->fields('request_id,user_id,request_type,mechanic_id,request_status,request_date,user_vehicles_id,comment,request_lat,request_lng')->with_transaction('fields: transaction_reference,transaction_charge_id')->with_mechanic('fields: name, email_id, contact_number,mechanic_uuid, mechanic_address, mechanic_picture')->with_newService('fields:part_name, price, description,isCompleted')->with_user('fields: name,email_id,contact_number')->get(['request_id' => $requestId]);

		if(empty($getMechanic)){
			$this->response(['status' => false, 'message' => 'Request Deatails not available.']);die;
		}

		$address = "";

		$user_vehicles_id = $getMechanic['user_vehicles_id'];
		if($user_vehicles_id == "" || $user_vehicles_id == 0){
			$where = ['user_id' => $getMechanic['user_id'],'isDefault' => 101];
		} else {
			$where = ['user_vehicles_id'=>$user_vehicles_id];
		}

		$userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id','isDefault','vehicle_picture'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where($where)->get();

		if($userVehicle){
			$vehicleDetails = [
				'user_vehicles_id' =>$userVehicle['user_vehicles_id'],
				'vehicle_picture' =>$userVehicle['vehicle_picture'],
				'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
				'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
				'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
				'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
				'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
				'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
			];
		} else {
			$vehicleDetails = array();
		}

		if($getMechanic['request_status'] == 2 || $getMechanic['request_status'] == 9 || $getMechanic['request_status'] == 10) {
			$isJourney = true;
		} else {
			$isJourney = false;
		}

		if($getMechanic['request_type'] == 2){
			if($getMechanic['request_lat'] != '')
			$address = $this->googleapi->getAddress($getMechanic['request_lat'],$getMechanic['request_lng']);
		}

		$requestDate = $getMechanic['request_date'];
		$explodeDate = explode(" ", $requestDate);
		$date = $explodeDate[0];
		$time = $explodeDate[1];
		$getResult['request'] = [
			'request_id'=>$getMechanic['request_id'],
			'request_type'=>$getMechanic['request_type'],
			'request_status'=>$getMechanic['request_status'],
			'transaction_id' => !empty($getMechanic['transaction']['transaction_reference']) ? $getMechanic['transaction']['transaction_reference'] : $getMechanic['transaction']['transaction_charge_id'],
			'isJourney' => $isJourney
		];

        $getResult['user'] = $getMechanic['user'];
		$getResult['mechanic'] = $getMechanic['mechanic'];
		$mechanicsID = $getMechanic['mechanic_id'];

		if($mechanicsID != '') {
			$getRating = $this->Mechanic_rating_model
			->where(['mechanic_id'=>$mechanicsID, 'from_at'=>1])->get_all();
			if(!empty($getRating)) {
				foreach($getRating as $getRatings) {
					$ratingSum += $getRatings['rating'];
				}
			}
			$avgRating = $ratingSum/count($getRating);
			if(isset($avgRating) && $avgRating != '') {
			$getResult['mechanics']['avgRating'] = round($avgRating);
			} else {
				$getResult['mechanics']['avgRating'] = 0;
			}
		}else{
			$getResult['mechanics']['avgRating'] = 0;
		}

		$comment = $getMechanic['comment'];
		if(empty($comment) || $comment == null){
			$comment = "";
		}

		if(!empty($getAllRequestIssuse)){
			
			$total ='';
			$newServicePrice ='';
			$result = [];
			foreach($getAllRequestIssuse as $key=>$getAllRequestIssuses) {
				$date = $getAllRequestIssuses['date'];
				$address = $getAllRequestIssuses['location'];
				$time = $getAllRequestIssuses['timepicker'];
				$issuseID = $getAllRequestIssuses['issue_id'];
				$getIssuePrice = $this->Issue_model->fields(['issue_id','issue_name','issuse_price'])->where('issue_id',$issuseID)->get();


				$getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price','issuse_id','isCompleted'])->get(['request_id'=>$requestId, 'issuse_id'=>$issuseID]);
				
				if(!empty($getNewPriceOfIssuse['issuse_price'])) {
					$issusePrice = $getNewPriceOfIssuse['issuse_price'];
					$isCompleted = $getNewPriceOfIssuse['isCompleted'];
				} else {
					$issusePrice = $getIssuePrice['issuse_price'];
					$isCompleted = $getAllRequestIssuses['isCompleted'];
				}

				$service = [];
				if(!empty($getNewAddService)) {
					foreach($getNewAddService as $getNewAddServices) {
						$service[] = [
							'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
							'issue_name'=>$getNewAddServices['part_name'],
							'issue_price'=>$getNewAddServices['price'],
							'status'=>101,  //101 for edit service price
							'request_issue_status'=>$getNewAddServices['isDeleted'],
							'isCompleted' => $getNewAddServices['isCompleted']
							// 'status' => '101' //Services added by mechanincs
						];
					}
				}
	
				$result[] = ['issue_id'=>$getAllRequestIssuses['issue']['issue_id'],
					'issue_name'=>$getAllRequestIssuses['issue']['issue_name'],
					'issue_price'=>$issusePrice,
					'status'=>100, //100 for edit issue price
					'request_issue_status' => $getAllRequestIssuses['request_issue_status'],
					'isCompleted' => $isCompleted
					// 'status' => '100' //Issue added by User
					];
			}

			$getResult['request']['request_date'] = $date;
			$getResult['request']['request_time'] = $time;
			$getResult['request']['location'] = $address;

			$response = array_merge($result, $service);
			if(!empty($response)) {
				$this->response(['status' => true, 'message'=> 'All Completed Issse With Price','response' => ['issue' => $response,'comment' => $comment,'vehicleDetails' => $vehicleDetails,'ratingDetails' => $ratingDetails,'requestDetails' => $getResult]]);die;
			} else {
				 $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
			} 
		} elseif(!empty($getNewAddService)) {
			$service = [];
			
			$getResult['request']['request_date'] = $date;
			$getResult['request']['request_time'] = $time;
			$getResult['request']['location'] = $address;

            if(!empty($getNewAddService)) {
				foreach($getNewAddService as $getNewAddServices) {
					$service[] = [
						'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
						'issue_name'=>$getNewAddServices['part_name'],
						'issue_price'=>$getNewAddServices['price'],
						'request_issue_status'=>$getNewAddServices['isDeleted'],
						'isCompleted' => $getNewAddServices['isCompleted'],
						'status' => '101' //Services added by mechanincs 
					];
				}

            }
            if(!empty($service)) {
                $this->response(['status' => true, 'message'=> 'All Completed Issue With Price','response' => ['issue' => $service,'vehicleDetails' => $vehicleDetails,'ratingDetails' => $ratingDetails,'comment' => $comment,'requestDetails' => $getResult]]);die;
            } else {
                 $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            } 

        } else {
			$getResult['request']['request_date'] = $date;
			$getResult['request']['request_time'] = $time;
			$getResult['request']['location'] = $address;
			$this->response(['status' => true, 'message'=> 'Issue Details not available','response' => ['issue' => [],'vehicleDetails' => $vehicleDetails,'ratingDetails' => $ratingDetails,'comment' => $comment,'requestDetails' => $getResult]]);die;
		}
	}
    
}