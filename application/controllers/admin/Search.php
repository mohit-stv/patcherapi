<?php defined('BASEPATH') or exit('No direct script access allowed');
class Search extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->param = json_decode(file_get_contents('php://input'), true);
        $this->load->model(['User_model', 'Mechanic_model']);
    }

    // For Mechanic (Admin End)
    public function searchMechanic_post()
    {
        if (!isset($this->param['search']) || empty($this->param['search'])) {
            $this->response(['status' => false, 'message' => 'Unable to find Search Keyword.']);
        }

        $search_keyword = $this->param['search'];
        $limit = (isset($this->param['limit']) && !empty($this->param['limit'])) ? $this->param['limit'] : 0;
        $offset = (isset($this->param['offset']) && !empty($this->param['offset'])) ? $this->param['offset'] : 0;

        $mechanic_list = $this->Mechanic_model->searchMechanic($search_keyword, $limit, $offset);

        if ($mechanic_list) {
            // For counting Rows
            $count = count($mechanic_list);

            $response = [
                'count' => $count,
                'contacts' => $mechanic_list,
            ];
            $this->response(['status' => true, 'message' => 'Mechanic Details Available ', 'response' => $response]);
        } else {
            $this->response(['status' => false, 'message' => 'Mechanic Not found.']);
        }
    }

    // For User (Admin End)
    public function searchUser_post()
    {
        if (!isset($this->param['search']) || empty($this->param['search'])) {
            $this->response(['status' => false, 'message' => 'Unable to find Search Keyword.']);
        }

        $search_keyword = $this->param['search'];
        $limit = (isset($this->param['limit']) && !empty($this->param['limit'])) ? $this->param['limit'] : 0;
        $offset = (isset($this->param['offset']) && !empty($this->param['offset'])) ? $this->param['offset'] : 0;

        $user_list = $this->User_model->searchUser($search_keyword, $limit, $offset);

        if ($user_list) {
            // For counting Rows
            $count = count($user_list);

            $response = [
                'count' => $count,
                'user' => $user_list,
            ];
            $this->response(['status' => true, 'message' => 'User Details Available ', 'response' => $response]);
        } else {
            $this->response(['status' => false, 'message' => 'User Not found.']);
        }
    }

    // For Request
    public function searchRequest_post()
    {
        if (!isset($this->param['search']) || empty($this->param['search'])) {
            $this->response(['status' => false, 'message' => 'Unable to find Search Keyword.']);
        }

        $search_keyword = $this->param['search'];
        $limit = (isset($this->param['limit']) && !empty($this->param['limit'])) ? $this->param['limit'] : 0;
        $offset = (isset($this->param['offset']) && !empty($this->param['offset'])) ? $this->param['offset'] : 0;
        $user_list = $this->User_model->searchUser($search_keyword, $limit, $offset);

        $request_list = $this->Request_model->searchRequest($search_keyword, $limit, $offset);

        if ($request_list) {
            // For counting Rows
            $count = count($request_list);

            $response = [
                'count' => $count,
                'request' => $request_list,
            ];
            $this->response(['status' => true, 'message' => 'Request Details Available ', 'response' => $response]);
        } else {
            $this->response(['status' => false, 'message' => 'Request Not found.']);
        }
    }

};
