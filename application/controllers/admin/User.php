<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model','Mechanic_model','Request_model','User_model','Request_issues_model','Issue_model','Transaction_model','Mechanic_issuses_model','Mechanic_add_service_model']);
    }

    public function userList_post() {
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('users')->count_all_results();
        $userList = $this->User_model->fields('user_id,name,email_id,contact_number,isBlocked')->limit($limit,$offset)->order_by('user_id','desc')->get_all();
        if(!empty($userList)) {
            echo json_encode(['status' => true, 'message' => 'All Users List.', 'response' => ['count' => $count, 'user'=>$userList]]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'No User here']);die;
        }
    }


    public function userDetail_post() {
        $id = $this->param['user_id'];
        $userDetail = $this->User_model->fields('name,email_id,contact_number,profile_picture,address')->where('user_id',$id)->get();
        if(!empty($userDetail)) {
            echo json_encode(['status' => true, 'message' => 'Users Details.', 'response' => $userDetail]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'No Details here']);die;
        }
    }


    public function userRequest_post() {
        $id = $this->param['user_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('requests')->where('user_id',$id)->count_all_results();
        $userRequest = $this->Request_model->fields('request_id,request_type,request_status,created_at')->where('user_id',$id)->limit($limit,$offset)->get_all();
       //$userRequest = $this->User_model->with_request('fields: request_id,request_type,created_at,request_status')->where('user_id',$id)->get_all();
       
       if(!empty($userRequest)) {
        foreach($userRequest as $key=>$userRequests) {
            $requestId = $userRequests['request_id'];
            $requestLocation = $this->Request_issues_model->fields('location')->where('request_id',$requestId)->get();
            $userRequest[$key]['location'] = (isset($requestLocation['location']) ? $requestLocation['location'] : '');

            $requestTransaction = $this->Transaction_model->fields('amount,paid_status,transaction_status,created_at')->where(['user_id'=>$id,'request_id'=>$requestId])->get();
            $userRequest[$key]['transaction'] = (!empty($requestTransaction) ? $requestTransaction : [] );
        }
       }

        if(!empty($userRequest)) {
            echo json_encode(['status' => true, 'message' => 'Users Details.', 'response' => ['count'=>$count, 'request'=>$userRequest]]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'No Details here']);die;
        }
    }


    public function requestDetail_post() {
        $id = $this->param['request_id'];
        $requestDetail = $this->Request_model->fields('request_status')->where('request_id', $id)->get();
        $response['request_detail'] = $requestDetail;
        
        $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id','part_name','price'])->where(['request_id'=>$id])->get_all();

        $requestIssue = $this->Request_issues_model->fields(['issue_id','date','timepicker','location'])->where(['request_id'=>$id,'request_issue_status'=>101])->get_all();
        
        if(!empty($requestIssue)) {
            foreach($requestIssue as $key=>$requestIssues) {
                $issueId = $requestIssues['issue_id'];
                $issueDetail = $this->Issue_model->fields(['issue_id','issue_name','issuse_price'])->get(['issue_id'=>$issueId]);
            
                $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price','issuse_id'])->get(['request_id'=>$id, 'issuse_id'=>$issueId]);
                
                if(!empty($getNewPriceOfIssuse['issuse_price'])) {
                    $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                } else {
                    $issusePrice = $issueDetail['issuse_price'];
                }
    
                $service = [];
                if(!empty($getNewAddService)) {
                    foreach($getNewAddService as $getNewAddServices) {
                        $service[] = [
                            'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
                            'issue_name'=>$getNewAddServices['part_name'],
                            'issue_price'=>$getNewAddServices['price'],
                            'status'=>101  //101 for service price
                        ];
                    }
                }
    
                $result[] = ['issue_id'=>$issueDetail['issue_id'],
                'issue_name'=>$issueDetail['issue_name'],
                'issue_price'=>$issusePrice,
                'status'=>100 //100 for issue price
                ];
            }
            $response['issue'] = array_merge($result, $service);
            if(!empty($response)) {
                $this->response(['status' => true, 'message'=> 'All Completed Issse With Price','response' => $response]);die;
            } else {
                 $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            } 
        } elseif(!empty($getNewAddService)) {
            $service = [];
            if(!empty($getNewAddService)) {
                foreach($getNewAddService as $getNewAddServices) {
                    $service[] = [
                        'issue_id'=>$getNewAddServices['mechanic_add_service_id'],
                        'issue_name'=>$getNewAddServices['part_name'],
                        'issue_price'=>$getNewAddServices['price']
                    ];
                }
            }
            if(!empty($service)) {
                $this->response(['status' => true, 'message'=> 'All Completed Issue With Price','response' => $service]);die;
            } else {
                 $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            } 
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }
    

    public function userVehicle_post() {
        $id = $this->param['user_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];

        $this->load->model(['User_vehicles_model','Vehicle_bodytypes_model','Vehicle_drivetypes_model','Vehicle_engines_model','Vehicle_make_model','Vehicle_models_model','Vehicle_weight_model','Vehicle_year_model']);
        
        $count = $this->db->from('user_vehicles')->where(['user_id'=>$id,'isDeleted' => 101])->count_all_results();
        
        $userVehicle = $this->User_vehicles_model->fields('*')->where(['user_id'=>$id])->limit($limit,$offset)->get_all(['isDeleted' => 101]);
        
        if(!empty($userVehicle)) {
            $vehicleDetails = array();
        foreach($userVehicle as $key=>$getUserVehicles) {
            $vehicleDetails[$key]['vehicles_id'] = $getUserVehicles['user_vehicles_id'];
            $vehicleDetails[$key]['vehicle_picture'] = $getUserVehicles['vehicle_picture'];


            $makeId = $getUserVehicles['vehicle_make_id'];
            $getMake = $this->Vehicle_make_model->fields(['vehicle_make_name'])->where(['vehicle_make_id'=>$makeId])->get();
            $vehicleDetails[$key]['make_name'] = ($getMake['vehicle_make_name'] != null) ? $getMake['vehicle_make_name'] : '';


            $modelId = $getUserVehicles['vehicle_model_id'];
            $getModel = $this->Vehicle_models_model->fields(['     vehicle_modelname'])->where(['vehicle_model_id'=>$modelId])->get();
            $vehicleDetails[$key]['model_name'] = ($getModel['vehicle_modelname'] != null) ? $getModel['vehicle_modelname'] : '';


            $bodyId = $getUserVehicles['vehicle_bodytype_id'];
            $getBody = $this->Vehicle_bodytypes_model->fields(['vehicle_bodytypes_name'])->where(['vehicle_bodytypes_id'=>$bodyId])->get();
            $vehicleDetails[$key]['body_type_name'] = ($getBody['vehicle_bodytypes_name'] != null) ? $getBody['vehicle_bodytypes_name'] : '';


            $yearId = $getUserVehicles['vehicle_year_id'];
            $getYear = $this->Vehicle_year_model->fields(['vehicle_years'])->where(['vehicle_years_id'=>$yearId])->get();
            $vehicleDetails[$key]['year_name'] = ($getYear['vehicle_years'] != null) ? $getYear['vehicle_years'] : '';


            $weightId = $getUserVehicles['vehicle_weight_id'];
            $getWeight = $this->Vehicle_weight_model->fields(['vehicle_weight'])->where(['vehicle_weight_id'=>$weightId])->get();
            $vehicleDetails[$key]['weight_name'] = ($getWeight['vehicle_weight'] != null) ? $getWeight['vehicle_weight'] : '';


            $engineId = $getUserVehicles['vehicle_engine_id'];
            $getEngine = $this->Vehicle_engines_model->fields(['vehicle_engine'])->where(['vehicle_engines_id'=>$engineId])->get();
            $vehicleDetails[$key]['engine_name'] = ($getEngine['vehicle_engine'] != null) ? $getEngine['vehicle_engine'] : '';


            $driveId = $getUserVehicles['vehicle_drivetype_id'];
            $getDrive = $this->Vehicle_drivetypes_model->fields(['vehicle_drivetype_name'])->where(['vehicle_drivetypes_id'=>$driveId])->get();
            $vehicleDetails[$key]['drivetype_name'] = ($getDrive['vehicle_drivetype_name'] != null) ? $getDrive['vehicle_drivetype_name'] : '';
            }
        }

        if(!empty($vehicleDetails)){
            $this->response(['status' => true, 'message'=> 'All User Vehicle Here','response' => ['count'=>$count, 'vehicle_list'=>$vehicleDetails]]);die;
        }else{
            $this->response(['status' => false, 'message' => 'No Vehicles']);die;
        }
    }


    public function userTransaction_post() {
        $id = $this->param['user_id'];
        $offset = $this->param['offset'];
        $limit = $this->param['limit'];
        $count = $this->db->from('transactions')->where(['user_id'=>$id])->count_all_results();

        $userTransaction = $this->Transaction_model->fields('request_id,amount,paid_status,transaction_status,created_at')->where(['user_id'=>$id])->limit($limit,$offset)->get_all();
       
        if(!empty($userTransaction)) {
            foreach($userTransaction as $key=>$userTransactions) {
                $requestDetail = $this->Request_model->fields('request_type,request_status,mechanic_id')->where('request_id', $id)->get();
                $userTransaction[$key]['request'] = ['request_status'=>$requestDetail['request_status']];
               // $userTransaction[$key]['mechanic'] = ['mechanic_name'=>$requestDetail['mechanic']['name']];
            }
        }
        if(!empty($userTransaction)){
            $this->response(['status' => true, 'message'=> 'All User Transaction Here','response' => ['count'=>$count, 'user_transaction'=>$userTransaction]]);die;
        }else{
            $this->response(['status' => false, 'message' => 'No Vehicles']);die;
        }
    }

}