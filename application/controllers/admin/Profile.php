<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MY_Controller
{

    public $redis;

    public function __construct()
    {
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model', 'Mechanic_model', 'Mechanic_service_model', 'Mechanic_document_model', 'Mechanic_experience_model', 'Experience_model', 'Mechanic_qualification_model', 'Qualification_model', 'Tool_itinerary_model', 'Mechanic_itineraries_model', 'Mechanic_health_model', 'Health_safety_check_model', 'Admin_transaction_model', 'Mechanic_registrations_model', 'Mechanic_issuses_model', 'Mechanic_request_detail_model', 'Bank_detail_model']);
        $this->load->library(['Curl', 'Mypubnub', 'Notification']);
        $this->load->helper('common_helper');
        ini_set('upload_max_filesize', '2MB');
        ini_set('max_execution_time', 300);
        $this->redis = createConnectionToRedis();
    }

    ## For Add And Update Mechanics Health
    public function addUpdateMechanicHealth_post()
    {
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_health']) && !empty($this->param['mechanic_health'])) {
            $this->load->model('Mechanic_health_model');
            $mechanicsHealthDocuments_array = [
                'health_id' => trim($this->param['mechanic_health']),
                'mechanic_id' => $id,
            ];

            $getVal = $this->Mechanic_health_model->get(['mechanic_id' => $id]);
            if (!empty($getVal)) {
                $updateData = ['health_id' => trim($this->param['mechanic_health'])];
                $update = $this->Mechanic_health_model->where('mechanic_id', $id)->update($updateData);
                if ($update) {

                    $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['health_status' => 101, 'comment' => 'Health Updated by Mechanic']);

                    $this->response(['status' => true, 'message' => 'Update Successfuly.', 'response' => $updateData]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
                }
            } else {
                $insert = $this->Mechanic_health_model->insert($mechanicsHealthDocuments_array);

                if ($insert) {

                    $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['health_status' => 101, 'comment' => 'Health Updated by Mechanic']);

                    $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $insert]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong.']);die;
                }
            }
        }
    }

    ## For Add And Update Mechanics Itinerary BY MOHIT 09-01-2018 13:10:00
    public function addUpdateMechanicItinerary_post()
    {
        $this->load->model(['Mechanic_itineraries_model', 'Mechanic_service_model', 'Service_model', 'Tool_itinerary_model']);

        $id = $this->param['mechanic_id'];
        //$toolId=$this->param['tool_itinerary_id'];

        if (!isset($this->param['tool_itineraries']) || empty($this->param['tool_itineraries'])) {
            $this->response(['status' => false, 'message' => 'Tool Itinerary Details are mandatory.']);
        }

        $toolItineraries = $this->param['tool_itineraries'];
        $mechanicServices = $this->Mechanic_service_model->fields('mechanic_services_id,services_id')->get_all(['mechanic_id' => $id, 'mechanic_services_status' => 101]);

        if (!$mechanicServices) {
            $this->response(['status' => false, 'mesage' => 'Please add services first.']);
        }

        $services = [];
        foreach ($mechanicServices as $mechanicService) {
            #adding temporary soln lookinf for good aporach if available
            $services[] = $mechanicService['services_id'];
        }

        $updateData = [];
        foreach ($toolItineraries as $toolItinerary) {
            $serviceId = $toolItinerary['services_id'];
            if (in_array($serviceId, $services)) {
                // $toolItinerary['mechanic_id'] = $id;
                $updateData[] = $this->Mechanic_service_model->where(['services_id' => $serviceId, 'mechanic_id' => $id])->update(['tool_itinerary_id' => $toolItinerary['tool_itinerary_id']]);
            }
        }

        if (count($updateData) > 0) {
            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['tools_status' => 101, 'comment' => 'Tool Updated by Mechanic']);
            // $this->redis->zrem('mechanics',$id);
            $this->response(['status' => true, 'message' => 'Update Successful.', 'response' => $toolItineraries]);die;
        } elseif (count($updateData) == 0) {
            $this->response(['status' => false, 'message' => 'Invalid Details for mechanic tool itneraries.']);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong. Please try again later.']);die;
        }
    }

    ## For Add And Update Mechanics Service
    public function addUpdateMechanicsService_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model('Mechanic_service_model');
        if (isset($this->param['mechanic_services']) && !empty($this->param['mechanic_services'])) {
            $services = $this->param['mechanic_services'];
            $mechanicsInsertData = array();
            $update = array();
            $updateData = array();
            $insert = array();

            $this->db->trans_start();

            foreach ($services as $key => $service) {
                $serviceId = $service['service_id'];

                $mechanicsService_array = ['mechanic_id' => $id,
                    'services_id' => $serviceId,
                    'mechanic_services_status' => $service['mechanic_status']];

                $getVal = $this->Mechanic_service_model->get(['mechanic_id' => $id, 'services_id' => $serviceId]);
                if (!empty($getVal)) {
                    $updateWhere = array('services_id' => $serviceId,
                        'mechanic_id' => $id,
                    );
                    $updateData[] = ['services_id' => $serviceId, 'mechanic_services_status' => $service['mechanic_status']];

                    $update[] = $this->Mechanic_service_model->where($updateWhere)->update(['mechanic_services_status' => $service['mechanic_status']]);
                } else {
                    $insert[] = $this->Mechanic_service_model->insert($mechanicsService_array);

                    $mechanicsInsertData[] = ['mechanic_id' => $id,
                        'services_id' => $serviceId,
                        'mechanic_services_status' => $service['mechanic_status']];
                }
            }

            // echo $this->db->last_query();die;

            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['service_status' => 101]);

            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $this->response(['status' => true, 'message' => 'Services updated successfully.']);
            } else {
                $this->db->trans_rollback();
                $this->response(['status' => false, 'message' => 'Something went wrong. Please try again later.']);
            }
        }
    }

    ## For Add And Update Mechanics Documentation
    public function addUpdateMechanicsDocumentation_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_documentation']) && !empty($this->param['mechanic_documentation'])) {

            $this->load->model('Mechanic_document_model');
            $documentation = $this->param['mechanic_documentation'];

            $update = array();
            $insert = array();
            foreach ($documentation as $documentations) {

                $documentData = [
                    'document_id' => $documentations['document_id'],
                    'mechanic_id' => $id,
                    'document_url' => $documentations['document_url'],
                    'mechanic_document_status' => $documentations['document_status'],
                    'public_id' => (isset($documentations['public_id']) ? $documentations['public_id'] : 0),
                ];

                if (!empty($documentations['mechanic_document_id'])) {
                    $documentData['mechanic_document_id'] = $documentations['mechanic_document_id'];
                    $update[] = $documentData;
                    $mechanic_document_id[] = $documentations['mechanic_document_id'];
                } else {
                    $insert[] = $documentData;
                }
            }

            $this->db->trans_start();
            if (!empty($insert)) {
                $this->Mechanic_document_model->insert($insert);
            }

            if (!empty($update)) {
                $this->Mechanic_document_model->update($update, 'mechanic_document_id');
            }

            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['documentation_status' => 101]);

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                echo json_encode(['status' => true, 'message' => 'Document Updated.']);die;
            } else {
                $this->db->trans_rollback();
                echo json_encode(['status' => false, 'message' => 'Error in process. Please try again after sometime.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'mechanic_documentation is mandatory.']);
        }
    }

    ## For Add ANd Update Mechanics Experience
    public function addUpdateMechanicsExperience_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model', 'common_model']);
        $experience = $this->param['experience_id'];
        if (isset($this->param['mechanic_experience_status']) && !empty($this->param['mechanic_experience_status'])) {
            $mechanicExperienceStatus = $this->param['mechanic_experience_status'];
        } else {
            $mechanicExperienceStatus = 101;
        }

        $updateData = array();
        $insert = array();
        $update = array();

        $experienceData = ['experience_id' => $experience,
            'mechanic_id' => $id,
            'mechanic_experience_status' => 102];

        $this->db->trans_start();

        $getExperience = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_url' => '', 'isPass' => '0']);
        if (!empty($getExperience)) {
            $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_url' => '', 'isPass' => '0'])->update(['experience_id' => $experience, 'mechanic_experience_status' => $mechanicExperienceStatus]);
            $updateData['range'] = ['experience_id' => $experience];
        } else {
            $insert[] = $this->Mechanic_experience_model->insert($experienceData);
        }

        if (isset($this->param['isPass']) && !empty($this->param['isPass'])) {

            $isPass = $this->param['isPass'];
            foreach ($isPass as $isPasses) {
                if (isset($isPasses['mechanicExperienceFile']) && $isPasses['mechanicExperienceFile'] != '') {

                    $imagePath = $isPasses['mechanicExperienceFile'];
                    // $imageData = $this->common_model->convertBase64ToImage($img);

                    // $data = $imageData['data'];
                    // $mime = $imageData['mime'];

                    // $image_name ='uploads/experience/'.rand(1000,5000) . '.'.$mime;
                    // file_put_contents($image_name, $data);
                    // chmod($image_name, 0777);
                    // $imagePath = ROOTPATH.$image_name;

                    $isPassData = ['experience_id' => $isPasses['experience_id'],
                        'mechanic_id' => $id,
                        'isPass' => $isPasses['mechanic_experience_status'],
                        'experience_url' => $imagePath,
                        'mechanic_experience_status' => 101];

                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']]);

                    if (!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']])->update(['experience_url' => $imagePath, 'isPass' => $isPasses['mechanic_experience_status']]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id'], 'experience_url' => $imagePath, 'isPass' => $isPasses['mechanic_experience_status']];

                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                } else {
                    $isPassData = ['experience_id' => $isPasses['experience_id'],
                        'mechanic_id' => $id,
                        'isPass' => $isPasses['mechanic_experience_status'],
                        'mechanic_experience_status' => 101];

                    $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']]);

                    if (!empty($getIsPass)) {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id']])->update(['isPass' => $isPasses['mechanic_experience_status'], 'experience_url' => '']);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $isPasses['experience_id'], 'experience_url' => '', 'mechanic_experience_status' => $getIsPass['isPass']];
                    } else {
                        $insert[] = $this->Mechanic_experience_model->insert($isPassData);
                    }
                }
            }
        }

        if (isset($this->param['refrences']) && !empty($this->param['refrences'])) {
            $refrence = $this->param['refrences'];
            foreach ($refrence as $refrences) {

                if (isset($refrences['mechanicExperienceFile']) && $refrences['mechanicExperienceFile'] != '') {

                    $root_Refrenceimage_name = $refrences['mechanicExperienceFile'];
                } else {
                    $root_Refrenceimage_name = '';
                }

                if (isset($refrences['public_id']) && $refrences['public_id'] != '') {

                    $public_id = $refrences['public_id'];
                } else {
                    $public_id = 0;
                }

                $refrenceData = ['experience_id' => $refrences['experience_id'],
                    'mechanic_id' => $id,
                    'experience_url' => $root_Refrenceimage_name,
                    'mechanic_experience_status' => 101,
                    'public_id' => $public_id,
                ];

                $getIsPass = $this->Mechanic_experience_model->get(['mechanic_id' => $id, 'isPass' => 0, 'experience_id' => $refrences['experience_id']]);

                if (!empty($getIsPass)) {
                    if ($root_Refrenceimage_name != '') {
                        $update[] = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'isPass' => '0'])->update(['experience_url' => $root_Refrenceimage_name, 'public_id' => $public_id, 'mechanic_experience_status' => 101]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'experience_url' => $root_Refrenceimage_name, 'public_id' => $public_id];
                    } else {
                        $update = $this->Mechanic_experience_model->where(['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], 'isPass' => '0'])->update(['experience_url' => $root_Refrenceimage_name, 'public_id' => $public_id]);

                        $updateData['isPass'][] = ['mechanic_id' => $id, 'experience_id' => $refrences['experience_id'], ['experience_url' => $root_Refrenceimage_name, 'public_id' => $public_id]];
                    }

                } else {
                    $insert[] = $this->Mechanic_experience_model->insert($refrenceData);
                }
            }
        }

        $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['experience_status' => 101]);

        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            echo json_encode(['status' => true, 'message' => 'Update Successful.', 'response' => $updateData]);die;
        } else {
            $this->db->trans_rollback();
            echo json_encode(['status' => false, 'message' => 'Something went wrong. Please try again later.']);die;
        }
    }

    ## For Add and Update Mechanics Qualification
    public function addUpdateMechanicsQualification_post()
    {
        $this->load->model('common_model');
        $id = $this->param['mechanic_id'];
        if (isset($this->param['mechanic_qualification']) && !empty($this->param['mechanic_qualification'])) {
            $this->load->model(['Mechanic_qualification_model', 'common_model']);
            $qualification = $this->param['mechanic_qualification'];

            $update = array();
            $insert = array();

            $updateDriver = false;
            foreach ($qualification as $key => $qualifications) {
                $qualificationStatus = $qualifications['mechanic_qualification_status'];
                $mechanicsQualification = [
                    'qualification_id' => $qualifications['qualification_id'],
                    'mechanic_id' => $id,
                    'qualification_url' => (isset($qualifications['qualification_url']) ? $qualifications['qualification_url'] : ""),
                    'mechanic_qualification_status' => $qualificationStatus,
                    'public_id' => (isset($qualifications['public_id']) ? $qualifications['public_id'] : 0),
                ];

                if (!$updateDriver && $qualificationStatus != 102) {
                    $updateDriver = true;
                }

                if (!isset($qualifications['mechanic_qualification_id']) || !empty($qualifications['mechanic_qualification_id'])) {
                    $mechanicsQualification['mechanic_qualification_id'] = $qualifications['mechanic_qualification_id'];
                    $update[] = $mechanicsQualification;
                } else {
                    $insert[] = $mechanicsQualification;
                }
            }

            $this->db->trans_start();

            if (!empty($insert)) {
                $this->Mechanic_qualification_model->insert($insert);
            }

            if (!empty($update)) {
                $this->Mechanic_qualification_model->update($update, 'mechanic_qualification_id');
            }

            $this->Mechanic_registrations_model->where('mechanic_id', $id)->update(['qualification_status' => 101]);

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                echo json_encode(['status' => true, 'message' => 'Qualification Updated.']);die;
            } else {
                $this->db->trans_rollback();
                echo json_encode(['status' => false, 'message' => 'Error in process. Please try again after sometime.']);die;
            }
        }
    }

}
