<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profiles extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function mechanicProfiles_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('mechanicProfiles') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 
            $this->load->model(['Mechanics_model']);
            $mechanicId = $this->param['mechanicId'];
            $profile_array=[
                'name' => $this->param['name'],
                'mechanicDOB' => $this->param['mechanicDOB'],
                'mechanicAddress' => $this->param['mechanicAddress'],
                'mechanicTelephone' => $this->param['mechanicTelephone'],
            ];
            $update = $this->Mechanics_model->where('mechanicId',$mechanicId)->update($profile_array);
                
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function mechanicDetails_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('mechanicDetails') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 

            $this->load->model(['Mechanics_model','Mechanic_documents_model','Mechanic_qualifications_model']);            
            $mechanicId = $this->param['mechanicId'];

            $mechanicServices = implode(",", $this->param['mechanicServices']);
            $mechanicExperience = $this->param['mechanicExperience'];
            $mechanicItinerary = implode(",", $this->param['mechanicItinerary']);

            $mechanics_array=[
                'mechanicServices' => $mechanicServices,
                'mechanicExperience' => $mechanicExperience,
                'mechanicItinerary' => $mechanicItinerary,                        
            ];

            $update = $this->Mechanics_model->where('mechanicId',$mechanicId)->update($mechanics_array);
            
            
            if(isset($this->param['documents']) && $this->param['documents'] != NULL){
                for($i=0;$i<count($this->param['documents']);$i++){
                    $documentId = $this->param['documents'][$i]['documentId'];
                    $documentUrl = $this->param['documents'][$i]['documentUrl'];
                    $document_name = "assets/mechanic/".rand(1000,5000).time().".png";
                    $document = $this->common_model->getImageBase64Code($documentUrl);
                    file_put_contents($document_name,$document);
                    chmod($document_name, 0777);
                    $documents_array=[
                        'mechanicId' => $mechanicId,
                        'documentId' => $documentId,
                        'documentUrl' => $document_name,                        
                    ];
                    $this->Mechanic_documents_model->insert($documents_array);
                }
            }
            
            if(isset($this->param['qualifications']) && $this->param['qualifications'] != NULL){
                for($i=0;$i<count($this->param['qualifications']);$i++){
                    $qualificationId = $this->param['qualifications'][$i]['qualificationId'];
                    $qualificationUrl = $this->param['qualifications'][$i]['qualificationUrl'];  
                    $qualification_name = "assets/mechanic/".rand(1000,5000).time().".png";
                    $qualification = $this->common_model->getImageBase64Code($qualificationUrl);
                    file_put_contents($qualification_name,$qualification);
                    chmod($qualification_name, 0777);
                    $qualifications_array=[
                        'mechanicId' => $mechanicId,
                        'qualificationId' => $qualificationId,
                        'qualificationUrl' => $qualification_name,                        
                    ];
                    $this->Mechanic_qualifications_model->insert($qualifications_array);                                  
                }
            }            
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 


}