<?php defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');
date_default_timezone_set('Asia/Kolkata');
class Login extends CI_Controller
{
	private $param = array();
	public function __construct(){
		parent::__construct();
		$this->param = (array)json_decode(file_get_contents('php://input'), true);
		$this->load->model(['User_model','common_model']);
		$this->load->library('encrypt');
	}	

	/*----------------------------------------------------------------------------------------
    |   FUNCTIONS
    |----------------------------------------------------------------------------------------- 
    */

    # Send Email By MailChimp
    private function sendEmail( $message, $email ){
        // $this->load->library('mandrill',['GIj6O_S6xBnYrNuXVJgZEw']);
        // $mandrill = new Mandrill('GIj6O_S6xBnYrNuXVJgZEw');

        $this->load->library('mandrill',['ttXdKUiptJz7Oy6Er1czww']);
        $mandrill = new Mandrill('ttXdKUiptJz7Oy6Er1czww');

        $content = array(
            'html' => $message,
            'text' => 'Boyku',
            'subject' => 'Boyku Registration',
            'from_email' => 'contact@boyku.com',
            'from_name' => 'BOYKU',
            'to' => array(
                array(
                    'email' => $email,
                    'name' => 'Boyku',
                    'type' => 'to'
                )
            ),
            'track_opens'=>true
        );
        $async = true;
        $ip_pool = 'Main Pool';
        $send_at = 'example send_at';
        $result = $mandrill->messages->send($content, $async);
        
    }


	# Genrate Key Using Email And Password.
    public function GenrateKey($value)
    {
        $key = $this->encrypt->encode($value);
        return $key;
    }

    # Function is used to insert genrated key in key table. 
    public function InsertKey($key_parameters)
    {
       $this->load->model('Key_model'); 
       $key_id = $this->Key_model->insert($key_parameters); 
       return $key_id;
    }

    # Genrate Random String
    function GenrateRandomString($length=8){
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";//length:36
        $random_string="";
        for($i=0;$i<$length; $i++)
        {
            $random_string .= $chars[ rand(0,strlen($chars)-1)];// dont remove . before =
        }
        return $random_string;
    }

    # Send SMS
    // private function SendOtp($to,$message,$country_code)
    // {
    //     $this->load->library('nexmo');
    //     //$this->load->model('Restaurant/Country_model');
    //     //$country = $ci->Country_model->fields('country_code')->get(['id' => $country_id]);
    //     //$to = $country['country_code'].$to;
    //     $from = '918871305041';
    //     $to = $country_code.$to;
    //     $response = $this->nexmo->send_message($from, $to, $message);
    //     //$this->nexmo->d_print($response);
    //     return $this->nexmo->get_http_status();
    // }

	/*----------------------------------------------------------------------------------------
    |  user RESPONSE 
    |----------------------------------------------------------------------------------------- 
    |  It is used to fetch user details.
    |-----------------------------------------------------------------------------------------
    */
    public function UserResponse($id)
    { 
        $this->load->model(['User_model', 'User_vehicles_model']);
         $some = ['user_id','name','country_code','contact_number','email_id','signup_type','user_channel','uuid','mobVerified','address','profile_picture','otp','otp_created']; 
         $user = $this->User_model->fields($some)->get($id);
         
         if(!empty($user)){
            $timestamp = time();
            $session_key = $this->GenrateKey($timestamp);

            $this->load->model('Key_model');
            $keys = $this->Key_model->where(['user_id' => $user['user_id'],'user_type' => 1])->update(['key' => $session_key]);// it will be userwise
           
            $user['X-API-KEY'] = isset($session_key) ? $session_key : '';
            $user['api_key'] = "PATCHERAUTHKEY";
            $user['api_value'] = isset($session_key) ? $session_key : '';
            
            $user['pubnub_publish_key'] = PUBNUB_PUB_KEY;
            $user['pubnub_subscribe_key'] = PUBNUB_SUB_KEY;
            // $user['user_group_channel'] = user_GROUP;
            $user['pubnub_secret_key'] = PUBNUB_SECRET_KEY;
            $user['stripe_key'] = $this->_isproduction();

            #Bank Details Flag
            $user['bank_detail_flag'] = $this->getUserbankDetail($id);

            #After login Change the User Vehicles status
            $userVehicleStatus = $this->User_vehicles_model->where(['user_id' => $user['user_id'],'isDeleted' => 101])->get_all();

           if(!empty($userVehicleStatus)) {
                $user['isVehicles'] = count($userVehicleStatus);
           } else {
                $user['isVehicles'] = 0;
           }

           $updateVehicleStatus = $this->User_model->where('user_id', $user['user_id'])->update(['isVehicles' => $user['isVehicles']]);

            return $user;
         }else{
            return false;
         }
    }

    public function Test()
    {
        $this->load->model('Referral/Rs_referral_validity_model');
        $data = $this->Rs_referral_validity_model->get();
        print_r($data);
        die;
        /*Add there channels to chanel group*/
        $this->load->library('mypubnub');
        //$uuid = $this->mypubnub->uuid(); 
        $uuid = 'AC83B54C-764F-46D6-A7E0-E09ED77B0514'; 
        $channel_group = '8655E66B-060E-4AC2-B95E-5E7A9622AEE5';
        $status = $this->mypubnub->ChanneGroupAddChannel($channel_group,[$uuid]);
        print_r($status);
    }


     /*----------------------------------------------------------------------------------------
    |  GET USER BANK DETAILS 
    |----------------------------------------------------------------------------------------- 
    |  It is used for GET USER BANK DETAILS service.
    |-----------------------------------------------------------------------------------------
    */
    private function getUserbankDetail($user_id)
    {
        $this->load->model('Bank_detail_model');
        $where = ['user_id'=>$user_id];
        $getBankDetail = $this->Bank_detail_model->where($where)->get();
        if(!empty($getBankDetail)) {
            return true;
        } else {
            return false;
        }
    }

	/*----------------------------------------------------------------------------------------
    |  user REGISTRATION 
    |----------------------------------------------------------------------------------------- 
    |  It is responsable for register new user.
    |-----------------------------------------------------------------------------------------
    */
	public function Registration()
	{
		$this->form_validation->set_data($this->param);
		if($this->form_validation->run('Registration'))
		{  
			$signup_type = $this->param['signup_type'];
            $name =  $this->param['name'];
            # Call Genratekey function to genrate key using user email combination.
            $timestamp = time();

            $session_key = $this->GenrateKey($timestamp);
			$user_data = ['name' => $name,
							  'signup_type' => $signup_type,
                              'device_key' => $this->param['device_key'],
                              'platform_type' => (isset($this->param['platform_type'])) ? $this->param['platform_type'] : 1,
                              'isProduction' => (isset($this->param['isProduction']) ? $this->param['isProduction'] : 101)
							 ];
	
            if(isset($this->param['contact_number']) && !empty($this->param['contact_number'])){ 
                $number_exist = $this->User_model->fields('contact_number')->get(['contact_number' =>$this->param['contact_number']]);
				if($number_exist){
                    echo json_encode(['status' => false,'message' => 'Contact number already exist']);die;
                }
                $user_data['contact_number'] = $this->param['contact_number'];
                $user_data['country_code'] = $this->param['country_code'];
            }else{
                if($signup_type == 1){
                    echo json_encode(['status' => false,'message' => 'Contact number field is required']);die;
                }
            }
            if(isset($this->param['email_id']) && !empty($this->param['email_id'])){
                $email_id = $this->param['email_id'];
                $email_exist = $this->User_model->fields('email_id')->get(['email_id' => $email_id]);
                if($email_exist){
                    if($signup_type == 1){
                        echo json_encode(['status' => false, 'message' => 'Email already exist']);die;
                    }
                }
                $user_data['email_id'] = $email_id;
            }else{
                if($signup_type == 1 || $signup_type == 2){
                    echo json_encode(['status' => false,'message' => 'Email_id field is required']);die;
                }
            }
         
            $some = ['user_id','name','contact_number','email_id','signup_type','facebook_id','google_id'];
			if($signup_type == 1){ 
				if(!isset($this->param['password']) || $this->param['password'] == NULL){
					echo json_encode(['status' => false,'message' => 'Password field is required']);die;
				}
				$user_data['password'] = $this->encrypt->encode($this->param['password']);
			}
			elseif ($signup_type == 2) {
                if(isset($this->param['google_id']) && $this->param['google_id'] != NULL){
                    $user = $this->User_model->fields($some)->get(['google_id' => $this->param['google_id']]);
                    if($user){
                        # If user exist then direct login
                        $response = $this->userResponse($user['user_id']);
                        echo json_encode(['status' => true,'message' => 'SuccessFully Login','response' => $response]);die;
                    }
                    else{
                        $user_data['google_id'] = $this->param['google_id'];
                        if(isset($this->param['email_id']) && !empty($this->param['email_id'])){
                            $email_exist = $this->User_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);
                            if($email_exist){
                                echo json_encode(['status' => false, 'message' => 'Email already exist']);die;
                            }
                            $user_data['email_id'] = $this->param['email_id'];
                        }
                    }
                }else{
					echo json_encode(['status' => false,'message' => 'Google id is required']);die;
                }
			}
			elseif($signup_type == 3){
                if(isset($this->param['facebook_id']) && $this->param['facebook_id'] != NULL){
                    $user = $this->User_model->fields($some)->get(['facebook_id' => $this->param['facebook_id']]);
                    if($user){ 
                        $response = $this->UserResponse($user['user_id']);
                        echo json_encode(['status' => true,'message' => 'SuccessFully Login','response' => $response]);die;
                    }
                    else{
                        $user_data['facebook_id'] = $this->param['facebook_id'];
                        if(isset($this->param['email_id']) && !empty($this->param['email_id'])){
                            $email_exist = $this->User_model->fields('email_id')->get(['email_id' => $this->param['email_id']]);
                            if($email_exist){
                                echo json_encode(['status' => false, 'message' => 'Email already exist']);die;
                            }
                            $user_data['email_id'] = $this->param['email_id'];
                        }
                    }
                }else{
					echo json_encode(['status' => false,'message' => 'Facebook id is required']);die;
                }
			}	
			# Call function to insert data
            $result = $this->doRegister($user_data);
			if($result){
				echo json_encode(['status' => true, 'message' => 'Successfully Registered', 'response' => $result]); die;
			}else{
				echo json_encode(['status' => false,'error_status' => 1,'message' => 'Failed In Process']);die;
			}		 
		}else{
			echo json_encode(['status' => false,'error_status' => 2,'message' => $this->form_validation->get_errors_as_array()],200);die;
		}
	}
    
    /*----------------------------------------------------------------------------------------
    |  DO REGISTER 
    |----------------------------------------------------------------------------------------- 
    |  It is used for registration service.
    |-----------------------------------------------------------------------------------------
    */
    private function doRegister($user_data)
    { 
        # Call Genratekey function to genrate key using user email combination.
        $timestamp = time();

        $session_key = $this->GenrateKey($timestamp);
        $this->db->trans_start();
        $this->load->library('nexmo');
        # Create user channel
       
        $this->load->library('mypubnub');
        $uuid = $this->mypubnub->uuid(); 
        
        $user_data['patcher_channel'] = 'User';
        $user_data['user_uuid'] = $uuid;


        // For Insert In DataBase
        $user_data['user_channel'] = 'User';
        $user_data['uuid'] = $uuid;
        
        
        # Genrate OTP
        if($user_data['signup_type'] == 1 || (isset($user_data['signin_type'])) && $user_data['signin_type'] == 1){
            # Genrate OTP
            $otp = mt_rand(1001,9999);
            $user_data['otp'] = $otp;
            // $user_data['otp'] = $otp;
            $user_data['otp_created'] = date('Y:m:d H:i:s');
        }
        $user_id = $this->User_model->insert($user_data); 

        # Insert Session Key In key Table.
        $key_parameters = [
            'key' =>  $session_key,
            'user_id' => $user_id,
            'user_type' => 1
        ];
        $key_id = $this->InsertKey($key_parameters);    

        if(isset($this->param['deviceDetails']) && !empty($this->param['deviceDetails'])){
            $this->load->library('Device_detail');
            $deviceDetails = $this->param['deviceDetails'];
            $deviceDetails['user_type'] = 1;
            $deviceDetails['user_id'] = $user_id;
            $deviceDetails['os_type'] = (isset($this->param['platform_type']) && !empty($this->param['platform_type'])) ? $this->param['platform_type'] : 3;
            $this->device_detail->registerDetail($deviceDetails);
        }
        /*--End Of Signupcode code*/

        $this->db->trans_complete();
        if($this->db->trans_status())
        { 
            /*--Send SMS--*/
            if($user_data['signup_type'] == 1 ||(isset($user_data['signin_type'])) && $user_data['signin_type'] == 1){ 
                
                $otpMesssage = $otp.' is your one-time verification code. Please do not share it with anyone.';
                $to = $user_data['country_code'].$user_data['contact_number'];//with country code
                $message = array('text' => $otpMesssage);
                $this->load->helper('api');
                $otpSend = $this->nexmo->send_message("Patcher", '44'.$user_data['contact_number'],["text" =>  $message]);

                $args = [
                    'message' => $otpMesssage,
                    'subject' => 'Patcher: OTP Verifiction',
                    'email_id' => $user_data['email_id'],
                    'template_name' => 'Sign Up',
                    'vars' => [
                        [
                            'name' => 'message',
                            'content' => $otpMesssage
                        ]
                    ],
                ];
                $mailResponse = sendMandrillEmailTemlate($args);

            }
            
            /*---send mail to user--*/
            if(isset($user_data['email_id'])){
                /*$this->load->library('mail');
                $msg = 'Welcome to Boyku you are successfully registered.'; 
                $subject = 'Boyku';
                $send = $this->mail->SendMail($user_data['email_id'],$subject, $msg,$cc=NULL); 
                */

                # Send Email by Mailchimp
                $msg = 'Welcome to Patcher you are successfully registered.'; 
                $to = $user_data['email_id'];
                // $this->sendEmail( $msg, $to );
            }
            $response = $this->UserResponse($user_id);
            return $response;
        }else{
            $this->db->trans_rollback();
            return false;
        }        
    }

   


    # IsEmail check that user name is email_id or contact_number.
    private function IsEmail($user_name)
    {
        # If the username input string is an e-mail, return true
        if(filter_var($user_name, FILTER_VALIDATE_EMAIL)) {
            return true;
        }else {
            return false;
        }
    }

    /*----------------------------------------------------------------------------------------
    |  DO LOGIN 
    |----------------------------------------------------------------------------------------- 
    |  It is used for login user it take user credential as parameter .
    |-----------------------------------------------------------------------------------------
    */
    private function doLogin($data){ //var_dump($data);die;
        $this->load->library('encrypt');
        $this->load->model(['User_model', 'User_vehicles_model']);
        $signin_type = $data['signin_type'];
        $user = $this->User_model->fields('user_id,password,name,email_id,contact_number,facebook_id,google_id,mobVerified,isBlocked,isOnline,platform_type,isProduction,address,profile_picture')
                                            ->get($data['where']);
        //var_dump($user);die;
        if($user){
            if($user['isBlocked'] == 101){
                echo json_encode(['status'=> false ,"error" =>['message'=> 'User is Blocked']]);die;
            }
            if($user['isOnline'] == 101){
                $this->User_model->where('user_id',$user['user_id'])->update(['isOnline' => 100]);
            }

            if(isset($data['platform_type'])){
                $update_user_data['platform_type'] = $this->param['platform_type'];
            }
            if(isset($data['isProduction'])){
                $update_user_data['isProduction'] = $this->param['isProduction'];
            }
             if(isset($data['device_key'])){
                $update_user_data['device_key'] = $this->param['device_key'];
            } 
            if(isset($update_user_data)){
                $this->User_model->where('user_id',$user['user_id'])->update($update_user_data);
            }
           
            if($signin_type == 1){ 
                if(!isset($this->param['password']) || $this->param['password'] == NULL){
                    echo json_encode(['status' => false,'message' => 'Password field is required']);die;
                }
                $password = $this->param['password'];
                $decrypted_password = $this->encrypt->decode($user['password']);
                $login_status = ($decrypted_password == $password) ? true : false;
            }	
            elseif($signin_type == 2){ 
                $google_id = $this->param['google_id']; 
                $login_status = ($user['google_id'] == $google_id) ? true : false;
            
                $updateGoogleData = ['email_id'=>$this->param['email_id'],
                                'isProduction'=>$this->param['isProduction'],
                                'name'=>$this->param['name'],
                                'platform_type'=>$this->param['platform_type'],
                                'signup_type'=>$this->param['signin_type']];

                $this->User_model->where('user_id',$user['user_id'])->update($updateGoogleData);
            }
            elseif($signin_type == 3){
                $facebook_id = $this->param['facebook_id'];
                $login_status = ($user['facebook_id'] == $facebook_id) ? true : false;
            }
        
            if($login_status === true){
                # user response
                $response = $this->UserResponse($user['user_id']);

                if(isset($this->param['deviceDetails']) && !empty($this->param['deviceDetails'])){
                    $this->load->library('Device_detail');
                    $deviceDetails = $this->param['deviceDetails'];
                    $deviceDetails['user_type'] = 1;
                    $deviceDetails['user_id'] = $user['user_id'];
                    $deviceDetails['os_type'] = (isset($this->param['platform_type']) && !empty($this->param['platform_type'])) ? $this->param['platform_type'] : 3;
                    $this->device_detail->registerDetail($deviceDetails);
                }

                # Update Status
                $if_update = $this->User_model->where('user_id',$user['user_id'])->update(['isOnline' => 101,'device_key' => $data['device_key'] ]);
                if($if_update){
                    echo json_encode(['status' => true, 'message' => 'Successfully Login', 'response' => $response]);die;
                }else{
                    echo json_encode(['status' => false, 'message' => 'Failed In Process']);die;
                }
            }else{
                echo json_encode(['status' => false, 'message' => 'User name or password is incorrect']);die;
            }
        }
        else{ 
            # Register user
            if($signin_type == 3 || $signin_type == 2){
                $insert_user = ['name' => (isset($data['name'])) ? $data['name'] : "",
                                    'signup_type' => $signin_type,
                                    'device_key' => $data['device_key']
                                    ];
                if(isset($data['email_id'])){
                    $email_exist = $this->User_model->fields('email_id')->get(['email_id' => $data['email_id']]);
                    if($email_exist){
                        echo json_encode(['status' => false, 'message' => 'Email already exist']);die;
                    }
                    $insert_user['email_id'] = $data['email_id'];
                }                    
                if($signin_type == 2){
                    if(isset($data['google_id'])){
                        $insert_user['google_id'] = $data['google_id'];
                        $insert_user['email_id'] = $data['email_id'];
                        $insert_user['isProduction'] = $data['isProduction'];
                        $insert_user['platformType'] = $data['platform_type'];
                        $insert_user['name'] = $data['name']; 
                    }else{
                        echo json_encode(['status' => false, 'message' => 'google id is required']);die;
                    }                  
                } 
                elseif($signin_type == 3){
                    if(isset($data['facebook_id'])){
                        $insert_user['facebook_id'] = $data['facebook_id'];                   
                    }else{
                        echo json_encode(['status' => false,'message' => 'Facebook id is required']);die;
                    }
                }           
                 
                # Call function to register
                $response = $this->doRegister($insert_user);
                if($response){        
                    echo json_encode(['status' => true, 'message' => 'Successfully Registered', 'response' => $response]); die;
                }         
            }else{
                echo json_encode(['status' => false,'message' => 'User Not Found']);die;
            }
        }					
    }

	/*----------------------------------------------------------------------------------------
    |  user LOGIN 
    |----------------------------------------------------------------------------------------- 
    |  It is used for login user it take user credential as parameter .
    |-----------------------------------------------------------------------------------------
    */
	public function Login()
	{ 
        $this->load->library('encrypt');
        $this->load->model(['User_model']);
        $signin_type = $this->param['signin_type'];
        $data = ['signin_type' => $signin_type,
                 'device_key' => $this->param['device_key']
                ];
        if(isset($this->param['isProduction'])){
            $data['isProduction'] = $this->param['isProduction'];
        }  
        if(isset($this->param['platform_type'])){
            $data['platform_type'] = $this->param['platform_type'];
        }        

        if($signin_type == 3){
            if(isset($this->param['email_id'])){
                $data['email_id'] = $this->param['email_id'];
            }
            if(isset($this->param['facebook_id']) && !empty($this->param['facebook_id'])){
                $where = ['facebook_id' => $this->param['facebook_id']];
                $data['facebook_id'] = $this->param['facebook_id'];
            }else{
                echo json_encode(['status' => false,'message' => 'Facebook id is required']);die;
            }
        }
        elseif($signin_type == 2){
            if(isset($this->param['email_id'])){ 
                $data['email_id'] = $this->param['email_id'];
            }
            if(isset($this->param['google_id']) && !empty($this->param['google_id'])){
                $where = ['google_id' => $this->param['google_id']];
                $data['google_id'] = $this->param['google_id'];
            }else{
                echo json_encode(['status' => false,'message' => 'Google id is required']);die;
            }
        }
        else{
            if(isset($this->param['user_name']) && !empty($this->param['user_name'])){
                $user_name = $this->param['user_name'];
            }else{
                echo json_encode(['status' => false,'message' => 'User name is required']);die;
            }

            $check_email = $this->IsEmail($user_name);
            if($check_email){
                # email & password combination
                $where = [ "email_id" => $user_name];
            }else{ 
                # phone_no & password combination
                $where = [ "contact_number" => $user_name];
            }
        }
        $data['where'] = $where;
        if(isset($this->param['name'])){ 
            $data['name'] =$this->param['name'];
        }

        if($signin_type == 1){ 
            if(isset($this->param['password']) && !empty($this->param['password'])){
               $data['password'] = $this->param['password'];
            }else{
                echo json_encode(['status' => false,'message' => 'Password field is required']);die;
            }
        }
       # Call function to login
        $response = $this->doLogin($data);	
        if($response){
            echo json_encode(['status' => true,'message' => 'Successfully Login','response' => $response]);
        }else{
            echo json_encode(['status' =>false,'message' => 'Failed in process ']);
        }
	}	
    
	/*----------------------------------------------------------------------------------------
    |  LOGOUT 
    |----------------------------------------------------------------------------------------- 
    |  It is used for logout user.
    |-----------------------------------------------------------------------------------------
    */
    public function Logout()
    { 
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('Logout')){
            $this->load->model('User_model');
            $user_id = $this->param['user_id'];
            $logout = $this->User_model->where('user_id',$user_id)->update(['isOnline' => 100,'device_key' => '','platform_type' => 0]);
            if($logout){
                echo json_encode(['status'=> true ,'message'=> 'Successfully Logout']);  die;
            }else{
                echo json_encode(['status'=> false  ,'message'=> 'Failed In Logout Process']); die;
            }
        }
        else{
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()],200);die;
        }
    }

	/*----------------------------------------------------------------------------------------
    |  CHANGE PASSWORD 
    |----------------------------------------------------------------------------------------- 
    |  It is responsable for change user password.
    |-----------------------------------------------------------------------------------------
    */
    public function ChangePassword()
    {
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('ChangePasswordUser')){
            $user_id = $this->param['user_id'];
            $previous_password = $this->param['old_password'];
            $new_password = $this->param['new_password'];

            $user = $this->User_model->fields('password')->get($user_id); 
            $decrypted_password = $this->encrypt->decode($user['password']);
            if($decrypted_password == $previous_password){
                $encrypt_newpwd = $this->encrypt->encode($new_password);
                $data = ['password' => $encrypt_newpwd]; 
                $update = $this->User_model->where('user_id',$user_id)->update($data);
                if($update){
                    echo json_encode(['status'=> true ,'message'=> 'Successfully Changed Password']); die;
                }else{
                    echo json_encode(['status'=> false ,'message'=> 'Failed In Process']); die;
                }
            }else{
                echo json_encode(['status'=> false ,'message'=> 'Previous password in incorrect']); die;
            }
        }else{
            echo json_encode( ["status" =>false,"error_status" => 2,"message" => $this->form_validation->get_errors_as_array()],200); die;
        }
    }

	/*----------------------------------------------------------------------------------------
    |  FORGOT PASSWORD 
    |----------------------------------------------------------------------------------------- 
    |  It is responsable for send otp to user mob.no. to reset password.
    |-----------------------------------------------------------------------------------------
    */
    public function ForgotPassword()
    {
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('ForgotPassword')){
            //$contact_number = $this->param['contact_number'];
            // $split_number = explode("-", $this->param['contact_number']);
            // $country_code = $split_number[0];
            $this->load->library('nexmo');
            $contact_number =$this->param['contact_number'];

            $user = $this->User_model->fields('user_id,email_id,country_code')->get(['contact_number' => $contact_number]);
            if($user){
                # Genrate OTP
                $otp =  mt_rand(1001,9999);
                // $otp =  2222;
                $where = ['user_id' => $user['user_id']];
                $date = date('Y-m-d H:i:s');
                $update = $this->User_model->where($where)->update(['otp' => $otp,'otp_created' => $date]);
                if($update){

                    $otpMesssage = '<b>'.$otp.'</b>'.' is your one-time verification code, which is valid for the next 10 minutes. Please do not share it with anyone.';
                    $to = $user['country_code'].$contact_number; 
                    $this->load->helper('api');
                    $otpSend = $this->nexmo->send_message("Patcher", '44'.$contact_number,["text" => $otpMesssage]);

                    $args = [
                        'message' => $otpMesssage,
                        'subject' => 'Patcher: Forgot Password OTP',
                        'email_id' => $user['email_id'],
                        'template_name' => 'Sign Up',
                        'vars' => [
                            [
                                'name' => 'message',
                                'content' => $otpMesssage
                            ]
                        ],
                    ];
                    $mailResponse = sendMandrillEmailTemlate($args);

                    //$send_otp = SendOtp($to,$message); //print_r($send_otp);die;
                   $send_otp = true;
                    if($send_otp){
                        echo json_encode(["status" => true, "message" => "OTP sent to your mobile number","response" => ["otp" => $otp,'user_id' => $user['user_id']]]);die;
                    }else{
                        echo json_encode(["status" => true, "message" => "Failed To Send OTP"]);die;
                    }
                }else{
                    echo json_encode(["status" => true, "message" => "Failed in process"]);die;
                }
            }else{
               echo json_encode(["status" => false, "message" => "User Not found "]);die;
            }
        }else{
            echo json_encode(["status" =>false,"error_status" => 2,"message" => $this->form_validation->get_errors_as_array()],200);die;
        }
    } 

    /*----------------------------------------------------------------------------------------
    |  RESET PASSWORD 
    |----------------------------------------------------------------------------------------- 
    |  It is responsable for reset passwprd.
    |-----------------------------------------------------------------------------------------
    */
    public function ResetPassword()
    {
        $this->form_validation->set_data($this->param);
        if($this->form_validation->run('ResetPasswordUser')){  
            $new_password = $this->param['new_password'];
            // $otp = $this->param['otp']; 
            $user_id = $this->param['user_id'];
            # Fetch OTP
            $user = $this->User_model->fields('user_id,email_id,otp,otp_created')->get(['user_id'=> $user_id]);
            if($user){ 
                // $exp_time = 600; // 600 seconds
                // $time = strtotime($user['otp_created']);
                // $curtime = time();
                // if(($curtime - $time) > $exp_time) {     
                //    echo json_encode(["status" => false, "message" => "OTP has expired"]);die;
                // }

                $encrypt_pwd = $this->encrypt->encode($new_password);
                $update = $this->User_model->where('user_id',$user_id)->update(['password' => $encrypt_pwd]);
                if($update){
                    $args = [
                        'message' => 'Password Change Successful.',
                        'subject' => 'Patcher: Password Change Successful.',
                        'email_id' => $user['email_id'],
                        'template_name' => 'Sign Up',
                        'vars' => [
                            [
                                'name' => 'message',
                                'content' => ''
                            ]
                        ],
                    ];
                    $mailResponse = sendMandrillEmailTemlate($args);

                    echo json_encode(['status' => true, 'message' => 'Successfully Reset Password']);die;
                }else{
                    echo json_encode(['status' => false,'message' => 'Failed In Process']);die;
                }
            }else{
                echo json_encode(['status' => false,'message' => 'Otp is incorrect']);die;
            }
        }else{
            echo json_encode(["status" =>false,"error_status" => 2,"message" => $this->form_validation->get_errors_as_array()],200);die;
        }
    }

    private function _isproduction(){
        $data = $this->Constant_model->get(['keyname' => 'isProduction']);

        if($data){
            if($data['value'] == "true"){
                return PROD_STRIPE_API_KEY;
            } else {
                return TEST_STRIPE_API_KEY;
            }
        } else {
            return TEST_STRIPE_API_KEY;
        }
    }

}