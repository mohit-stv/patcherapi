<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model','Mechanic_model','Request_model']);
    }

    public function getDashboardDetails_post() {
        $mechanicId = $this->param['mechanic_id']; 
        $getMechanic = $this->Mechanic_model->fields(['name','contact_number','mechanic_address','mechanic_picture'])->where('mechanic_id',$mechanicId)->get();
        $countval = $this->Request_model->count_rows(['mechanic_id'=>$mechanicId, 'request_status'=>1]);
        // echo $this->db->last_query();die;
        $response = [
                   'mechanic_id' => $mechanicId,
                   'mechanic_name'=> $getMechanic['name'],
                   'mechanic_address'=> $getMechanic['mechanic_address'],
                   'mechanic_picture'=> $getMechanic['mechanic_picture'],
                   'request_count'=> $countval
        ];

        if(!empty($getMechanic)) {
			$this->response(['status' => true, 'message'=> 'Your Profile Details Here.','response' => $response]);die;
		} else {
			$this->response(['status' => false, 'message' => 'No Any Details Here.']);die;
		}
    }

    public function updateMechanicProfilePicture_post() {
        $mechanicId = $this->param['mechanic_id'];
        if(isset($this->param['mechanic_picture']) && !empty($this->param['mechanic_picture'])) {
          
             $img = $this->param['mechanic_picture']; 
             $imageData = $this->common_model->convertBase64ToImage($img);
 
             $data = $imageData['data'];
             $mime = $imageData['mime'];
 
             $image_name ='uploads/mechanics/'.rand(1000,5000) . '.'.$mime;
             file_put_contents($image_name, $data);
             chmod($image_name, 0777);
             $mechanics_array['mechanic_picture'] = ROOTPATH.$image_name;
        } else {
            echo json_encode(['status' => false, 'message' => 'Please Select Image']);die;
        }

        $update = $this->Mechanic_model->where(['mechanic_id'=>$mechanicId])->update($mechanics_array);
        if($update) {
              echo json_encode(['status' => true, 'message'=> 'Profile Update Successfully.','response' => $mechanics_array,JSON_UNESCAPED_SLASHES]);die;
        } else {
              echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        } 
    }
}