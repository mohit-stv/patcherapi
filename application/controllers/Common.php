<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller
{
	private $param = array();
	public function __construct(){
		parent::__construct();
		$this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model(['User_model']);
    }
    
    public function cancelReason_post() {
        $this->load->model('Cancel_reason_model');
        $userType = $this->param['user_type'];
        $requestType = $this->param['request_type'];

        $cancelReason = $this->Cancel_reason_model
        ->fields('cancel_reason_id,user_type,reason,request_type,status')
        ->where(['user_type'=>$userType,'request_type'=>$requestType])->get_all();
        if($cancelReason) {
            $this->response(['status' => true, 'message'=> 'All Cancel reason Here.','response' => $cancelReason]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    
}