<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Answers extends MY_Controller {

    public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }
    
    public function addAnswer_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addAnswer') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 
            $this->load->model(['Answers_model']);            
            
            $answer_array=[
                'answer' => $this->param['answer'],
                'answerStatus' => $this->param['answerStatus'],
            ];
            $insert = $this->Answers_model->insert($answer_array);
                
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editAnswer_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editAnswer') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 
            $this->load->model(['Answers_model']);            
            $answerId = $this->param['answerId'];
            $answer_array=[
                'answer' => $this->param['answer'],
                'answerStatus' => $this->param['answerStatus'],
            ];
            $update = $this->Answers_model->where('answerId',$answerId)->update($answer_array);   
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }  

    public function allAnswers_post() {
        
        $this->load->model(['Answers_model']);                
        
        if(isset($this->param['answerId']) && $this->param['answerId'] != NULL){
            $where = array("answerId"=>$this->param['answerId']);
        }else{
            $where = array();
        }

        $answer   =  $this->Answers_model->where($where)->fields('answerId,answer')->get_all();

        if($answer){
            $this->response(['status' => true , 'message' => 'Successfully','response' => $answer]);
        }else{
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteAnswer_post() {
        
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteAnswer') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
            $answerId = $this->param['answerId'];

            $updateOptions = array(
                'where' => array('answerId' => $answerId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'answers'
            );
            $deleteAnswer = $this->common_model->customUpdate($updateOptions);

            if($deleteAnswer){
                $this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteAnswer]);
            }else{
                $this->response(['status' => false, 'message' => 'Record Not Found']);
            }           
        }
    }
}