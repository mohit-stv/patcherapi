<?php defined('BASEPATH') or exit('No direct script access allowed');

class MechanicFlow extends MY_Controller
{
    private $param = array();
    public function __construct()
    {
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['User_model', 'Request_model', 'Mechanic_request_detail_model']);
        $this->load->library(['Notification', 'Googleapi']);
        $this->load->helper('common');
    }

    ## Get Specific Request of User After Accept(show user location in map with user information)
    public function getRequestUser_post()
    {
        $requestId = $this->param['request_id'];
        $mechanicId = $this->param['mechanic_id'];

        $getRequestInfo = $this->Request_model->fields(['request_id', 'request_type', 'user_id', 'mechanic_id', 'request_date', 'request_lat', 'request_lng'])->get(['request_id' => $requestId, 'mechanic_id' => $mechanicId]);

        if (!empty($getRequestInfo)) {
            $userId = $getRequestInfo['user_id'];
            $getUserInfo = $this->User_model->fields(['name', 'email_id', 'country_code', 'contact_number', 'profile_picture', 'address'])->where('user_id', $userId)->get();

            $address = $this->googleapi->getAddress($getRequestInfo['request_lat'], $getRequestInfo['request_lng']);
            if (!$address) {
                $address = "";
            }

            $result = [
                'request_type' => $getRequestInfo['request_type'],
                'request_date' => $getRequestInfo['request_date'],
                'user_lat' => $getRequestInfo['request_lat'],
                'user_lng' => $getRequestInfo['request_lng'],
                'user_name' => $getUserInfo['name'],
                'user_email_id' => $getUserInfo['email_id'],
                'user_country_code' => $getUserInfo['country_code'],
                'user_contact_number' => $getUserInfo['contact_number'],
                'user_profile_picture' => $getUserInfo['profile_picture'],
                'user_address' => $address,
            ];
            if (!empty($getUserInfo)) {
                $this->response(['status' => true, 'message' => 'User Infomation.', 'response' => $result]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Not found Data']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Not Found']);die;
        }
    }

    ## Add mechanics issuse price
    public function addMechanicsIssuePrice_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Mechanic_issuses_model', 'Mechanic_add_service_model', 'Request_model', 'Issue_model']);
        $requestId = $this->param['request_id'];

        $requestUserDetails = $this->Request_model->with_user('fields: name,platform_type,isProduction,device_key')
            ->where('request_id', $requestId)->get();

        if (!$requestUserDetails) {
            $this->response(['status' => false, 'message' => 'Invalid Request Details']);die;
        }

        $responseData = $update = false;

        $notificationUserMessage = "Mechanic updated price of ";
        // 100 for Edit Default Issue Price // 101 for Edit Service Price
        if ($this->param['status'] == 100) {
            $where = ['request_id' => $this->param['request_id'],
                'mechanic_id' => $this->param['mechanic_id'],
                'issuse_id' => $this->param['issue_id']];

            $getPrice = $this->Mechanic_issuses_model->fields('isCompleted')->where($where)->get();
            $issueDetails = $this->Mechanic_issuses_model->fields('issuse_price')->where($where)->get();

            if ($getPrice['isCompleted'] == 101) {
                $this->response(['status' => false, 'message' => 'This job has been completed, unable to update.']);
            }

            $status = $this->param['isCompleted'];
            if (!empty($getPrice)) {
                $updateData = ['issuse_price' => $this->param['issue_price'], 'isCompleted' => $status];
                $update = $this->Mechanic_issuses_model->where($where)
                    ->update($updateData);

            } else {

                $updateData = [
                    'request_id' => $this->param['request_id'],
                    'mechanic_id' => $this->param['mechanic_id'],
                    'issuse_id' => $this->param['issue_id'],
                    'issuse_price' => $this->param['issue_price'],
                    'isCompleted' => $status,
                ];
                $update = $this->Mechanic_issuses_model->insert($updateData);
            }

            if (!empty($issueDetails)) {
                //    $issueName = $issueDetails['issue']['issue_name'];
                $getIssue = $this->Issue_model->fields('issue_name')
                    ->where(['issue_id' => $this->param['issue_id']])->get();
                $responseData = ['issue_name' => $getIssue['issue_name'],
                    'issue_price' => $issueDetails['issuse_price']];
                $issueName = $getIssue['issue_name'];
            }

            //Update User's request issue to complete
            if ($status == 101) {
                $notificationUserMessage = "Your mechanic has completed the job! Thanks for using Patcher and have a great day";
                $this->Request_issues_model->where(['request_id' => $requestId, 'issue_id' => $this->param['issue_id']])->update(['isCompleted' => 101]);
            } else {
                $notificationUserMessage = "Mechanic has updated the job.";
            }
        } elseif ($this->param['status'] == 101) {
            $where = [
                'request_id' => $this->param['request_id'],
                'mechanic_id' => $this->param['mechanic_id'],
                'mechanic_add_service_id' => $this->param['issue_id']];

            $getPriceService = $this->Mechanic_add_service_model->fields('isCompleted,part_name,price')->where($where)->get();

            if (!empty($getPriceService)) {
                if ($getPriceService['isCompleted'] == 101) {
                    $this->response(['isCompleted' => false, 'message' => 'This job has been completed, unable to update.']);
                }
                $status = $this->param['isCompleted'];

                if ($status == 101) {
                    $notificationUserMessage = "Your mechanic has completed the job! Thanks for using Patcher and have a great day";
                } else {
                    $notificationUserMessage = "You mechanic has updated the job.";
                }

                # add service type
                $service_type = 2;
                if (isset($this->param['service_type']) && !empty($this->param['service_type'])) {
                    $service_type = $this->param['service_type'];
                }

                $updateData = ['price' => $this->param['issue_price'], 'isCompleted' => $status, 'service_type' => $service_type];
                $update = $this->Mechanic_add_service_model->where($where)->update($updateData);
                $issueName = $getPriceService['part_name'];
                $responseData = ['issue_name' => $getPriceService['part_name'],
                    'issue_price' => $getPriceService['price']];
            } else {
                $this->response(['status' => false, 'message' => 'Invalid Request Issue Details']);die;
            }
        }

        if (!empty($requestUserDetails) && $responseData != false) {

            $responseData = ['request_type' => $requestUserDetails['request_type']];

            $deviceId = $requestUserDetails['user']['device_key'];
            $platformType = $requestUserDetails['user']['platform_type'];
            $isProduction = $requestUserDetails['user']['isProduction'];

            $bodyData = $responseData;

            if ($platformType == 1) {
                // print_r($bodyData);die;
                $bodyData['notification_type'] = 10;
                $bodyData['request_id'] = $requestId;
                $message = [
                    'message' => "$notificationUserMessage",
                    'data' => $bodyData,
                ];
                $notify = $this->notification->fcmUser($deviceId, $message);
            }

            if ($platformType == 2) {
                $messageTitle = [
                    'title' => 'Mechanic Added New Services',
                    'category' => ['notification_type' => 10, 'request_id' => $requestId],
                ];
                $bodyData = ['test' => 'test'];
                $notify = $this->notification->apnsUser($deviceId, $messageTitle, $isProduction, $platformType, $bodyData);
            }
        }

        if ($update) {
            $this->response(['status' => true, 'message' => 'Updated Successfully.', 'response' => $updateData]);
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);
        }
    }

    ## Mechanic Add New Service
    public function mechanicAddNewService_post()
    {
        $this->load->model(['Mechanic_add_service_model', 'Request_model']);
        $requestId = $this->param['request_id'];

        # add service type
        $service_type = 2;
        if (isset($this->param['service_type']) && !empty($this->param['service_type'])) {
            $service_type = $this->param['service_type'];
        }

        $data = [
            'mechanic_id' => $this->param['mechanic_id'],
            'request_id' => $this->param['request_id'],
            'part_name' => $this->param['part_name'],
            'price' => $this->param['price'],
            'description' => $this->param['description'],
            'service_type' => $service_type,
        ];
        $insert = $this->Mechanic_add_service_model->insert($data);

        if ($insert) {
            $getResult = $this->Mechanic_add_service_model->where('mechanic_add_service_id', $insert)->fields(['mechanic_add_service_id', 'part_name', 'price', 'description'])->get();
            $response = [
                'request_id' => $this->param['request_id'],
                'service_id' => $getResult['mechanic_add_service_id'],
                'issue_id' => $getResult['mechanic_add_service_id'], //Duplicated for update
                'issue_name' => $getResult['part_name'],
                'issue_price' => $getResult['price'],
                'issue_description' => $getResult['description'],
                'status' => 101,
            ];

            $requestUserDetails = $this->Request_model->with_user('fields: name,platform_type,isProduction,device_key')->where('request_id', $requestId)->get();
            if (!empty($requestUserDetails)) {
                $userDetails = $requestUserDetails['user'];

                $deviceId = $userDetails['device_key'];
                $platformType = $userDetails['platform_type'];
                $isProduction = $userDetails['isProduction'];

                $bodyData = ['issue_name' => $this->param['part_name']];

                if ($platformType == 1) {
                    $notificationMessage = ['notification_type' => 16, 'request_id' => $requestId, 'request_type' => $requestUserDetails['request_type'],
                        'message' => 'Mechanic Added New Services'];
                    $message = [
                        'message' => 'Mechanic Added New Services',
                        'data' => $notificationMessage,
                    ];
                    $notify = $this->notification->fcmUser($deviceId, $message);
                } elseif ($platformType == 2) {
                    $messageTitle = [
                        'title' => 'Mechanic Added New Services',
                        'category' => ['notification_type' => 16, 'request_id' => $requestId, 'request_type' => $requestUserDetails['request_type']],
                    ];
                    $notify = $this->notification->apnsUser($deviceId, $messageTitle, $isProduction, $platformType, $bodyData);
                }
            }
            $this->response(['status' => true, 'message' => 'Successfully Added.', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Mechanic Delete The Services And Issue
    public function deleteServiceIssue_post()
    {
        $this->load->model(['Request_issues_model', 'Mechanic_add_service_model',
            'Mechanic_issuses_model']);

        $mechanicId = $this->param['mechanic_id'];
        $status = $this->param['status']; // Only for repair
        $id = $this->param['issue_id'];
        $requestId = $this->param['request_id'];

        $getAllRequestIssuse = $this->Request_issues_model
            ->fields('issue_id,isCompleted')
            ->with_issue('fields:issue_name,issuse_price', 'where:issue_status=101')
            ->where(['request_id' => $requestId, 'issue_id' => $id,
                'request_issue_status' => 101])->get();

        $getNewAddService = $this->Mechanic_add_service_model
            ->fields(['mechanic_add_service_id', 'part_name', 'price', 'isCompleted'])
            ->where(['request_id' => $requestId, 'mechanic_id' => $mechanicId,
                'mechanic_add_service_id' => $id])->get();

        if (($status != '' && $status == 100) || ($status != '' && $status == 101)) {

            // Delete service 101 // Delete Issue 100
            if ($status == 101) {
                if (!empty($getNewAddService)) {
                    if ($getNewAddService['isCompleted'] == 101) {
                        $this->response(['status' => true, 'message' => 'This service has been already completed. Unable to delete.',
                            'response' => ['isCompleted' => 101]]);die;
                    }

                    $deleteService = $this->Mechanic_add_service_model
                        ->where('mechanic_add_service_id', $id)->update(['isDeleted' => 100]);
                    if ($deleteService != '') {
                        $this->response(['status' => true, 'message' => 'Successfully Deleted.',
                            'response' => ['isDeleted' => 100]]);die;
                    } else {
                        $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                    }
                }
            } elseif ($status == 100) {
                if (!empty($getAllRequestIssuse)) {

                    if ($getAllRequestIssuse['isCompleted'] == 101) {
                        $this->response(['status' => true, 'message' => 'This service has been already completed. Unable to delete.',
                            'response' => ['isCompleted' => 101]]);die;
                    }

                    $deleteIssue = $this->Request_issues_model
                        ->where(['request_id' => $requestId, 'issue_id' => $id])
                        ->update(['isDeleted' => 100]);
                    $mechIssue = $this->Mechanic_issuses_model
                        ->where(['request_id' => $requestId, 'issuse_id' => $id])
                        ->force_delete();
                    if ($deleteIssue) {
                        $this->response(['status' => true, 'message' => 'Successfully Deleted.',
                            'response' => ['isDeleted' => 100]]);die;
                    } else {
                        $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                    }
                }
            }

            $requestUserDetails = $this->Request_model->with_user('fields: name,platform_type,isProduction,device_key')->where('request_id', $requestId)->get();
            if (!empty($requestUserDetails)) {
                $userDetails = $requestUserDetails['user'];

                $deviceId = $userDetails['device_key'];
                $platformType = $userDetails['platform_type'];
                $isProduction = $userDetails['isProduction'];

                $bodyData = ['issue_name' => 'test'];

                if ($platformType == 1) {

                    $notificationMessage = ['notification_type' => 10,
                        'request_id' => $requestId, 'request_type' => $requestUserDetails['request_type'],
                        'message' => 'Mechanic Accepted Your Request'];
                    $message = [
                        'message' => 'Mechanic Deleted a Services',
                        'data' => $notificationMessage,
                    ];
                    $notify = $this->notification->fcmUser($deviceId, $message);
                } elseif ($platformType == 2) {
                    $messageTitle = [
                        'title' => 'Mechanic Deleted a Services',
                        'category' => ['notification_type' => 10, 'request_id' => $requestId, 'request_type' => $requestUserDetails['request_type']],
                    ];
                    $notify = $this->notification->apnsUser($deviceId, $messageTitle, $isProduction, $platformType, $bodyData);
                }
            }
            $this->response(['status' => true, 'message' => 'Successfully Deleted.',
                'response' => ['isDeleted' => $status]]);die;
        } else {
            $this->response(['status' => false,
                'message' => 'Status Mismatch.']);die;
        }
    }

    ## Manage The Busy Schdule
    public function busySchedule_post()
    {
        $this->load->model(['Busy_schedule_model']);

        $mechanicid = $this->param['mechanic_id'];

        if (isset($this->param['busy_schedule_id']) &&
            $this->param['busy_schedule_id'] != '') {

            if (isset($this->param['status']) &&
                $this->param['status'] == 100) {
                $data = ['isDeleted' => 100];

                $update = $this->Busy_schedule_model
                    ->where(['busy_schedule_id' => $this->param['busy_schedule_id'],
                        'mechanic_id' => $mechanicid, 'isDeleted' => 101])
                    ->update(['isDeleted' => 100]);

            } else {
                $startDate = $this->param['start_date'];
                $endDate = $this->param['end_date'];
                $startTime = $this->param['start_time'];
                $endTime = $this->param['end_time'];

                if ($endDate < $startDate) {
                    $this->response(['status' => false, 'message' => 'Start Date must be less than End Date.']);
                } else if ((strtotime($endTime) < strtotime($startTime)) && $endDate == $startDate) {
                    $this->response(['status' => false, 'message' => 'Start Time must be less than End Time.']);
                }

                if (isset($this->param['busy_schedule_id']) && $this->param['busy_schedule_id'] != '') {
                    $busy_schedule_id = $this->param['busy_schedule_id'];
                } else {
                    $this->response(['status' => false, 'message' => 'Schedule Id Is Required.']);
                }

                if (isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
                    $mechanicid = $this->param['mechanic_id'];
                } else {
                    $this->response(['status' => false, 'message' => 'Mechanic Id Is Required.']);
                }

                $getBusySchedule = $this->Busy_schedule_model
                    ->where(['busy_schedule_id !=' => $busy_schedule_id,
                        'mechanic_id' => $mechanicid,
                        'isDeleted' => 101])
                    ->get();
                $getSDate = $getBusySchedule['start_date'];
                $getEDate = $getBusySchedule['end_date'];
                $getSTime = $getBusySchedule['start_time'];
                $getETime = $getBusySchedule['end_time'];

                if ($getSDate == $startDate) {
                    if ($getSTime == $startTime && $getETime == $endTime) {
                        $this->response(['status' => 0, 'message' => 'Already Busy.']);die;
                    }
                    if (($startTime >= $getSTime && $endTime <= $getETime)) {
                        $this->response(['status' => true, 'message' => 'Already Busy.']);die;
                    }
                }

                $data = ['start_date' => $this->param['start_date'],
                    'end_date' => $this->param['end_date'],
                    'start_time' => $this->param['start_time'],
                    'end_time' => $this->param['end_time']];

                $update = $this->Busy_schedule_model
                    ->where(['busy_schedule_id' => $this->param['busy_schedule_id']])
                    ->update($data);
            }
        } else {

            $startDate = $this->param['start_date'];
            $endDate = $this->param['end_date'];
            $startTime = $this->param['start_time'];
            $endTime = $this->param['end_time'];
            $mechanicid = $this->param['mechanic_id'];

            if ($endDate < $startDate) {
                $this->response(['status' => false, 'message' => 'Start Date must be less than End Date.']);
            } else if ((strtotime($endTime) <= strtotime($startTime)) && $endDate == $startDate) {
                $this->response(['status' => false, 'message' => 'Start Time must be less than End Time.']);
            }

            $getSchedule = $this->Busy_schedule_model
                ->where(['mechanic_id' => $mechanicid, 'isDeleted' => 101])
                ->get_all();

            if (!empty($getSchedule)) {
                foreach ($getSchedule as $getSchedules) {
                    $getSDate = $getSchedules['start_date'];
                    $getEDate = $getSchedules['end_date'];
                    $getSTime = $getSchedules['start_time'];
                    $getETime = $getSchedules['end_time'];

                    if ($getSDate == $startDate) {
                        if ($getSTime == $startTime && $getETime == $endTime) {
                            $this->response(['status' => 0, 'message' => 'Already Busy.']);die;
                        } else if (($startTime >= $getSTime && $endTime <= $getETime)) {
                            $this->response(['status' => true, 'message' => 'Already Busy.']);die;
                        }
                    }
                }
            }

            $data = ['mechanic_id' => $this->param['mechanic_id'],
                'start_date' => $this->param['start_date'],
                'end_date' => $this->param['end_date'],
                'start_time' => $this->param['start_time'],
                'end_time' => $this->param['end_time']];
            $update = $this->Busy_schedule_model->insert($data);
        }

        if ($update) {
            if (isset($this->param['busy_schedule_id']) && $this->param['busy_schedule_id'] != '') {
                $busyScheduleId = $this->param['busy_schedule_id'];
            } else {
                $busyScheduleId = $update;
            }
            $getSchedule = $this->Busy_schedule_model
                ->fields('busy_schedule_id,mechanic_id,start_date,end_date,start_time,
                end_time')
                ->where(['busy_schedule_id' => $busyScheduleId])
                ->get();
            $this->response(['status' => true, 'message' => 'Updated Successfully.', 'response' => $getSchedule]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Get Request timepicker withlocation for Mechanic Calender
    public function getRequestForMechanicCalender_post()
    {
        $this->load->model(['Request_issues_model', 'User_model', 'Busy_schedule_model']);

        $mechanicId = $this->param['mechanic_id'];
        $requestType = $this->param['request_type']; // Only for repair

        //For Initialization
        $getRequest = $this->Request_model->fields(['user_id', 'request_id'])
            ->with_user('fields:name,profile_picture')
            ->where(['mechanic_id' => $mechanicId, 'request_type' => $requestType])
            ->where('request_status', array('1', '2'))
            ->get_all();

        if (!empty($getRequest)) {
            foreach ($getRequest as $getRequests) {
                $requestId = $getRequests['request_id'];
                $getTimeDate = $this->Request_issues_model->where('request_id', $requestId)
                    ->where('date >=', date('Y-m-d'))
                    ->get();

                if ($getTimeDate['date'] != '' && $getTimeDate['timepicker'] != '') {

                    $response[] = [
                        'request_id' => $getRequests['request_id'],
                        'date' => $getTimeDate['date'],
                        'time' => $getTimeDate['timepicker'],
                        'location' => $getTimeDate['location'],
                        'user_name' => $getRequests['user']['name'],
                        'user_profilepicture' => $getRequests['user']['profile_picture'],
                        'isRequest' => 101,
                    ];
                }
            }
        }

        $getBusySchedule = $this->Busy_schedule_model
            ->fields('busy_schedule_id,start_date,end_date,start_time,end_time')
            ->get_all(['mechanic_id' => $mechanicId, 'isDeleted' => 101, 'end_date >=' => date("Y-m-d")]);
        if (!empty($getBusySchedule)) {
            foreach ($getBusySchedule as $getBusySchedules) {
                $startDate = $getBusySchedules['start_date'];
                $endDate = $getBusySchedules['end_date'];
                $startTime = $getBusySchedules['start_time'];
                $endTime = $getBusySchedules['end_time'];

                $start = explode(' ', $startDate);
                $end = explode(' ', $endDate);

                $response[] = ['busy_schedule_id' => $getBusySchedules['busy_schedule_id'],
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'isRequest' => 100,
                ];
            }
        }

        if (!empty($response)) {
            $this->response(['status' => true, 'message' => 'Your Request Here.', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Request Here.']);die;
        }
    }

    ## Accept The Request(Only For Repair Case)
    public function requestAcceptOnCalender_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model']);
        $requestId = $this->param['request_id'];
        $notify = "";
        $getStatus = $this->Request_model->fields('request_status')
            ->with_transaction('fields: transaction_id,transaction_charge_id')
            ->where('request_id', $requestId)->get();
        $updateStatus = false;

        if ($getStatus['request_status'] == 1) {

            $requestTransactionId = $getStatus['transaction']['transaction_id'];
            $requestTransactionRef = $getStatus['transaction']['transaction_charge_id'];

            $confirmPayment = captureAmount($requestTransactionRef);
            if ($confirmPayment != null && $confirmPayment->status == 'succeeded') {
                $confirmPaymentData = [
                    'transaction_charge_id' => $confirmPayment->id,
                    'object_type' => $confirmPayment->object,
                    'amount' => ($confirmPayment->amount / 100),
                    'network_status' => $confirmPayment->outcome->network_status,
                    'message' => $confirmPayment->outcome->seller_message,
                    'paid_status' => 2,
                    'source_id' => $confirmPayment->source->id,
                    'transaction_type' => 1, // 1 for callout fee
                    // 'last4'=>$lastFourDigit,
                    'payment_type' => 1, //Which type payment
                    'source_brand' => $confirmPayment->source->brand,
                    'transaction_status' => $confirmPayment->status,
                ];
                $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($confirmPaymentData);

                if (!$updateTransaction) {
                    $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                }
            } else {
                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
            }

            $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => 2]);

            $getRequest = $this->Request_model->fields('user_id,request_type')->where('request_id', $requestId)->get();

            $getUserDetails = $this->User_model->fields('device_key,platform_type,isProduction,platform_type')
                ->where(['user_id' => $getRequest['user_id'], 'isOnline' => 101])->get();

            $device_id = $getUserDetails['device_key'];
            $isProduction = $getUserDetails['isProduction'];
            $platform_type = $getUserDetails['platform_type'];

            // Send Notification From Andriod To User
            if ($getUserDetails['platform_type'] == 1) {

                $notificationMessage = [
                    'notification_type' => 4,
                    'message' => 'Mechanic Accepted Your Request',
                    'request_id' => $requestId,
                    'request_type' => $getRequest['request_type'],
                ];
                $message = [
                    'message' => 'Notification For Accepted Your Request',
                    'data' => $notificationMessage,
                ];

                $notify = $this->notification->fcmUser($device_id, $message);

            } else if ($getUserDetails['platform_type'] == 2) {
                $bodyData = ['message' => 'Notification For Accepted Your Request'];
                $message = [
                    'title' => 'Mechanic Accepted your request',
                    'category' => ['notification_type' => 4, 'request_id' => $requestId],
                ];

                $notify = $this->notification->apnsUser($device_id, $message, $isProduction, $platform_type, $bodyData);
            }

            if ($updateStatus) {
                $this->response(['status' => true, 'message' => 'Successfully Accepted.']);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something Went wrong']);die;
            }
        }

        $this->response(['status' => false, 'message' => 'Request is not available now.']);die;
    }

    ## Deny The Request(Only For Repair Case)
    public function requestDenyOnCalender_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model']);
        $requestId = $this->param['request_id'];
        $notify = "";
        // Status 5 for cancel by mechanic
        $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => 3]);

        $getRequest = $this->Request_model->fields(['user_id', 'request_type'])->with_mechanic('fields: name')->get('request_id', $requestId);

        $mechanicName = "Mechanic ";
        if (isset($getRequest['mechanic']) && $getRequest['mechanic'] != null) {
            $mechanicName = $getRequest['mechanic']['name'];
        }
        $getUserDetails = $this->User_model->fields(['device_key', 'platform_type', 'isProduction'])->where('user_id', $getRequest['user_id'])->get();

        // Send Notification From Andriod To User
        $deviceId = $getUserDetails['device_key'];
        $platformType = $getUserDetails['platform_type'];
        $isProduction = $getUserDetails['isProduction'];

        if ($getUserDetails['platform_type'] == 1) {
            $message = [
                'message' => 'Notification For Reject Your Request',
                'data' => [
                    'message' => "$mechanicName is unavailable. Click here to book another mechanic or call us for assistance!",
                    'notification_type' => 8, 'request_id' => $requestId,
                    'request_type' => $getRequest['request_type'],
                ],
            ];

            $notify = $this->notification->fcmUser($deviceId, $message);

        } elseif ($getUserDetails['platform_type'] == 2) {
            $bodyData = ['message' => 'Notification For Reject Your Request'];
            $message = [
                'body' => "$mechanicName is unavailable. Click here to book another mechanic or call us for assistance!",
                'category' => ['notification_type' => 8],
                'icon' => 'myicon',
                'sound' => 'mySound', 'request_id' => $requestId,
            ];
            //$device_id = $getUserDetails['device_key'];
            $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platformType, $bodyData);

        }

        if ($updateStatus) {
            $this->response(['status' => true, 'message' => 'Cancelled Successfully']);die;
        } else {
            $this->response(['status' => false, 'message' => 'Error in process. Please try again later']);die;
        }
    }

    ## For Reschedule time in mechanic calender
    public function rescheduleTime_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_issues_model', 'User_model', 'Request_model']);
        $requestId = $this->param['request_id'];
        $time = $this->param['time'];
        $date = $this->param['date'];
        $updateData = ['timepicker' => $time, 'date' => $date];
        $update = $this->Request_issues_model->where('request_id', $requestId)->update($updateData);

        // Send Notification From Andriod To User
        $getRequest = $this->Request_model->fields('user_id,request_type')->get('request_id', $requestId);
        $getUserDetails = $this->User_model->fields(['device_key', 'platform_type', 'isProduction', 'platform_type'])->where('user_id', $getRequest['user_id'])->get();

        $device_id = $getUserDetails['device_key'];
        $platform_type = $getUserDetails['platform_type'];
        $isProduction = $getUserDetails['isProduction'];

        if ($getUserDetails['platform_type'] == 1) {

            $notificationMessage = [
                'notification_type' => 3,
                'message' => 'Mechanic Reschedule Your Request',
                'date' => $this->param['date'],
                'time' => $this->param['time'],
                'request_type' => $getRequest['request_type'],
            ];
            $message = [
                'message' => 'Notification For Patcher: Mechanic Rescheduled Request',
                'data' => $notificationMessage,
            ];

            $notify = $this->notification->fcmUser($device_id, $message);
        } elseif ($getUserDetails['platform_type'] == 2) {
            $bodyData = ['message' => 'Mechanic Reschedule Your Work'];
            $message = [
                'title' => 'Patcher: Mechanic Reschedule Your Request',
                'category' => ['notification_type' => 3],
                'icon' => 'myicon',
                'sound' => 'mySound',
            ];
            $notify = $this->notification->apnsUser($device_id, $message, $isProduction, $platform_type, $bodyData);
        }

        //

        if ($update) {
            $this->response(['status' => true, 'message' => 'Updated Successfully.', 'response' => $updateData]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Updated']);die;
        }
    }

    ## Get the Repair Work with Total Payment of Issuse For Mechanic Section
    public function getRepairWorkWithPaymentForMechanic_post()
    {
        $this->load->model(['Request_issues_model', 'Mechanic_add_service_model', 'Mechanic_rating_model', 'Mechanic_issuses_model', 'Issue_model', 'Request_model']);
        $requestId = $this->param['request_id'];
        $mechanicId = $this->param['mechanic_id'];
        $isCompleted = 100;

        $getAllRequestIssuse = $this->Request_issues_model->fields('issue_id,isCompleted')->with_issue('fields:issue_name,issuse_price')->where(['request_id' => $requestId, 'request_issue_status' => 101, 'isDeleted' => 101])->get_all();

        $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id', 'part_name', 'price', 'isCompleted'])->where(['request_id' => $requestId, 'mechanic_id' => $mechanicId, 'isDeleted' => 101])->get_all();

        if (!empty($getAllRequestIssuse)) {
            foreach ($getAllRequestIssuse as $getAllRequestIssuses) {
                $issueId = $getAllRequestIssuses['issue']['issue_id'];

                $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price', 'issuse_id', 'isCompleted'])->get(['mechanic_id' => $mechanicId, 'request_id' => $requestId, 'issuse_id' => $issueId]);

                if (!empty($getNewPriceOfIssuse['issuse_price'])) {
                    $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                    $isCompleted = $getNewPriceOfIssuse['isCompleted'];
                } else {
                    $issusePrice = $getAllRequestIssuses['issue']['issuse_price'];
                    $isCompleted = $getNewPriceOfIssuse['isCompleted'];
                }

                $service = [];
                if (!empty($getNewAddService)) {
                    foreach ($getNewAddService as $getNewAddServices) {
                        $service[] = [
                            'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                            'issue_name' => $getNewAddServices['part_name'],
                            'issue_price' => $getNewAddServices['price'],
                            'status' => 101, //101 for edit service price,
                            'isCompleted' => $getNewAddServices['isCompleted'],
                        ];
                    }
                }

                $result[] = ['issue_id' => $getAllRequestIssuses['issue']['issue_id'],
                    'issue_name' => $getAllRequestIssuses['issue']['issue_name'],
                    'issue_price' => $issusePrice,
                    'status' => 100, //100 for edit issue price
                    'isCompleted' => $isCompleted,
                ];
            }
            $response = array_merge($result, $service);
            if (!empty($response)) {
                $this->response(['status' => true, 'message' => 'All Completed Issse With Price', 'response' => $response]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }
        } elseif (!empty($getNewAddService)) {
            $service = [];
            if (!empty($getNewAddService)) {
                foreach ($getNewAddService as $getNewAddServices) {
                    $service[] = [
                        'issue_id' => $getNewAddServices['mechanic_add_service_id'],
                        'issue_name' => $getNewAddServices['part_name'],
                        'issue_price' => $getNewAddServices['price'],
                        'status' => 101,
                    ];
                }
            }
            if (!empty($service)) {
                $this->response(['status' => true, 'message' => 'All Completed Issue With Price', 'response' => $service]);die;
            } else {
                $this->response(['status' => false, 'message' => 'No Any Issuses']);die;
            }

        } else {
            $this->response(['status' => false, 'message' => 'More details not available for this request.']);die;
        }
    }

    ## Get Request Complete Details : MOHIT
    public function requestCompleteDetails_post()
    {
        $this->load->model(['Request_issues_model', 'Mechanic_add_service_model', 'Mechanic_rating_model', 'Mechanic_issuses_model', 'Issue_model', 'Request_model', 'Transaction_model', 'User_vehicles_model']);

        $request_id = $this->param['request_id'];

        $requestDetails = $this->Request_model->fields('request_status,request_type,user_vehicles_id,request_lat,request_lng,request_date')
            ->with_transactionAll()
            ->with_requestIssues(['fields' => 'issue_id,request_issue_status,isCompleted,date,timepicker', 'where' => ['request_issue_status' => 101, 'isDeleted' => 101], 'with' => ['relation' => 'issue', 'fields' => 'issue_name,issuse_price']])
            ->with_newService(['fields' => 'mechanic_add_service_id,description,part_name,price,isCompleted', 'where' => ['isDeleted' => 101]])
            ->with_rating(['fields' => 'rating,comment', 'where' => ['from_at' => 1]])
            ->get(['request_id' => $request_id]);

        if (empty($requestDetails)) {
            $this->response(['status' => false, 'message' => 'Request Details not found.']);
        }

        $transactioDetails = $this->Transaction_model->fields('transaction_charge_id,transaction_reference')
            ->order_by('transaction_id', 'desc')
            ->get(['request_id' => $requestDetails['request_id']]);
        $requestDate = explode(" ", $requestDetails['request_date']);
        $date = $requestDate[0];
        $time = $requestDate[1];

        $transactionsList = [];
        if (isset($requestDetails['transactionAll']) & !empty($requestDetails['transactionAll'])) {
            foreach ($requestDetails['transactionAll'] as $trans) {

                $transactionDateTime = $trans['created_at'];
                $explodeTransactionDateTime = explode(' ', $transactionDateTime);
                $transactionDate = $explodeTransactionDateTime[0];
                $transactionTime = $explodeTransactionDateTime[1];
                $transaction_type = $trans['transaction_type'];
                if ($transaction_type == 1) {
                    $transaction_type = "Callout Fee";
                } else {
                    $transaction_type = "Service Fee";
                }

                $object_type = $trans['object_type'];

                $transactionsList[] = [
                    'transaction_id' => !empty($trans['transaction_reference']) ? $trans['transaction_reference'] : $trans['transaction_charge_id'],
                    'amount' => $trans['amount'],
                    'payment_type' => $trans['payment_type'],
                    'transaction_status' => $trans['transaction_status'],
                    'transaction_date' => $transactionDate,
                    'transaction_time' => $transactionTime,
                    'transaction_type' => $transaction_type,
                    'remark' => ($object_type == "charge") ? "Paid" : ucfirst($object_type),
                    'remark_status' => ($object_type == "charge") ? 1 : 2, //Charge = 1 || refund = 2
                ];
            }
        }

        $requestIssues = $requestDetails['requestIssues'];
        $issueDetails = array();
        if (!empty($requestIssues)) {
            foreach ($requestIssues as $requestIssue) {
                $issusePrice = $requestIssue['issue']['issuse_price'];
                $isCompleted = $requestIssue['isCompleted'];
                $date = $requestIssue['date'];
                $time = $requestIssue['timepicker'];
                $mechanicIssues = $this->Mechanic_issuses_model->get(['request_id' => $request_id, 'issuse_id' => $requestIssue['issue_id']]);
                if (!empty($mechanicIssues)) {
                    $issusePrice = $mechanicIssues['issuse_price'];
                    $isCompleted = $mechanicIssues['isCompleted'];
                }

                $issueDetails[] = array(
                    'issue_id' => $requestIssue['issue_id'],
                    'issue_name' => $requestIssue['issue']['issue_name'],
                    'issue_price' => $issusePrice,
                    'status' => 100,
                    'request_issue_status' => $requestIssue['request_issue_status'],
                    'isCompleted' => $isCompleted,
                    'description' => '',
                );
            }

        }

        $newServices = $requestDetails['newService'];
        if (!empty($newServices)) {
            foreach ($newServices as $newService) {
                $issueDetails[] = array(
                    'issue_id' => $newService['mechanic_add_service_id'],
                    'issue_name' => $newService['part_name'],
                    'issue_price' => $newService['price'],
                    'status' => 101,
                    'request_issue_status' => 101,
                    'isCompleted' => $newService['isCompleted'],
                    'description' => $newService['description'],
                );
            }

        }

        $ratingUser = $requestDetails['rating'];
        if (empty($requestDetails['rating'])) {
            $rating = "";
            $review = "";
        } else {
            $rating = $ratingUser['rating'];
            $review = $ratingUser['comment'];
        }
        $transaction = $transactioDetails['transaction_reference'];
        if (!$transaction) {
            $transaction = $transactioDetails['transaction_charge_id'];
        }

        $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault', 'vehicle_number', 'vehicle_picture'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_vehicles_id' => $requestDetails['user_vehicles_id']])->get();

        if ($userVehicle) {
            $vehicleDetails = [
                'user_vehicles_id' => $userVehicle['user_vehicles_id'],
                'vehicle_number' => $userVehicle['vehicle_number'],
                'vehicle_picture' => $userVehicle['vehicle_picture'],
                'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
            ];
        } else {
            $vehicleDetails = new stdClass;
        }

        $responseData = [
            'request_id' => $requestDetails['request_id'],
            'request_status' => $requestDetails['request_status'],
            'request_type' => $requestDetails['request_type'],
            'request_date' => $date,
            'request_time' => $time,
            'latitude' => $requestDetails['request_lat'],
            'longitude' => $requestDetails['request_lng'],
            'issueDetails' => $issueDetails,
            'rating' => $rating,
            'review' => $review,
            'transaction' => $transaction,
            'vehicleDetails' => $vehicleDetails,
            'transactionsList' => $transactionsList,
        ];

        $this->response(['status' => true, 'message' => 'Request Details found.', 'requestDetails' => $responseData]);
    }

    ## Get Request Issue Details submitted bu user : MOHIT
    public function userRequestIssueDetails_post()
    {
        $this->load->model(['Request_issues_model', 'Issue_model', 'Request_model', 'User_model', 'User_vehicles_model']);

        $request_id = $this->param['request_id'];

        $requestDetails = $this->Request_model->fields('request_status,request_type,request_lat,request_lng,request_date,user_id,user_vehicles_id')
            ->with_requestIssues(['fields' => 'issue_id,request_issue_status,isCompleted,date,timepicker', 'where' => ['request_issue_status' => 101, 'isDeleted' => 101], 'with' => ['relation' => 'issue', 'fields' => 'issue_name,issuse_price']])
            ->get(['request_id' => $request_id]);

        if (empty($requestDetails)) {
            $this->response(['status' => false, 'message' => 'Request Details not found.']);
        }

        $requestDate = explode(" ", $requestDetails['request_date']);
        $date = $requestDate[0];
        $time = $requestDate[1];

        $requestIssues = $requestDetails['requestIssues'];
        $issueDetails = array();
        if (!empty($requestIssues)) {
            foreach ($requestIssues as $requestIssue) {
                $issusePrice = $requestIssue['issue']['issuse_price'];
                $isCompleted = $requestIssue['isCompleted'];
                $date = $requestIssue['date'];
                $time = $requestIssue['timepicker'];
                $issueDetails[] = array(
                    'issue_id' => $requestIssue['issue_id'],
                    'issue_name' => $requestIssue['issue']['issue_name'],
                    'issue_price' => $issusePrice,
                    'status' => 100,
                    'request_issue_status' => $requestIssue['request_issue_status'],
                    'isCompleted' => $isCompleted,
                );
            }

        }

        $user_vehicles_id = $requestDetails['user_vehicles_id'];

        if ($user_vehicles_id == "" || $user_vehicles_id == 0) {
            $where = ['user_id' => $requestDetails['user_id'], 'isDefault' => 101];
        } else {
            $where = ['user_vehicles_id' => $user_vehicles_id];
        }

        $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where($where)->get();

        if ($userVehicle) {
            $vehicleDetails = [
                'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
            ];
        } else {
            $vehicleDetails = array();
        }

        $responseData = [
            'issueDetails' => $issueDetails,
            'vehicleDetails' => $vehicleDetails,
        ];

        $this->response(['status' => true, 'message' => 'Issue details available.', 'requestDetails' => $responseData]);
    }

    ## Event Details For Request
    public function getEventDetailsForRequest_post()
    {
        $this->load->model(['Request_model', 'User_model']);
        $requestId = $this->param['request_id'];
        $getUserID = $this->Request_model->fields('user_id')->with_request_issues('fields: date, timepicker, location')->get('request_id', $requestId);
        $userId = $getUserID['user_id'];
        $getUser = $this->User_model->fields('name')->get('user_id', $userId);
        $userRequestDetails = [
            'user_name' => $getUser['name'],
            'location' => $getUserID['request_issues']['location'],
            'date' => $getUserID['request_issues']['date'],
            'time' => $getUserID['request_issues']['timepicker'],
            'request_type' => $this->param['request_type'],
        ];
        if (!empty($getUserID)) {
            $this->response(['status' => true, 'message' => 'Request Details Here.', 'response' => $userRequestDetails]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Repair Issue Complete By Mechanic
    public function issueCompleteByMechanic_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model']);
        $requestId = $this->param['request_id'];
        $mechaniId = $this->param['mechanic_id'];
        $getAcceptedRequest = $this->Request_model->fields('user_id')->where(['request_id' => $requestId, 'mechanic_id' => $mechaniId, 'request_status' => 2])->get();

        if (!empty($getAcceptedRequest)) {
            $getUser = $this->User_model->fields(['name', 'platform_type', 'isProduction', 'device_key'])->get(['user_id' => $getAcceptedRequest['user_id']]);

            //Status 8 for complete by mechanic
            $updateCompleteByMechanic = $this->Request_model->where(['request_id' => $requestId, 'mechanic_id' => $mechaniId])->update(['request_status' => 8]);
            if ($updateCompleteByMechanic) {
                $updateData = ['request_status' => 8];

                // Send Notification From Andriod To User
                $deviceId = $getUser['device_key'];
                $isProducation = $getUser['isProduction'];
                $platform_type = $getUser['platform_type'];

                if ($getUser['platform_type'] == 1) {
                    $message = [
                        'message' => 'Completed Your Work',
                        'data' => [
                            'message' => 'Mechanic Completed Your Work',
                            'notification_type' => 5,
                            'request_id' => $requestId,
                            'request_type' => $getAcceptedRequest['request_type'],
                        ],
                    ];

                    $notify = $this->notification->fcmUser($deviceId, $message);
                } elseif ($getUser['platform_type'] == 2) {
                    $bodyData = ['message' => 'Mechanic Completed Your Work'];
                    $message = [
                        'title' => 'Completed Your Work',
                        'category' => ['notification_type' => 5, 'request_id' => $requestId],
                    ];

                    $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platform_type, $bodyData);
                }

                $this->response(['status' => true, 'message' => 'Completed Successfully.', 'response' => $updateData]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Not Updated']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Already Completed.']);die;
        }
    }

    ## Change Request Status By Mechanic (5 For Cancel And 8 For Completed By Mechanic And 9 For Arrived)
    public function changeRequestStatus_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model', 'Transaction_model', 'Mechanic_add_service_model', 'Request_issues_model']);

        $redis = createConnectionToRedis();
        $requestId = $this->param['request_id'];
        $mechanicId = $this->param['mechanic_id'];
        $status = $this->param['status'];
        //$requestType = $this->param['request_type'];

        $getAcceptedRequest = $this->Request_model->fields(['user_id', 'request_type', 'request_status'])
            ->with_transaction(['fields' => 'transaction_id,amount,transaction_charge_id', 'where' => ['transaction_type' => 1]])
            ->with_transactionFinal(['fields' => 'transaction_id,amount,transaction_charge_id', 'where' => ['transaction_type' => 2]])
            ->where(['request_id' => $requestId])->get();
        if (!$getAcceptedRequest) {
            $this->response(['status' => false, 'message' => 'Invalid Request.']);die;
        }

        $getUser = $this->User_model->fields(['name', 'platform_type', 'isProduction', 'device_key'])
            ->where(['user_id' => $getAcceptedRequest['user_id'], 'isOnline' => 101])
            ->get();

        $deviceId = $getUser['device_key'];
        $isProduction = $getUser['isProduction'];
        $platformType = $getUser['platform_type'];

        if ($getAcceptedRequest['request_status'] == 4 || $getAcceptedRequest['request_status'] == 7) {
            $this->response(['status' => false, 'message' => 'User cancelled this request.']);die;
        }

        if (!empty($getAcceptedRequest)) {
            if ($status == 10) { //Status 10 for mechanic Arriving

                //status 10 for mechanic Arriving
                $mechanicArriving = $this->Request_model->where(['request_id' => $requestId, 'mechanic_id' => $mechanicId])->update(['request_status' => $status]);
                if ($mechanicArriving) {
                    $updateData = ['request_status' => $status];

                    //Send Notification To User (Andriod)
                    if ($getUser['platform_type'] == 1) {
                        $message = [
                            'message' => 'Mechanic arriving at your location. ',
                            'data' => ['message' => 'Mechanic arriving at your location. ',
                                'notification_type' => 11,
                                'icon' => 'myicon'],
                        ];
                        $notify = $this->notification->fcmUser($deviceId, $message);
                    } elseif ($getUser['platform_type'] == 2) {
                        $bodyData = ['message' => 'Mechanic arriving at your location.'];
                        $message = [
                            'title' => 'Mechanic arriving at your location.',
                            'category' => ['notification_type' => 11],
                            'icon' => 'myicon',
                            'sound' => 'mySound',
                        ];
                        $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platformType, $bodyData);
                    }

                    $this->response(['status' => true, 'message' => 'Mechanic Arriving.', 'response' => $updateData]);
                } else {
                    $this->response(['status' => false, 'message' => 'Mechanic Not Arriving.']);
                }
            } elseif ($status == 9) { //Status 9 for mechanic arrived

                //Status 9 for mechanic arrived
                $mechanicArrive = $this->Request_model->where(['request_id' => $requestId, 'mechanic_id' => $mechanicId])->update(['request_status' => $status]);

                if ($mechanicArrive) {
                    $updateData = ['request_status' => $status];

                    // Status 9 For Mechanic Arrived
                    // Send Notification From Andriod To User
                    if ($getUser['platform_type'] == 1) {
                        $message = [
                            'message' => 'Mechanic has arrived at your location. ',
                            'data' => [
                                'message' => 'Mechanic has arrived at your location. ',
                                'request_id' => $requestId,
                                'notification_type' => 9,
                                'request_type' => $getAcceptedRequest['request_type'],
                            ],
                        ];
                        $notify = $this->notification->fcmUser($deviceId, $message);
                    }

                    if ($getUser['platform_type'] == 2) {
                        $bodyData = ['message' => 'Mechanic has arrived at your location.'];
                        $message = [
                            'title' => 'Mechanic has arrived at your location.',
                            'category' => ['request_id' => $requestId, 'notification_type' => 9],
                        ];
                        $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platformType, $bodyData);
                    }
                    $this->response(['status' => true, 'message' => 'Mechanic Has Been Arrive.', 'response' => $updateData]);
                } else {
                    $this->response(['status' => false, 'message' => 'Mechanic Not Arrive.', 'response' => $updateData]);
                }
            } elseif ($status == 8) { //Status 8 for complete by mechanic

                $redis->set($mechanicId, 'ONLINE');

                //Status 8 for complete by mechanic
                // $amount = $getAcceptedRequest['transactionFinal']['amount'];
                // $completeTrip = false;

                $getAllRequestIssuse = $this->Request_issues_model->fields('issue_id')->with_issue('fields:issue_name,issuse_price')->where(['request_id' => $requestId, 'request_issue_status' => 101, 'isDeleted' => 101, 'isCompleted' => 101])->get_all();

                $getNewAddService = $this->Mechanic_add_service_model->fields(['mechanic_add_service_id'])->where(['request_id' => $requestId, 'isDeleted' => 101, 'isCompleted' => 101])->get_all();

                if (!$getAllRequestIssuse && !$getNewAddService) {
                    // $status = 6;
                    // $completeTrip = true;
                    $this->response(['status' => false, 'message' => 'Please process Issue and services first.']);
                }

                $updateCompleteByMechanic = $this->Request_model->where(['request_id' => $requestId, 'mechanic_id' => $mechanicId])->update(['request_status' => $status]);
                if ($updateCompleteByMechanic) {
                    $updateData = ['request_status' => $status];

                    if ($getUser['platform_type'] == 1) {
                        $message = [
                            'message' => 'Completed Your Work',
                            'data' => [
                                'message' => 'Mechanic Completed Your Work',
                                'notification_type' => 5,
                                'request_id' => $requestId,
                                'request_type' => $getAcceptedRequest['request_type'],
                            ],
                        ];
                        $notify = $this->notification->fcmUser($deviceId, $message);
                    } elseif ($getUser['platform_type'] == 2) {
                        $bodyData = ['message' => 'Mechanic Completed Your Work'];
                        $message = [
                            'title' => 'Completed Your  Work',
                            'category' => ['notification_type' => 5, 'request_id' => $requestId],
                        ];
                        $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platformType, $bodyData);
                    }

                    $this->response(['status' => true, 'message' => 'Completed Successfully.', 'response' => $updateData]);
                } else {
                    $this->response(['status' => false, 'message' => 'Not Completed.']);
                }
            } elseif ($status == 5) {
                //Status 5 for cancel by mechanic

                $redis->set($mechanicId, 'ONLINE');
                $requestTransactionId = $getAcceptedRequest['transaction']['transaction_id'];
                $requestTransactionRef = $getAcceptedRequest['transaction']['transaction_charge_id'];

                $refundPayment = refundAuthAmount($requestTransactionRef);
                if ($refundPayment != null && $refundPayment->status == 'succeeded') {
                    $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                    $refundPaymentData = [
                        'transaction_charge_id' => $refundPayment->id,
                        'transaction_reference' => $transaction_reference,
                        'object_type' => $refundPayment->object,
                        'amount' => ($refundPayment->amount / 100),
                        'paid_status' => 3, //Refund Amount
                        'transaction_type' => 1, // 1 for callout fee
                        'payment_type' => 1, //Which type payment
                        'transaction_status' => $refundPayment->status,
                    ];
                    $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($refundPaymentData);

                    if (!$updateTransaction) {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                    }
                } else {
                    $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                }

                $updateMechReqData = [
                    'mechanic_id' => $mechanicId,
                    'request_id' => $requestId,
                    'request_amount' => 0,
                    'comission' => $getAcceptedRequest['transaction']['amount'],
                    'remarks' => 'Charged for Payout user refund.',
                    'isChargable' => 101,
                ];

                $updateMechReqDetail = $this->Mechanic_request_detail_model->insert($updateMechReqData);
                if (!$updateMechReqDetail) {
                    $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to charge mechanic.']);
                }

                //Status 5 for cancel by mechanic
                $cancelByMechanic = $this->Request_model->where(['request_id' => $requestId])->update(['request_status' => $status, 'mechanic_id' => $mechanicId]);
                $getRequestType = $this->Request_model->fields('request_type')->where('request_id', $requestId)->get();

                if ($cancelByMechanic) {
                    $updateData = ['request_status' => $status];

                    // Status 8 For Cancel By Mechanic
                    // Send Notification From Andriod To User

                    if ($getRequestType['request_type'] == 1) { // For Repair
                        $notification_type = 14;
                    } elseif ($getRequestType['request_type'] == 2) { // For Emergency
                        $notification_type = 13;
                    }

                    if ($getUser['platform_type'] == 1) {
                        $message = [
                            'message' => 'Your Request Cancel BY Mechanic',
                            'data' => [
                                'message' => 'Request Cancel BY Mechanic',
                                'notification_type' => $notification_type, 'request_id' => $requestId,
                                'request_type' => $getAcceptedRequest['request_type'],
                            ],
                        ];
                        $notify = $this->notification->fcmUser($deviceId, $message);
                    } elseif ($getUser['platform_type'] == 2) {
                        $bodyData = ['message' => 'Request Cancel BY Mechanic'];
                        $message = [
                            'title' => 'Your Request Cancel BY Mechanic',
                            'category' => ['request_id' => $requestId, 'notification_type' => $notification_type],

                        ];
                        //  $deviceId = 'A731FBC7A2041DD89D9F47CFB0111A46806539F96CC97026E52D45FF7A40E683';
                        $notify = $this->notification->apnsUser($deviceId, $message, $isProduction, $platformType, $bodyData);
                    }
                    $this->response(['status' => true, 'message' => 'Successfully Cancel Request.', 'response' => $updateData]);
                } else {
                    $this->response(['status' => false, 'message' => 'Request Not Cancel.']);
                }
            }
        } else {
            $this->response(['status' => false, 'message' => ' Something went wrong.']);die;
        }
    }

    ## Delete The Request
    public function cancelRequest_post()
    {
        $this->load->model(['Request_model']);
        $requestId = $this->param['request_id'];

        // 5 for cancel by mechanic
        $cancelByMechanic = $this->Request_model->where(['request_id' => $requestId])->update(['request_status' => 5]);

        if ($cancelByMechanic) {
            $this->response(['status' => true, 'message' => 'Cancelled successfully!!', 'response' => $requestId]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Delete The Request
    public function deleteRequest_post()
    {
        $this->load->model(['Request_model']);
        $requestId = $this->param['request_id'];

        $updateOptions = array(
            'where' => array('request_id' => $requestId, 'deleted_at' => null),
            'data' => array('deleted_at' => date('Y-m-d h-m-s')),
            'table' => 'requests',
        );
        $deleterequest = $this->common_model->customUpdate($updateOptions);

        if ($deleteRequest) {
            $this->response(['status' => true, 'message' => 'Request deleted successfully!!', 'response' => $requestId]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    private function _completeRequest($request_id)
    {
        $calloutFee = $amount = 0;
        $getCalloutFee = $this->Transaction_model->fields('amount,created_at')->get(['request_id' => $request_id, 'transaction_type' => 1]);
        if ($getCalloutFee) {
            $calloutFee = $getCalloutFee['amount'];
        }

        $mechanicDetail = $this->Request_model->with_mechanic('fields:mechanic_id,name,device_key,isProduction,platform_type,mechanic_uuid')
            ->where('request_id', $request_id)->get();

        $mechanicId = $mechanicDetail['mechanic']['mechanic_id'];

        //Insert Mech Details
        $updateMechReqData = [
            'mechanic_id' => $mechanicId,
            'request_id' => $request_id,
            'request_amount' => $amount,
            'comission' => ($amount * 0.75) + $calloutFee, //0.75 i.e 75% stack of mechanic in a request
            'remarks' => 'Trip Payment',
            'isChargable' => 100,
        ];

        $paymentMesssage = "Request Id: $request_id has not been completed! Here service amount: $amount, Callout Fee: $calloutFee and Total Amount: " . ($amount + $calloutFee) . ". For any query please use Request Id.";

        $args = [
            'message' => $paymentMesssage,
            'subject' => 'Patcher: Request Completed',
            'email_id' => $emailId,
            'template_name' => 'Sign Up',
            'vars' => [
                [
                    'name' => 'message',
                    'content' => $paymentMesssage,
                ],
            ],
        ];

        $mailResponse = sendMandrillEmailTemlate($args);

        $updateMechReqDetail = $this->Mechanic_request_detail_model->insert($updateMechReqData);

        // $getTransactionDate = $this->Transaction_model
        //     ->fields('')
        //     ->get(['transaction_id' => $insert]);
        $explodeDate = explode(' ', $getCalloutFee['created_at']);

        $getUserRating = $this->Mechanic_rating_model
            ->fields('rating')
            ->where(['user_id' => $userId, 'mechanic_id' => $mechanicId, 'from_at' => 2])
            ->get();

        if (!empty($getUserRating)) {
            $userRating = $getUserRating['rating'];
        } else {
            $userRating = 0;
        }

        $deviceId = $mechanicDetail['mechanic']['device_key'];
        $isProduction = $mechanicDetail['mechanic']['isProduction'];
        $platformType = $mechanicDetail['mechanic']['platform_type'];

        $bodyData = ['request_id' => $request_id, 'amount' => $amount, 'user_id' => $userId, 'user_rating' => $userRating, 'date' => $explodeDate[0], 'time' => $explodeDate[1]];

        $pubnub_args = ['channel' => $mechanicDetail['mechanic']['mechanic_uuid'],
            'message' => ['type' => 'R12', 'ratingDetails' => $bodyData]];

        $publish_status = $this->mypubnub->publish($pubnub_args);

        // Send Notification From Andriod To User
        if ($platformType == 1) {
            $message = [
                'title' => 'Payment Successfully Transferred By User',
                'body' => $bodyData,
                'tag' => ['notification_type' => 12, 'request_id' => $request_id],
            ];

            $notify = $this->notification->fcmMechanic($deviceId, $message);
        } else if ($platformType == 2) {

            $message = [
                'title' => 'Payment Successfully Transferred By User',
                'category' => ['notification_type' => 12, 'request_id' => $request_id],
            ];

            $notify = $this->notification->apnsMechanic($deviceId, $message, $isProduction, $platformType, $bodyData);

        }

    }
}
