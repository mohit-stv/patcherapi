<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('Service_model');
    }
   
    public function addService_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addService') == FALSE) {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()],400);die;
        } else {
            $this->load->model(['Service_model']);   
            $addService['service_name'] = $this->param['service_name'];        	
    		if(isset($this->param['service_description']) && $this->param['service_description'] != NUll){
                $addService['service_description'] = $this->param['service_description'];
                $addService['service_status'] = 101;                
            }
            $insert = $this->Service_model->insert($addService);
    		if($insert){
    	       $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
         	}else{
    	       $this->response(['status' => false, 'message' => 'Something went wrong']);die;
    	    }
        }
    } 

    public function editService_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editService') == FALSE) {
            echo json_encode(["status" => false, "error_status" => 2, "message" => $this->form_validation->get_errors_as_array()],400);die;            
        } else {
            $this->load->model(['Service_model']);   
            $service_id = $this->param['service_id'];
            $editService['service_name'] = $this->param['service_name']; 
            if(isset($this->param['service_description']) && $this->param['service_description'] != NUll){
                $editService['service_description'] = $this->param['service_description'];                
            }
            if(isset($this->param['service_status']) && $this->param['service_status'] != NUll){
                $editService['service_status'] = $this->param['service_status'];                
            }            
            $update = $this->Service_model->where('service_id',$service_id)->update($editService);
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => ['service_id'=>$service_id]]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }  

    public function allService_post() {
    	
    	$this->load->model(['Service_model']);              	
		
		$services   =  $this->Service_model->where('service_status','101')->fields('service_id,service_name')->get_all();

        if($services){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => ['service' => $services]]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteSercives_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteSercives') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {        
        	$serviceId = $this->param['serviceId'];
        	
			$updateOptions = array(
                'where' => array('serviceId' => $serviceId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'services'
            );
            $deleteService = $this->common_model->customUpdate($updateOptions);

	        if($deleteService){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteService]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}