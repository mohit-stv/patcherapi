<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ToolItinerary extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addToolItinerary_post() {
         
    	$this->load->model(['ToolItineraries_model']);           	
		if(isset($this->param['toolItineraries']) && $this->param['toolItineraries'] != NUll){
			for($i=0;$i<count($this->post('toolItineraries'));$i++){				
				$toolItineraries_array=[
					'toolItinerarieName' => $this->param['toolItineraries'][$i]		
				];
				$insert = $this->Issues_model->insert($toolItineraries_array);
			}
		}
		if($insert){
	       $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
     	}else{
	       $this->response(['status' => false, 'message' => 'Something went wrong']);die;
	    }
    }  

    public function allToolItinerary_post() {
    	
		$this->load->model(['Tool_itinerary_model','Service_model','Qualification_model','Document_model']);    
		
		$toolItineraries   =  $this->Service_model->fields('service_name')
												->with_tools('fields:tool_itinerary_id,tool_itinerary_name','where:`tool_itinerary_status`=\'101\'')
												->where('service_id',array('1','2','3'))->get_all();

		/*Different combination of where clause
		$toolItineraries1   =  $this->Service_model->fields('service_name')
								->with_tools('fields:tool_itinerary_id,tool_itinerary_name')->where('service_id',array('1','2','3'))->get_all();
		
		$toolItineraries2   =  $this->Service_model->fields('service_name')
								->with_tools('fields:tool_itinerary_id,tool_itinerary_name','where:`tool_itinerary_status`=\'101\' AND `tool_description`=\'abdeali\'')->where('service_id',array('1','2','3'))->get_all();
		
		$toolItineraries3   =  $this->Service_model->fields('service_name')
								->with_tools('fields:tool_itinerary_id,tool_itinerary_name','where:`tool_itinerary_status`=\'101\' AND `tool_description`=\'abdeali\'')->where('service_id',array('1','2','3'))->get_all(array('service_status'=>'100'));
		
		$toolItineraries4   =  $this->Service_model->fields('service_name')
								->with_tools('fields:tool_itinerary_id,tool_itinerary_name','where:`tool_itinerary_status`=\'101\' AND `tool_description`=\'abdeali\'')->where('service_id',array('1','2','3'))->get_all(array('service_status'=>'100','service_description'=>'abdeali'));
		
		$toolItineraries5   =  $this->Service_model->fields('service_name')
							->with_tools('fields:tool_itinerary_id,tool_itinerary_name','where:`tool_itinerary_status`=\'101\'')
							->with_qualifications('fields:qualification_name','where:`qualification_status`=\'101\'')
							->with_documents('fields:document_name','where:`document_status`=\'101\'')
							->where('service_id',array('1','2','3'))->get_all();
		*/

		if($toolItineraries){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => $toolItineraries]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteQualifications_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteQualifications') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
        	$qualificationId = $this->param['qualificationId'];

			$updateOptions = array(
                'where' => array('qualificationId' => $qualificationId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'qualifications'
            );
            $deleteQualification = $this->common_model->customUpdate($updateOptions);

	        if($deleteQualification){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteQualification]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}