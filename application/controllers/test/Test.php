<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
        $this->load->helper('common');
        $this->load->library('Notification');
    }

    public function authCard_post()
    {
        $data = [
            'customerId' => $this->param['customerId'],
            'amount' => $this->param['amount'],
        ];

        echo json_encode(authorizePayment($data));die;
    }

    public function unser_post()
    {
        echo json_encode(unserialize('a:13:{s:11:"callout_fee";s:2:"30";s:14:"lastfour_digit";s:1:"0";s:7:"comment";s:0:"";s:11:"customer_id";s:18:"cus_Cs2CCiguPfUU29";s:8:"image_id";a:1:{i:0;i:84;}s:8:"latitude";s:10:"51.1034343";s:9:"longitude";s:9:"-0.516456";s:14:"questionanswer";a:7:{i:0;a:3:{s:9:"answer_id";s:1:"5";s:11:"question_id";s:1:"5";s:18:"questionIsMultiple";s:3:"101";}i:1;a:3:{s:9:"answer_id";s:3:"101";s:11:"question_id";s:1:"6";s:18:"questionIsMultiple";s:3:"100";}i:2;a:3:{s:9:"answer_id";s:3:"101";s:11:"question_id";s:1:"7";s:18:"questionIsMultiple";s:3:"100";}i:3;a:3:{s:9:"answer_id";s:3:"100";s:11:"question_id";s:1:"8";s:18:"questionIsMultiple";s:3:"100";}i:4;a:3:{s:9:"answer_id";s:3:"101";s:11:"question_id";s:2:"11";s:18:"questionIsMultiple";s:3:"100";}i:5;a:3:{s:9:"answer_id";s:3:"100";s:11:"question_id";s:2:"12";s:18:"questionIsMultiple";s:3:"100";}i:6;a:3:{s:9:"answer_id";s:3:"101";s:11:"question_id";s:2:"13";s:18:"questionIsMultiple";s:3:"100";}}s:8:"recovery";s:1:"0";s:12:"request_type";s:1:"2";s:8:"roadside";s:1:"1";s:7:"user_id";i:89;s:16:"user_vehicles_id";s:2:"23";}'));

        // echo unserialize('a:2:{s:14:"contact_number";s:11:"09584990750";s:7:"user_id";s:2:"32";}');
    }

    public function chargePayment_post()
    {
        $data = $this->param['transID'];
        echo json_encode(captureAmount($data));die;
    }

    public function refundPayment_post()
    {
        $data = $this->param['transID'];
        echo json_encode(refundAuthAmount($data));die;
    }

    public function processPayment_post()
    {
        $data = [
            'customerId' => $this->param['customerId'],
            'amount' => $this->param['amount'],
        ];

        echo json_encode(processPayment($data));die;
    }

    public function testDate_post()
    {
        $data = [
            'accountId' => $this->param['accountId'],
            'amount' => $this->param['amount'],
        ];

        echo json_encode(transferAccount($data));die;
    }

    public function transferToAccount_post()
    {
        $data = [
            'user_id' => $this->param['user_id'],
            'amount' => $this->param['amount'],
        ];
        $this->load->model(['User_model', 'Transfer_model']);
        echo json_encode(transferToAccount($data));die;
    }

    public function SendOtp_post()
    {
        $this->load->library('nexmo');

        $from = 'Patcher';
        $response = $this->nexmo->send_message($from, '918319691365', ["text" => "Patcher SMS check"]);
        // $this->nexmo->d_print($response);
        // echo $this->nexmo->get_http_status();die;
        print_r($response);die;
    }

    public function sendMail_post()
    {
        $args = [
            'message' => 'Hi Mohit',
            'subject' => 'Test Patcher',
            'email_id' => $this->param['email'],
            'template_name' => 'OTP Mail',
            'vars' => [
                [
                    'name' => 'OTP',
                    'content' => '9988',
                ],
            ],

        ];
        $mail = sendMandrillEmailTemlate($args);
        echo json_encode($mail);die;
    }

    public function userDetail_post()
    {
        $this->load->model('User_model');

        $contact = 9584990750;
        $email = 'mkg.stv@gmail.com';

        $data = $this->User_model->where(['email_id' => $email, 'contact_number' => $contact], 'like', '', true, false)->get_all();

        echo $this->db->last_query();die;
        echo json_encode($data);die;
    }

    public function getMechanicsExperience_post()
    {
        $id = $this->param['mechanic_id'];
        $this->load->model(['Mechanic_experience_model', 'Experience_model']);

        $experienceList = $this->Experience_model->where('experience_status', 101)->fields(['experience_id', 'experience_name', 'isOptional', 'isUpload', 'experience_status'])
            ->with_mechanicExp(['fields' => 'mechanic_experience_id,mechanic_id', 'non_exclusive_where' => "mechanic_id = $id"])
            ->get_all();

        $this->response($experienceList);die;
    }

    public function redis_post()
    {
        $redis = createConnectionToRedis();
        $da = $redis->georadius('mechanics', '75.8730544802508', '22.80321316553531', '500', 'km', 'WITHDIST', 'WITHCOORD');

        print_r($da);

        echo $redis->zrem('mechanics', 17);
    }

    public function sendAPNS_post()
    {
        $to = $this->param['to'];
        $device_id = $this->param['device_id'];
        $message = $this->param['message'];

        if ($to == "MECH") {
            $jsonData = ['mechanic_uuid' => "CA69CE13-EC07-47C0-B93C-0AF12986F06B", 'request_id' => '1'];

            $messageTitle = [
                'title' => 'Patcher Request For Emergency',
                'category' => ['notification_type' => 2],
            ];
            $notify = $this->notification->apnsMechanic($device_id, $messageTitle, 100, 2, $jsonData);
        } else {
            $jsonData = ['mechanic_uuid' => "1", 'request_id' => '1'];

            $messageTitle = [
                'title' => 'Patcher Request For Emergency',
                'category' => ['notification_type' => 2],
                'icon' => 'myicon',
                'sound' => 'mySound',
            ];
            $notify = $this->notification->apnsMechanic($device_id, $messageTitle, 100, 2, $jsonData);
        }
        var_dump($notify);die;
    }

    public function createToken_post()
    {
        $stripInclude = stripIncludeFiles();
        \Stripe\Stripe::setApiKey("sk_live_WJWifgTmDSxIKW49dMnJ4OVk");

        $card = \Stripe\Token::create(array(
            "card" => array(
                "number" => "4084084084084084",
                "exp_month" => 12,
                "exp_year" => 2022,
                "cvc" => "076",
            ),
        ));

        echo $card;die;
    }

    public function payout_post()
    {
        $accountId = $this->param['accountId'];
        $amount = $this->param['amount'];
        // echo $amount;die;
        $stripInclude = stripIncludeFiles();
        \Stripe\Stripe::setApiKey("sk_live_WJWifgTmDSxIKW49dMnJ4OVk");
        $transfer = \Stripe\Transfer::create(
            array("amount" => ($amount * 100),
                "currency" => "GBP",
                // "country" => "GB",
                "destination" => $accountId,
            )
        );

        echo $transfer;die;
    }

    public function balance_post()
    {
        $accountId = $this->param['accountId'];
        $amount = $this->param['amount'];

        $stripInclude = stripIncludeFiles();
        \Stripe\Stripe::setApiKey("sk_live_WJWifgTmDSxIKW49dMnJ4OVk");
        $transfer = \Stripe\Balance::retrieve();;

        echo $transfer;die;
    }
}
