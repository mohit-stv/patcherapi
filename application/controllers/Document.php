<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }
    
    public function addDocuments_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addDocuments') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Documents_model']);             
            if(isset($this->param['documents']) && $this->param['documents'] != NUll){
                for($i=0;$i<count($this->post('documents'));$i++){               
                    $documents_array=[
                        'documentName' => $this->param['documents'][$i]['documentName'],
                        'documentStatus' => $this->param['documents'][$i]['documentStatus']
                    ];
                    $insert = $this->Documents_model->insert($documents_array);
                }
            }
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editDocuments_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editDocuments') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Documents_model']);             
            if(isset($this->param['documents']) && $this->param['documents'] != NUll){
                for($i=0;$i<count($this->post('documents'));$i++){               
                    $documents_array=[
                        'documentName' => $this->param['documents'][$i]['documentName'],
                        'documentStatus' => $this->param['documents'][$i]['documentStatus']      
                    ];
                    $update = $this->Documents_model->where('documentId',$this->param['documents'][$i]['documentId'])->update($documents_array);
                }
            }
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function allDocument_post() {
    	
    	$this->load->model(['Document_model']);              	

        $documents   =  $this->Document_model->fields('document_id,document_name')
                                            ->where('document_status','101')->get_all();

        if($documents){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => $documents]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteDocuments_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteDocuments') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
        	$documentId = $this->param['documentId'];

			$updateOptions = array(
                'where' => array('documentId' => $documentId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'documents'
            );
            $deleteDocument = $this->common_model->customUpdate($updateOptions);

	        if($deleteDocument){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteDocument]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}