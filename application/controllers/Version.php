<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Version extends MY_Controller {

	private $param = array();
	public function __construct(){
		parent::__construct();
		$this->param = (array)json_decode(file_get_contents('php://input'), true);

         $currentVersion = "1.1";
         $appUrl = "http://35.176.36.170/patcherAPI/";
	}

   public function appVersion_post(){
        $appVersion = $this->param['appversion'];
        if($appVersion == ""){
            $this->response(["status" => false, "message" => "App Version is mandatory field!!"]);
        }
        if($appVersion == $this->currentVersion){
            $this->response(["status" => true, "message" => "Application is fully updated!!"]);  
        } else {
            $this->response(["status" => false, "message" => "Please Update your application!!","appUrl"=> "$this->appUrl"]);
        }
    }
}
