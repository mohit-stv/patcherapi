<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bank extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model', 'Bank_model', 'Bank_detail_model','Mechanic_model','User_model']);
        $this->load->helper('common');
    }

    public function bankList_post()
    {
        $bankList = $this->Bank_model->get_all();
        $response = [];
        foreach ($bankList as $bankLists) {
            $response[] = [
                'bank_id' => $bankLists['bank_id'],
                'bank_name' => $bankLists['bank_name'],
                'status' => $bankLists['status'],
            ];
        }
        if (!empty($response)) {
            $this->response(['status' => true, 'message' => 'All Banks Here.', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Any Bank Here.', 'response' => $response]);die;
        }
    }

    public function addUpdateBankDetails_post()
    {
        $bankId = 00;

        if (isset($this->param['account_number']) && $this->param['account_number'] != '') {
            $accountNumber = $this->param['account_number'];
        } else {
            $this->response(['status' => false, 'message' => 'Account Number is required.']);
        }

        if (isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
            $where = ['mechanic_id' => $this->param['mechanic_id']];
            $data = [
                'mechanic_id' => $this->param['mechanic_id'],
                'account_number' => $accountNumber,
                'account_holder_name' => $this->param['account_holder_name'],
                'sort_code' => (isset($this->param['sort_code']) ? $this->param['sort_code'] : ''),
                'bank_id' => $bankId,
            ];
            $mechEmail = $this->Mechanic_model->fields('email_id')->get($where);

            $email_id = $mechEmail['email_id'];

            $getResult = $this->Bank_detail_model
                ->get(['mechanic_id' => $this->param['mechanic_id']]);
        }

        if (isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $where = ['user_id' => $this->param['user_id']];
            $data = [
                'user_id' => $this->param['user_id'],
                'account_number' => $accountNumber,
                'account_holder_name' => $this->param['account_holder_name'],
                'sort_code' => (isset($this->param['sort_code']) ? $this->param['sort_code'] : ''),
                'bank_id' => $bankId,
            ];

            $userEmail = $this->User_model->fields('email_id')->get($where);

            if(empty($userEmail))
            $email_id = "no-reply@patcher.co.uk";
            else
            $email_id = $userEmail['email_id'];

            $getResult = $this->Bank_detail_model
                ->get(['user_id' => $this->param['user_id']]);
        }

        if (!empty($getResult)) {
            $data = [
                'account_number' => $this->param['account_number'], 'account_holder_name' => $this->param['account_holder_name'],
                'bank_id' => $this->param['bank_id'],
                'sort_code' => $this->param['sort_code']
            ];

            $update = $this->Bank_detail_model->where($where)->update($data);
            $bank_detail_id = $getResult['bank_detail_id'];
        } else {
            $update = $this->Bank_detail_model->insert($data);
            $bank_detail_id = $update;

            
        }

        if ($update) {

            $data['email_id'] = $email_id;
            $stripe_account_id = $this->stripeAccountId($data);
            $this->Bank_detail_model->where(['bank_detail_id' => $bank_detail_id])->update(['stripe_account_id' => $stripe_account_id]);

            $this->response(['status' => true, 'message' => 'Bank Details Updated Successfully.', 'response' => $data]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something Went Wrong.']);die;
        }
    }

    public function getBankDetails_post()
    {
        if (isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
            $where = ['mechanic_id' => $this->param['mechanic_id']];
        }

        if (isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $where = ['user_id' => $this->param['user_id']];
        }

        $getResult = $this->Bank_detail_model->fields(['bank_detail_id', 'account_number', 'bank_id', 'account_holder_name', 'sort_code', 'status', 'created_at'])->with_bank('fields:bank_name')
            ->where($where)
            ->get();

        if (!empty($getResult)) {
            $response = [
                'bank_detail_id' => $getResult['bank_detail_id'],
                'account_number' => $getResult['account_number'],
                'account_holder_name' => $getResult['account_holder_name'],
                'account_status' => $getResult['status'],
                'bank_name' => $getResult['bank']['bank_name'],
                'sort_code' => $getResult['sort_code'],
                'bank_id' => $getResult['bank_id'],
            ];
            $this->response(['status' => true, 'message' => 'Bank Details Here.', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No bank details avialble.']);die;
        }

    }

    public function stripeAccountId($args)
    {
        stripIncludeFiles();
        
        $account = \Stripe\Account::create(
            array(
                "email" => $args['email_id'],
                "type" => "custom",
                'external_account' => array(
                    "object" => "bank_account",
                    "currency" => "gbp",
                    "country" => "GB",
                    "account_holder_name" => $args['account_holder_name'],
                    "account_holder_type" => 'individual',
                    "routing_number" => $args['sort_code'],
                    "account_number" => $args['account_number'],
                ),
            ));
        return $account->id;
    }
}
