<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Question extends MY_Controller
{

    private $param = array();
    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(array('common_model', 'Question_model', 'Answer_model'));
        $this->load->helper('common');
    }

    public function addQuestion_post()
    {

        $val = $this->param['question'];
        $newAnswerID = '';
        foreach ($val as $key => $vals) {
            if (isset($vals['newAnswer']) && !empty($vals['newAnswer'])) {
                $newAnswer = $vals['newAnswer'];
                foreach ($newAnswer as $newAnswers) {
                    $newAnswer_array = ['answer' => $newAnswers];
                    $insertAnswerID = $this->Answer_model->insert($newAnswer_array);
                    $newAnswerID .= $insertAnswerID . ',';
                }
            }

            $answersID = $vals['answerIds'];
            if ($newAnswerID != '') {
                $allAnswersIDs = $answersID . ',' . rtrim($newAnswerID, ',');
            } else {
                $allAnswersIDs = $vals['answerIds'];
            }

            $questions_array = [
                'question' => $vals['questionname'],
                'answer_ids' => $allAnswersIDs,
                'question_choice' => $vals['question_choice'],
                'question_type' => $vals['question_type'],
                'question_description' => $this->param['question_description'],
            ];

            $insert = $this->Question_model->insert($questions_array);
        }

        if ($insert) {
            $this->response(['status' => true, 'message' => 'Question Added Successfully ', 'response' => $insert]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }

    }

    public function updateQuestion_post()
    {
        $id = $this->param['question_id'];
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run('editQuestion') == false) {
            $message = $this->form_validation->error_array();
            $response = array('status' => false, 'message' => $message);
            $this->response($response, 400);
        } else {
            if (isset($this->param['newAnswer']) && !empty($this->param['newAnswer'])) {
                $newAnswer = $this->param['newAnswer'];
                $newAnswerID = '';
                foreach ($newAnswer as $newAnswers) {
                    $newAnswer_array = ['answer' => $newAnswers];
                    $insertAnswerID = $this->Answer_model->insert($newAnswer_array);
                    $newAnswerID .= $insertAnswerID . ',';
                }
            }

            $answersID = $this->param['answerIds'];
            if ($newAnswerID != '') {
                $allAnswersIDs = $answersID . ',' . rtrim($newAnswerID, ',');
            } else {
                $allAnswersIDs = $this->param['answerIds'];
            }

            $questions_array = [
                'question' => $this->param['question'],
                'answer_ids' => $allAnswersIDs,
                'question_choice' => $this->param['question_choice'],
                'question_type' => $this->param['question_type'],
                'question_description' => $this->param['question_description'],
            ];

            $update = $this->Question_model->where('question_id', $id)->update($questions_array);
            if ($update) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $update]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    public function deleteQuestion_post()
    {
        $id = $this->param['question_id'];
        $deleteQuestion = $this->Question_model->where('question_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteQuestion) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteQuestion]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    ## Get All Near Mechanics
    public function allNearMechanics_post($longitude, $latitude)
    {
        $res = $this->getNearByMechanicOfLongLat_post($longitude, $latitude);
        if ($res) {
            $this->response(['status' => true, 'message' => 'All Mechanics Here.', 'request' => $res]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Emergency Question Tips
    public function emergencyTips_post()
    {
        $this->load->model(['Emergency_tip_model']);
        $getTips = $this->Emergency_tip_model->fields(['emergency_tips_id', 'tips'])->get_All(['status' => 101]);
        if (!empty($getTips)) {
            $this->response(['status' => true, 'message' => 'Emergency Tips Here.', 'response' => $getTips]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    // Get All Emergency Question
    public function emergencyQuestion_post()
    {

        $this->load->model(['question_model', 'Emergency_tip_model', 'Constant_calloutfee_model', 'User_vehicles_model']);
        $getTips = $this->Emergency_tip_model->fields(['emergency_tips_id', 'tips'])->get_All(['status' => 101]);

        $requestType = $this->param['request_type'];
        $getCallout = $this->Constant_calloutfee_model->fields('value')
            ->get(['request_type' => $requestType]);
        $callOut = $getCallout['value'];

        $getQuestion = $this->question_model->fields(['question_id', 'question', 'question_description', 'question_choice',
            'questionIsMultiple'])->get_all(['question_type' => $requestType, 'question_status' => 101]);

        if (isset($this->param['user_id']) && $this->param['user_id'] != "") {
            $user_id = $this->param['user_id'];
        } else {
            $user_id = 0;
        }

        $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault', 'vehicle_picture'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_id' => $user_id, 'isDefault' => 101])->get();

        if ($userVehicle) {
            $isDefault = 101;
            $vehicleDetails = [
                'user_vehicles_id' => $userVehicle['user_vehicles_id'],
                'vehicle_picture' => $userVehicle['vehicle_picture'],
                'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
            ];

            $userVehicleList = ['isDefault' => $isDefault, 'vehicleDetails' => $vehicleDetails];
        } else {
            $isDefault = 100;
            $userVehicleList = ['isDefault' => $isDefault];
        }

        if (!empty($getQuestion)) {
            $this->response(['status' => true, 'message' => 'All Questions Here.',
                'response' => ['question' => $getQuestion, 'tips' => $getTips, 'calloutfee' => $callOut], 'vehicles' => $userVehicleList]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Any Questions']);die;
        }
    }

    ## Delete Images Of Emergency
    public function deleteEmergencyImage_post()
    {
        $this->load->model(['Request_image_model', 'Common_model']);
        $requestImageId = $this->param['request_image_id'];
        $status = $this->param['status'];
        if ($status == 100 && isset($requestImageId) && $requestImageId != '') {
            $update = $this->Request_image_model
                ->where('request_image_id', $requestImageId)
                ->update(['status' => $status]);
            if ($update) {
                $this->response(['status' => true,
                    'message' => 'Image Deleted Successfully.', 'response' => ['status' => 100]]);
            } else {
                $this->response(['status' => false, 'message' => 'Not Deleted.']);
            }
        } else {
            $this->response(['status' => false, 'message' => 'Wrong imageId and status.']);
        }
    }

    ## Upload Image For Emergency
    public function emergencyImage_post()
    {
        $this->load->model(['Request_image_model', 'Common_model']);
        if (isset($this->param['image_url']) && isset($this->param['user_id'])) {
            $user_id = $this->param['user_id'];
            $imageData = $this->common_model->convertBase64ToImage(
                $this->param['image_url']);

            $data = $imageData['data'];
            $mime = $imageData['mime'];
            $image_name = 'uploads/request/' . rand(1000, 9999) . '.' . $mime;
            file_put_contents($image_name, $data);
            chmod($image_name, 0777);
            $requestImage = ROOTPATH . $image_name;

            $data = [
                'user_id' => $user_id,
                'image_url' => $requestImage,
            ];

            if (isset($this->param['request_image_id']) && $this->param['request_image_id'] != '') {
                $updated = $this->Request_image_model
                    ->where(['request_image_id' => $this->param['request_image_id'],
                        'user_id' => $user_id])
                    ->update(['image_url' => $requestImage]);
                $updateImage = $this->param['request_image_id'];
            } else {
                $updateImage = $this->Request_image_model->insert($data);
            }

            if ($updateImage) {
                $getImage = $this->Request_image_model
                    ->fields(['request_image_id', 'image_url'])
                    ->where('request_image_id', $updateImage)->get();

                $this->response(['status' => true,
                    'message' => 'Image Uploaded Successfully ',
                    'response' => $getImage]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Not Uploaded']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Parameter Mismatch.']);
        }
    }

    public function userQuestionAnswer_post()
    {
        $this->load->model(['Mechanic_rating_model', 'User_question_answers_model', 'Request_image_model', 'Request_model', 'Mechanic_model']);

        $questionanswer = $this->param['questionanswer'];
        $imagesId = $this->param['image_id'];
        $userId = $this->param['user_id'];

        $user_vehicles_id = 0; //maintain for previous app version
        if (isset($this->param['user_vehicles_id']) && $this->param['user_vehicles_id'] != "") {
            $user_vehicles_id = $this->param['user_vehicles_id'];
        }

        $recovery = $this->param['recovery'];
        $roadside = $this->param['roadside'];

        $this->db->trans_start();
        $insert = array();
        foreach ($questionanswer as $questionanswers) {
            $questionId = $questionanswers['question_id'];
            $answerId = $questionanswers['answer_id'];
            $questionIsMultiple = $questionanswers['questionIsMultiple'];

            $questionAnswer_array[] = [
                //'request_id' => $this->param['request_id'],
                'user_id' => $this->param['user_id'],
                'question_id' => $questionId,
                'answer_id' => $answerId,
                'questionIsMultiple' => $questionIsMultiple,
                'recovery' => $recovery,
                'roadside' => $roadside,
                'comment' => (isset($this->param['comment']) ? $this->param['comment'] : ''),
            ];
        }
        $userAnswerId = $this->User_question_answers_model->insert($questionAnswer_array);
        $insert['inserted'] = $userAnswerId;
        $request = $this->param['request_type'];
        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];

        $requestrray = ['request_type' => $request,
            'user_id' => $this->param['user_id'],
            'request_date' => date('Y:m:d H:i:s'),
            'request_lat' => $latitude,
            'request_lng' => $longitude,
            'user_vehicles_id' => $user_vehicles_id,
        ];
        $request_id = $this->Request_model->insert($requestrray);

        // Pay Callout Fee
        if (isset($this->param['callout_fee']) && $this->param['callout_fee'] != '') {
            $callOutFee = $this->param['callout_fee'];
            $lastfour_digit = $this->param['lastfour_digit'];
            if (isset($this->param['customer_id']) && $this->param['customer_id'] != '') {
                $customerId = $this->param['customer_id'];
            } else {
                $customerId = '';
            }

            if (isset($this->param['token']) && $this->param['token'] != '') {
                $token = $this->param['token'];
            } else {
                $token = '';
            }
            $updateCallOutFee = $this->Request_model->where('request_id', $request_id)->update(['callout_fee' => $callOutFee]);

            $paymentData = [
                'user_id' => $userId,
                'request_id' => $request_id,
                'amount' => $callOutFee,
                'token' => $token,
                'last_four_digit' => $lastfour_digit,
                'customer_id' => $customerId,
            ];
            $payCallout = newPayCallOutFee($paymentData);
            if (!isset($payCallout['status']) || $payCallout['status'] != 'succeeded') {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $this->response(['status' => false, 'message' => 'Payment failed. Please try again after sometime.', 'response' => $payCallout]);die;
            }
        }
        // EDO Of Callout Fee

        if (!empty($imagesId)) {
            foreach ($imagesId as $imagesIds) {
                $updateRequestImage = $this->Request_image_model
                    ->where(['request_image_id' => $imagesIds, 'user_id' => $this->param['user_id']])
                    ->update(['request_id' => $request_id]);
            }
        }

        $insert['request_id'] = $request_id;
        $update = $this->User_question_answers_model->where('user_question_answer_id', $userAnswerId)->update(['request_id' => $request_id]);
        $nearestMechanics = $this->getNearByMechanicOfLongLat_post($longitude, $latitude);

        // print_r($nearestMechanics);die;
        foreach ($nearestMechanics as $nearestMechanicss) {
            if (isset($nearestMechanicss['mechanicsData']['mechanic_id'])) {
                $nearest[] = [
                    'request_id' => $request_id,
                    'mechanic_id' => $nearestMechanicss['mechanicsData']['mechanic_id'],
                    'status' => 1,
                ];
            }
        }
        $nearMechanics = $nearestMechanics;

        //Send request to mechanic using pubnub
        $req_args = ['request_id' => $request_id,
            'user_id' => $this->param['user_id'],
            'mechanic' => $nearMechanics,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'user_vehicles_id' => $user_vehicles_id,
            'roadside' => $roadside,
            'recovery' => $recovery,
        ];
        $sendMessageToMechanics['req_uuid'] = $this->scheduleRequestToMech_post($req_args);
        $response = array_merge($insert, $sendMessageToMechanics);

        $this->db->trans_complete();
        if ($response) {
            $this->db->trans_commit();
            $this->response(['status' => true, 'message' => 'Your help is on the way.', 'response' => $response]);die;
        } else {
            $this->db->trans_rollback();
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Get NearBy Mechanics
    public function getNearByMechanicOfLongLat_post($longitude, $latitude)
    {
        $this->load->model(['Mechanic_model', 'Mechanic_service_model']);

        $redis = createConnectionToRedis();

        $geolist = $redis->georadius('mechanics', $longitude, $latitude, '500', 'mi', 'WITHDIST', 'WITHCOORD');

        $mechanicsData = array();
        $rating = 0;
        foreach ($geolist as $key => $geolists) {

            $mechanicsID = $geolists[0];

            if (!$this->isAppVerified($mechanicsID)) {
                continue;
            }

            $mechanic_status = $redis->get($mechanicsID);
            if ($mechanic_status != "ONLINE") {
                continue;
            }

            $mechanicsData[$key]['distancevalue'] = $geolists[1];
            $mechanicsData[$key]['longitude'] = $geolists[2][0];
            $mechanicsData[$key]['latitude'] = $geolists[2][1];

            $mechanicsData[$key]['mechanicsData'] = $this->Mechanic_model->where('mechanic_id', $mechanicsID)->fields(['mechanic_id', 'name', 'email_id', 'country_code', 'contact_number', 'mechanic_uuid', 'mechanic_dob', 'mechanic_address', 'mechanic_picture', 'isBlocked'])->get();

            $mechanicsData[$key]['mechanicsData']['distance'] = $mechanicsData[$key]['distancevalue'];
            unset($mechanicsData[$key]['distancevalue']);

            $mechanicsData[$key]['mechanicsData']['longitude'] = $mechanicsData[$key]['longitude'];
            $mechanicsData[$key]['mechanicsData']['latitude'] = $mechanicsData[$key]['latitude'];

            unset($mechanicsData[$key]['longitude']);
            unset($mechanicsData[$key]['latitude']);

            if ($mechanicsData[$key]['mechanicsData']['isBlocked'] == 101) {
                continue;
            }

            $skipMechanic = true;

            //For Mechanic Service
            $getServiceVal = $this->Mechanic_service_model->fields('services_id')->where(['mechanic_id' => $mechanicsID])
                ->with_service('fields:service_name', 'where:parent_id=0')->get_all();

            $serviceNamedata = array();
            if (!empty($getServiceVal)) {
                foreach ($getServiceVal as $getServiceVals) {
                    if ($getServiceVals['services_id'] == 2 || $getServiceVals['services_id'] == 3) {
                        $skipMechanic = false;
                    }
                    $serviceNamedata[] = $getServiceVals['services_id'];
                }
            }

            if ($skipMechanic) {
                unset($mechanicsData[$key]);
                continue;
            }

            $mechanicsData[$key]['serviceNames'] = $serviceNamedata;

            $mechanicsData[$key]['mechanicsData']['services'] = $mechanicsData[$key]['serviceNames'];

            unset($mechanicsData[$key]['serviceNames']);

            //For Mechanic Rating
            // if (!empty($mechanicsID)) {

            $this->load->model('Mechanic_rating_model');
            $getRating = $this->Mechanic_rating_model
                ->where(['mechanic_id' => $mechanicsID, 'from_at' => 1])->get_all();

            if (!empty($getRating['rating'])) {
                foreach ($getRating as $getRatings) {
                    $ratingSum += $getRatings['rating'];
                }
                $rating = $ratingSum / count($getRating);
            }
            // $mechanicsData[$key]['average_rating'] = $rating;
            // }

            $mechanicsData[$key]['mechanicsData']['average_rating'] = $rating;
        }
        return $mechanicsData;
    }

    public function scheduleRequestToMech_post($req_args)
    {

        $this->load->library(array('Curl', 'Mypubnub', 'Notification', 'Googleapi'));
        $this->load->model(['Mechanic_model', 'Request_model', 'User_model', 'User_vehicles_model']);

        $request_id = $req_args['request_id'];
        $user_id = $req_args['user_id'];

        $roadside = $req_args['roadside'];
        $recovery = $req_args['recovery'];

        $req_uuid = $this->mypubnub->uuid();
        $responseStatus = false;
        $totalPubnubSend = 0;
        $channel_uuid = $this->mypubnub->uuid();
        $notifyToMechanic = false;
        $address = $this->googleapi->getAddress($req_args['latitude'], $req_args['longitude']);
        if (!$address) {
            $address = "";
        }

        foreach ($req_args['mechanic'] as $mechanic) {
            if (!isset($mechanic['mechanicsData']['mechanic_id']) || empty($mechanic['mechanicsData']['mechanic_id'])) {
                continue;
            }
            //$mechanic_state = $redis->GET($mechanic[0]);
            $mechanicId = $mechanic['mechanicsData']['mechanic_id'];

            // Check Mechanic Status(Any Accepted, Arrive, Arriving)
            $getMechanicStatus = $this->Request_model
                ->where('request_status', [2, 9, 10])
                ->get_all(['mechanic_id' => $mechanicId]);

            // if(!empty($getMechanicStatus)) {
            $mechanicUuid = $mechanic['mechanicsData']['mechanic_uuid'];
            if ($mechanicUuid != '') {
                $pubnunb_data = array();

                $mechanicDetail = $this->Mechanic_model
                    ->where(['mechanic_id' => $mechanicId, 'isOnline' => 101])
                    ->fields(['name', 'device_key', 'isProduction', 'platform_type'])->get();

                $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'vehicle_number', 'isDefault'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_vehicles_id' => $req_args['user_vehicles_id']])->get();

                $userDetail = $this->User_model->fields(['name', 'uuid', 'user_channel'])->where(['user_id' => $user_id])->get();

                $publisUserData = [
                    'request_id' => $request_id,
                    'User_id' => $user_id,
                    'user_name' => $userDetail['name'],
                    'user_uuid' => $userDetail['uuid'],
                    'req_uuid' => $req_uuid,
                    'request_type' => 2,
                    'mechanics_uuid' => $mechanicUuid,
                    'mechanics_id' => $mechanic['mechanicsData']['mechanic_id'],
                    'vehicle_number' => $userVehicle['vehicle_number'],
                    'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                    'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                    'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                    'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                    'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                    'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
                    'distance' => round($mechanic['mechanicsData']['distance'], 2),
                    'location' => $address,
                    'roadside' => $roadside,
                    'recovery' => $recovery,
                ];

                //R1 -> Request Sent, R2 -> Request Accepted, R3 -> Not Accpeted
                $pubnub_args = ['channel' => $mechanic['mechanicsData']['mechanic_uuid'],
                    'message' => ['type' => 'R1', 'mechanicDetails' => $publisUserData]];

                $publish_status = $this->mypubnub->publish($pubnub_args);

                if (!empty($publish_status)) {

                    $deviceId = $mechanicDetail['device_key'];
                    $platformType = $mechanicDetail['platform_type'];
                    $isProduction = $mechanicDetail['isProduction'];

                    $arrayvalMsg = [
                        'request_id' => $request_id,
                        'User_id' => $user_id,
                        'user_name' => $userDetail['name'],
                        'user_uuid' => $userDetail['uuid'],
                        'req_uuid' => $req_uuid,
                        'vehicle_number' => $userVehicle['vehicle_number'],
                        'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                        'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                        'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                        'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                        'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                        'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
                        'distance' => round($mechanic['mechanicsData']['distance'], 2),
                        'location' => $address,
                        'roadside' => $roadside,
                        'recovery' => $recovery,
                    ];

                    ## Send Notification For Android To Mechanic
                    if ($mechanicDetail['platform_type'] == 1) {
                        $message = [
                            'title' => 'Patcher Request For Emergency',
                            'body' => $arrayvalMsg,
                            'tag' => ['notification_type' => 2, 'request_id' => $request_id],
                        ];
                        $notify = $this->notification->fcmMechanic($deviceId, $message);
                    } else if ($mechanicDetail['platform_type'] == 2) {
                        if (!$notifyToMechanic) {
                            $pubnub_args = ['channel' => $mechanicUuid,
                                'message' => ['type' => 'EmergencyNotificationToMechanic', 'userDetails' => $arrayvalMsg]];

                            $publish_status = $this->mypubnub->publish($pubnub_args);
                            $notifyToMechanic = true;
                        }
                        $jsonData = ['mechanic_uuid' => $mechanicUuid, 'request_id' => $request_id];

                        $messageTitle = [
                            'title' => 'Patcher Request For Emergency',
                            'category' => ['notification_type' => 2, 'request_id' => $request_id],
                        ];
                        $notify = $this->notification->apnsMechanic($deviceId, $messageTitle, $isProduction, $platformType, $jsonData);
                    }
                }

                if (!empty($publish_status)) {
                    ++$totalPubnubSend;
                }
            }
            // }
        }

        // if($totalPubnubSend > 0) {
        return $req_uuid;
        // }
    }

    ## Accept The Request For Emergency Case
    public function mechanicAcceptEmergencyRequest_post()
    {
        $this->load->library(array('Notification', 'Mypubnub'));
        $this->load->model(['Mechanic_rating_model', 'Request_model', 'User_model', 'Mechanic_model', 'Transaction_model']);

        $redis = createConnectionToRedis();

        $notify = "";
        $requestId = $this->param['request_id'];
        $mechanicId = $this->param['mechanic_id'];
        $reqUuid = $this->param['req_uuid'];
        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];
        $mechanic_address = $this->param['mechanic_address'];

        $channel = $this->mypubnub->uuid();

        $getRequestStatus = $this->Request_model->fields('request_status')
            ->with_transaction('fields: transaction_id,transaction_charge_id')
            ->where(['request_id' => $requestId])->get();

        $requestStatus = $getRequestStatus['request_status'];

        if ($requestStatus == 2) {
            $this->response(['status' => false, 'message' => 'Ohh..Another Mechanic Accepted This Request.']);die;
        } elseif ($requestStatus == 6) {
            $this->response(['status' => false, 'message' => 'Completed Request.']);die;
        } elseif ($requestStatus == 4 || $requestStatus == 7) {
            $this->response(['status' => false, 'message' => 'User cancelled this request.']);die;
        } elseif ($requestStatus == 8) {
            $this->response(['status' => false, 'message' => 'Mechanic Completed Request.']);die;
        } elseif ($requestStatus == 5) {
            $this->response(['status' => false, 'message' => 'Mechanic Cancelled Request.']);die;
        } else {
            if (empty($getRequestStatus['transaction']) || $getRequestStatus['transaction']['transaction_charge_id'] == "") {
                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User Transaction Details not available.']);die;
            }
            $requestTransactionId = $getRequestStatus['transaction']['transaction_id'];
            $requestTransactionRef = $getRequestStatus['transaction']['transaction_charge_id'];

            $confirmPayment = captureAmount($requestTransactionRef);
            if ($confirmPayment != null && $confirmPayment->status == 'succeeded') {
                $confirmPaymentData = [
                    'transaction_charge_id' => $confirmPayment->id,
                    'object_type' => $confirmPayment->object,
                    'amount' => ($confirmPayment->amount / 100),
                    'network_status' => $confirmPayment->outcome->network_status,
                    'message' => $confirmPayment->outcome->seller_message,
                    'paid_status' => 2,
                    'source_id' => $confirmPayment->source->id,
                    'transaction_type' => 1, // 1 for callout fee
                    // 'last4'=>$lastFourDigit,
                    'payment_type' => 1, //Which type payment
                    'source_brand' => $confirmPayment->source->brand,
                    'transaction_status' => $confirmPayment->status,
                ];
                $updateTransaction = $this->Transaction_model->where(['transaction_id' => $requestTransactionId])->update($confirmPaymentData);

                if (!$updateTransaction) {
                    $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                }
            } else {
                $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
            }

            // Update request_status is 2 for Accept
            $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => 2, 'mechanic_id' => $mechanicId, 'req_uuid' => $reqUuid]);

            //$redis->zrem('mechanics', $mechanicId);
            $redis->set($mechanicId, 'BUSY');

            $getRequest = $this->Request_model->fields('user_id,request_type')->where(['request_id' => $requestId])->get();
            $userId = $getRequest['user_id'];

            $getUserDetails = $this->User_model
                ->fields('device_key,uuid,platform_type,isProduction')
                ->where(['user_id' => $userId, 'isOnline' => 101])->get();

            $mechanicDetails = $this->Mechanic_model
                ->fields('mechanic_id,name,country_code,contact_number,mechanic_uuid', 'mechanic_picture')
                ->where('mechanic_id', $mechanicId)->get();

            if ($mechanicId != '') {
                $ratingSum = '';

                $getRating = $this->Mechanic_rating_model
                    ->where(['mechanic_id' => $mechanicId, 'from_at' => 1])->get_all();
                if (!empty($getRating)) {
                    foreach ($getRating as $getRatings) {
                        $ratingSum += $getRatings['rating'];
                    }
                    $mechanicAvgRating = $ratingSum / count($getRating);
                }
            }

            $message = [
                'mechanic_id' => $mechanicDetails['mechanic_id'],
                'name' => $mechanicDetails['name'],
                'req_uuid' => $reqUuid,
                'mechanic_uuid' => $mechanicDetails['mechanic_uuid'],
                'country_code' => $mechanicDetails['country_code'],
                'contact_number' => $mechanicDetails['contact_number'],
                'mechanic_image' => (isset($mechanicDetails['mechanic_picture']) ? $mechanicDetails['mechanic_picture'] : ''),
                'longitude' => $longitude,
                'latitude' => $latitude,
                'mechanic_address' => $mechanic_address,
                'rating' => (isset($mechanicAvgRating) ? $mechanicAvgRating : 0),
                'notification_type' => 6, //mechanic accept the emergency request
                'request_id' => $requestId,
                'request_type' => $getRequest['request_type'],
            ];

            $device_id = $getUserDetails['device_key'];

            // Send Notification Andriod User
            if ($getUserDetails['platform_type'] == 1) {
                $notificationMessage = [
                    'message' => 'Accepted Your Emergency Request',
                    'data' => $message,
                ];

                $notify = $this->notification->fcmUser($device_id, $notificationMessage);
                // print_r($notify);
            } elseif ($getUserDetails['platform_type'] == 2) {
                $platform_type = $getUserDetails['platform_type'];
                $isProduction = $getUserDetails['isProduction'];

                $pubnub_args = ['channel' => $getUserDetails['uuid'],
                    'message' => ['type' => 'EmergencyNotificationToUser', 'mechanicDetails' => $message, 'longitude' => (string) $longitude, 'latitude' => (string) $latitude]];

                $publish_status = $this->mypubnub->publish($pubnub_args);

                /*$bodyData = ['request_id'=>$requestId,
                'acceptMechanic'=>$mechanicDetails['mechanic_uuid']];*/
                $bodyData = ['request_id' => $requestId,
                    'acceptMechanic' => $mechanicDetails['mechanic_uuid']];

                $messageTitle = [
                    'title' => 'Accepted Your Emergency Request',
                    'category' => [
                        'notification_type' => 6, 'request_id' => $requestId,
                        'request_type' => $getRequest['request_type'],
                    ],
                ];

                $notify = $this->notification->apnsUser($device_id, $messageTitle, $isProduction, $platform_type, $bodyData);
                // $decodeNotify = json_decode($notify);
            }

            $result = ['request_id' => $requestId];
            // print_r($result);die;
            if ($updateStatus) {
                $this->response(['status' => true, 'message' => 'Successfully Accepted.', 'response' => $result]);
            } else {
                $this->response(['status' => false, 'message' => 'Something Went wrong']);
            }
        }
    }

    ## Retry For Emergency Case
    public function retryRequest_post()
    {
        $this->load->model(['Request_model', 'User_question_answers_model']);
        $requestId = $this->param['request_id'];
        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];
        $userId = $this->param['user_id'];

        $statusUpdate = $this->Request_model->where(['request_id' => $requestId])->update(['request_status' => 1]);

        $requestDetails = $this->Request_model->fields('user_vehicles_id')->with_userQuestionAnswer('fields: recovery,roadside')->get(['request_id' => $requestId]);

        $nearestMechanics = $this->getNearByMechanicOfLongLat_post($longitude, $latitude);

        if ($nearestMechanics) {
            foreach ($nearestMechanics as $nearestMechanicss) {
                $nearest[] = [
                    'request_id' => $requestId,
                    'mechanic_id' => $nearestMechanicss['mechanicsData']['mechanic_id'],
                    'status' => 1,
                ];
            }
            $nearMechanics = $nearestMechanics;

            //Send request to mechanic using pubnub
            $req_args = ['request_id' => $requestId,
                'user_id' => $userId,
                'mechanic' => $nearMechanics,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'user_vehicles_id' => $requestDetails['user_vehicles_id'],
                'roadside' => $requestDetails['userQuestionAnswer']['roadside'],
                'recovery' => $requestDetails['userQuestionAnswer']['recovery'],
            ];
            $response['req_uuid'] = $this->scheduleRequestToMech_post($req_args);
            //$response = array_merge($insert, $sendMessageToMechanics);

            if ($response) {
                $this->response(['status' => true, 'message' => 'Your help is on the way.', 'response' => $response]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }

        } else {
            $this->response(['status' => false, 'message' => 'No mechanic is available at your location.']);die;
        }
    }

    ## Cancel Request By User
    public function cancelEmergencyRequest_post()
    {
        $this->load->library('Notification');
        $this->load->model(['Request_model', 'User_model', 'Mechanic_model', 'Transaction_model']);
        $requestId = $this->param['request_id'];
        $userId = $this->param['user_id'];
        $status = $this->param['status']; // 4 for cancel and 7 for auto

        $updateStatus = "";

        // Check Initilisation Status
        $checkStatusByUser = $this->Request_model
            ->with_mechanic('fields:mechanic_id,name,mechanic_address,contact_number,mechanic_uuid,platform_type,device_key,isProduction', 'where:isOnline=101')
            ->with_transaction(['fields' => 'transaction_id,source_id,transaction_charge_id,customer_id,amount', 'where' => ['transaction_type' => 1]])
            ->fields(['request_type', 'request_status', 'mechanic_id'])->get(['request_id' => $requestId]);

        if (!empty($checkStatusByUser)) {

            if ($checkStatusByUser['request_status'] == 4 || $checkStatusByUser['request_status'] == 7) {
                $this->response(['status' => true, 'message' => 'Request hasbeen already cancelled.']);die;
            }

            if ($status == 4) {
                if ($checkStatusByUser['request_status'] == 1) {

                    if (empty($checkStatusByUser['transaction']) || $checkStatusByUser['transaction']['transaction_charge_id'] == "") {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User Transaction Details not available.']);die;
                    }
                    $requestTransactionId = $checkStatusByUser['transaction']['transaction_id'];
                    $requestTransactionRef = $checkStatusByUser['transaction']['transaction_charge_id'];

                    $refundPayment = refundAuthAmount($requestTransactionRef);
                    if ($refundPayment != null && $refundPayment->status == 'succeeded') {
                        $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                        $refundPaymentData = [
                            'message' => "Payment Refunded.",
                            'request_id' => $requestId,
                            'user_id' => $userId,
                            'transaction_reference' => $transaction_reference,
                            'transaction_charge_id' => $refundPayment->id,
                            'object_type' => $refundPayment->object,
                            'amount' => ($refundPayment->amount / 100),
                            'paid_status' => 3, //Refund Amount
                            'transaction_type' => 1, // 1 for callout fee
                            'payment_type' => 1, //Which type payment
                            'transaction_status' => $refundPayment->status,
                            'transaction_type' => 1,
                            'payment_type' => 1,
                            'amount' => $checkStatusByUser['transaction']['amount'],
                            'customer_id' => $checkStatusByUser['transaction']['customer_id'],
                            'source_id' => $checkStatusByUser['transaction']['source_id'],
                        ];
                        $updateTransaction = $this->Transaction_model->insert($refundPaymentData);

                        if (!$updateTransaction) {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                        }
                    } else {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                    }

                    $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => $status]);

                    $this->response(['status' => true, 'message' => 'Request Cancel Successfully.', 'response' => ['request_status' => $status, 'isRefund' => true]]);
                } else {
                    if ($checkStatusByUser['request_status'] == 2 || $checkStatusByUser['request_status'] == 5) {
                        $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => $status]);

                        $isRefund = false;

                        $message = [
                            'request_id' => $requestId,
                            'isRefund' => $isRefund,
                        ];

                        if (!empty($checkStatusByUser['mechanic'])) {
                            $redis = createConnectionToRedis();
                            $redis->set($checkStatusByUser['mechanic']['mechanic_id'], 'ONLINE');
                            $device_id = $checkStatusByUser['mechanic']['device_key'];

                            // Send Notification Andriod User
                            if ($checkStatusByUser['mechanic']['platform_type'] == 1) {
                                $notificationMessage = [
                                    'title' => 'Request Cancel By User',
                                    'body' => $message,
                                    'tag' => ['notification_type' => 7, 'request_id' => $requestId], //Cancel By User
                                ];

                                $notify = $this->notification->fcmMechanic($device_id, $notificationMessage);
                                //$decodeNotify = json_decode($notify);
                            } elseif ($checkStatusByUser['mechanic']['platform_type'] == 2) {
                                $bodyData = ['request_id' => $requestId,
                                    'isRefund' => $isRefund];
                                $messageTitle = [
                                    'title' => 'Request Cancel By User',
                                    'category' => ['notification_type' => 7, 'request_id' => $requestId], //Cancel By User
                                ];
                                $notify = $this->notification->apnsMechanic($checkStatusByUser['mechanic']['device_key'], $messageTitle, $checkStatusByUser['mechanic']['isProduction'], $checkStatusByUser['mechanic']['platform_type'], $bodyData);
                                // $decodeNotify = json_decode($notify);
                            }
                        }

                        $this->response(['status' => true, 'message' => 'Request Cancel Successfully.', 'response' => ['request_status' => $status]]);
                    }
                }
            } elseif ($status == 7) { // For Auto
                if ($checkStatusByUser['request_status'] == 1) {

                    if (empty($checkStatusByUser['transaction']) || $checkStatusByUser['transaction']['transaction_charge_id'] == "") {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User Transaction Details not available.']);die;
                    }

                    $requestTransactionId = $checkStatusByUser['transaction']['transaction_id'];
                    $requestTransactionRef = $checkStatusByUser['transaction']['transaction_charge_id'];

                    $refundPayment = refundAuthAmount($requestTransactionRef);
                    if ($refundPayment != null && $refundPayment->status == 'succeeded') {
                        $transaction_reference = "P" . $requestId . "RF" . mt_rand(1111, 9999);

                        $refundPaymentData = [
                            'message' => "Payment Refunded.",
                            'request_id' => $requestId,
                            'user_id' => $userId,
                            'transaction_reference' => $transaction_reference,
                            'transaction_charge_id' => $refundPayment->id,
                            'object_type' => $refundPayment->object,
                            'amount' => ($refundPayment->amount / 100),
                            'paid_status' => 3, //Refund Amount
                            'transaction_type' => 1, // 1 for callout fee
                            'payment_type' => 1, //Which type payment
                            'transaction_status' => $refundPayment->status,
                            'transaction_type' => 1,
                            'payment_type' => 1,
                            'amount' => $checkStatusByUser['transaction']['amount'],
                            'customer_id' => $checkStatusByUser['transaction']['customer_id'],
                            'source_id' => $checkStatusByUser['transaction']['source_id'],
                        ];
                        $updateTransaction = $this->Transaction_model->insert($refundPaymentData);

                        if (!$updateTransaction) {
                            $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'User payment transaction update fails.']);
                        }
                    } else {
                        $this->response(['status' => false, 'message' => 'Process failed.', 'details' => 'Unable to verify user transaction.']);
                    }

                    $updateStatus = $this->Request_model->where('request_id', $requestId)->update(['request_status' => $status]);
                    $this->response(['status' => true, 'message' => 'Updated Successfully.', 'response' => ['request_status' => $status]]);
                } else {
                    if ($checkStatusByUser['request_status'] == 2) {
                        $mechanicId = $checkStatusByUser['mechanic']['mechanic_id'];
                        $redis = createConnectionToRedis();
                        $specificMechanicLatLong = $redis->geopos('mechanics', $mechanicId);
                        $mechanicLong = $specificMechanicLatLong[0][0];
                        $mechanicLat = $specificMechanicLatLong[0][1];

                        $getRequestStatus = $this->Request_model->fields('request_status')->where('request_id', $requestId)
                            ->get();

                        $response = ['mechanic_name' => $checkStatusByUser['mechanic']['name'],
                            'mechanic_address' => $checkStatusByUser['mechanic']['mechanic_address'],
                            'contact_number' => $checkStatusByUser['mechanic']['contact_number'],
                            'mechanic_uuid' => $checkStatusByUser['mechanic']['mechanic_uuid'],
                            'latitude' => $mechanicLat,
                            'longitude' => $mechanicLong,
                            'request_status' => $getRequestStatus['request_status'],
                        ];
                        $this->response(['status' => true, 'message' => 'Mechanic Details', 'response' => $response]);die;
                    } else {
                        $this->response(['status' => false, 'message' => 'Not Updated Successfully.']);die;
                    }
                }
            }

            if ($updateStatus != '') {
                $this->response(['status' => true, 'message' => 'Request cancelled.', 'response' => ['request_status' => $status]]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Not Updated Successfully.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Invalid Journey Details.']);die;
        }
    }

    ## Emergency Accepted Details For User with Mechanic lat long
    public function emergencyAcceptedDetails_post()
    {
        $this->load->model(['Request_model', 'User_model', 'Mechanic_model']);
        $requestId = $this->param['request_id'];

        $getMechanicDetails = $this->Request_model->with_mechanic('fields:name,mechanic_address,country_code,contact_number,mechanic_uuid,platform_type,device_key')->where(['request_id' => $requestId, 'request_status' => 2])->get();
        if (!empty($getMechanicDetails)) {
            $mechanicId = $getMechanicDetails['mechanic_id'];
            $redis = createConnectionToRedis();

            $specificMechanicLatLong = $redis->geopos('mechanics', $mechanicId);
            $mechanicLong = $specificMechanicLatLong[0][0];
            $mechanicLat = $specificMechanicLatLong[0][1];

            $response = ['request_id' => $getMechanicDetails['request_id'],
                'mechanic_name' => $getMechanicDetails['mechanic']['name'],
                'mechanic_address' => $getMechanicDetails['mechanic_address'],
                'country_code' => $getMechanicDetails['country_code'],
                'contact_number' => $getMechanicDetails['contact_number'],
                'mechanic_uuid' => $getMechanicDetails['mechanic_uuid'],
                'latitude' => $mechanicLat,
                'longitude' => $mechanicLong,
            ];
        }
        if (!empty($response)) {
            $this->response(['status' => true, 'message' => 'Driver Found.', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Driver Found']);die;
        }
    }

    ## Get New latitude longitude using pubnub
    public function getNewLatLongUsingPubnub_post()
    {
        $this->load->library(array('Curl', 'Mypubnub', 'Notification'));

        $mechanicUuid = '24549494-5407-4021-BAF1-0AA9564C3DAB';

        $url = "https://ps.pndsn.com/subscribe/sub-c-b7c6d702-a1d1-11e7-8215-f6f5a6cf9223/$mechanicUuid/0/10";

        $reqTimeout = 35;
        $sub_data = $this->curl->_pubsubscribe($url, $reqTimeout);
        echo '<pre>';
        print_r($sub_data);
        die;

        if ($sub_data['curlStatus'] != '') {
            $data = $sub_data['curlData'][0];
            //$response = ['mechanic_uuid'=>$data[0]->mechanic_uuid];
            if (!empty($data[0]->type)) {
                echo $data[0]->type;
                $mechanicDetails = $data[0]->mechanicDetails;
                $response = ['mechanic_uuid' => $mechanicDetails->mechanics_uuid,
                ];
            } else {
                $response = ['mechanic_uuid' => $data[0]->mechanic_uuid];
            }
        }
        /*echo '<pre>';
    print_r($response);*/
    }

    private function isAppVerified($id)
    {
        $this->load->model('Mechanic_registrations_model');
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        if ($isAppVerified) {
            $mechanicsData = $this->Mechanic_model
                ->fields('mechanic_id, name, email_id, country_code,contact_number, mechanic_dob,hourly_rate,hourly_rate,mechanic_address, mechanic_picture,mechanic_uuid, isOnline, isBlocked, mechanic_status')
                ->where(['isBlocked' => 100, 'mechanic_id' => $id])->get();
            if ($mechanicsData) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
            }
        }

        return $isAppVerified;
    }
}
