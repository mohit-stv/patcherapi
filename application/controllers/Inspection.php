<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspection extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

     public function deleteVehicles_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteVehicles') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {        
        	$userId = $this->param['userId'];
        	$vehicleId = $this->param['vehicleId'];        	

			$updateOptions = array(
                'where' => array('vehicleId' => $vehicleId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'vehicles'
            );
            $deleteVehicle = $this->common_model->customUpdate($updateOptions);

	        if($deleteVehicle){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteVehicle]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }


    ## Delete Images Each Inspection
    public function deleteInspectionImage_post() {
        $this->load->model(['Inspection_image_model','Inspection_model','common_model']);
        $imageId = $this->param['image_id'];
        
        $updateOptions = array(
                'where' => array('inspection_image_id' => $imageId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'inspection_images'
            );
        $deleteImage = $this->common_model->customUpdate($updateOptions);

        if($deleteImage) {
            $this->response(['status' => true, 'message'=> 'Images deleted successfully!!','response' => $imageId]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }


    ## Add Images For Based On Each Inspection
    public function addImagesEachInspection_post() {
        $this->load->model(['Inspection_image_model','Inspection_model','common_model']); 
       /* if(isset($this->param['inspection']) && !empty($this->param['inspection'])) {
            $inspection = $this->param['inspection'];
            for($i=0;$i<count($inspection);$i++) {
                $data[] = [
                    'inspection_id'=>$inspection[$i]['inspection_id'],
                    'image_url'=>$inspection[$i]['images']
                ];
            }
        }*/

        if(isset($this->param['inspection_id']) && !empty($this->param['images'])) {
            $request_id = $this->param['request_id'];
            $imageData = $this->common_model->convertBase64ToImage($this->param['images']);
            $data = $imageData['data'];
            $mime = $imageData['mime'];
            $image_name ='assets/inspection/'.rand(1000,9999) . '.'.$mime;
            file_put_contents($image_name, $data);
            chmod($image_name, 0777);
            $inpectionImage = ROOTPATH.$image_name;

            $data = [
                'inspection_id'=>$this->param['inspection_id'],
                'image_url'=>$inpectionImage,
                'request_id'=>$request_id
            ];
            $insert = $this->Inspection_image_model->insert($data);
            if($insert)
            {
               $getImage = $this->Inspection_image_model->fields(['inspection_image_id','image_url','inspection_id'])->where('inspection_image_id', $insert)->get();
               $this->response(['status' => true, 'message'=> 'Image Added Successfully ','response' => $getImage]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }        
    }


    ## Add Mechanics Inspection
    public function addMechanicsInspection_post() {
        $this->load->model(['Mechanic_inspection_model','Inspection_image_model','Request_model','Inspection_model','common_model']); 
       
            $requestId = $this->param['request_id'];
            $inspection = $this->param['inspection'];
            if(isset($this->param['inspection']) && !empty($this->param['inspection'])) {
                for($i=0;$i<count($inspection);$i++) {

                    $getValue = $this->Mechanic_inspection_model->get([
                        'inspection_id'=>$this->param['inspection'][$i]['inspection_id'],
                        'request_id'=>$requestId
                        ]);
                    if(!empty($getValue)) {
                        $responseUpdateData[] = [
                            'request_id'=>$requestId,
                            'inspection_id'=>$this->param['inspection'][$i]['inspection_id'],
                            'status'=>$this->param['inspection'][$i]['status'],
                            'inspection_comment'=>$this->param['inspection_comment']
                        ];
                        $updateData = [
                            'status'=>$this->param['inspection'][$i]['status'],
                            'inspection_comment'=>$this->param['inspection_comment']
                        ];
                        $updateWhere = [
                            'request_id'=>$requestId,
                            'inspection_id'=>$this->param['inspection'][$i]['inspection_id']
                        ];
                        $update[] = $this->Mechanic_inspection_model->where($updateWhere)->update($updateData);
                    } else {
                        $inspections_array[] = [
                            'request_id'=>$requestId,
                            'inspection_comment'=>$this->param['inspection_comment'],
                            'inspection_id'=>$this->param['inspection'][$i]['inspection_id'],
                            'status'=>$this->param['inspection'][$i]['status'],
                        ];
                    }
                }

                $detailsUser = $this->Request_model->with_user('fields: platform_type,isProduction,device_key,uuid')->get(['request_id' => $requestId]);
                $deviceKey = $detailsUser['user']['device_key'];
                $isProduction = $detailsUser['user']['isProduction'];
                $platFormType = $detailsUser['user']['platform_type'];
                $uuid = $detailsUser['user']['uuid'];
                $request_type = $detailsUser['request_type'];

                if(!empty($update)) {
                    $notify = $this->_sendNotification($platFormType,$deviceKey,$isProduction,$uuid,$requestId,$request_type);
                    $this->response(['status' => true, 'message'=> "Thank you for doing the pre-inspection!",'response' => $responseUpdateData]);die;
                }
                if(!empty($inspections_array)) {
                    $insert = $this->Mechanic_inspection_model->insert($inspections_array);
                    if($insert)
                    {
                        $notify = $this->_sendNotification($platFormType,$deviceKey,$isProduction,$uuid,$requestId,$request_type);
                       $this->response(['status' => true, 'message'=> "Thank you for doing the pre-inspection!",'response' => $insert]);die;
                    }else{
                       $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                    }
                }   
            }
    }

    #Get Inspection
    public function getMechanicsInspection_post() {
        $this->load->model(['Mechanic_inspection_model','User_inspection_model','Inspection_image_model','Inspection_model','Request_model','Request_image_model','Common_model']); 
       
        if(isset($this->param['request_id']) && !empty($this->param['request_id'])) {
            $id = $this->param['request_id'];
            $getInspection = $this->Inspection_model
            ->fields(['inspection_id','inspection_name','inspection_status'])
            ->get_all();
            $getUserInspection = $this->User_inspection_model->fields('status')->get(['request_id'=>$id]);
    
            $getComment = $this->Mechanic_inspection_model->fields(['inspection_comment'])->get(['request_id'=>$id]);

            $requestImages = $this->Request_image_model->fields(['image_url'])->get_all(['request_id'=>$id]);
            if(empty($requestImages)){
                $requestImages = (object) [];
            }
         
           // $inspectionData = [];
            if(!empty($getInspection)) {
                foreach($getInspection as $key=>$getInspections) {
                    $id = $this->param['request_id'];
                    $inspectionId = $getInspections['inspection_id'];
                    $inspectionName = $getInspections['inspection_name'];

                    $getInspectionImage = $this->Inspection_image_model
                    ->fields(['inspection_image_id','inspection_id','inspection_status','image_url'])
                    ->where(['inspection_id'=>$inspectionId, 'request_id'=>$id])->get_all();
                
                    $mechanicInspection = $this->Mechanic_inspection_model
                    ->fields('status')->where(['inspection_id'=>$inspectionId, 'request_id'=>$id])->get();
                
                    /*$getMechanicInspection = $this->Mechanic_inspection_model->
                    with_inspectionImage('fields:inspection_image_id,image_url')
                    ->fields(['inspection_id','status','inspection_comment'])
                    ->where(['inspection_id'=>$inspectionId, 'request_id'=>$id])
                    ->get();
                    echo '<pre>';
                    print_r($getMechanicInspection);*/
                
                // $explodeUrl = explode(',',$getMechanicInspection['image_url']);
                    
                /* $imageData = [];
                    if(!empty($getMechanicInspection['inspectionImage'])) {
                        foreach($getMechanicInspection['inspectionImage'] as $inspectionImages) {
                            $imageData[] = [
                                            'inspection_image_id'=>$inspectionImages['inspection_image_id'],
                                            'image_url'=>$inspectionImages['image_url']
                                        ];
                        }
                    } 
                    
                    $inspectionData['inspection_data'][$key] = [
                                'inspection_id'=>$inspectionId,
                                'inspection_name'=>$inspectionName,
                                //'image_url'=>($getMechanicInspection['image_url'] ? $explodeUrl : ''),
                                'mechanic_inspection_status'=>($getMechanicInspection['status'] ? $getMechanicInspection['status'] : '100'),
                                'inspection_status'=>$getInspections['inspection_status'],
                                //'inspection_images'=>$imageData
                                //'isUpload'=>$getInspections['isUpload']
                        ];*/

                        $inspectionData['inspection_data'][] = [    
                            'inspection_id'=>$inspectionId,
                            'inspection_name'=>$inspectionName,
                            //'image_url'=>($getMechanicInspection['image_url'] ? $explodeUrl : ''),
                            'mechanic_inspection_status'=>(!empty($mechanicInspection['status']) ? $mechanicInspection['status'] : '100'),
                            'inspection_status'=>$getInspections['inspection_status'],
                            'inspection_images'=>(!empty($getInspectionImage) ? $getInspectionImage : [])
                            //'isUpload'=>$getInspections['isUpload']
                        ];
                }
            }
           
           /* $inspectionData = ['inspection_comment'=>$getComment['inspection_comment'],
                                'request_id'=>$id,
                                'user_inspection'=>(isset($getUserInspection['status']) ? $getUserInspection['status'] : '')
                            ];*/
            $inspectionData['inspection_comment'] = (isset($getComment['inspection_comment']) ? $getComment['inspection_comment'] : '');
            $inspectionData['request_id'] = $id;
            $inspectionData['user_inspection'] = (isset($getUserInspection['status']) ? $getUserInspection['status'] : '');
            $inspectionData['requestImages'] = $requestImages;

            if(!empty($inspectionData)) {
                $this->response(['status' => true, 'message'=> 'Get inspection based on request','response' => $inspectionData]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'You have no inspection at the moment']);die;
        }

    }

    # User Accept Or Reject The Pre-Inspection
    public function inspectionAcceptReject_post() {
    	$this->load->model(['User_inspection_model']); 

    	$data = [
		    	'request_id'=>$this->param['request_id'], 
		    	'user_id'=>$this->param['user_id'], 
                'status'=>$this->param['status'],
                'comment' =>$this->param['comment'],
		    	];

    	$insert = $this->User_inspection_model->insert($data);
    	if($insert) {
    		$this->response(['status' => true, 'message'=> "Thank you for doing the pre-inspection!",'response' => $insert]);die;
    	} else {
    		$this->response(['status' => false, 'message' => 'Something went wrong']);die;
    	}
    }

    private function _sendNotification($platFormType,$device_id,$isProduction,$uuid,$requestId,$request_type){
        $this->load->library(['Notification','Mypubnub']);

        $message = ['notification_type' => 10,'request_id' => $requestId,'request_type' => $request_type];
        if($platFormType == 1) {
            $notificationMessage =[
                'message'	=> 'Your mechanic has just updated the inspection. Check it out by clicking here!',
                'data' => $message
                ];
        
            $notify = $this->notification->fcmUser($device_id, $notificationMessage);
           // print_r($notify);
        } elseif($platFormType == 2) {
            $pubnub_args = ['channel' => $uuid,
            'message'=> ['type'=>10,'meesage' => $message,'request_id' => $requestId,'request_type' => $request_type]]; 
           
            $publish_status = $this->mypubnub->publish($pubnub_args);
            
            $bodyData = [
                'message'	=> 'Your mechanic has just updated the inspection. Check it out by clicking here!',
                'data' => $message
                ];
            
            $messageTitle =[
                'title'	=> 'Your mechanic has just updated the inspection. Check it out by clicking here!',
                'category'=>['notification_type' => 10,'request_id' => $requestId]
               ];

            $notify = $this->notification->apnsUser($device_id, $messageTitle, $isProduction, $platFormType, $bodyData);
        }

        return $notify;
    }
}