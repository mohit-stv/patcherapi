<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VehiclesInfo extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    #For add user vehicle
    public function addVehicleBasedOnUser_post()
    {
        $this->load->model('User_vehicles_model');
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('userVehicles') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {

            // Set primary vehicle
            $isDefault = 100;
            $getUserVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault'])->get_all(['user_id' => $this->param['user_id']]);
            if (!empty($getUserVehicle)) {
                $status = [];
                foreach ($getUserVehicle as $key => $getUserVehicles) {
                    $status[] = $getUserVehicles['isDefault'];
                }

                if (!in_array('101', $status)) {
                    $firstVehicleId = $getUserVehicle[0]['user_vehicles_id'];
                    $updateData = ['isDefault' => 101];
                    // Update first vehicle for primary
                    $update = $this->User_vehicles_model->update($updateData, $firstVehicleId);
                }
            } else {
                $isDefault = 101;
            }
            // End of Set primary vehicle

            $img = $this->param['vehicle_picture'];
            /*$image_name = "uploads/vehicles/".rand(1000,5000).time().".png";
            $image = $this->common_model->getImageBase64Code($vehiclePicture);
            file_put_contents($image_name,$image);
            chmod($image_name, 0777);*/

            if (filter_var($img, FILTER_VALIDATE_URL)) {
                $image_name = $img;
            } else {
                $imageData = $this->common_model->convertBase64ToImage($img);
                $data = $imageData['data'];
                $mime = $imageData['mime'];

                $image_name = 'uploads/vehicles/' . rand(1000, 5000) . '.' . $mime;
                file_put_contents($image_name, $data);
                chmod($image_name, 0777);
            }

            $vehicle_number = '';

            if (isset($this->param['vehicle_number']) && !empty($this->param['vehicle_number'])) {
                $vehicle_number = $this->param['vehicle_number'];
            }

            $userVehiclesArray = [
                'user_id' => $this->param['user_id'],
                'vehicle_make_id' => $this->param['vehicle_make_name'],
                'vehicle_model_id' => $this->param['vehicle_modelname'],
                'vehicle_bodytype_id' => $this->param['vehicle_bodytype_name'],
                'vehicle_year_id' => $this->param['vehicle_years'],
                // 'vehicle_weight_id' => $this->param['vehicle_weight'],
                'vehicle_engine_id' => $this->param['vehicle_engine'],
                'vehicle_drivetype_id' => $this->param['vehicle_drivetype_name'],
                'vehicle_picture' => ROOTPATH . $image_name,
                'isDefault' => $isDefault,
                'vehicle_number' => $vehicle_number,
            ];

            $userVehicleModelID = $this->User_vehicles_model->insert($userVehiclesArray);
            if ($userVehicleModelID) {
                $vehicleDetails = array();
                $userVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'isDefault', 'vehicle_number', 'vehicle_picture'])->with_make('fields:vehicle_make_name')->with_model('fields:vehicle_modelname')->with_body('fields:vehicle_bodytypes_name')->with_year('fields:vehicle_years')->with_weight('fields:vehicle_weight')->with_engine('fields:vehicle_engine')->with_drivetype('fields:vehicle_drivetype_name')->where(['user_vehicles_id' => $userVehicleModelID])->get();

                if ($userVehicle) {
                    $isDefault = 101;
                    $vehicleDetails = [
                        'user_vehicles_id' => $userVehicle['user_vehicles_id'],
                        'vehicle_picture' => $userVehicle['vehicle_picture'],
                        'vehicle_number' => ($userVehicle['vehicle_number'] != '' ? $userVehicle['vehicle_number'] : ''),
                        'year' => ($userVehicle['year']['vehicle_years'] != '' ? $userVehicle['year']['vehicle_years'] : ''),
                        'make' => ($userVehicle['make']['vehicle_make_name'] != '' ? $userVehicle['make']['vehicle_make_name'] : ''),
                        'model' => ($userVehicle['model']['vehicle_modelname'] != '' ? $userVehicle['model']['vehicle_modelname'] : ''),
                        'body' => ($userVehicle['body']['vehicle_bodytypes_name'] != '' ? $userVehicle['body']['vehicle_bodytypes_name'] : ''),
                        'engine_type' => ($userVehicle['engine']['vehicle_engine'] != '' ? $userVehicle['engine']['vehicle_engine'] : ''),
                        'drive_type' => ($userVehicle['drivetype']['vehicle_drivetype_name'] != '' ? $userVehicle['drivetype']['vehicle_drivetype_name'] : ''),
                    ];
                } else {
                    $isDefault = 100;
                    $vehicleDetails = array();
                }

                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $userVehicleModelID, 'vehicleDetails' => $vehicleDetails]);die;

            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For Get All User Vehicle
    public function getAllUserVehicles_post()
    {
        $userId = $this->param['userId'];
        $this->load->model(['User_vehicles_model', 'Vehicle_make_model', 'Vehicle_models_model', 'Vehicle_bodytypes_model', 'Vehicle_year_model', 'Vehicle_weight_model', 'Vehicle_engines_model', 'Vehicle_drivetypes_model']);

        // Set primary vehicle
        $getUserVehicle = $this->User_vehicles_model->fields(['user_vehicles_id', 'vehicle_number', 'isDefault', 'isDeleted'])->get_all(['user_id' => $userId, 'isDeleted' => 101]);
        if (!empty($getUserVehicle)) {
            $status = [];
            foreach ($getUserVehicle as $key => $getUserVehicles) {
                $status[] = $getUserVehicles['isDefault'];
            }

            if (!in_array('101', $status)) {
                $firstVehicleId = $getUserVehicle[0]['user_vehicles_id'];
                $updateData = ['isDefault' => 101];
                // Update first vehicle for primary
                $update = $this->User_vehicles_model->update($updateData, $firstVehicleId);
            }
        }

        // End of Set primary vehicle
        $getUserVehicle = $this->User_vehicles_model->fields('*')->where(['user_id' => $userId, 'isDeleted' => 101])->get_all();

        if (!empty($getUserVehicle)) {
            $vehicleDetails = array();
            foreach ($getUserVehicle as $key => $getUserVehicles) {
                $vehicleDetails[$key]['user_vehicles_id'] = $getUserVehicles['user_vehicles_id'];
                $vehicleDetails[$key]['vehicle_picture'] = $getUserVehicles['vehicle_picture'];
                $vehicleDetails[$key]['vehicle_number'] = $getUserVehicles['vehicle_number'];

                $makeId = $getUserVehicles['vehicle_make_id'];
                $getMake = $this->Vehicle_make_model->fields(['vehicle_make_name'])->where(['vehicle_make_id' => $makeId])->get();
                $vehicleDetails[$key]['vehicle_make_name'] = ($getMake['vehicle_make_name'] != null) ? $getMake['vehicle_make_name'] : ''
                ;

                $modelId = $getUserVehicles['vehicle_model_id'];
                $getModel = $this->Vehicle_models_model->fields(['     vehicle_modelname'])->where(['vehicle_model_id' => $modelId])->get();
                $vehicleDetails[$key]['vehicle_model_name'] = ($getModel['vehicle_modelname'] != null) ? $getModel['vehicle_modelname'] : ''
                ;

                $bodyId = $getUserVehicles['vehicle_bodytype_id'];
                $getBody = $this->Vehicle_bodytypes_model->fields(['vehicle_bodytypes_name'])->where(['vehicle_bodytypes_id' => $bodyId])->get();
                $vehicleDetails[$key]['vehicle_bodytypes_name'] = ($getBody['vehicle_bodytypes_name'] != null) ? $getBody['vehicle_bodytypes_name'] : '';

                $yearId = $getUserVehicles['vehicle_year_id'];
                $getYear = $this->Vehicle_year_model->fields(['vehicle_years'])->where(['vehicle_years_id' => $yearId])->get();
                $vehicleDetails[$key]['vehicle_years_name'] = ($getYear['vehicle_years'] != null) ? $getYear['vehicle_years'] : ''
                ;

                $weightId = $getUserVehicles['vehicle_weight_id'];
                $getWeight = $this->Vehicle_weight_model->fields(['vehicle_weight'])->where(['vehicle_weight_id' => $weightId])->get();
                $vehicleDetails[$key]['vehicle_weight_name'] = ($getWeight['vehicle_weight'] != null) ? $getWeight['vehicle_weight'] : '';

                $engineId = $getUserVehicles['vehicle_engine_id'];
                $getEngine = $this->Vehicle_engines_model->fields(['vehicle_engine'])->where(['vehicle_engines_id' => $engineId])->get();
                $vehicleDetails[$key]['vehicle_engine_name'] = ($getEngine['vehicle_engine'] != null) ? $getEngine['vehicle_engine'] : ''
                ;

                $driveId = $getUserVehicles['vehicle_drivetype_id'];
                $getDrive = $this->Vehicle_drivetypes_model->fields(['vehicle_drivetype_name'])->where(['vehicle_drivetypes_id' => $driveId])->get();
                $vehicleDetails[$key]['vehicle_drivetype_name'] = ($getDrive['vehicle_drivetype_name'] != null) ? $getDrive['vehicle_drivetype_name'] : '';
            }
        }

        if (!empty($vehicleDetails)) {
            $this->response(['status' => true, 'message' => 'All User Vehicle Here', 'response' => $vehicleDetails]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Vehicles']);die;
        }
    }

    ## User Default Set Vehicle
    public function primaryVehicleSet_post()
    {
        $this->load->model('User_vehicles_model');
        $vehicle = $this->param['vehicle'];
        $userId = $this->param['user_id'];
        $updateData = [];
        foreach ($vehicle as $vehicles) {
            $updateData[] = ['user_vehicles_id' => $vehicles['vehicle_id'],
                'isDefault' => $vehicles['status']];
        }
        $result = $this->User_vehicles_model->update($updateData, 'user_vehicles_id');
        if ($result) {
            $this->response(['status' => true, 'message' => 'Primary Vehicle Update Successfully.', 'response' => $updateData]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Vehicles']);die;
        }
    }

    #For edit user vehicle
    public function editUserVehicle_post()
    {
        $userId = $this->param['user_id'];
        $this->load->model(['User_vehicles_model', 'Vehicle_make_model', 'Vehicle_models_model', 'Vehicle_bodytypes_model', 'Vehicle_year_model', 'Vehicle_weight_model', 'Vehicle_engines_model', 'Vehicle_drivetypes_model']);

        $getUserVehicle = $this->User_vehicles_model->fields('*')->where(['user_id' => $userId, 'isDeleted' => 101])->get_all();

        if (!empty($getUserVehicle)) {
            $vehicleDetails = array();

            foreach ($getUserVehicle as $key => $getUserVehicles) {
                $vehicleDetails[$key]['user_vehicles_id'] = $getUserVehicles['user_vehicles_id'];
                $vehicleDetails[$key]['isDefault'] = $getUserVehicles['isDefault'];
                $vehicleDetails[$key]['vehicle_picture'] = $getUserVehicles['vehicle_picture'];
                $vehicleDetails[$key]['vehicle_number'] = $getUserVehicles['vehicle_number'];

                $makeId = $getUserVehicles['vehicle_make_id'];
                $getMake = $this->Vehicle_make_model->fields(['vehicle_make_name', 'vehicle_make_id'])->where(['vehicle_make_id' => $makeId])->get();
                $vehicleDetails[$key]['make'] = [
                    'make_id' => $getMake['vehicle_make_id'],
                    'make_name' => ($getMake['vehicle_make_name'] != null) ? $getMake['vehicle_make_name'] : '',
                ];

                $modelId = $getUserVehicles['vehicle_model_id'];
                $getModel = $this->Vehicle_models_model->fields(['     vehicle_modelname', 'vehicle_model_id'])->where(['vehicle_model_id' => $modelId])->get();
                $vehicleDetails[$key]['model'] = [
                    'model_id' => $getModel['vehicle_model_id'],
                    'model_name' => ($getModel['vehicle_modelname'] != null) ? $getModel['vehicle_modelname'] : '',
                ];

                $bodyId = $getUserVehicles['vehicle_bodytype_id'];
                $getBody = $this->Vehicle_bodytypes_model->fields(['vehicle_bodytypes_id', 'vehicle_bodytypes_name'])->where(['vehicle_bodytypes_id' => $bodyId])->get();
                $vehicleDetails[$key]['body_type'] = [
                    'body_id' => $getBody['vehicle_bodytypes_id'],
                    'body_name' => ($getBody['vehicle_bodytypes_name'] != null) ? $getBody['vehicle_bodytypes_name'] : ''];

                $yearId = $getUserVehicles['vehicle_year_id'];
                $getYear = $this->Vehicle_year_model->fields(['vehicle_years_id', 'vehicle_years'])->where(['vehicle_years_id' => $yearId])->get();
                $vehicleDetails[$key]['year'] = [
                    'years_id' => $getYear['vehicle_years_id'],
                    'year' => ($getYear['vehicle_years'] != null) ? $getYear['vehicle_years'] : '',
                ];

                $weightId = $getUserVehicles['vehicle_weight_id'];
                $getWeight = $this->Vehicle_weight_model->fields(['vehicle_weight_id', 'vehicle_weight'])->where(['vehicle_weight_id' => $weightId])->get();
                $vehicleDetails[$key]['weight'] = [
                    'weight_id' => $getWeight['vehicle_weight_id'],
                    'weight' => $getWeight['vehicle_weight'],
                ];

                $engineId = $getUserVehicles['vehicle_engine_id'];
                $getEngine = $this->Vehicle_engines_model->fields(['vehicle_engines_id', 'vehicle_engine'])->where(['vehicle_engines_id' => $engineId])->get();
                $vehicleDetails[$key]['engine'] = [
                    'engines_id' => $getEngine['vehicle_engines_id'],
                    'engine_name' => ($getEngine['vehicle_engine'] != null) ? $getEngine['vehicle_engine'] : '',
                ];

                $driveId = $getUserVehicles['vehicle_drivetype_id'];
                $getDrive = $this->Vehicle_drivetypes_model->fields(['vehicle_drivetypes_id', 'vehicle_drivetype_name'])->where(['vehicle_drivetypes_id' => $driveId])->get();
                $vehicleDetails[$key]['drive_type'] = [
                    'drive_id' => $getDrive['vehicle_drivetypes_id'],
                    'drive_name' => ($getDrive['vehicle_drivetype_name'] != null) ? $getDrive['vehicle_drivetype_name'] : '',
                ];
            }
        }

        if (!empty($vehicleDetails)) {
            $this->response(['status' => true, 'message' => 'All User Vehicle Here', 'response' => $vehicleDetails]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Vehicles']);die;
        }
    }

    #Set Default  Vehicle of User
    public function setUserDefaultVehicle_post()
    {

        $this->load->model(['User_vehicles_model']);
        $userId = $this->param['user_id'];
        $user_vehicles_id = $this->param['user_vehicles_id'];

        $this->db->trans_start();

        $this->User_vehicles_model->where(['user_id' => $userId])->update(['isDefault' => 100]);
        $this->User_vehicles_model->where(['user_vehicles_id' => $user_vehicles_id])->update(['isDefault' => 101]);

        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $this->response(['status' => true, 'message' => 'Vehicle set as default.']);
        } else {
            $this->db->trans_rollback();
            $this->response(['status' => false, 'message' => 'Error in process.']);
        }
    }

    #For update user vehicle
    public function updateUserVehicle_post()
    {
        $userId = $this->param['user_id'];
        $vehicleId = $this->param['vehicle_id'];

        $this->load->model(['User_vehicles_model']);
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('userVehicles') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {

            $vehicle_number = '';

            if (isset($this->param['vehicle_number']) && !empty($this->param['vehicle_number'])) {
                $vehicle_number = $this->param['vehicle_number'];
            }
           
            if (isset($this->param['vehicle_picture']) && $this->param['vehicle_picture'] != '') {
                $vehiclePicture = $this->param['vehicle_picture'];

                $strposition = strpos($vehiclePicture, 'http://');
                if ($strposition === false) {
                    $imageData = $this->common_model->convertBase64ToImage($vehiclePicture);
                    $data = $imageData['data'];
                    $mime = $imageData['mime'];

                    $image_name = 'uploads/vehicles/' . rand(1000, 5000) . '.' . $mime;
                    file_put_contents($image_name, $data);
                    chmod($image_name, 0777);

                    $userVehiclesArray = [
                        'vehicle_make_id' => $this->param['vehicle_make_name'],
                        'vehicle_model_id' => $this->param['vehicle_modelname'],
                        'vehicle_bodytype_id' => $this->param['vehicle_bodytype_name'],
                        'vehicle_year_id' => $this->param['vehicle_years'],
                        // 'vehicle_weight_id' => $this->param['vehicle_weight'],
                        'vehicle_engine_id' => $this->param['vehicle_engine'],
                        'vehicle_drivetype_id' => $this->param['vehicle_drivetype_name'],
                        'vehicle_picture' => ROOTPATH . $image_name,
                        'vehicle_number' => $vehicle_number,
                    ];
                } else {
                    $userVehiclesArray = [
                        'vehicle_make_id' => $this->param['vehicle_make_name'],
                        'vehicle_model_id' => $this->param['vehicle_modelname'],
                        'vehicle_bodytype_id' => $this->param['vehicle_bodytype_name'],
                        'vehicle_year_id' => $this->param['vehicle_years'],
                        // 'vehicle_weight_id' => $this->param['vehicle_weight'],
                        'vehicle_engine_id' => $this->param['vehicle_engine'],
                        'vehicle_drivetype_id' => $this->param['vehicle_drivetype_name'],
                        'vehicle_number' => $vehicle_number,
                    ];
                }

            } else {
                $userVehiclesArray = [
                    'vehicle_make_id' => $this->param['vehicle_make_name'],
                    'vehicle_model_id' => $this->param['vehicle_modelname'],
                    'vehicle_bodytype_id' => $this->param['vehicle_bodytype_name'],
                    'vehicle_year_id' => $this->param['vehicle_years'],
                    // 'vehicle_weight_id' => $this->param['vehicle_weight'],
                    'vehicle_engine_id' => $this->param['vehicle_engine'],
                    'vehicle_drivetype_id' => $this->param['vehicle_drivetype_name'],
                    'vehicle_number' => $vehicle_number,
                ];
            }

            $updateUserVehicle = $this->User_vehicles_model->where(['user_id' => $userId, 'user_vehicles_id' => $vehicleId])->update($userVehiclesArray);

            if ($updateUserVehicle) {
                $this->response(['status' => true, 'message' => 'Vehicle details updated successfully.', 'response' => $userVehiclesArray]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    ## Delete User Vehicle
    public function deleteUserVehicle_post()
    {
        $this->load->model(['User_vehicles_model']);
        $userVehicleId = $this->param['user_vehicles_id'];
        $status = $this->param['status'];

        if ($status == 100 && isset($userVehicleId) && $userVehicleId != '') {

            $update = $this->User_vehicles_model
                ->where('user_vehicles_id', $userVehicleId)
                ->update(['isDeleted' => $status]);
            if ($update) {

                #Get User Id from user_vehicles_id to avoid new application builds
                #Update it in future

                $user = $this->User_vehicles_model->fields('user_id')
                    ->where('user_vehicles_id', $userVehicleId)->get();

                $userVehicleStatus = $this->User_vehicles_model->where(['user_id' => $user['user_id'], 'isDeleted' => 101])->get_all();

                if (!empty($userVehicleStatus)) {
                    $isVehicles = count($userVehicleStatus);
                } else {
                    $isVehicles = 0;
                }
                $response = [
                    'isVehicles' => $isVehicles,
                ];
                $this->response(['status' => true,
                    'message' => 'Vehicle deleted successfully', 'response' => $response]);
            } else {
                $this->response(['status' => false, 'message' => 'Not Deleted.']);
            }
        } else {
            $this->response(['status' => false, 'message' => 'Wrong vehicleId and status.']);
        }
    }

    #For add vehicle make
    public function addVehicleMake_post()
    {
        $this->load->model(array('Vehicle_make_model', 'vehicle_models_model'));
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleMake') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehicleName = $this->param['vehicle_make_name'];
            $vehiclesArray = ['vehicle_make_name' => $vehicleName];
            $vehicleModelID = $this->Vehicle_make_model->insert($vehiclesArray);

            if ($vehicleModelID) {
                $vehiclesModelArray = ['vehicle_make_id' => $vehicleModelID, 'vehicle_modelname' => $this->param['vehicle_modelname']];
                $vehicleModelID = $this->vehicle_models_model->insert($vehiclesModelArray);

                if ($vehicleModelID) {
                    $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleModelID]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                }
            }
        }
    }

    #For update vehicle make
    public function updateVehicleMake_post()
    {
        $id = $this->param['vehicle_make_id'];
        $this->load->model(array('Vehicle_make_model', 'vehicle_models_model'));
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleMake') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesArray = ['vehicle_make_name' => $this->param['vehicle_make_name']];
            $vehicleModelID = $this->Vehicle_make_model->where('vehicle_make_id', $id)->update(['vehicle_make_name' => $this->param['vehicle_make_name']]);

            if ($vehicleModelID) {

                $vehicleModelID = $this->vehicle_models_model->where('vehicle_make_id', $vehicleModelID)->update(['vehicle_modelname' => $this->param['vehicle_modelname']]);

                if ($vehicleModelID) {
                    $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $vehicleModelID]);die;
                } else {
                    $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                }
            }
        }
    }

    #For get all vehicle make
    public function getVehicleMake_post()
    {
        $this->load->model('Vehicle_make_model');
        $allVehiclemake = $this->Vehicle_make_model->fields('*')->get_all();
        $response = [];
        foreach ($allVehiclemake as $allVehiclemakes) {
            $response[] = ['make_id' => $allVehiclemakes['vehicle_make_id'],
                'make_name' => $allVehiclemakes['vehicle_make_name']];
        }
        if (!empty($response)) {
            $this->response(['status' => true, 'message' => 'All vehicles here', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete vehicle make
    public function deleteVehicleMake_post()
    {
        $this->load->model('Vehicle_make_model');
        $id = $this->param['vehicle_make_id'];
        $deleteVehicleMake = $this->Vehicle_make_model->where('vehicle_make_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleMake) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleMake]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    #For get all vehicle model
    public function getAllVehiclemodel_post()
    {
        $id = $this->param['vehicle_make_id'];
        $this->load->model('vehicle_models_model');
        $allVehiclesModel = $this->vehicle_models_model->fields('*')->get_all(['vehicle_make_id' => $id]);

        if (!empty($allVehiclesModel)) {
            $response = [];
            foreach ($allVehiclesModel as $allVehiclesModels) {
                $response[] = ['make_id' => $id,
                    'model_id' => $allVehiclesModels['vehicle_model_id'],
                    'model_name' => $allVehiclesModels['vehicle_modelname']];
            }
        }

        if ($response) {
            $this->response(['status' => true, 'message' => 'All vehicles here', 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For add Vehicle Body Type
    public function addVehiclesBodyType_post()
    {
        $this->load->model('vehicle_bodytypes_model');
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleBodyType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesBodyTypeArray = ['vehicle_bodytype_name' => $this->param['vehicle_bodytype_name']];
            $vehicleBodyID = $this->vehicle_bodytypes_model->insert($vehiclesBodyTypeArray);
            if ($vehicleBodyID) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleBodyID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For update Vehicle Body Type
    public function updateVehiclesBodyType_post($id)
    {
        $id = $this->param['vehicle_body_id'];
        $this->load->model('vehicle_bodytypes_model');
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleBodyType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $updateVehicleBodyID = $this->vehicle_bodytypes_model->where('vehicle_bodytypes_id', $id)->update(['vehicle_bodytypes_name' => $this->param['vehicle_bodytype_name']]);
            if ($updateVehicleBodyID) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $updateVehicleBodyID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For Android
    # For Get all Vehicle (Body Type, Year Type, Weight Type, Engine Type, Drive Type)
    public function getAllTypesDetails_get()
    {
        $this->load->model(['vehicle_bodytypes_model', 'Vehicle_year_model', 'Vehicle_weight_model', 'Vehicle_engines_model', 'Vehicle_drivetypes_model']);
        $body_type = $this->vehicle_bodytypes_model->fields(['vehicle_bodytypes_id', 'vehicle_bodytypes_name'])->get_all();

        foreach ($body_type as $body_types) {
            $body[] = [
                'body_id' => $body_types['vehicle_bodytypes_id'],
                'body_name' => $body_types['vehicle_bodytypes_name'],
            ];
        }
        $allVehicles['body_type'] = $body;

        $year_type = $this->Vehicle_year_model->fields(['vehicle_years_id', 'vehicle_years'])->get_all();

        foreach ($year_type as $year_types) {
            $year[] = [
                'year_id' => $year_types['vehicle_years_id'],
                'year' => $year_types['vehicle_years'],
            ];
        }
        $allVehicles['year'] = $year;

        $weight_type = $this->Vehicle_weight_model->fields(['vehicle_weight_id', 'vehicle_weight'])->get_all();

        foreach ($weight_type as $weight_types) {
            $weight[] = [
                'weight_id' => $weight_types['vehicle_weight_id'],
                'weight' => $weight_types['vehicle_weight'],
            ];
        }
        $allVehicles['weight'] = $weight;

        $engine_type = $this->Vehicle_engines_model->fields(['vehicle_engines_id', 'vehicle_engine'])->get_all();

        foreach ($engine_type as $engine_types) {
            $engine[] = [
                'engines_id' => $engine_types['vehicle_engines_id'],
                'engine_name' => $engine_types['vehicle_engine'],
            ];
        }
        $allVehicles['engine'] = $engine;

        $drive_type = $this->Vehicle_drivetypes_model->fields(['vehicle_drivetypes_id', 'vehicle_drivetype_name'])->get_all();

        foreach ($drive_type as $drive_types) {
            $drive[] = [
                'drive_id' => $drive_types['vehicle_drivetypes_id'],
                'drive_name' => $drive_types['vehicle_drivetype_name'],
            ];
        }
        $allVehicles['drive'] = $drive;

        if ($allVehicles) {
            $this->response(['status' => true, 'message' => 'Get All Vehicles Type ', 'response' => $allVehicles]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For IOS
    # For Get all Vehicle (Body Type, Year Type, Weight Type, Engine Type, Drive Type)
    public function getAllTypesDetails_post()
    {
        $this->load->model(['vehicle_bodytypes_model', 'Vehicle_year_model', 'Vehicle_weight_model', 'Vehicle_engines_model', 'Vehicle_drivetypes_model']);
        $body_type = $this->vehicle_bodytypes_model->fields(['vehicle_bodytypes_id', 'vehicle_bodytypes_name'])->get_all();

        foreach ($body_type as $body_types) {
            $body[] = [
                'body_id' => $body_types['vehicle_bodytypes_id'],
                'body_name' => $body_types['vehicle_bodytypes_name'],
            ];
        }
        $allVehicles['body_type'] = $body;

        $year_type = $this->Vehicle_year_model->fields(['vehicle_years_id', 'vehicle_years'])->get_all();

        foreach ($year_type as $year_types) {
            $year[] = [
                'year_id' => $year_types['vehicle_years_id'],
                'year' => $year_types['vehicle_years'],
            ];
        }
        $allVehicles['year'] = $year;

        $weight_type = $this->Vehicle_weight_model->fields(['vehicle_weight_id', 'vehicle_weight'])->get_all();

        foreach ($weight_type as $weight_types) {
            $weight[] = [
                'weight_id' => $weight_types['vehicle_weight_id'],
                'weight' => $weight_types['vehicle_weight'],
            ];
        }
        $allVehicles['weight'] = $weight;

        $engine_type = $this->Vehicle_engines_model->fields(['vehicle_engines_id', 'vehicle_engine'])->get_all();

        foreach ($engine_type as $engine_types) {
            $engine[] = [
                'engines_id' => $engine_types['vehicle_engines_id'],
                'engine_name' => $engine_types['vehicle_engine'],
            ];
        }
        $allVehicles['engine'] = $engine;

        $drive_type = $this->Vehicle_drivetypes_model->fields(['vehicle_drivetypes_id', 'vehicle_drivetype_name'])->get_all();

        foreach ($drive_type as $drive_types) {
            $drive[] = [
                'drive_id' => $drive_types['vehicle_drivetypes_id'],
                'drive_name' => $drive_types['vehicle_drivetype_name'],
            ];
        }
        $allVehicles['drive'] = $drive;

        if ($allVehicles) {
            $this->response(['status' => true, 'message' => 'Get All Vehicles Type ', 'response' => $allVehicles]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For get all Vehicle Body Type based on vehicle make ID
    public function getAllVehicleBodyType_post()
    {
        $this->load->model('vehicle_bodytypes_model');
        $allVehiclesBodyType = $this->vehicle_bodytypes_model->fields('*')->get_all();
        if ($allVehiclesBodyType) {
            $this->response(['status' => true, 'message' => 'All vehicles BodyType here', 'response' => $allVehiclesBodyType]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete the vehicle body
    public function deleteVehicleBody_post()
    {
        $this->load->model('vehicle_bodytypes_model');
        $id = $this->param['vehicle_bodytypes_id'];
        $deleteVehicleBody = $this->vehicle_bodytypes_model->where('vehicle_bodytypes_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleBody) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleBody]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    #For get all vehicle model based on vehicle make id
    public function getVehicleModel_post()
    {
        $this->load->model('vehicle_models_model');
        $id = $this->param['vehicle_make_id'];
        $allVehiclesmodel = $this->vehicle_models_model->fields('*')->where('vehicle_make_id', $id)->get();
        if ($allVehiclesmodel) {
            $this->response(['status' => true, 'message' => 'All vehicles here', 'response' => $allVehiclesmodel]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For add vehicle year type
    public function addVehiclesYearType_post()
    {
        $this->load->model('Vehicle_year_model');
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleYearType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesYearArray = ['vehicle_years' => $this->param['vehicle_years']];
            $vehicleYearID = $this->Vehicle_year_model->insert($vehiclesYearArray);
            if ($vehicleYearID) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleYearID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }

    }

    #For update vehicle year type
    public function updateVehiclesYearType_post()
    {
        $id = $this->param['vehicle_year_id'];
        $this->load->model('Vehicle_year_model');
        $this->form_validation->set_data($this->param);

        if ($this->form_validation->run('vehicleYearType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $updateVehicleYearID = $this->Vehicle_year_model->where('vehicle_years_id', $id)->update(['vehicle_years' => $this->param['vehicle_years']]);
            if ($updateVehicleYearID) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $updateVehicleYearID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For get all vehicle year type
    public function getAllVehicleYearType_post()
    {
        $this->load->model('Vehicle_year_model');
        $allVehiclesYearType = $this->Vehicle_year_model->fields('*')->get_all();
        if ($allVehiclesYearType) {
            $this->response(['status' => true, 'message' => 'All Vehicle year', 'response' => $allVehiclesYearType]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete vehicle year type
    public function deleteVehicleYearType_post()
    {
        $this->load->model('Vehicle_year_model');
        $id = $this->param['vehicle_years_id'];
        $deleteVehicleYear = $this->vehicle_bodytypes_model->where('vehicle_years_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleYear) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleYear]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    #For add vehicle weight type
    public function addVehiclesWeightType_post()
    {
        $this->load->model('Vehicle_weight_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleWeightType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesWeightArray = ['vehicle_weight' => $this->param['vehicle_weight']];
            $vehicleWeightID = $this->Vehicle_weight_model->insert($vehiclesWeightArray);

            if ($vehicleWeightID) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleWeightID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For update vehicle weight type
    public function updateVehiclesWeightType_post()
    {
        $id = $this->param['vehicle_weight_id'];
        $this->load->model('Vehicle_weight_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleWeightType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $updateVehicleWeightID = $this->Vehicle_weight_model->where('vehicle_weight_id', $id)->update(['vehicle_weight' => $this->param['vehicle_weight']]);
            if ($updateVehicleWeightID) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $updateVehicleWeightID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For get all vehicle weight type
    public function getAllVehicleWeightType_post()
    {
        $this->load->model('Vehicle_weight_model');
        $allVehiclesWeightType = $this->Vehicle_weight_model->fields('*')->get_all();
        if ($allVehiclesWeightType) {
            $this->response(['status' => true, 'message' => 'All Vehicle Weight Type', 'response' => $allVehiclesWeightType]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete vehicle weight type
    public function deleteVehicleWeightType_post()
    {
        $this->load->model('Vehicle_weight_model');
        $id = $this->param['vehicle_weight_id'];
        $deleteVehicleWeightType = $this->Vehicle_weight_model->where('vehicle_weight_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleWeightType) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleWeightType]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    #For add vehicle engine type
    public function addVehiclesEngineType_post()
    {
        $this->load->model('Vehicle_engines_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleEngineType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesEngineArray = ['vehicle_engine' => $this->param['vehicle_engine']];
            $vehicleEngineID = $this->Vehicle_engines_model->insert($vehiclesEngineArray);

            if ($vehicleEngineID) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleEngineID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For update vehicle engine type
    public function updateVehiclesEngineType_post()
    {
        $id = $this->param['vehicle_engine_id'];
        $this->load->model('Vehicle_engines_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleEngineType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $updateVehicleEngineID = $this->Vehicle_engines_model->where('vehicle_engines_id', $id)->update(['vehicle_engine' => $this->param['vehicle_engine']]);
            if ($updateVehicleEngineID) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $updateVehicleEngineID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For get all vehicle engine type
    public function getAllVehicleEngineType_post()
    {
        $this->load->model('Vehicle_engines_model');
        $allVehiclesEngineType = $this->Vehicle_engines_model->fields('*')->get_all();
        if ($allVehiclesEngineType) {
            $this->response(['status' => true, 'message' => 'All Vehicle Engine Type', 'response' => $allVehiclesEngineType]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete vehicle engine type
    public function deleteVehicleEngineType_post()
    {
        $this->load->model('Vehicle_engines_model');
        $id = $this->param['vehicle_engines_id'];
        $deleteVehicleEngineType = $this->Vehicle_engines_model->where('vehicle_engines_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleEngineType) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleEngineType]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

    #For add vehicle drive type
    public function addVehiclesDriveType_post()
    {
        $this->load->model('Vehicle_drivetypes_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleDriveType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $vehiclesDriveTypeArray = ['vehicle_drivetype_name' => $this->param['vehicle_drivetype_name']];
            $vehicleDriveTypeID = $this->Vehicle_drivetypes_model->insert($vehiclesDriveTypeArray);

            if ($vehicleDriveTypeID) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'response' => $vehicleDriveTypeID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For update vehicle drive type
    public function updateVehiclesDriveType_post()
    {
        $id = $this->param['vehicle_driver_id'];
        $this->load->model('Vehicle_drivetypes_model');
        $this->form_validation->set_data($this->param);
        if ($this->form_validation->run('vehicleDriveType') == false) {
            $message = $this->form_validation->get_errors_as_array();
            $response = array('status' => false, 'message' => $message);
            echo json_encode($response);
        } else {
            $updateVehicleDriveTypeID = $this->Vehicle_drivetypes_model->where('vehicle_drivetypes_id', $id)->update(['vehicle_drivetype_name' => $this->param['vehicle_drivetype_name']]);
            if ($updateVehicleDriveTypeID) {
                $this->response(['status' => true, 'message' => 'Updated Successfully ', 'response' => $updateVehicleDriveTypeID]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    #For get all vehicle drive type
    public function getAllVehicleDriveType_post()
    {
        $this->load->model('Vehicle_drivetypes_model');
        $allVehiclesDriveType = $this->Vehicle_drivetypes_model->fields('*')->get_all();
        if ($allVehiclesDriveType) {
            $this->response(['status' => true, 'message' => 'All Vehicle Drive Type', 'response' => $allVehiclesDriveType]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    #For delete vehicle drive type
    public function deleteVehicleDriveType_post()
    {
        $this->load->model('Vehicle_drivetypes_model');
        $id = $this->param['vehicle_drivetypes_id'];
        $deleteVehicleDriveType = $this->Vehicle_drivetypes_model->where('vehicle_drivetypes_id', $id)->update(['deleted_at' => date('Y-m-d h-m-s')]);

        if ($deleteVehicleDriveType) {
            $this->response(['status' => true, 'message' => 'Delete Successfully', 'response' => $deleteVehicleDriveType]);
        } else {
            $this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }

}
