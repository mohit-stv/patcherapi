<?php
defined('BASEPATH') or exit('No direct script access allowed');
//include $_SERVER['DOCUMENT_ROOT'] . '/patcherAPI/application/third_party/predis/autoload.php';

//Predis\Autoloader::register();
//use Predis\Command\CommandInterface;
//use GeospatialGeoRadius\Command\CommandInterface;

class Request extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
        $this->load->helper('common');
    }

    public function addRequest_post()
    {
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run('addRequests') == false) {
            $message = $this->form_validation->error_array();
            $response = array('status' => false, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Requests_model', 'User_question_answers_model', 'Request_issues_model']);
            $requestType = $this->param['requestType'];
            $requestLocation = isset($this->param['requestLocation']) ? $this->param['requestLocation'] : '';
            $requestPostcode = isset($this->param['requestPostcode']) ? $this->param['requestPostcode'] : '';
            if ($requestType == 1) {
                $zip = trim($requestLocation) . ", " . trim($requestPostcode);
                $val = $this->common_model->getLnt($zip);
                $lat = isset($val['lat']) ? $val['lat'] : '';
                $lng = isset($val['lng']) ? $val['lng'] : '';
            } else {
                $lat = $this->param['lat'];
                $lng = $this->param['lng'];
            }
            $request_array = [
                'vehicleId' => $this->param['vehicleId'],
                'requestType' => $this->param['requestType'],
                'userId' => $this->param['userId'],
                'requestDate' => $this->param['requestDate'],
                'requestTime' => $this->param['requestTime'],
                'requestLocation' => $requestLocation,
                'requestPostcode' => $requestPostcode,
                'requestLat' => $lat,
                'requestLong' => $lng,
            ];
            $requestId = $this->Requests_model->insert($request_array);
            $requestId = 1;
            if ($requestType == 1) {
                echo "Hellllllooooo";exit;
                if (isset($this->param['questionAnswer']) && $this->param['questionAnswer'] != null) {
                    for ($i = 0; $i < count($this->param['questionAnswer']); $i++) {
                        $queAnsArray = [
                            "requestId" => $requestId,
                            "questionId" => $this->param['questionAnswer'][$i]['questionId'],
                            "answerIds" => implode(",", $this->param['questionAnswer'][$i]['answerIds']),
                        ];
                        $this->User_question_answers_model->insert($queAnsArray);
                    }
                }
            } else {
                if (isset($this->param['issues']) && $this->param['issues'] != null) {
                    for ($i = 0; $i < count($this->param['issues']); $i++) {
                        $issuesArray = [
                            "requestId" => $requestId,
                            "issueId" => $this->param['issues'][$i],
                        ];
                        $this->Request_issues_model->insert($issuesArray);
                    }
                }
            }

            if ($requestId) {
                $this->response(['status' => true, 'message' => 'Update Successful ', 'request' => $requestId]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }

    public function requestDetails_post()
    {
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run('requestDetails') == false) {
            $message = $this->form_validation->error_array();
            $response = array('status' => false, 'message' => $message);
            $this->response($response, 400);
        } else {

        }
    }

    ## Send Notification To Mechanics
    public function sendNotificationToMechanics_post()
    {
        $this->load->model(array('Send_notification_model'));

        $mechanic_id = $this->param['mechanic_id'];
        $message = $this->param['notification_message'];
        foreach ($mechanic_id as $mechanic_ids) {
            $data = ['user_id' => $this->param['user_id'],
                'mechanic_id' => $mechanic_ids,
                'notification_message' => $message,
                'notification_status' => 1,
                'flag' => 0];
            $insert[] = $this->Send_notification_model->insert($data);
        }

        if ($insert) {
            $this->response(['status' => true, 'message' => 'Update Successful.', 'request' => $insert]);die;
        } else {
            $this->response(['status' => true, 'message' => 'Something went wrong', 'request' => $insert]);die;
        }
    }

    ## Check the notifiction
    public function checkNotification_post()
    {
        $this->load->model('Send_notification_model');
        $mechanic_id = $this->param['mechanic_id'];
        $user_id = $this->param['user_id'];
        $updateFlag = $this->Send_notification_model->where(['user_id' => $user_id, 'mechanic_id' => $mechanic_id])->update(['flag' => 1]);
        if ($updateFlag) {
            $this->response(['status' => true, 'message' => 'Updated Flag Successfully.', 'request' => $updateFlag]);die;
        } else {
            $this->response(['status' => true, 'message' => 'Something went wrong']);die;
        }
    }

    ## Mechanics Accept the Request
    public function acceptTheMechanicsRequest_post()
    {
        $this->load->model(['Mechanic_model', 'Request_model']);
        $mechanic_id = $this->param['mechanic_id'];
        $user_id = $this->param['user_id'];
        $request_id = $this->param['request_id'];

        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];

        $updateFlag = $this->Request_model->where(['user_id' => $user_id, 'request_id' => $request_id])->update(['request_status' => 2, 'mechanic_id' => $mechanic_id]);
        $redis = createConnectionToRedis();
        //$redis=$this->createConnectionToRedis_post();

        $geolist = $redis->georadius('mechanics', $longitude, $latitude, '500', 'mi', 'WITHDIST', 'WITHCOORD');

        $mechanicsData = array();
        foreach ($geolist as $key => $geolists) {
            if ($geolists[0] == $mechanic_id) {
                $mechanicsData[$key]['distance'] = $geolists[1];
                $mechanicsData[$key]['radius'] = $geolists[2];
                $mechanicsData[$key]['mechanicsData'] = $this->Mechanic_model->where('mechanic_id', $mechanic_id)->get();
            }
        }

        if ($updateFlag) {
            $this->response(['status' => true, 'message' => 'Request Accepted Successfully.', 'request' => $mechanicsData]);die;
        } else {
            $this->response(['status' => true, 'message' => 'Something went wrong']);die;
        }
    }

    ## Get All Near Mechanics
    public function allNearMechanics_post()
    {
        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];
        $res = $this->getNearByMechanicOfLongLat_post($longitude, $latitude);

        if ($res) {
            $this->response(['status' => true, 'message' => 'Get Mechanics', 'response' => array_values($res)]);die;
        } else {
            $this->response(['status' => false, 'message' => 'There is currently no mechanics available. Call now to make a booking.', 'contact' => '1234567890']);die;
        }
    }

    ## Get All User request
    public function getAllUserCompletedRequest_post()
    {
        $this->load->model(['Mechanic_rating_model', 'Request_model', 'Mechanic_issuses_model', 'Issue_model', 'Request_issues_model', 'Mechanic_add_service_model', 'Mechanic_model', 'Transaction_model']);

        $user_id = $this->param['user_id'];
        $getData = $this->Request_model->fields(['request_id', 'request_type', 'mechanic_id', 'request_status', 'request_date'])->where('user_id', $user_id)->order_by('request_date', 'desc')->get_all();

        if (!empty($getData)) {
            $response = [];
            foreach ($getData as $getDatas) {
                $requestId = $getDatas['request_id'];
                $requestDate = $getDatas['request_date'];
                $mechanicId = $getDatas['mechanic_id'];
                $requestStatus = $getDatas['request_status'];

                $getTransaction = $this->Transaction_model->fields('transaction_id,transaction_charge_id')
                    ->get(['request_id' => $requestId]);

                $explodeDate = explode(" ", $requestDate);

                $mechanicDetails = $this->Mechanic_model
                    ->fields('mechanic_id,name,email_id,country_code,
                    contact_number,mechanic_address,mechanic_picture,
                    mechanic_uuid,isOnline')
                    ->where('mechanic_id', $mechanicId)->get();

                if ($requestStatus == 2 || $requestStatus == 9 || $requestStatus == 10) {
                    $isJourney = true;
                } else {
                    $isJourney = false;
                }

                //Rating Flag
                $getRatingVal = $this->Mechanic_rating_model->fields('rating,comment')
                    ->where(['user_id' => $user_id, 'mechanic_id' => $mechanicId, 'request_id' => $requestId, 'from_at' => 1])
                    ->get();
                if (!empty($getRatingVal)) {
                    $isRating = true;
                    $rating = $getRatingVal['rating'];
                    $ratingComment = $getRatingVal['comment'];
                } else {
                    $isRating = false;
                    $rating = 0;
                    $ratingComment = "";
                }

                if ($getDatas['request_type'] == 1) {
                    $dateTime = $this->Request_issues_model->fields('date,timepicker,location,location')->get(['request_id' => $requestId]);

                    $issueDate = $dateTime['date'];
                    $issueTime = $dateTime['timepicker'];
                    $location = $dateTime['location'];
                } else {
                    $issueDate = $explodeDate[0];
                    $issueTime = $explodeDate[1];
                    $location = "";
                }

                /* $status = $getDatas['request_status'];
                if($status == 6) {
                $getIssue = $this->Mechanic_issuses_model->fields(['issuse_id','issuse_price'])->where('request_id',$requestId)->get();
                } else {

                }*/
                $response[] = [
                    'request_id' => $getDatas['request_id'],
                    'request_type' => $getDatas['request_type'],
                    'request_status' => $getDatas['request_status'],
                    'request_date' => $explodeDate[0],
                    'request_time' => $explodeDate[1],
                    'isJourney' => $isJourney,
                    'mechanic_id' => (isset($mechanicDetails['mechanic_id']) ? $mechanicDetails['mechanic_id'] : ''),
                    'name' => (isset($mechanicDetails['name']) ? $mechanicDetails['name'] : ''),
                    'email_id' => (isset($mechanicDetails['email_id']) ? $mechanicDetails['email_id'] : ''),
                    'country_code' => (isset($mechanicDetails['country_code']) ? $mechanicDetails['country_code'] : ''),
                    'contact_number' => (isset($mechanicDetails['contact_number']) ? $mechanicDetails['contact_number'] : ''),
                    'mechanic_address' => (isset($mechanicDetails['mechanic_address']) ? $mechanicDetails['mechanic_address'] : ''),
                    'mechanic_picture' => (isset($mechanicDetails['mechanic_picture']) ? $mechanicDetails['mechanic_picture'] : ''),
                    'mechanic_uuid' => (isset($mechanicDetails['mechanic_uuid']) ? $mechanicDetails['mechanic_uuid'] : ''),
                    'isOnline' => (isset($mechanicDetails['isOnline']) ? $mechanicDetails['isOnline'] : ''),
                    'isRating' => $isRating,
                    'rating' => $rating,
                    'ratingComment' => $ratingComment,
                    'transaction_id' => (isset($getTransaction['transaction_charge_id']) ? $getTransaction['transaction_charge_id'] : ''),
                    'date' => $issueDate,
                    'timepicker' => $issueTime,
                    'location' => $location,
                ];
            }
            if (!empty($response)) {
                $this->response(['status' => true, 'message' => 'Pending and Completed Request', 'request' => $response]);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'You have yet to make any requests.']);die;
        }
    }

    ## Redis connection
    /* public function createConnectionToRedis_post()
    {
    $redis = new Predis\Client([
    //'scheme' => 'tcp',
    'host'   => '127.0.0.1',
    'port'   => 6379,
    ]);

    echo "Connection to server sucessfully";
    echo "Server is running: ".$redis->ping();
    echo '</br>';
    $redis = $redis;
    return $redis;
    }*/

    ## Get NearBy Mechanics
    public function getNearByMechanicOfLongLat_post($longitude, $latitude)
    {
        $this->load->model(array('Mechanic_rating_model', 'Mechanic_model', 'Mechanic_service_model', 'Service_model'));

        $allService = $this->Service_model->fields('service_id')->get_all(['service_status' => 101]);
        foreach ($allService as $allServices) {
            $service[] = $allServices['service_id'];
        }

        //  $redis=$this->createConnectionToRedis_post();
        $redis = createConnectionToRedis();

        $geolist = $redis->georadius('mechanics', $longitude, $latitude, '500', 'mi', 'WITHDIST', 'WITHCOORD');

        if (!empty($geolist)) {
            $mechanicsData = array();

            foreach ($geolist as $key => $geolists) {
                $mechanicsID = $geolists[0];
                $mechanic_state = $redis->get($mechanicsID);;

                $isAppVerified = $this->isAppVerified($mechanicsID);
                if ($isAppVerified && $mechanic_state == 'ONLINE') {

                    $mechanicsData[$key]['distancevalue'] = $geolists[1];
                    $mechanicsData[$key]['longitude'] = $geolists[2][0];
                    $mechanicsData[$key]['latitude'] = $geolists[2][1];
                    $mechanicsData[$key]['mechanicsID'] = $mechanicsID;

                    $mechanicsData[$key]['mechanicsData'] = $this->Mechanic_model->fields('mechanic_id, name, email_id, country_code, contact_number, mechanic_dob,hourly_rate, mechanic_address, mechanic_picture, mechanic_uuid, isOnline, isBlocked, mechanic_status')->where('mechanic_id', $mechanicsID)->get();

                    $mechanicsData[$key]['mechanicsData']['distance'] = $mechanicsData[$key]['distancevalue'];
                    unset($mechanicsData[$key]['distancevalue']);

                    $mechanicsData[$key]['mechanicsData']['longitude'] = $mechanicsData[$key]['longitude'];
                    $mechanicsData[$key]['mechanicsData']['latitude'] = $mechanicsData[$key]['latitude'];

                    unset($mechanicsData[$key]['longitude']);
                    unset($mechanicsData[$key]['latitude']);

                    //For Mechanic Service
                    $getServiceVal = $this->Mechanic_service_model->fields('services_id')->where(['mechanic_id' => $mechanicsID, 'mechanic_services_status' => 101])
                        ->with_service('fields:service_name', 'where:parent_id=0')->get_all();

                    $serviceNamedata = array();
                    $skipMechanic = true;
                    if (!empty($getServiceVal)) {
                        foreach ($getServiceVal as $getServiceVals) {
                            if ($getServiceVals['services_id'] == 2 || $getServiceVals['services_id'] == 3) {
                                $skipMechanic = false;
                            }
                            $serviceNamedata[] = $getServiceVals['services_id'];
                        }
                    }

                    if ($skipMechanic) {
                        unset($mechanicsData[$key]);
                        continue;
                    }

                    if (!empty($serviceNamedata)) {
                        $mechanicsData[$key]['serviceNames'] = $serviceNamedata;

                        $mechanicsData[$key]['mechanicsData']['services'] = $mechanicsData[$key]['serviceNames'];
                    }
                    // For Mechanic Type
                    $type = array_intersect($service, $serviceNamedata);

                    if (count(array_intersect(array('1', '2', '3'), $type)) > 1 && in_array('1', $type)) {
                        $mechanicType = '3'; // Both Emergency And repair
                    } elseif (in_array('1', $type)) {
                        $mechanicType = '1'; // Repair
                    } else {
                        $mechanicType = '2'; // Emergency
                    }

                    $mechanicsData[$key]['mechanicsData']['mechanicType'] = $mechanicType;

                    unset($mechanicsData[$key]['serviceNames']);

                    //For Mechanic Rating
                    $getRating = $this->Mechanic_rating_model
                        ->where(['mechanic_id' => $mechanicsID, 'from_at' => 1])->get_all();
                    if (!empty($getRating)) {
                        $ratingSum = '';
                        foreach ($getRating as $getRatings) {
                            $ratingMechanic = $getRatings['mechanic_id'];
                            $ratingSum += $getRatings['rating'];
                        }
                        if (isset($ratingSum) && $ratingSum != '') {
                            $mechanicsData[$key]['average_rating'] = $ratingSum / count($getRating);
                            $mechanicsData[$key]['mechanicsData']['average_rating'] = $mechanicsData[$key]['average_rating'];
                        }
                    } else {
                        $mechanicsData[$key]['average_rating'] = 0;
                        $mechanicsData[$key]['mechanicsData']['average_rating'] = 0;
                    }

                    if (!isset($mechanicsData[$key]['mechanicsData']['average_rating'])) {
                        $mechanicsData[$key]['mechanicsData']['average_rating'] = 0;
                    }
                    unset($mechanicsData[$key]['average_rating']);
                    round($mechanicsData[$key]['mechanicsData']['average_rating']);
                }
            }
            return $mechanicsData;
        } else {
            return false;
        }
    }

    ## Manage Mechanic Online Status With Add and Remove the GeoRadius
    public function onlineMechanicStatus_post()
    {
        $this->load->model('Mechanic_model');
        $mechanicId = $this->param['mechanic_id'];
        $longitude = $this->param['longitude'];
        $latitude = $this->param['latitude'];
        $status = $this->param['status']; // For Enable and Disable
        if (isset($longitude) && isset($latitude)) {
            $getMechanicStatus = $this->Mechanic_model->fields('*')->where('mechanic_id', $mechanicId)->get();
            $mechanicId = $getMechanicStatus['mechanic_id'];
            $redis = createConnectionToRedis(); //For Redis Connection
            //   $redis = $this->createConnectionToRedis_post(); //For Redis Connection

            if ($status == 100) {
                $updateOnlineStatus = $this->Mechanic_model->where('mechanic_id', $mechanicId)->update(['isEnable' => 100, 'isOnline' => 100]);
                $updateData = ['status' => 100];
                //Offline Machanic Remove in Geoadd
                $redis->set($mechanicId, 'OFFLINE');
                // $redis->zrem('mechanics', $mechanicId);
            } else {
                $updateOnlineStatus = $this->Mechanic_model->where('mechanic_id', $mechanicId)->update(['isEnable' => 101, 'isOnline' => 101]);
                $updateData = ['status' => 101];
                //Online Machanic Add in Geoadd
                $redis->set($mechanicId, 'ONLINE');
                $redis->geoadd('mechanics', $longitude, $latitude, $mechanicId);
            }

            if ($updateOnlineStatus) {
                echo json_encode(['status' => true, 'message' => 'Updated Status', 'response' => $updateData]);die;
            } else {
                echo json_encode(['status' => false, 'message' => 'Not Updated']);die;
            }
        }
    }

    ## Change The Longitude And Latitude Of Online Mechanic For Each 5 Sec
    public function changeLatLngOnlineMechanic_post()
    {
        $this->load->model('Mechanic_model');
        $mechanicId = $this->param['mechanic_id'];
        $newlongitude = $this->param['longitude'];
        $newlatitude = $this->param['latitude'];
        $getOnlineStatus = $this->Mechanic_model->where('mechanic_id', $mechanicId)->get_all(['isEnable' => 101]);
        if (!empty($getOnlineStatus)) {
            $redis = createConnectionToRedis();
            //$redis->zrem('mechanics', $mechanicId);
            $redis->set($mechanicId, 'OFFLINE');
            $addMechanicsForNewPosition = $redis->geoadd('mechanics', $newlongitude, $newlatitude, $mechanicId);

            /*$geolist=$redis->georadius('mechanics','75','20','500','mi','WITHDIST','WITHCOORD');
            $specificMechanicLatLong=$redis->geopos('mechanics',$mechanicId);
            $mechanicLong = $specificMechanicLatLong[0][0];
            $mechanicLat = $specificMechanicLatLong[0][1];*/

            $response = ['longitude' => $newlongitude, 'latitude' => $newlatitude];
            $this->response(['status' => true, 'message' => 'Updated Latitude Logitude', 'response' => $response]);
            die;
        } else {
            $this->response(['status' => false, 'message' => 'Not Updated']);die;
        }
    }

    ## User send request to specifc mechanics
    public function sendRequestToMechanics_post()
    {
        $this->load->model('Request_mechanics_model');
        //$userID=$this->param['user_id'];
        $mechanics = $this->param['mechanic_id'];
        $requestID = $this->param['request_id'];
        $data = ['request_id' => $this->param['request_id'],
            'mechanic_id' => $this->param['mechanic_id']];

        $result = $this->Request_mechanics_model->insert($data);
        if ($result) {
            $this->response(['status' => true, 'message' => 'Request Send Successfully!!', 'response' => $result]);die;
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);die;
        }
    }

    ## Check Status of Request
    public function checkStatusOfUserToMechanics_get()
    {
        $request_id = $this->param['request_id'];
        $this->load->model('Request_mechanics_model');
        $result = $this->Request_mechanics_model->get(['request_id' => $request_id, 'status' => 100]);
        if (!empty($result)) {
            $this->response(['status' => true, 'message' => 'Already Use Mechanics', 'response' => $result]);
        } else {
            $this->response(['status' => false, 'message' => 'Something went wrong']);
        }
    }

    ## For test add Geo Radius
    public function addMechanicGeoLongLat_post()
    {
        $redis = createConnectionToRedis();
        //$redis=$this->createConnectionToRedis_post();
        $redis->geoadd('supplyment', '75.8701574', '22.7012391', '2');
        $redis->geoadd('supplyment', '72.8701574', '21.7011111', '3');
        $redis->geoadd('supplyment', '75.8701554', '22.7012321', '1');
        //$geolistadd=$redis->geoadd('supplyment','75.8701654','22.7012421','d4');
    }

    public function getNearByLocation_post()
    {
        $address = 'BTM 2nd Stage, Bengaluru, Karnataka 560076';

        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');

        // Convert the JSON to an array
        $geo = json_decode($geo, true);

        if ($geo['status'] == 'OK') {
            // Get Lat & Long
            $latitude = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }

        echo $latitude . '</br>';
        echo $longitude . '</br>';

        die;
        //$getNearByMechanics = ['']
        $lat = $this->param['lat'];
        $lng = $this->param['lng'];
        $mergeLatLng = $lat . ',' . $lng;

        $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.7012321,75.8701554&radius=500&key=AIzaSyCyfl6EZ8VK7-C4DTyxmG53XS_4xj0VS2I';

        // $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.6818927, 75.8323882&radius=500&types=food&name=cruise&key=AIzaSyCyfl6EZ8VK7-C4DTyxmG53XS_4xj0VS2I';
        //  $address = "India+Panchkula";
        //$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        echo '<pre>';
        print_r($response_a);
        die;
        echo $lat = $response_a->results[0]->geometry->location->lat;
        echo "<br />";
        echo $long = $response_a->results[0]->geometry->location->lng;
    }

    private function isAppVerified($id)
    {
        $this->load->model('Mechanic_registrations_model');
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        if ($isAppVerified) {
            $mechanicsData = $this->Mechanic_model
                ->fields('mechanic_id, name, email_id, country_code,contact_number, mechanic_dob,hourly_rate,hourly_rate,mechanic_address, mechanic_picture,mechanic_uuid, isOnline, isBlocked, mechanic_status')
                ->where(['isBlocked' => 100, 'mechanic_id' => $id])->get();
            if ($mechanicsData) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
            }
        }

        return $isAppVerified;
    }

}
