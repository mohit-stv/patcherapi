<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qualification extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addQualifications_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addQualifications') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Qualifications_model']);             
            if(isset($this->param['qualifications']) && $this->param['qualifications'] != NUll){
                for($i=0;$i<count($this->post('qualifications'));$i++){       
                    $qualifications_array=[
                        'qualificationName' => $this->param['qualifications'][$i]['qualificationName'],
                        'qualificationStatus' => $this->param['qualifications'][$i]['qualificationStatus']
                    ];
                    $insert = $this->Qualifications_model->insert($qualifications_array);
                }
            }
            if($insert){
               $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    } 

    public function editQualifications_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editQualifications') == FALSE) {
            $message = $this->form_validation->error_array();
            $response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {
            $this->load->model(['Qualifications_model']);             
            if(isset($this->param['qualifications']) && $this->param['qualifications'] != NUll){
                for($i=0;$i<count($this->post('qualifications'));$i++){         
                    $qualifications_array=[
                        'qualificationName' => $this->param['qualifications'][$i]['qualificationName'],
                        'qualificationStatus' => $this->param['qualifications'][$i]['qualificationStatus']       
                    ];
                    $update = $this->Qualifications_model->where('qualificationId',$this->param['qualifications'][$i]['qualificationId'])->update($qualifications_array);
                }
            }
            if($update){
               $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $update]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        }
    }    

    public function allQualification_post() {
    	
    	$this->load->model(['Qualification_model']);              	
		
        $qualifications   =  $this->Qualification_model->fields('qualification_id,qualification_name')
                                                        ->where('qualification_status','101')->get_all();

        if($qualifications){
          	$this->response(['status' => true , 'message' => 'Successfully','response' => $qualifications]);
        }else{
          	$this->response(['status' => false, 'message' => 'Record Not Found']);
        }
    }   

    public function deleteQualifications_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteQualifications') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {   
        	$qualificationId = $this->param['qualificationId'];

			$updateOptions = array(
                'where' => array('qualificationId' => $qualificationId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'qualifications'
            );
            $deleteQualification = $this->common_model->customUpdate($updateOptions);

	        if($deleteQualification){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteQualification]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}