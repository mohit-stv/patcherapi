<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicles extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

    public function addVehicles_post() {
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('addVehicles') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {     
        	$this->load->model(['Vehicles_model']);     
        	$userId = $this->param['userId'];        	
			if(isset($this->param['vehicles']) && $this->param['vehicles'] != NUll){
				for($i=0;$i<count($this->post('vehicles'));$i++){
					$vehicle_number = '';

					if(isset($this->param['vehicles'][$i]['vehicle_number']) && !empty($this->param['vehicles'][$i]['vehicle_number'])){
						$vehicle_number = $this->param['vehicles'][$i]['vehicle_number'];
					}

					$vehicles_array=[
						'userId'=> $userId,
						'vehicleYear' => $this->param['vehicles'][$i]['vehicleYear'],
						'vehicleMake' => $this->param['vehicles'][$i]['vehicleMake'],
						'vehicleModel' => $this->param['vehicles'][$i]['vehicleModel'],
						'vehicleBodyType' => $this->param['vehicles'][$i]['vehicleBodyType'],
						'vehicleEngineType' => $this->param['vehicles'][$i]['vehicleEngineType'],
						'vehicleDriveType' => $this->param['vehicles'][$i]['vehicleDriveType'],
						'vehicle_number' => $vehicle_number,
					];
					$insert = $this->Vehicles_model->insert($vehicles_array);
				}
			}
			if($insert)
		 	{
		       $this->response(['status' => true, 'message'=> 'Update Successful ','response' => $userId]);die;
	     	}else{
		       $this->response(['status' => false, 'message' => 'Something went wrong']);die;
		    }
        }
    }

    public function editVehicles_post() {    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('editVehicles') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {  
        	$this->load->model(['Vehicles_model']);        
        	$userId = $this->param['userId'];        	
			if(isset($this->param['vehicles']) && $this->param['vehicles'] != NUll){
				for($i=0;$i<count($this->post('vehicles'));$i++){	
					
					$vehicle_number = '';
					
					if(isset($this->param['vehicles'][$i]['vehicle_number']) && !empty($this->param['vehicles'][$i]['vehicle_number'])){
						$vehicle_number = $this->param['vehicles'][$i]['vehicle_number'];
					}

					$vehicles_array=[
						'userId'=> $userId,
						'vehicleYear' => $this->param['vehicles'][$i]['vehicleYear'],
						'vehicleMake' => $this->param['vehicles'][$i]['vehicleMake'],
						'vehicleModel' => $this->param['vehicles'][$i]['vehicleModel'],
						'vehicleBodyType' => $this->param['vehicles'][$i]['vehicleBodyType'],
						'vehicleEngineType' => $this->param['vehicles'][$i]['vehicleEngineType'],
						'vehicleDriveType' => $this->param['vehicles'][$i]['vehicleDriveType'],
						'vehicle_number' => $vehicle_number,
					];
					if(isset($this->param['vehicles'][$i]['vehicleId']) && $this->param['vehicles'][$i]['vehicleId'] != NULL){
						$update = $this->Vehicles_model->where('vehicleId',$this->param['vehicles'][$i]['vehicleId'])->update($vehicles_array);
					}else{
						$insert = $this->Vehicles_model->insert($vehicles_array);
					}
				}
			}

			if(isset($this->param['deleteIds']) && $this->param['deleteIds'] != NULL){
				for($i=0;$i<count($this->param['deleteIds']);$i++){			
			 		
			 		$updateOptions = array(
                        'where' => array('vehicleId' => $this->param['deleteIds'][$i]),
                        'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                        'table' => 'vehicles'
                    );
                    $this->common_model->customUpdate($updateOptions);
			 	}
			}
			if($userId){
		       $this->response(['status' => true, 'message'=> 'Update Successfuly ','response' => $userId]);die;
	     	}else{
		       $this->response(['status' => false, 'message' => 'Something went wrong']);die;
		    }
        }
    }    

    public function vehicleDetails_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('vehicleDetails') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else { 
        	$this->load->model(['Vehicles_model']);         
        	$userId = $this->param['userId'];        	
			
			$vehicles   =  $this->Vehicles_model->where('userId',$userId)->fields('vehicleId,userId,vehicleYear,vehicleMake,vehicleModel,vehicleBodyType,vehicle_number,vehicleEngineType,vehicleDriveType')->get_all();

	        if($vehicles){
	          	$this->response(['status' => true , 'message' => 'Successfully','response' => $vehicles]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }   

     public function deleteVehicles_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteVehicles') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {        
        	$userId = $this->param['userId'];
        	$vehicleId = $this->param['vehicleId'];        	

			$updateOptions = array(
                'where' => array('vehicleId' => $vehicleId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'vehicles'
            );
            $deleteVehicle = $this->common_model->customUpdate($updateOptions);

	        if($deleteVehicle){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteVehicle]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }
}