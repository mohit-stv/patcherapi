<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nearest extends MY_Controller
{

    private $param = array();
    public function __construct()
    {
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
        $this->load->helper('common');
    }

    public function nearestMechanic_post()
    {
        $this->load->model(['Mechanic_rating_model', 'Mechanic_model',
            'Mechanic_service_model', 'Service_model']);

        $latitude = $this->param['latitude'];
        $longitude = $this->param['longitude'];
        $allMechanic = $this->Mechanic_model->get();

        $allService = $this->Service_model->fields('service_id')->get_all(['service_status' => 101]);
        foreach ($allService as $allServices) {
            $service[] = $allServices['service_id'];
        }

        $redis = createConnectionToRedis();

        $geolist = $redis->georadius('mechanicSpotLatLng', $longitude, $latitude, '500', 'mi', 'WITHDIST', 'WITHCOORD');

        $mechanicsData = [];
        foreach ($geolist as $key => $geolists) {
            $mechanicsID = $geolists[0];
            $isAppVerified = $this->isAppVerified($mechanicsID);

            #If mechainc registration is verified then only this'll appear for repair
            if ($isAppVerified) {
                $ratingSum = 0;
                $mechanicsData[$key]['distancevalue'] = $geolists[1];
                $mechanicsData[$key]['longitude'] = $geolists[2][0];
                $mechanicsData[$key]['latitude'] = $geolists[2][1];

                $mechanicsData[$key]['mechanicsData'] = $this->Mechanic_model
                    ->fields('mechanic_id, name, email_id, country_code,
                contact_number, mechanic_dob,hourly_rate,hourly_rate,mechanic_address, mechanic_picture,
                mechanic_uuid, isOnline, isBlocked, mechanic_status')
                    ->where('mechanic_id', $mechanicsID)->get();

                $mechanicsData[$key]['mechanicsData']['distance'] = $mechanicsData[$key]['distancevalue'];
                unset($mechanicsData[$key]['distancevalue']);

                // $mechanicsData[$key]['mechanicsData']['hourly_rate'] = $mechanicsData[$key]['hourly_rate'];
                $mechanicsData[$key]['mechanicsData']['longitude'] = $mechanicsData[$key]['longitude'];
                $mechanicsData[$key]['mechanicsData']['latitude'] = $mechanicsData[$key]['latitude'];

                unset($mechanicsData[$key]['longitude']);
                unset($mechanicsData[$key]['latitude']);

                if ($mechanicsData[$key]['mechanicsData']['isBlocked'] == 101) {
                    continue;
                }

                //For Mechanic Service
                $getServiceVal = $this->Mechanic_service_model->fields('services_id')->where(['mechanic_id' => $mechanicsID, 'mechanic_services_status' => 101])
                    ->with_service('fields:service_name', 'where:parent_id=0')->get_all();

                $skipMechanic = true;

                $serviceNamedata = array();
                if (!empty($getServiceVal)) {
                    foreach ($getServiceVal as $getServiceVals) {
                        if ($getServiceVals['services_id'] == 1) {
                            $skipMechanic = false;
                        }

                        $serviceId = $getServiceVals['services_id'];
                        $serviceNamedata[] = $getServiceVals['services_id'];
                    }
                }

                if ($skipMechanic) {
                    unset($mechanicsData[$key]);
                    continue;
                }

                // For Mechanic Type
                $type = array_intersect($service, $serviceNamedata);

                if (count(array_intersect(array('1', '2', '3'), $type)) > 1) {
                    $mechanicType = '3'; // Both Emergency And repair
                } elseif (in_array('1', $type)) {
                    $mechanicType = '1'; // Repair
                } else {
                    $mechanicType = '2'; // Emergency
                }

                $mechanicsData[$key]['mechanicsData']['mechanicType'] = $mechanicType;

                $mechanicsData[$key]['serviceNames'] = $serviceNamedata;

                $mechanicsData[$key]['mechanicsData']['services'] = $mechanicsData[$key]['serviceNames'];

                unset($mechanicsData[$key]['serviceNames']);

                //For Mechanic Rating
                $getRating = $this->Mechanic_rating_model
                    ->where(['mechanic_id' => $mechanicsID, 'from_at' => 1])->get_all();
                if (!empty($getRating)) {
                    foreach ($getRating as $getRatings) {
                        $ratingSum += $getRatings['rating'];
                    }
                    if (isset($ratingSum) && $ratingSum != '') {
                        $mechanicsData[$key]['average_rating'] = $ratingSum / count($getRating);
                        $mechanicsData[$key]['mechanicsData']['average_rating'] = round($mechanicsData[$key]['average_rating']);
                    }
                } else {
                    $mechanicsData[$key]['average_rating'] = 0;
                    $mechanicsData[$key]['mechanicsData']['average_rating'] = $mechanicsData[$key]['average_rating'];
                }
                $mechanicsData[$key]['isAppVerified'] = $isAppVerified;
                unset($mechanicsData[$key]['average_rating']);
            }
        }
        if (!empty($mechanicsData)) {
            $mechanicsData = array_values($mechanicsData);
            $this->response(['status' => true, 'message' => 'All Nearest Mechanics.', 'response' => $mechanicsData]);
        } else {
            $this->response(['status' => false, 'message' => 'No Any Nearest Mechanics.']);
        }
    }

    private function isAppVerified($id)
    {
        $this->load->model('Mechanic_registrations_model');
        $getStatus = $this->Mechanic_registrations_model->get(['mechanic_id' => $id]);

        $applicationStatus = [
            ['name' => 'Profile', 'status' => ($getStatus['basic_info'] ? $getStatus['basic_info'] : 100)],
            ['name' => 'Services', 'status' => ($getStatus['service_status'] ? $getStatus['service_status'] : 100)],
            ['name' => 'Documentations', 'status' => ($getStatus['documentation_status'] ? $getStatus['documentation_status'] : 100)],
            ['name' => 'Experience', 'status' => ($getStatus['experience_status'] ? $getStatus['experience_status'] : 100)],
            ['name' => 'Qualification', 'status' => ($getStatus['qualification_status'] ? $getStatus['qualification_status'] : 100)],
            ['name' => "Tool Itinerary", 'status' => ($getStatus['tools_status'] ? $getStatus['tools_status'] : 100)],
            ['name' => "Health and Safety", 'status' => ($getStatus['health_status'] ? $getStatus['health_status'] : 100)],
        ];

        foreach ($applicationStatus as $flag) {
            if ($flag['status'] == 102) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
                break;
            }
        }

        if ($isAppVerified) {
            $mechanicsData = $this->Mechanic_model
                ->fields('mechanic_id, name, email_id, country_code,contact_number, mechanic_dob,hourly_rate,hourly_rate,mechanic_address, mechanic_picture,mechanic_uuid, isOnline, isBlocked, mechanic_status')
                ->where(['isBlocked' => 100, 'mechanic_id' => $id])->get();
            if ($mechanicsData) {
                $isAppVerified = true;
            } else {
                $isAppVerified = false;
            }
        }

        return $isAppVerified;
    }
}
