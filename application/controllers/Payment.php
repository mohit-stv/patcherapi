<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['Mechanic_rating_model', 'common_model', 'User_model', 'Payment_type_model', 'Transaction_model', 'Request_model', 'Mechanic_request_detail_model']);

    }

    public function getPaymentTypes_get()
    {
        $getPaymentType = $this->Payment_type_model->fields(['payment_type_id', '    payment_type_name', 'payment_type_status'])->get_all();

        if (!empty($getPaymentType)) {
            echo json_encode(['status' => true, 'message' => 'All Payment Types Here.', 'response' => $getPaymentType]);die;
        } else {
            echo json_encode(['status' => false, 'message' => 'Something went wrong.']);die;
        }
    }

    public function payPayment_post()
    {
        $this->load->helper('common');
        $this->load->library(['Notification', 'Mypubnub', 'Curl']);
        $stripInclude = stripIncludeFiles();
        if (isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $userId = $this->param['user_id'];
        } else {
            echo json_encode(['status' => false, 'message' => 'UserId is required.']);
        }

        if (isset($this->param['token']) && $this->param['token'] != '') {
            $token = $this->param['token'];
        } else {
            $token = '';
        }

        if (isset($this->param['customer_id']) && $this->param['customer_id'] != '') {
            $customerId = $this->param['customer_id'];
        } else {
            $customerId = '';
        }

        // For Card Payment
        if ($this->param['payment_type'] == 1) {

            $requestId = $this->param['request_id'];
            $amount = $this->param['amount'];
            $lastFourDigit = $this->param['lastfour_digit'];

            $getTransaction = $this->Transaction_model
                ->fields('customer_id')
                ->get_all(['user_id' => $userId, 'last4' => $lastFourDigit]);

            $getUserDetails = $this->User_model
                ->fields('email_id')
                ->where(['user_id' => $userId])->get();

            $emailId = $getUserDetails['email_id'];

            if ($customerId != '') {
                $customerId = $customerId;
            } else {
                if (!empty($getTransaction)) {
                    $customerId = $getTransaction['customer_id'];

                    /* $cu = \Stripe\Customer::retrieve($customerId);

                $cu->description = "Customer for ".$emailId;
                $cu->source = $data['token'];
                $cu->save();*/
                } else {
                    $customerArray = ['description' => 'Customer for ' . $emailId,
                        //'email'=>$emailId,
                        'source' => $token];
                    $customer = \Stripe\Customer::create($customerArray);
                    $customerId = $customer->id;
                }
            }

            $amount = $this->param['amount'];
            $chargeAmount = $amount * 100;
            $charge = \Stripe\Charge::create(array('customer' => $customerId, 'amount' => $chargeAmount, 'currency' => 'gbp'));

            //$charge = \Stripe\Charge::create(array('card' => $myCard, 'amount' => $this->param['amount'], 'currency' => $this->param['currency']));

            // $charge = \Stripe\Charge::create(array('amount' => 3, 'currency' => 'USD', 'source' => 'btok_1B1pV92eZvKYlo2Ci499Dpu0'));

            if ($charge != null && $charge->status == 'succeeded') {
                $transaction_reference = "P" . $requestId . "SA" . mt_rand(1111, 9999);

                $data = [
                    'request_id' => $requestId,
                    'user_id' => $this->param['user_id'],
                    'customer_id' => $customerId,
                    'transaction_reference' => $transaction_reference,
                    'transaction_charge_id' => $charge->id,
                    'object_type' => $charge->object,
                    'amount' => $amount,
                    'network_status' => $charge->outcome->network_status,
                    'message' => $charge->outcome->seller_message,
                    'paid_status' => $charge->paid,
                    'source_id' => $charge->source->id,
                    'transaction_type' => 2, // 2 For Final Payment
                    'last4' => $lastFourDigit,
                    //  'payment_type'=>$charge->source->object, //Which type payment
                    'payment_type' => 1, //Which type payment
                    'source_brand' => $charge->source->brand,
                    // 'source_country'=>$charge->source->country,
                    'expire_month' => $charge->source->exp_month,
                    'expire_year' => $charge->source->exp_year,
                    'transaction_status' => $charge->status,
                ];

                $paymentMesssage = "You Just Paid $amount for Request Id: $requestId. For any payment related query please use Transaction Id: " . $transaction_reference;

                $args = [
                    'message' => $paymentMesssage,
                    'subject' => 'Patcher: Request Payment Details',
                    'email_id' => $emailId,
                    'template_name' => 'Sign Up',
                    'vars' => [
                        [
                            'name' => 'message',
                            'content' => $paymentMesssage,
                        ],
                    ],
                ];

                $mailResponse = sendMandrillEmailTemlate($args);

                $insert = $this->Transaction_model->insert($data);
                if ($insert) {
                    $update = $this->Request_model->where(['request_id' => $this->param['request_id'], 'user_id' => $this->param['user_id']])->update(['request_status' => 6]);

                    $calloutFee = 0;
                    $getCalloutFee = $this->Transaction_model->fields('amount')->get(['request_id' => $requestId, 'transaction_type' => 1]);
                    if ($getCalloutFee) {
                        $calloutFee = $getCalloutFee['amount'];
                    }

                    $mechanicDetail = $this->Request_model->with_mechanic('fields:mechanic_id,name,device_key,isProduction,platform_type,mechanic_uuid,request_commission')
                        ->where('request_id', $this->param['request_id'])->get();

                    $mechanicId = $mechanicDetail['mechanic']['mechanic_id'];
                    $request_commission = $mechanicDetail['mechanic']['request_commission'] / 100;
                    //Insert Mech Details
                    $updateMechReqData = [
                        'mechanic_id' => $mechanicId,
                        'request_id' => $requestId,
                        'request_amount' => $amount,
                        'comission' => ($amount * $request_commission) + $calloutFee, //0.75 i.e 75% stack of mechanic in a request
                        'remarks' => 'Trip Payment',
                        'isChargable' => 100,
                    ];

                    $paymentMesssage = "Request Id: $requestId has completed now. Here service amount: $amount, Callout Fee: $calloutFee and Total Amount: " . ($amount + $calloutFee) . ". For any query please use Request Id.";

                    $args = [
                        'message' => $paymentMesssage,
                        'subject' => 'Patcher: Request Completed',
                        'email_id' => $emailId,
                        'template_name' => 'Sign Up',
                        'vars' => [
                            [
                                'name' => 'message',
                                'content' => $paymentMesssage,
                            ],
                        ],
                    ];

                    $mailResponse = sendMandrillEmailTemlate($args);

                    $updateMechReqDetail = $this->Mechanic_request_detail_model->insert($updateMechReqData);

                    $getTransactionDate = $this->Transaction_model
                        ->fields('created_at')
                        ->get(['transaction_id' => $insert]);
                    $explodeDate = explode(' ', $getTransactionDate['created_at']);

                    $getUserRating = $this->Mechanic_rating_model
                        ->fields('rating')
                        ->where(['user_id' => $userId, 'mechanic_id' => $mechanicId, 'from_at' => 2])
                        ->get();

                    if (!empty($getUserRating)) {
                        $userRating = $getUserRating['rating'];
                    } else {
                        $userRating = 0;
                    }

                    $deviceId = $mechanicDetail['mechanic']['device_key'];
                    $isProduction = $mechanicDetail['mechanic']['isProduction'];
                    $platformType = $mechanicDetail['mechanic']['platform_type'];

                    $bodyData = ['request_id' => $requestId, 'amount' => $amount, 'user_id' => $userId, 'user_rating' => $userRating, 'date' => $explodeDate[0], 'time' => $explodeDate[1]];

                    $pubnub_args = ['channel' => $mechanicDetail['mechanic']['mechanic_uuid'],
                        'message' => ['type' => 'R12', 'ratingDetails' => $bodyData]];

                    $publish_status = $this->mypubnub->publish($pubnub_args);

                    // Send Notification From Andriod To User
                    if ($platformType == 1) {
                        $message = [
                            'title' => 'Payment Successfully Transferred By User',
                            'body' => $bodyData,
                            'tag' => ['request_id' => $requestId, 'notification_type' => 12],
                        ];

                        $notify = $this->notification->fcmMechanic($deviceId, $message);
                    } else if ($platformType == 2) {

                        $bodyData = ['request_id' => $requestId,];

                        $message = [
                            'title' => 'Payment Successfully Transferred By User',
                            'notification_type' => 12,
                        ];

                        $notify = $this->notification->apnsMechanic($deviceId, $message, $isProduction, $platformType, $bodyData);

                    }

                    $response = ['amount' => $charge->amount, 'status' => $charge->status, 'request_id' => $this->param['request_id']];
                    $this->response(['status' => true, 'message' => 'Transaction Completed.', 'response' => $response]);
                } else {
                    $this->response(['status' => false, 'message' => 'Error In Transaction.']);die;
                }
            } else {
                $this->response(['status' => false, 'message' => 'Unable to make payment.', 'details' => $charge]);
            }
        } else {
            $this->response(['status' => false, 'message' => 'Invalid payment type']);
        }
    }

    public function reusablePayment_post()
    {
        $this->load->helper('common');

        /*$emailId = $this->param['email_id'];
        $userId = $this->param['user_id'];
        $amount = $this->param['amount'];
        $getResult = $this->Transaction_model->fields('customer_id')->with_user('fields: email_id')->get_all(['user_id'=>$userId, 'paid_status'=>1]);
         */
        $stripInclude = stripIncludeFiles();

        $token = \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 8,
                "exp_year" => 2018,
                "cvc" => "314",
            ),
        ));
        echo $token->id;
        die;
        echo '<pre>';
        print_r($token);

        // Create Toke For Based On Card Number
        $token = \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 8,
                "exp_year" => 2018,
                "cvc" => "314",
            ),
        ));
        echo '<pre>';
        print_r($token);

        $customerArray = ['description' => 'Customer for' . $emailId,
            'email' => $emailId,
            'source' => $token->id];

        // Create Customer
        $customer = \Stripe\Customer::create($customerArray);

        die;

        try {
            $charge = \Stripe\Charge::create(array('customer' => $getResult->customer_id, 'amount' => $amount, 'currency' => 'gbp'));

            if ($charge != null && $charge->status == 'succeeded') {
                $transaction_reference = "P" . $this->param['request_id'] . "SA" . mt_rand(1111, 9999);

                $data = [
                    'request_id' => $this->param['request_id'],
                    'user_id' => $this->param['user_id'],
                    'customer_id' => $getResult->customer_id,
                    'transaction_charge_id' => $charge->id,
                    'object_type' => $charge->object,
                    'transaction_reference' => $transaction_reference,
                    'amount' => ($charge->amount / 100),
                    'network_status' => $charge->outcome->network_status,
                    'message' => $charge->outcome->seller_message,
                    'paid_status' => $charge->paid,
                    'source_id' => $charge->source->id,
                    //  'payment_type'=>$charge->source->object, //Which type payment
                    'payment_type' => 1, //Which type payment
                    'source_brand' => $charge->source->brand,
                    'source_country' => $charge->source->country,
                    'expire_month' => $charge->source->exp_month,
                    'expire_year' => $charge->source->exp_year,
                    'transaction_status' => $charge->status,
                ];
                $insert = $this->Transaction_model->insert($data);
                if ($insert) {
                    $update = $this->Request_model->where(['request_id' => $this->param['request_id'], 'user_id' => $this->param['user_id']])->update(['request_status' => 6]);
                    $response = ['amount' => $charge->amount, 'status' => $charge->status, 'request_id' => $this->param['request_id']];
                    $this->response(['status' => true, 'message' => 'Transaction Completed.', 'response' => $response]);
                } else {
                    $this->response(['status' => false, 'message' => 'Error In Transaction.']);die;
                }
            }

        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->response(['status' => true, 'message' => $err['message']]);
        } catch (Stripe_CardError $e) {
            $error1 = $e->getMessage();
            echo $error1;
        } catch (Stripe_InvalidRequestError $e) {
            // Invalid parameters were supplied to Stripe's API
            $this->response(['status' => true, 'message' => 'Invalid parameters were supplied to Stripe API.']);
        } catch (Stripe_AuthenticationError $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $this->response(['status' => true, 'message' => 'Authentication with Stripe API failed.']);
        } catch (Stripe_ApiConnectionError $e) {
            // Network communication with Stripe failed
            $this->response(['status' => true, 'message' => 'Network communication with Stripe failed.']);
        } catch (Stripe_Error $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $this->response(['status' => true, 'message' => 'Display a very generic error to the user, and maybe send.']);
        }
    }

    public function testPayment()
    {
        $this->load->helper('common');
        $stripInclude = stripIncludeFiles();
        $userId = $this->param['user_id'];

        $token = \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 8,
                "exp_year" => 2018,
                "cvc" => "314",
            ),
        ));

        echo $token->id . '</br>';

        $customerArray = ['description' => 'Customer for aubrey.taylor@example.com',
            'email' => 'aubrey.taylor@example.com',
            'source' => $token->id];
        $customer = \Stripe\Customer::create($customerArray);

        echo $customer->id . '</br>';

        /*  $chargeArray = array('card' => $myCard, 'amount' => 40, 'currency' => 'gbp');
        $charge = \Stripe\Charge::create($chargeArray); */

        $charge = \Stripe\Charge::create(array('customer' => $customer->id, 'amount' => 100, 'currency' => 'gbp'));

        echo '<pre>';
        print_r($charge);

        $retrieveCustomer = \Stripe\Customer::retrieve($customer->id);

        echo '<pre>';
        print_r($retrieveCustomer);
        die;
    }
}
