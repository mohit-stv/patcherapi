<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends MY_Controller
{

    public function __construct()
    {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array) json_decode(file_get_contents('php://input'), true);
        $this->load->model(['common_model', 'Payment_type_model']);
    }

    // All Transaction History (User and Mechanic Both)
    public function transactionHistory_post()
    {
        $this->load->model(['Request_model', 'Transaction_model', 'Request_issues_model', 'Mechanic_issuses_model', 'Mechanic_add_service_model']);

        if (isset($this->param['mechanic_id']) && $this->param['mechanic_id'] != '') {
            $mechanicId = $this->param['mechanic_id'];
            $getMechanicRequest = $this->Request_model->fields(['request_id', 'user_id', 'request_type', 'request_date'])->where(['request_status' => 6, 'mechanic_id' => $mechanicId])->get_all();
            $requestId = [];
            if ($getMechanicRequest) {
                foreach ($getMechanicRequest as $getRequests) {
                    $requestId[] = $getRequests['request_id'];
                }
            }
        } else if (isset($this->param['user_id']) && $this->param['user_id'] != '') {
            $userId = $this->param['user_id'];
            $getRequest = $this->Request_model->fields(['request_id', 'mechanic_id', 'request_type', 'request_date'])->where(['request_status' => 6, 'user_id' => $userId])->get_all();
            $requestId = [];
            if ($getRequest) {
                foreach ($getRequest as $getRequests) {
                    $requestId[] = $getRequests['request_id'];
                }
            }
        }

        if (!empty($requestId)) {
            $getTransaction = $this->Transaction_model->where('request_id', $requestId)->order_by('created_at', 'desc')->get_all();
            if (!empty($getTransaction)) {
                foreach ($getTransaction as $getTransactions) {
                    $transactionDateTime = $getTransactions['created_at'];
                    $explodeTransactionDateTime = explode(' ', $transactionDateTime);
                    $transactionDate = $explodeTransactionDateTime[0];
                    $transactionTime = $explodeTransactionDateTime[1];
                    $transaction_type = $getTransactions['transaction_type'];
                    if($transaction_type == 1){
                        $transaction_type = "Callout Fee";
                    } else{
                        $transaction_type = "Service Fee";
                    }

                    $object_type = $getTransactions['object_type'];

                    
                    $response[] = [
                        'request_id' => $getTransactions['request_id'],
                        'user_id' => $getTransactions['user_id'],
                        'transaction_id' => !empty($getTransactions['transaction_reference']) ? $getTransactions['transaction_reference'] : $getTransactions['transaction_charge_id'],
                        'amount' => $getTransactions['amount'],
                        'payment_type' => $getTransactions['payment_type'],
                        'transaction_status' => $getTransactions['transaction_status'],
                        'transaction_date' => $transactionDate,
                        'transaction_time' => $transactionTime,
                        'transaction_type' => $transaction_type,
                        'remark' => ($object_type == "charge") ? "Paid" : ucfirst($object_type) ,
                        'remark_status' => ($object_type == "charge") ? 1 : 2 //Charge = 1 || refund = 2
                    ];
                }
            }

        }

        if (!empty($response)) {
            $transactionCount = count($response);
            $this->response(['status' => true, 'message' => "Your have $transactionCount transaction history.", 'response' => $response]);die;
        } else {
            $this->response(['status' => false, 'message' => 'You have no transaction history.']);die;
        }
    }

    public function getTransactionDetails_post()
    {
        $this->load->model(['Transaction_model', 'Request_issues_model', 'Request_model', 'Mechanic_issuses_model', 'Issue_model', 'Mechanic_add_service_model']);
        $requestId = $this->param['request_id'];
        $userId = $this->param['user_id'];

        $getStatus = $this->Request_model->fields(['request_id', 'request_type', 'request_date'])->get(['request_id' => $requestId, 'user_id' => $userId]);

        $requestDate = $getStatus['request_date'];
        $explodeDate = explode(" ", $requestDate);
        if (!empty($getStatus)) {
            $getTransaction = $this->Transaction_model->get(['request_id' => $requestId, 'user_id' => $userId]);
            $getIssue = $this->Request_issues_model->fields('issue_id')->with_issue('fields:issue_name,issuse_price')->get_all(['request_id' => $requestId, 'request_issue_status' => 101]);
            $getNewService = $this->Mechanic_add_service_model->get_all(['request_id' => $requestId]);

            if (!empty($getIssue)) {
                $issueDetails = [];
                $issuePrice = '';
                foreach ($getIssue as $key => $getIssues) {
                    $issuseID = $getIssues['issue_id'];
                    $getNewPriceOfIssuse = $this->Mechanic_issuses_model->fields(['issuse_price', 'issuse_id'])->get(['request_id' => $requestId, 'issuse_id' => $issuseID]);

                    if (!empty($getNewPriceOfIssuse['issuse_price'])) {
                        $issusePrice = $getNewPriceOfIssuse['issuse_price'];
                    } else {
                        $issusePrice = $getIssues['issue']['issuse_price'];
                    }

                    $issueDetails[] = [
                        'service_id' => $getIssues['issue_id'],
                        'service_name' => $getIssues['issue']['issue_name'],
                        'service_price' => $issusePrice,
                    ];
                    $issuePrice += $issusePrice;
                }
            }

            if (!empty($getNewService)) {
                $newService = [];
                $newServicePrice = '';
                foreach ($getNewService as $key => $getNewServices) {
                    $newService[] = [
                        'service_id' => $getNewServices['mechanic_add_service_id'],
                        'service_name' => $getNewServices['part_name'],
                        'service_price' => $getNewServices['price'],
                    ];
                    $newServicePrice += $getNewServices['price'];
                }
            }

            if ($issuePrice != '' && $newServicePrice != '') {
                $total_amount = $issuePrice + $newServicePrice;
            } elseif ($issuePrice == '' && $newServicePrice != '') {
                $total_amount = $newServicePrice;
            } elseif ($issuePrice != '' && $newServicePrice == '') {
                $total_amount = $issuePrice;
            } else {
                $total_amount = '';
            }

            $result = [
                'transaction_id' => $getTransaction['transaction_id'],
                'request_id' => $requestId,
                'user_id' => $userId,
                'total' => $getTransaction['amount'],
                'country' => $getTransaction['source_country'],
                'transaction_datetime' => $getTransaction['created_at'],
                'issue_detail' => (isset($issueDetails) ? $issueDetails : ''),
                'new_service' => (isset($newService) ? $newService : ''),
                'request_type' => $getStatus['request_type'],
                'total_amount' => $total_amount,
                'request_date' => $explodeDate[0],
                'request_time' => $explodeDate[1],
            ];
            if (!empty($getTransaction)) {
                $this->response(['status' => true, 'message' => 'Transaction Details.', 'response' => $result]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Not Any Details For This Request.']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'Not Any Details For This Request.']);die;
        }
    }

    // User Card List
    public function getUserSavedCard_post()
    {
        $this->load->model('Transaction_model');

        $userId = $this->param['user_id'];
        $cardDetails = $this->Transaction_model
            ->fields('last4,customer_id')
            ->group_by('last4')
            ->get_all(['user_id' => $userId]);

        if (!empty($cardDetails)) {
            $this->response(['status' => true, 'message' => 'User Card Details Here.', 'response' => $cardDetails]);die;
        } else {
            $this->response(['status' => false, 'message' => 'No Any Cards.']);die;
        }
    }

}
