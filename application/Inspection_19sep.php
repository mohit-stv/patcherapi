<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspection extends MY_Controller {

	public function __construct() {
        /// -- Create Database Connection instance --
        parent::__construct();
        $this->param = (array)json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model');
    }

     public function deleteVehicles_post() {
    	
        $this->form_validation->set_data($this->post());   
        if ($this->form_validation->run('deleteVehicles') == FALSE) {
            $message = $this->form_validation->error_array();
           	$response = array('status' => FALSE, 'message' => $message);
            $this->response($response, 400);
        } else {        
        	$userId = $this->param['userId'];
        	$vehicleId = $this->param['vehicleId'];        	

			$updateOptions = array(
                'where' => array('vehicleId' => $vehicleId, 'deleted_at' => NULL),
                'data' => array('deleted_at' => date('Y-m-d h-m-s')),
                'table' => 'vehicles'
            );
            $deleteVehicle = $this->common_model->customUpdate($updateOptions);

	        if($deleteVehicle){
	          	$this->response(['status' => true , 'message' => 'Delete Successfully','response' => $deleteVehicle]);
	        }else{
	          	$this->response(['status' => false, 'message' => 'Record Not Found']);
	        }			
        }
    }



    ## Add Mechanics Inspection
    public function addMechanicsInspection_post() {
        $this->load->model(['Mechanic_inspection_model','Request_model','Inspection_model','common_model']); 
       
            $requestId = $this->param['request_id'];
            $inspection = $this->param['inspection'];
            if(isset($this->param['inspection']) && !empty($this->param['inspection'])) {
                for($i=0;$i<count($inspection);$i++) {
                    $img = $this->param['inspection'][$i]['images'];
                    $url = ''; 
                    for($j=0;$j<count($img);$j++) {
                        $imageData = $this->common_model->convertBase64ToImage($img[$j]);
	                    $data = $imageData['data'];
                        $mime = $imageData['mime'];
                        $image_name ='assets/inspection/'.rand(1000,9999) . '.'.$mime;
	                    file_put_contents($image_name, $data);
	                    chmod($image_name, 0777);
	                    $inpectionImage = ROOTPATH.$image_name;
                        $url .= $inpectionImage.',';
                    }
                    $inspections_array[] = [
                                'request_id'=>$requestId,
                                'inspection_comment'=>$this->param['inspection_comment'],
                                'inspection_id'=>$this->param['inspection'][$i]['inspection_id'],
                                'image_url'=>rtrim($url, ','),
                                'status'=>$this->param['inspection'][$i]['status'],
                                
                            ];
                }
                $insert = $this->Mechanic_inspection_model->insert($inspections_array);
                if($insert)
                {
                   $this->response(['status' => true, 'message'=> 'Inspection Added Successfully ','response' => $insert]);die;
                }else{
                   $this->response(['status' => false, 'message' => 'Something went wrong']);die;
                }
                
                
               /* $inspections_array = [];
                for($i=0;$i<count($inspection);$i++) {

                    $img = $this->param['inspection'][$i]['images']; 
                    if(!empty($img)) {
                    	$imageData = $this->common_model->convertBase64ToImage($img);
	                    $data = $imageData['data'];
	                    $mime = $imageData['mime'];

	                    $image_name ='assets/inspection/'.rand(1000,5000) . '.'.$mime;
	                    file_put_contents($image_name, $data);
	                    chmod($image_name, 0777);
	                    $inpectionImage = ROOTPATH.$image_name;
                    } else {
                    	$inpectionImage = '';
                    }
                    
                    if($this->param['inspection'][$i]['images'] != ' ') {
                        $inspections_array[]=[
                            'inspection_id'=>$this->param['inspection'][$i]['inspection_id'],
                            'request_id'=>$requestId,
                            'image_url'=>$inpectionImage,
                            'status'=>$this->param['inspection'][$i]['status'],
                            'inspection_comment'=>$this->param['inspection_comment']
                        ];
                    }
                }
                $insert = $this->Mechanic_inspection_model->insert($inspections_array);*/
            }

            //$inspectionComment = isset($this->param['inspectionComment']) ? $this->param['inspectionComment'] : '';         
            
           // $this->Request_model->where('request_id',$requestId)->update(['inspectionComment'=>$inspectionComment]);
            
            /*if($insert)
            {
               $this->response(['status' => true, 'message'=> 'Insert Successfully ','response' => $insert]);die;
            }else{
               $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }*/
    }



    #Get Inspection
    public function getMechanicsInspection_post() {
        $this->load->model(['Mechanic_inspection_model','Inspection_model','Request_model']); 
       
        if(isset($this->param['request_id']) && !empty($this->param['request_id'])) {
            $id = $this->param['request_id'];
            $getInspection = $this->Inspection_model->fields(['inspection_id','inspection_name','inspection_status','isUpload'])->get_all();
    
            $getComment = $this->Mechanic_inspection_model->fields(['inspection_comment'])->get(['request_id'=>$id]);
    
            $inspectionData = [];
            foreach($getInspection as $key=>$getInspections) {
                $inspectionId = $getInspections['inspection_id'];
                $inspectionName = $getInspections['inspection_name'];
    
                $getMechanicInspection = $this->Mechanic_inspection_model->fields(['image_url','status','inspection_comment'])->where(['inspection_id'=>$inspectionId, 'request_id'=>$id])->get();
                $explodeUrl = explode(',',$getMechanicInspection['image_url']);
                $inspectionData['inspection_data'][$key] = [
                            'inspection_id'=>$inspectionId,
                            'inspection_name'=>$inspectionName,
                            'image_url'=>($getMechanicInspection['image_url'] ? $explodeUrl : ''),
                            'mechanic_inspection_status'=>($getMechanicInspection['status'] ? $getMechanicInspection['status'] : '100'),
                            'inspection_status'=>$getInspections['inspection_status'],
                            'isUpload'=>$getInspections['isUpload']
                    ];
            }
            $inspectionData['inspection_comment'] = $getComment['inspection_comment'];
    
            if(!empty($inspectionData)) {
                $this->response(['status' => true, 'message'=> 'Get inspection based on request','response' => $inspectionData]);die;
            } else {
                $this->response(['status' => false, 'message' => 'Something went wrong']);die;
            }
        } else {
            $this->response(['status' => false, 'message' => 'You have no inspection at the moment']);die;
        }

    }


    # User Accept Or Reject The Pre-Inspection
    public function inspectionAcceptReject_post() {
    	$this->load->model(['User_inspection_model']); 

    	$data = [
		    	'request_id'=>$this->param['request_id'], 
		    	'user_id'=>$this->param['user_id'], 
		    	'status'=>$this->param['status']
		    	];

    	$insert = $this->User_inspection_model->insert($data);
    	if($insert) {
    		$this->response(['status' => true, 'message'=> 'Insert Successfully !!','response' => $insert]);die;
    	} else {
    		$this->response(['status' => false, 'message' => 'Something went wrong']);die;
    	}
    }
}