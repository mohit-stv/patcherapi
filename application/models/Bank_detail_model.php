<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Bank_detail_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'bank_detail_id';
        parent::__construct();
        $this->load->model(['Bank_model']);
        $this->has_one['bank'] = array('Bank_model','bank_id','bank_id');
    }
}