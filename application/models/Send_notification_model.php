<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Send_notification_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'send_notification_id';
        parent::__construct();
    }
}