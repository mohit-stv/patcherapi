<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_service_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_services_id';
        parent::__construct();
        $this->load->model(['Service_model']);
        $this->has_one['service'] = array('Service_model','service_id','services_id');
    }
}