<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Service_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'service_id';
        $this->load->model(['Tool_itinerary_model','Qualification_model','Document_model']);
        $this->has_many['tools'] = array('Tool_itinerary_model','service_id','service_id');
        $this->has_many['qualifications'] = array('Qualification_model','service_id','service_id');
        $this->has_many['documents'] = array('Document_model','service_id','service_id');
        parent::__construct();
    }
}