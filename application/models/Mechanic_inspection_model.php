<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_inspection_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_inspection_id';
        parent::__construct();
        $this->load->model(['Inspection_image_model']);
        $this->has_many['inspectionImage'] = array('Inspection_image_model','inspection_id','inspection_id');
    }
}