<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_year_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'vehicle_years_id';
        parent::__construct();
    }
}