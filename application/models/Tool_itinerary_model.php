<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Tool_itinerary_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'tool_itinerary_id';
        $this->load->model(['Service_model']);
        $this->has_one['service'] = array('Service_model','service_id','service_id');
        parent::__construct();
    }
}