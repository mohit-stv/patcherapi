<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'admin_id';
        parent::__construct();
    }
}