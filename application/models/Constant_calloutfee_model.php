<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Constant_calloutfee_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'constant_calloutfee_id';
        parent::__construct();
    }
}