<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_document_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_document_id';
        parent::__construct();
        $this->load->model(['Document_model']);
        $this->has_one['document'] = array('Document_model','document_id','document_id');
    }
}