<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_weight_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'vehicle_weight_id';
        parent::__construct();
    }
}