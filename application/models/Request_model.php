<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Request_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'request_id';
        parent::__construct();
        $this->load->model(['Mechanic_model', 'User_model', 'Request_issues_model', 'Mechanic_add_service_model']);
        $this->has_one['mechanic'] = array('Mechanic_model', 'mechanic_id', 'mechanic_id');
        $this->has_one['request_issues'] = array('Request_issues_model', 'request_id', 'request_id');
        $this->has_many['requestIssues'] = array('Request_issues_model', 'request_id', 'request_id');
        $this->has_many['newService'] = array('Mechanic_add_service_model', 'request_id', 'request_id');
        $this->has_one['user'] = array('User_model', 'user_id', 'user_id');
        $this->has_one['transaction'] = array('Transaction_model', 'request_id', 'request_id');
        $this->has_many['transactionAll'] = array('Transaction_model', 'request_id', 'request_id');
        $this->has_one['mechanicIssues'] = array('Mechanic_issuses_model', 'request_id', 'request_id');
        $this->has_many['mechanic_issues'] = array('Mechanic_issuses_model', 'request_id', 'request_id');
        $this->has_one['rating'] = array('Mechanic_rating_model', 'request_id', 'request_id');
        $this->has_one['transactionFinal'] = array('Transaction_model', 'request_id', 'request_id');
        $this->has_one['userQuestionAnswer'] = array('User_question_answers_model', 'request_id', 'request_id');
    }

    // For Searching Users
    public function searchRequest($search_keyword, $limit, $offset)
    {
        $this->db->select('request_id,users.user_id,mechanics.mechanic_id,request_type,request_status,request_date,users.name as user_name,mechanics.name as mechanic_name');
        $this->db->join('users', 'requests.user_id = users.user_id', 'inner');
        $this->db->join('mechanics', 'requests.mechanic_id = mechanics.mechanic_id', 'inner');
        $this->db->where("users.name like '%$search_keyword%'");
        $this->db->or_where("mechanics.name like '%$search_keyword%'");
        $this->db->or_where("requests.request_id like '%$search_keyword%'");
        $this->db->or_where("requests.request_date like '%$search_keyword%'");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('requests');
        return $query->result_array();
    }
}
