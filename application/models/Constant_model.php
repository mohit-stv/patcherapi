<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Constant_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'id';
        parent::__construct();
    }
}