<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_engines_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'vehicle_engines_id';
        parent::__construct();
    }
}