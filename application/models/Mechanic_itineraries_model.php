<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_itineraries_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_itinerary_id';

        //$this->has_many['tool_itineraries'] = ['tool_itinerary_model', 'tool_itinerary_id', '	itinerary_id'];
        parent::__construct();
    }


    public function getToolItinerary($toolId) {
    	$this->db->select('tool_itineraries.tool_itinerary_name');
    	$this->db->join('tool_itineraries', 'tool_itineraries.tool_itinerary_id = mechanic_itineraries.itinerary_id', 'RIGHT');
    	$this->db->where_in('tool_itineraries.tool_itinerary_id', $toolId);
    	$data = $this->db->get('mechanic_itineraries');
    	return $data->result_array();
    }

}