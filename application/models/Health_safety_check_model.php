<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Health_safety_check_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'health_safety_check_id';
        parent::__construct();
    }
}