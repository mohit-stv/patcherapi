<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_rating_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_rating_id';
        parent::__construct();
        $this->has_one['userDetail'] = ['User_model','user_id','user_id'];
    }

}