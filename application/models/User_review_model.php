<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_review_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'review_id';
        parent::__construct();
        $this->load->model(['User_model']);
        $this->has_one['user'] = array('User_model','user_id','user_id');
    }
}