<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'user_id';
        parent::__construct();
        $this->load->model(['Request_model', 'Mechanic_model', 'Transaction_model']);
        $this->has_one['request'] = array('Request_model', 'user_id', 'user_id');
        $this->has_one['transaction'] = array('Transaction_model', 'user_id', 'user_id');
        $this->has_one['bankDetails'] = array('Bank_detail_model', 'user_id', 'user_id');
    }

    // For Searching Users
    public function searchUser($search_keyword, $limit, $offset)
    {
        $this->db->select('user_id,name,contact_number,email_id,isBlocked');
        $this->db->where("user_id like '%search_keyword%'");
        $this->db->or_where("name like '%$search_keyword%'");
        $this->db->or_where("contact_number like '%$search_keyword%'");
        $this->db->or_where("email_id like '%$search_keyword%'");
        $this->db->or_where("created_at like '%search_keyword%'");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('users');
        return $query->result_array();
    }

}
