<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Cancel_reason_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'cancel_reason_id';
        parent::__construct();
    }
}