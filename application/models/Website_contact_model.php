<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Website_contact_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'contact_id';
        parent::__construct();
    }
}