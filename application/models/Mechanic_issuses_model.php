<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_issuses_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_issuse_id';
        parent::__construct();
        $this->has_many['issue'] = array('Issue_model','issue_id','issue_id');
    }

}