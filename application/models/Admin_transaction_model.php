<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_transaction_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'admin_transaction_id';
        parent::__construct();
        $this->load->model(['User_model','Request_model','Mechanic_model']);
        $this->has_one['user'] = array('User_model', 'user_id', 'user_id');
        $this->has_one['request'] = array('Request_model', 'request_id', 'request_id');
        $this->has_one['mechanic'] = array('Mechanic_model', 'mechanic_id', 'mechanic_id');
    }
}