<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Busy_schedule_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'busy_schedule_id';
        parent::__construct();
    }
}