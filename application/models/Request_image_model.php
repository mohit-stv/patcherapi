<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Request_image_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'request_image_id';
        parent::__construct();
    }
}