<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Charge_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'charge_id';
        parent::__construct();
    }
}