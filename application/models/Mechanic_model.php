<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mechanic_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_id';
        parent::__construct();
        $this->load->model(['Request_model', 'Mechanic_rating_model']);
        $this->has_many['request'] = array('Request_model', 'mechanic_id', 'mechanic_id');
        $this->has_one['bankDetails'] = array('Bank_detail_model', 'mechanic_id', 'mechanic_id');
        $this->has_many['rating'] = array('Mechanic_rating_model', 'mechanic_id', 'mechanic_id');
    }

    // For Searching Mechanic
    public function searchMechanic($search_keyword, $limit, $offset)
    {
        $this->db->select('mechanic_id,name,email_id,country_code,contact_number,mechanic_picture,created_at,isBlocked');
        $this->db->where("name like '%$search_keyword%' OR email_id like '%$search_keyword%' OR mechanic_id like '%$search_keyword%' OR contact_number like '%$search_keyword' OR created_at like '%search_keyword%'");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('mechanics');
        return $query->result_array();

    }
}
