<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_models_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'vehicle_model_id';
        parent::__construct();
    }
}