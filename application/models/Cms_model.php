<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Cms_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'cmsId';
        parent::__construct();
    }
}