<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_donate_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'Transaction_donate_id';
        parent::__construct();
    }
}