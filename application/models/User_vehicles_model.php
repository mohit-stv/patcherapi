<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_vehicles_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'user_vehicles_id';
        parent::__construct();
        $this->load->model(['Vehicle_make_model','Vehicle_models_model','Vehicle_bodytypes_model','Vehicle_year_model','Vehicle_weight_model','Vehicle_engines_model','Vehicle_drivetypes_model']);
        $this->has_one['make'] = array('Vehicle_make_model','vehicle_make_id','vehicle_make_id');
        $this->has_one['model'] = array('Vehicle_models_model','vehicle_model_id','vehicle_model_id');
        $this->has_one['body'] = array('Vehicle_bodytypes_model','vehicle_bodytypes_id','vehicle_bodytype_id');
        $this->has_one['year'] = array('Vehicle_year_model','vehicle_years_id','vehicle_year_id');
        $this->has_one['weight'] = array('Vehicle_weight_model','vehicle_weight_id','vehicle_weight_id');
        $this->has_one['engine'] = array('Vehicle_engines_model','vehicle_engines_id','vehicle_engine_id');
        $this->has_one['drivetype'] = array('Vehicle_drivetypes_model','vehicle_drivetypes_id','vehicle_drivetype_id');
    }
}