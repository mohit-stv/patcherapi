<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Answer_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'answer_id';
        parent::__construct();
    }
}