<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle_bodytypes_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'vehicle_bodytypes_id';
        parent::__construct();
    }
}