<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Experience_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'experience_id';
        $this->has_one['mechanicExp'] = ['Mechanic_experience_model','experience_id','experience_id'];
        parent::__construct();
    }
}