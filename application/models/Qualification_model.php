<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Qualification_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'qualification_id';
        parent::__construct();
    }
}