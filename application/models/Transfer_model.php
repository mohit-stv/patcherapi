<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transfer_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'transfer_id';
        parent::__construct();
    }
}