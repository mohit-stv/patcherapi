<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Issue_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'issue_id';
        parent::__construct();
    }
}