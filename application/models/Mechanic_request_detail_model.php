<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_request_detail_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'id';
        parent::__construct();
        $this->has_one['request'] = array('Request_model', 'request_id', 'request_id');
        $this->has_one['mechanic'] = array('Mechanic_model', 'mechanic_id', 'mechanic_id');
    }
}