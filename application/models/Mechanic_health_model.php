<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_health_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_health_id';
        parent::__construct();
    }

    public function getToolHealth($healthID) {
    	$this->db->select('health_safety_checks.health_safety_name');
    	$this->db->join('health_safety_checks', 'health_safety_checks.health_safety_check_id = mechanic_healths.mechanic_health_id', 'RIGHT');
    	$this->db->where_in('health_safety_checks.health_safety_check_id', $healthID);
    	$data = $this->db->get('mechanic_healths');
    	return $data->result_array();
    }
}