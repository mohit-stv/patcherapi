<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Request_issues_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'request_issue_id';
        parent::__construct();
        $this->load->model(['Mechanic_model','Issue_model']);
        $this->has_one['mechanic'] = array('Mechanic_model','mechanic_id','issuse_id');
        $this->has_one['issue'] = array('Issue_model','issue_id','issue_id');
    }
}