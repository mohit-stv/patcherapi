<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_registrations_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_registrations_id';
        parent::__construct();
    }
}