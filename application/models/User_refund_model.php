<?php defined('BASEPATH') or exit('No direct script access allowed');
class User_refund_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'refund_id';
        parent::__construct();
    }
}
