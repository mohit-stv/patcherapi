<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Inspection_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'inspection_id';
        parent::__construct();
    }
}