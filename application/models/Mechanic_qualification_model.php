<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_qualification_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_qualification_id';
        parent::__construct();
    }
}