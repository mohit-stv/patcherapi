<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_question_answers_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'user_question_answers_id';
        parent::__construct();
    }
}