<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Request_mechanics_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'request_mechanic_id';
        parent::__construct();
    }
}