<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_model extends CI_Model {

    public function insert($table_name = '', $data = '') {
        $query = $this->db->insert($table_name, $data);
        if ($query)
            return $this->db->insert_id();
        else
            return FALSE;
    }

    // Shoaib Custom Function
    public function customInsert($options) {
        $table = false;
        $data = false;

        extract($options);

        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }

    public function customUpdate($options) {
        $table = false;
        $where = false;
        $orwhere = false;
        $data = false;

        extract($options);

        if (!empty($where)) {
            $this->db->where($where);
        }

        // using or condition in where  
        if (!empty($orwhere)) {
            $this->db->or_where($orwhere);
        }
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    public function customGet($options) {

        $select = false;
        $table = false;
        $join = false;
        $order = false;
        $limit = false;
        $offset = false;
        $where = false;
        $or_where = false;
        $single = false;
        $where_not_in = false;

        extract($options);

        if ($select != false)
            $this->db->select($select);

        if ($table != false)
            $this->db->from($table);

        if ($where != false)
            $this->db->where($where);

        if ($where_not_in != false) {
            foreach ($where_not_in as $key => $value) {
                if (count($value) > 0)
                    $this->db->where_not_in($key, $value);
            }
        }

        if ($or_where != false)
            $this->db->or_where($or_where);

        if ($limit != false) {

            if (!is_array($limit)) {
                $this->db->limit($limit);
            } else {
                foreach ($limit as $limitval => $offset) {
                    $this->db->limit($limitval, $offset);
                }
            }
        }


        if ($order != false) {

            foreach ($order as $key => $value) {

                if (is_array($value)) {
                    foreach ($order as $orderby => $orderval) {
                        $this->db->order_by($orderby, $orderval);
                    }
                } else {
                    $this->db->order_by($key, $value);
                }
            }
        }


        if ($join != false) {

            foreach ($join as $key => $value) {

                if (is_array($value)) {
                    if (count($value) == 3) {
                        $this->db->join($value[0], $value[1], $value[2]);
                    } else {
                        foreach ($value as $key1 => $value1) {
                            $this->db->join($key1, $value1);
                        }
                    }
                } else {
                    $this->db->join($key, $value);
                }
            }
        }
        
        if (isset($having) && $having != null) 
            $this->db->having($having);
        
        if (isset($group_by) && $group_by != null) 
            $this->db->group_by($group_by);
        
        $query = $this->db->get();

        if ($single) {
            return $query->row();
        }
        return $query->result();
    }
    
    function loadtemplate($page,$data = '',$script = ''){
        $this->load->view('templates/homepage_header',$data);
        $this->load->view($page);
        $this->load->view('templates/homepage_footer');
        if(isset($script) && $script != ''){
            $this->load->view($script);
        }
    }

    //calculate lat long
    function getLnt($zip) {

        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, TRUE);
        if ($result['status'] != "ZERO_RESULTS") {
            //echo "HERE";
            if ($result['status'] != 'OVER_QUERY_LIMIT') {
                //echo "NT HERE";
                $result1[] = $result['results'][0];
                $result2[] = $result1[0]['geometry'];
                $result3[] = $result2[0]['location'];
                return $result3[0];
            } else {
                //echo "HELLO";
                $result3 = array("Error" => "Error Message");
                return $result3;
            }
        }
    }

    //image decode
    function getImageBase64Code($img) {
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $img = str_replace('[removed]', '', $img);
        $data = base64_decode($img);
        return $data;
    }
    //Shoaib Custom Function


   
    public function convertBase64ToImage($img) {
        $img = explode(',',$img);
        $mime = explode('/',$img[0]);
        $mime = explode(';',$mime[1]);
        $img = str_replace(' ', '+', $img[1]);
        $data = base64_decode($img);
        $arrayVal = ['data'=>$data, 'mime'=>$mime[0]];
        return $arrayVal;
    }
}