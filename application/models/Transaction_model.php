<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'Transaction_id';
        parent::__construct();
        $this->has_one['user'] = array('User_model', 'user_id', 'user_id');
        $this->has_one['request'] = array('Request_model', 'request_id', 'request_id');
    }
}