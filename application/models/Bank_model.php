<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Bank_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'bank_id';
        parent::__construct();
    }
}