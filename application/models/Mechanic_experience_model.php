<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Mechanic_experience_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'mechanic_experience_id';
        parent::__construct();
    }
}