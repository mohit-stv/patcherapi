<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Emergency_request_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'emergency_request_id';
        parent::__construct();
    }
}