<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Payment_type_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'payment_type_id';
        parent::__construct();
    }
}