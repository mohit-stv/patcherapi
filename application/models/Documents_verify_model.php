<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Documents_verify_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'document_verify_id';
        parent::__construct();
    }
}