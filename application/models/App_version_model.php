<?php defined('BASEPATH') or exit('No direct script access allowed');
class App_version_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'id';
        parent::__construct();
    }
}
