<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_inspection_model extends MY_model
{
    public function __construct()
    {
        $this->primary_key = 'user_inspection_id';
        parent::__construct();
    }
}